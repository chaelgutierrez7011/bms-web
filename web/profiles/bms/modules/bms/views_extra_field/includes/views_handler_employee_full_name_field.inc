<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_employee_full_name_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {
    $fullname = '-';
    if(isset($values->field_field_username[0]['raw']['value'])) {
      $account = user_load_by_name($values->field_field_username[0]['raw']['value']);
      $name = profile2_load_by_user($account, $type_name = NULL);

      $firstname = $name['main']->field_firstname['und'][0]['value'];
      $lastname = $name['main']->field_lastname['und'][0]['value'];
      if(isset($firstname) && isset($lastname)) {
        $fullname = $firstname . ' ' . $lastname;
      }
    }
    return $fullname;
  }
}
