<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_product_sales_volume_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {
    $date = explode(' ', $values->field_field_created[0]['raw']['value']);
    $sid = $values->_field_data['field_reference_store_id_eck_store_id']['entity']->field_store_id['und'][0]['value'];
    $item_id = $values->field_field_item_id[0]['raw']['value'];
    $category = $values->_field_data['field_collection_item_field_data_field_cashier_items_item_id']['entity']->field_item_id['und'][0]['value'];

    $peso_total = $this->get_product_percentage_volume_to_sales($date[0], $sid, $item_id, $category);

    return $peso_total;
  }

  /**
   * Callback handler
   * Return percentage to sales
   */
  function get_product_percentage_volume_to_sales($date, $sid, $item_id, $category) {
    if(empty($category)) {
      $category = arg(4);
    }

    $sub_query = db_query("SELECT
    COUNT(ii.field_item_id_value)
    FROM
    {eck_store} eck
    INNER JOIN {field_data_field_created} fc ON fc.entity_id = eck.id
    INNER JOIN {field_data_field_reference_store_id} si ON si.entity_id = eck.id
    INNER JOIN {field_data_field_cashier_items} ci ON ci.entity_id = si.entity_id
    INNER JOIN {field_data_field_cashier_components} cc ON cc.entity_id = ci.field_cashier_items_value
    INNER JOIN {field_data_field_item_id} ii ON ii.entity_id = cc.field_cashier_components_value
    INNER JOIN {field_data_field_item_id} iid ON iid.entity_id = ci.field_cashier_items_value
    WHERE (type = 'cashier') 
    AND (si.bundle = 'cashier')
    AND (si.field_reference_store_id_target_id = $sid) 
    AND (iid.field_item_id_value = $category)
    AND (fc.field_created_value LIKE '%$date%')")->fetchField();

    $query = db_query("SELECT
    ROUND(COUNT(ii.field_item_id_value) / $sub_query * 100, 2)
    FROM
    {eck_store} eck
    INNER JOIN {field_data_field_created} fc ON fc.entity_id = eck.id
    INNER JOIN {field_data_field_reference_store_id} si ON si.entity_id = eck.id
    INNER JOIN {field_data_field_cashier_items} ci ON ci.entity_id = si.entity_id
    INNER JOIN {field_data_field_cashier_components} cc ON cc.entity_id = ci.field_cashier_items_value
    INNER JOIN {field_data_field_item_id} ii ON ii.entity_id = cc.field_cashier_components_value
    WHERE (type = 'cashier') 
    AND (field_reference_store_id_target_id = $sid) 
    AND (field_created_value LIKE '%$date%')
    AND (field_item_id_value = $item_id)")->fetchField();
    
    return number_format($query, 2);
  }
}
