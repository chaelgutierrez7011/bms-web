<?php

class views_handler_button_link_transfer_field extends views_handler_area {
  function render($empty = FALSE) {
      $path = current_path();
      $type = arg(1);

      $button_value = '';

      switch($type) {
        case 'transfer-out-request':
          $button_value = t('Add Transfer Out');
          break;
        case 'transfer-in-request':
          $button_value = t('Add Transfer In');
          break;
      }

      return '<a href="/' . $path . '/request">' . $button_value . '</a>';
  }
}