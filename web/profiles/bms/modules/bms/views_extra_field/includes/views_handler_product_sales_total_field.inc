<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_product_sales_total_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {
    $date = explode(' ', $values->field_field_created[0]['raw']['value']);
    $sid = $values->_field_data['field_reference_store_id_eck_store_id']['entity']->field_store_id['und'][0]['value'];
    $item_id = $values->field_field_item_id[0]['raw']['value'];

    return $this->get_sales_total($date[0], $sid, $item_id);
  }

  /**
   * Return SUM of sales total.
   */
  function get_sales_total($date, $sid, $item_id) {
    $query = db_query("SELECT
    SUM(field_unit_price_value)
    FROM
    {eck_store} eck
    INNER JOIN {field_data_field_created} fc ON fc.entity_id = eck.id
    INNER JOIN {field_data_field_reference_store_id} si ON si.entity_id = eck.id
    INNER JOIN {field_data_field_cashier_items} ci ON ci.entity_id = si.entity_id
    INNER JOIN {field_data_field_unit_price} up ON up.entity_id = ci.field_cashier_items_value
    INNER JOIN {field_data_field_cashier_components} cc ON cc.entity_id = ci.field_cashier_items_value
    INNER JOIN {field_data_field_item_id} ii ON ii.entity_id = cc.field_cashier_components_value
    WHERE (type = 'cashier') 
    AND (field_reference_store_id_target_id = $sid) 
    AND (field_created_value LIKE '%$date%')
    AND (field_item_id_value = $item_id)")->fetchField();

    return number_format($query, 2);
  }
}
