<?php

/**
 * @file
 * Template to display a view as a table.
 *
 * - $title : The title of this group of rows.  May be empty.
 * - $header: An array of header labels keyed by field id.
 * - $caption: The caption for this table. May be empty.
 * - $header_classes: An array of header classes keyed by field id.
 * - $fields: An array of CSS IDs to use for each field id.
 * - $classes: A class or classes to apply to the table, based on settings.
 * - $row_classes: An array of classes to apply to each row, indexed by row
 *   number. This matches the index in $rows.
 * - $rows: An array of row items. Each row is an array of content.
 *   $rows are keyed by row number, fields within rows are keyed by field ID.
 * - $field_classes: An array of classes to apply to each field, indexed by
 *   field id, then row number. This matches the index in $rows.
 * @ingroup views_templates
 */
?>

<div class="tabbed-block">

  <div class="body-tabs">
    <div id="tab-0" class="tab-content">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?> <?php print $attributes; ?>>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if (in_array($field, $vault)): ?>
                <th><?php print $label; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr>
              <?php foreach ($row as $field => $content): ?>
                <?php if (in_array($field, $vault)): ?>
                  <td><?php print $content; ?></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div id="tab-1" class="tab-content">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?> <?php print $attributes; ?>>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if (in_array($field, $sales)): ?>
                <th><?php print $label; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr>
              <?php foreach ($row as $field => $content): ?>
                <?php if (in_array($field, $sales)): ?>
                  <td><?php print $content; ?></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div id="tab-2" class="tab-content">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?> <?php print $attributes; ?>>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if (in_array($field, $pullout)): ?>
                <th><?php print $label; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr>
              <?php foreach ($row as $field => $content): ?>
                <?php if (in_array($field, $pullout)): ?>
                  <td><?php print $content; ?></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div id="tab-3" class="tab-content">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?> <?php print $attributes; ?>>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if (in_array($field, $deposit)): ?>
                <th><?php print $label; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr>
              <?php foreach ($row as $field => $content): ?>
                <?php if (in_array($field, $deposit)): ?>
                  <td><?php print $content; ?></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
    <div id="tab-4" class="tab-content">
      <table <?php if ($classes) { print 'class="'. $classes . '" '; } ?> <?php print $attributes; ?>>
        <thead>
          <tr>
            <?php foreach ($header as $field => $label): ?>
              <?php if (in_array($field, $other)): ?>
                <th><?php print $label; ?></th>
              <?php endif; ?>
            <?php endforeach; ?>
          </tr>
        </thead>
        <tbody>
          <?php foreach ($rows as $row_count => $row): ?>
            <tr>
              <?php foreach ($row as $field => $content): ?>
                <?php if (in_array($field, $other)): ?>
                  <td><?php print $content; ?></td>
                <?php endif; ?>
              <?php endforeach; ?>
            </tr>
          <?php endforeach; ?>
        </tbody>
      </table>
    </div>
  </div>
</div>