<?php if (user_is_logged_in()): ?>
<div class="wrapper">

  <!-- Main Header -->
  <header class="main-header">

    <!-- Logo -->
    <a href="<?php print $front_page ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></span>
    </a>
    <?php if($logged_in): ?>
      <!-- Header Navbar -->
      <nav class="navbar navbar-static-top" role="navigation">

        <!-- Sidebar toggle button-->
        <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
          <span class="logo-mini"><img src="<?php print $logo; ?>" alt="<?php print $site_name; ?>" /></span>
          <span class="sr-only">Toggle navigation</span>
        </a>

        <!-- Navbar Right Menu -->
        <div class="navbar-custom-menu">
          <ul class="nav navbar-nav">
            <!-- User Account Menu -->
            <li class="dropdown user user-menu">
              <!-- Menu Toggle Button -->
              <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                <!-- The user image in the navbar-->
                <?php print $avatarsm; ?>
                <!-- hidden-xs hides the username on small devices so only the image appears. -->
                <span class="hidden-xs"><?php print $fullname; ?></span>
              </a>
              <ul class="dropdown-menu">
                <!-- The user image in the menu -->
                <li class="user-header">
                  <?php print $avatar; ?>
                  <p>
                    <?php print $fullname; ?>  - <?php print $role; ?>
                    <small><?php print $history; ?></small>
                  </p>
                </li>
                <!-- Menu Footer-->
                <li class="user-footer">
                  <div class="pull-left">
                    <a href="/<?php print $profile; ?>" class="btn btn-default btn-flat">Profile</a>
                  </div>
                  <div class="pull-right">
                    <a href="<?php print $logout; ?>" class="btn btn-default btn-flat">Sign out</a>
                  </div>
                </li>
              </ul>
            </li>
          </ul>
        </div>

      </nav>
    <?php endif; ?>
  </header>

  <?php if($logged_in): ?>
    <!-- Left side column. contains the logo and sidebar -->
    <aside class="main-sidebar">

      <!-- sidebar: style can be found in sidebar.less -->
      <section class="sidebar">

        <!-- Sidebar user panel (optional) -->
        <div class="user-panel">
          <div class="pull-left image">
            <?php print $avatar; ?>
          </div>
          <div class="pull-left info">
            <p><?php print $fullname; ?></p>
            <!-- Status -->
            <a href="#"><i class="fa fa-user"></i> <?php print $role; ?></a>
          </div>
        </div>
        <!-- Sidebar Menu -->
        <?php if(!empty($page['sidebar_first'])): ?>
          <?php print render($page['sidebar_first']); ?>
        <?php endif; ?>

      </section>
      <!-- /.sidebar -->
    </aside>
  <?php endif; ?>

  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <h1>
        <?php print render($title_prefix); ?>
        <?php if (!empty($title)): ?>
          <h1 class="page-header"><?php print $title; ?></h1>
        <?php endif; ?>
        <?php print render($title_suffix); ?>
      </h1>
      <!-- print breadcrumbs -->
      <?php print $breadcrumb; ?>
    </section>

    <!-- Main content -->
    <section class="content main-content">

      <!-- Your Page Content Here -->
      <?php print $messages; ?>
      <?php if (!empty($tabs)): ?>
        <?php print render($tabs); ?>
      <?php endif; ?>
      <?php if (!empty($page['help'])): ?>
        <?php print render($page['help']); ?>
      <?php endif; ?>
      <?php print render($page['top_content']); ?>
      <?php print render($page['content']); ?>
      <?php print render($page['bottom_content']); ?>

    </section><!-- /.content -->

    <div class="throbber-wrapper">
      <div class="throbber-inner">
<div class="sk-fading-circle">
  <div class="sk-circle1 sk-circle"></div>
  <div class="sk-circle2 sk-circle"></div>
  <div class="sk-circle3 sk-circle"></div>
  <div class="sk-circle4 sk-circle"></div>
  <div class="sk-circle5 sk-circle"></div>
  <div class="sk-circle6 sk-circle"></div>
  <div class="sk-circle7 sk-circle"></div>
  <div class="sk-circle8 sk-circle"></div>
  <div class="sk-circle9 sk-circle"></div>
  <div class="sk-circle10 sk-circle"></div>
  <div class="sk-circle11 sk-circle"></div>
  <div class="sk-circle12 sk-circle"></div>
</div>
      </div>
    </div>
  </div><!-- /.content-wrapper -->

  <!-- Main Footer -->
  <footer class="main-footer">
    <!-- To the right -->
    <div class="pull-right hidden-xs">
      Vielsoft
    </div>
    <!-- Default to the left -->
    <strong>Copyright &copy; 2015 <a href="https://vielsoft.com">Adams</a>.</strong> All rights reserved.
  </footer>
</div><!-- ./wrapper -->
<?php else: ?>
  <?php if ($status_header == '404 Not Found'): ?>
    <?php if(isset($form_class)) : ?>
      <?php $form_class = "not-found"; ?>
    <?php endif; ?>
  <?php endif; ?>
  <div class="row user-login-block <?php print $form_class; ?>">
    <?php print $messages; ?>
    <div class="form col-md-4 col-md-offset-4" align="center">
      <div class="logo-adjust">
        <img src="/profiles/bms/themes/bms_adminlte/assets/images/colored-logo.png" class="colored-logo"/>
      </div>
      <?php print render($page['top_content']); ?>
      <?php print render($page['content']); ?>
      <?php print render($page['bottom_content']); ?>
    </div>
    <div class="copyright text-center">
      Copyright &copy; 2015
      <a href="https://vielsoft.com">Adams.</a>
      All rights reserved.
    </div>
  </div>
<?php endif; ?>
