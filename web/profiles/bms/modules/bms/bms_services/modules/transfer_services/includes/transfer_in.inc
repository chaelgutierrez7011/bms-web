<?php

function transfer_in_services($id, $data) {
  module_load_include('inc', 'bms_services', 'resources/store');
  $created = _datetime_format_UTC($data['created']);
  $store = entity_load_single('store', $id);

  $query = db_query("SELECT ie.eid FROM inventory_entry ie
  INNER JOIN store_transin st ON ie.eid = st.eid
  WHERE FROM_UNIXTIME(st.timestamp, '%Y-%m-%d') = :date_filter AND store_id = :sid",
  array(':date_filter' => date('Y-m-d', strtotime($created)), ':sid' => $id));

  $eid = $query->fetchColumn();
  if(!empty($eid)) {
    try {
      if(!empty($data['items']) && empty($data['transaction_id'])) {
        // Insert none existing data.
        foreach($data['items'] as $item) {
          $product = entity_load_single('store', $item['item_id']);
          $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

          $store_transin = new stdClass;
          $store_transin->timestamp = strtotime($created);
          $store_transin->eid = $eid;
          $store_transin->request_number = $data['request_number'];
          //$store_transin->store_source = $data['branch_name'];
          $store_transin->transaction_number = $data['transaction_number'];
          $store_transin->category = $category->name;
          $store_transin->item_id = $item['item_id'];
          $store_transin->item = $item['item_name'];
          $store_transin->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
          $store_transin->quantity = $item['quantity'];
          $store_transin->unit_of_measurement = $item['item_uom'];
          $store_transin->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
          drupal_write_record('store_transin', $store_transin);
        }
        module_load_include('inc', 'bms_services', 'resources/store');
        // Record process status.
        record_ending_inventory_status($id, $created, 'store_transfer_in');
      }
      else {
        // Update the items
        if(!empty($data['transaction_id'])) {
          foreach($data['items'] as $item) {
            db_update('store_transin')
            ->expression('timestamp', ':timestamp', array(':timestamp' => strtotime($created)))
            ->expression('request_number', ':request_number', array(':request_number' => $data['request_number']))
            ->expression('transaction_number', ':transaction_number', array(':transaction_number' => $data['transaction_number']))
            ->expression('quantity', 'quantity + :quantity', array(':quantity' => $item['quantity']))
            ->condition('item_id', $item['item_id'])
            ->condition('eid', $eid)
            ->execute();
          }
        }
      }
    }
    catch (Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }
  else {
    try {
      $record = new stdClass;
      $record->timestamp = strtotime($created);
      $record->store_id = $id;
      $record->store_name = $store->title;

      $user = user_load($data['created_by_id']);

      $profile = profile2_load_by_user($user, $type_name = NULL);

      $first_name = $profile['main']->field_firstname['und'][0]['value'];
      $last_name = $profile['main']->field_lastname['und'][0]['value'];
      
      if(isset($first_name) && isset($last_name)) {
        $fullname = $first_name . ' ' . $last_name;
      }
      else {
        $fullname = $user->name;
      }
      
      $record->crew_name = $fullname;

      drupal_write_record('inventory_entry', $record);

      foreach($data['items'] as $item) {
        $product = entity_load_single('store', $item['item_id']);
        $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

        $store_transin = new stdClass;
        $store_transin->timestamp = strtotime($created);
        $store_transin->eid = $record->eid;
        $store_transin->request_number = $data['request_number'];
        //$store_transin->store_source = $data['branch_name'];
        $store_transin->transaction_number = $data['transaction_number'];
        $store_transin->category = $category->name;
        $store_transin->item_id = $item['item_id'];
        $store_transin->item = $item['item_name'];
        $store_transin->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
        $store_transin->quantity = $item['quantity'];
        $store_transin->unit_of_measurement = $item['item_uom'];
        $store_transin->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
        drupal_write_record('store_transin', $store_transin);
      }
      module_load_include('inc', 'bms_services', 'resources/store');
      // Record process status.
      record_ending_inventory_status($id, $created, 'store_transfer_in');
    }
    catch(Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }
}