(function ($) {
  Drupal.behaviors.employeeSchedule = {
    calendar: $('#calendar'),

    attach: function (context, settings) {
      $('#edit-submit').prop('disabled', true);

      // Make sure that date and store fields are set before saving.
      let changed_date = false;
      let changed_store = false;
      $('#edit-schedule-calendar-reservationtime-date').change(function() {
        changed_date = true;
        if (changed_date == true && changed_store == true) {
          $('#edit-submit').prop('disabled', false);
        }
      });
      $('#edit-store-assigment').change(function() {
        changed_store = true;
        if (changed_date == true && changed_store == true) {
          $('#edit-submit').prop('disabled', false);
        }
      });

      var schedule = settings.bms_calendar.schedule,
        user = settings.bms_calendar.user,
        parent = Drupal.behaviors.employeeSchedule,
        reprocessLink = [
          '.fc-next-button span',
          '.fc-prev-button span',
          '.fc-month-button',
          '.fc-agendaWeek-button',
          '.fc-agendaDay-button'
        ];

      /* Initialize the external events.
      -----------------------------------------------------------------*/
      function ini_events(ele) {
        ele.each(function () {

          // create an Event Object (http://arshaw.com/fullcalendar/docs/event_data/Event_Object/)
          // It doesn't need to have a start or end.
          var eventObject = {
            // Use the element's text as the event title.
            title: $.trim($(this).text())
          };

          // Store the Event Object in the DOM element so we can get to it later
          $(this).data('eventObject', eventObject);

          // Make the event draggable using jQuery UI.
          // Will cause the event to go back to its original
          // position after the drag.
          $(this).draggable({
            zIndex: 1070,
            revert: true,
            revertDuration: 0
          });
        });
      }

      ini_events($('#external-events div.external-event'));

      parent.createCalendar(settings, parent.calendar);

      $(reprocessLink.join()).once().click(function() {
        parent.reprocessAutoDialog();
      });

      $('input[id*="reservationtime"]').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY'
      });

      /*  Event Add Schedule
      -------------------------------------------*/
      // Date range picker with time picker.
      var dateNow = new Date();
      $('#edit-schedule-calendar-reservationtime-date').daterangepicker({
        timePicker: true,
        timePickerIncrement: 1,
        format: 'MM/DD/YYYY h:mm A'
      });

      // Check the selected date range if it is past dates already.
      $('.page-employee-schedule .ranges .applyBtn').click(function() {
        // Pass the selected start and end schedule date.
        var data = {
          start: $('.daterangepicker_start_input input').val(),
          end: $('.daterangepicker_end_input input').val()
        };

        $.ajax({
          url: '/validate-schedule',
          type: "post",
          data: data,
          dataType: 'json',
          success: function(response) {
            var message = "<div class='callout callout-danger'><span>The schedule date is invalid.</span></div>";
            // If the selected date is passed date disable the Save button as
            // well as the store assignment and request time select options.
            if (!response) {
              // Avoid duplicate validation message.
              $('.callout.callout-danger').remove();
              // Re-attach the message.
              $('#calendar-schedule-box .box.box-solid').prepend(message);
              $('#edit-store-assigment').prop('disabled', 'disabled');
              $('#edit-request').prop('disabled', 'disabled');
              $('#edit-submit').attr('disabled', 'disabled');
            }
            else {
              $('.callout.callout-danger').remove();
              $('#edit-store-assigment').prop('disabled', false);
              $('#edit-request').prop('disabled', false);
              //$('#edit-submit').prop('disabled', false);
            }
          },
          complete: function(data) {}
        });
      });

      // Reset the submit button state when the user changed the undertime.
      $(document).on('change','#date-undertime, #date-overtime', function() {
        // Get the timeout time.
        var timeout = $('.daterangepicker_end_input input').val();
        var timeout_obj = timeout.split(/ /);
        delete timeout_obj[0];
        var timeout_time = timeout_obj.join();
        timeout_time = timeout_time.slice(1);
        timeout_time = timeout_time.replace(',', ' ');

        // Determine if the request is undertime or overtime.
        var request = $('#edit-request').val();

        // Validation for undertime.
        if (request == 'undertime') {
          // Reset the value of overtime if accidentally filled.
          $('#date-overtime').val('');
          // Get the request undertime.
          var undertime_request = $('#date-undertime').val();

          // Compare the request undertime to timeout.
          if (Date.parse('08/20/2016 ' + undertime_request) < Date.parse('08/20/2016 ' + timeout_time)) {
            $('.callout.callout-danger').remove();
            $('#edit-submit').prop('disabled', false);
          }
          else {
            $('.callout.callout-danger').remove();
            var message = "<p><div class='callout callout-danger'><span>Undertime is invalid.</span></div></p>";
            $('#calendar-schedule-box .box.box-solid').prepend(message);
            $('#edit-submit').prop('disabled', true);
          }
        }

        // Validation for overtime.
        if (request == 'overtime') {
          // Make sure to clear the undertime if accidentally filled.
          //parent.serializeDeleteItem(data, 'date_undertime');
          $('#date-undertime').val('');
          console.log('The value of undertime is :' + $('#date-undertime').val());
          // Get the request overtime.
          var overtime_request = $('#date-overtime').val();

          // Compare the request date-overtime to timeout.
          if (Date.parse('08/20/2016 ' + overtime_request) > Date.parse('08/20/2016 ' + timeout_time)) {
            $('.callout.callout-danger').remove();
            $('#edit-submit').prop('disabled', false);
          }
          else {
            $('.callout.callout-danger').remove();
            var message = "<p><div class='callout callout-danger'><span>Overtime is invalid.</span></div></p>";
            $('#calendar-schedule-box .box.box-solid').prepend(message);
            $('#edit-submit').prop('disabled', true);
          }
        }
      });

      flag = false;

      // Submit.
      $('form#employee-schedule-form').submit(function(e) {

        // Avoid duplicates of element everytime it submits
        e.stopImmediatePropagation();

        // This flag signifies if the operation can be recorded or not.
        flag = false;

        // Validate that the fields are set before doing validation.
        if ($('#edit-request').val()) {
          // Determine if the request is undertime or overtime.
          var request = $('#edit-request').val();

          // Get the timeout time.
          var timeout = $('.daterangepicker_end_input input').val();

          var timeout_obj = timeout.split(/ /);
          delete timeout_obj[0];
          var timeout_time = timeout_obj.join();
          timeout_time = timeout_time.slice(1);
          timeout_time = timeout_time.replace(',', ' ');

          // Validation for undertime.
          if (request == 'undertime') {
            // Reset the value of overtime if accidentally filled.
            $('#date-overtime').val('');
            // Get the request undertime.
            var undertime_request = $('#date-undertime').val();

            // Compare the request undertime to timeout.
            if (Date.parse('08/20/2016 ' + undertime_request) < Date.parse('08/20/2016 ' + timeout_time)) {
              $('.callout.callout-danger').remove();
              flag = true;
            }
            else {
              flag = false;
              $('.callout.callout-danger').remove();
              var message = "<p><div class='callout callout-danger'><span>Undertime is invalid.</span></div></p>";
              $('#calendar-schedule-box .box.box-solid').prepend(message);
              $('#edit-submit').attr('disabled','disabled');
            }
          }

          // Validation for overtime.
          if (request == 'overtime') {
            // Make sure to clear the undertime if accidentally filled.
            //parent.serializeDeleteItem(data, 'date_undertime');
            $('#date-undertime').val('');
            console.log('The value of undertime is :' + $('#date-undertime').val());
            // Get the request overtime.
            var overtime_request = $('#date-overtime').val();

            // Compare the request date-overtime to timeout.
            if (Date.parse('08/20/2016 ' + overtime_request) > Date.parse('08/20/2016 ' + timeout_time)) {
              $('.callout.callout-danger').remove();
              flag = true;
            }
            else {
              $('.callout.callout-danger').remove();
              var message = "<p><div class='callout callout-danger'><span>Overtime is invalid.</span></div></p>";
              $('#calendar-schedule-box .box.box-solid').prepend(message);
              $('#edit-submit').attr('disabled','disabled');
              flag = false;
            }
          }
        }
        else {
          flag = true;
        }

        var data = $(this).serialize();

        if (flag) {
          console.log('Saving the schedule.');
          $.ajax({
            url: '/save-schedule',
            type: "post",
            data: data,
            dataType: 'json',
            success: function(response) {
              console.log(response);
              var message = "<p><div class='callout callout-danger'><span>Conflict in adding schedule.</span></div></p>";
              if (response == 'conflict') {
                console.log('Error in saving the schedule.');
                $('#calendar-schedule-box .box.box-solid').prepend(message);
              }
              else {
                $.each(response, function(key, value) {
                  parent.calendar.fullCalendar('renderEvent', {
                    'title': 'test',
                    'start': value.start,
                    'end': value.end,
                    'url': value.url,
                    'className': 'autodialog',
                    'id': value.id,
                    'backgroundColor': value.backgroundColor,
                    'borderColor': value.borderColor
                  });
                });
                console.log('Successfully saved the schedule.');
              }
            },
            complete: function(data) {
              $('form#employee-schedule-form').val('');
              $('input#edit-schedule-calendar-reservationtime-date').val('');
              $('select#edit-store-assigment').val('');
              $('select#edit-request').val('');

              Drupal.attachBehaviors($('.fc-event-container'));
            }
          });
        }

        return false;

      });

      /* Event update schedule
      -----------------------------------------------*/
      $('#update-schedule').on('click', function(e) {
        $('form#add-schedule-edit-form').submit(function(e) {
          e.stopImmediatePropagation();

          var data = $(this).serialize();

          $.ajax({
            url: '/update-schedule',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(response) {
              if (response.message == 'Conflict') {
                $('.callout.callout-danger').remove();
                if (typeof response.ops === 'undefined') {
                  var message = Drupal.t('Operation is invalid.');
                }
                if (response.ops === 'overtime') {
                  var message = Drupal.t('Invalid overtime.');
                }
                if (response.ops === 'undertime') {
                  var message = Drupal.t('Invalid undertime.');
                }
                var markup = "<p><div class='callout callout-danger'><span>" + message + "</span></div></p>";
                $('#calendar-schedule-box .box.box-solid').prepend(markup);
              }
              $('.autodialog-content').dialog('close');
            },
            complete: function(data) {
              Drupal.attachBehaviors($('.fc-event-container'));
            },
          });

          return false;
        });
      });

      /* Event delete schedule
      -----------------------------------------------*/
      $('#delete-schedule').on('click', function(e) {
        $('form#add-schedule-edit-form').submit(function() {
          var data = $(this).serialize();

          $.ajax({
            url: '/delete-schedule',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(response) {
              parent.calendar.fullCalendar('removeEvents', response);

              $('.autodialog-content').dialog('close');
            },

            complete: function(data) {
              Drupal.attachBehaviors($('.fc-event-container'));
            },
            error: function(errorThrown) {
              console.log(errorThrown);
            }
          });

          return false;
        });
      });

      /* Event update undertime.
      -----------------------------------------------*/
      $('#update-undertime').on('click', function(e) {
        $('form#add-undertime-edit-form').submit(function() {
          var data = $(this).serialize();

          $.ajax({
            url: '/update-undertime',
            type: 'post',
            data: data,
            dataType: 'json',
            success: function(response) {
              parent.calendar.fullCalendar('removeEvents');
              parent.calendar.fullCalendar('addEventSource', schedule);
              $('.autodialog-content').dialog('close');
              alert('udertime has been successfully updated!');
              document.location.reload(true);
            }
          });

          return false;
        });
      });
    },

    /**
     * Initialize the Calendar
     */
    createCalendar: function(settings, calendar) {
      calendar.once().fullCalendar({
        header: {
          left: 'prev,next today',
          center: 'title',
          right: 'month,agendaWeek,agendaDay'
        },

        buttonText: {
          today: 'today',
          month: 'month',
          week: 'week',
          day: 'day'
        },

        events: settings.bms_calendar.schedule,

        eventOverlap: false,

        eventRender: function(event, element) {
          element.attr('data-dialog-ajax-disable-redirect', 'true');
          element.attr('id', 'autodialog-' + event.id);
        },

        eventAfterRender: function(calEvent, element, view ) {
          var moment1 = moment(calEvent.start);
          var moment2 =  moment(calEvent.end);

          if(!isNaN(moment2)) {
            var fomatee = $.fullCalendar.formatRange(moment1, moment2, 'hh:mm a');
          }

          element.find(".fc-time").text(fomatee);
          element.find(".fc-title").text('');
        },

        editable: false,

        eventClick: function(calEvent, jsEvent, view) {
          $(this).css('border-color', 'red');
        }
      });
    },

    /**
     * This function is used to make sure that those element having the .autodialog class
     * will get reprocess again. Meaning, will have to autodialog event back, whenever
     * the user is navigating through the calendar, using the next and previous button
     */
    reprocessAutoDialog: function() {
      setTimeout(function() {
        Drupal.autodialog.processLink('.autodialog');
      }, 1000);
    },

    /**
     * Removes entry in a serialized array.
     */
    serializeDeleteItem: function(strSerialize, strParamName) {
      var arrSerialize = strSerialize.split("&");
      var i = arrSerialize.length;

      while (i--) {
        if (arrSerialize[i].indexOf(strParamName+"=") == 0) {
          arrSerialize.splice(i,1);
          break;  // Found the one and only, we're outta here.
        }
      }

      return arrSerialize.join("&");
    }
  };
})(jQuery);

