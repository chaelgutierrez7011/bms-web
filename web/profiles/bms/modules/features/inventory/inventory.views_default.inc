<?php
/**
 * @file
 * inventory.views_default.inc
 */

/**
 * Implements hook_views_default_views().
 */
function inventory_views_default_views() {
  $export = array();

  $view = new view();
  $view->name = 'inventory';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'eck_store';
  $view->human_name = 'Inventory';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Inventory Settings';
  $handler->display->display_options['use_ajax'] = TRUE;
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    30037204 => '30037204',
    65280424 => '65280424',
    49915785 => '49915785',
    140330203 => '140330203',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['exposed_form']['options']['autosubmit'] = TRUE;
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '25';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'default';
  $handler->display->display_options['row_plugin'] = 'fields';
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['header']['area_text_custom']['content'] = '<a class="btn btn-block btn-primary btn-flat" href="/raw-materials/add">Add Product</a>';
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = '<div class="empty-inner">
<img src="/profiles/bms/themes/bms_adminlte/assets/images/view-no-results.jpg">
<h1>NO RESULTS FOUND</h1>
<p>Sorry, we couldn\'t find any data matching your search.</p>
<p>Please update your search and try again.</p>
</div>';
  /* Relationship: Store: Inventory (field_inventory) */
  $handler->display->display_options['relationships']['field_inventory_value']['id'] = 'field_inventory_value';
  $handler->display->display_options['relationships']['field_inventory_value']['table'] = 'field_data_field_inventory';
  $handler->display->display_options['relationships']['field_inventory_value']['field'] = 'field_inventory_value';
  $handler->display->display_options['relationships']['field_inventory_value']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_inventory_value']['delta'] = '-1';
  /* Relationship: Entity Reference: Referenced Entity */
  $handler->display->display_options['relationships']['field_item_autofill_target_id']['id'] = 'field_item_autofill_target_id';
  $handler->display->display_options['relationships']['field_item_autofill_target_id']['table'] = 'field_data_field_item_autofill';
  $handler->display->display_options['relationships']['field_item_autofill_target_id']['field'] = 'field_item_autofill_target_id';
  $handler->display->display_options['relationships']['field_item_autofill_target_id']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['relationships']['field_item_autofill_target_id']['required'] = TRUE;
  /* Field: Store: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_store';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  /* Sort criterion: Field collection item: Item (field_item) */
  $handler->display->display_options['sorts']['field_item_value']['id'] = 'field_item_value';
  $handler->display->display_options['sorts']['field_item_value']['table'] = 'field_data_field_item';
  $handler->display->display_options['sorts']['field_item_value']['field'] = 'field_item_value';
  $handler->display->display_options['sorts']['field_item_value']['relationship'] = 'field_inventory_value';
  /* Filter criterion: Store: store type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_store';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'store' => 'store',
  );

  /* Display: Inventory Settings */
  $handler = $view->new_display('page', 'Inventory Settings', 'page');
  $handler->display->display_options['defaults']['css_class'] = FALSE;
  $handler->display->display_options['css_class'] = 'view-boxes';
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['header']['area_text_custom']['content'] = '<a class="btn btn-block btn-primary btn-flat autodialog" href="/raw-materials/add" data-dialog-ajax="true" data-dialog-ajax-disable-redirect="true">Add Item</a>';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Field collection item: Field collection item ID */
  $handler->display->display_options['fields']['item_id']['id'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['table'] = 'field_collection_item';
  $handler->display->display_options['fields']['item_id']['field'] = 'item_id';
  $handler->display->display_options['fields']['item_id']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['fields']['item_id']['exclude'] = TRUE;
  $handler->display->display_options['fields']['item_id']['separator'] = '';
  /* Field: Field collection item: Item */
  $handler->display->display_options['fields']['field_item_autofill']['id'] = 'field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['table'] = 'field_data_field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['field'] = 'field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['fields']['field_item_autofill']['label'] = '';
  $handler->display->display_options['fields']['field_item_autofill']['alter']['make_link'] = TRUE;
  $handler->display->display_options['fields']['field_item_autofill']['alter']['path'] = 'field-collection/field-inventory/[item_id]/edit';
  $handler->display->display_options['fields']['field_item_autofill']['alter']['absolute'] = TRUE;
  $handler->display->display_options['fields']['field_item_autofill']['alter']['link_class'] = 'autodialog';
  $handler->display->display_options['fields']['field_item_autofill']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['field_item_autofill']['settings'] = array(
    'link' => 0,
  );
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Store: store type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_store';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'inventory_placeholder' => 'inventory_placeholder',
  );
  $handler->display->display_options['filters']['type']['group'] = 1;
  /* Filter criterion: Field collection item: Item (field_item) */
  $handler->display->display_options['filters']['field_item_value']['id'] = 'field_item_value';
  $handler->display->display_options['filters']['field_item_value']['table'] = 'field_data_field_item';
  $handler->display->display_options['filters']['field_item_value']['field'] = 'field_item_value';
  $handler->display->display_options['filters']['field_item_value']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['filters']['field_item_value']['operator'] = 'word';
  $handler->display->display_options['filters']['field_item_value']['group'] = 1;
  $handler->display->display_options['filters']['field_item_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_item_value']['expose']['operator_id'] = 'field_item_value_op';
  $handler->display->display_options['filters']['field_item_value']['expose']['label'] = 'Item';
  $handler->display->display_options['filters']['field_item_value']['expose']['operator'] = 'field_item_value_op';
  $handler->display->display_options['filters']['field_item_value']['expose']['identifier'] = 'field_item_value';
  $handler->display->display_options['filters']['field_item_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    204923137 => 0,
    64780366 => 0,
    246010071 => 0,
    65280424 => 0,
    140330203 => 0,
    154199432 => 0,
    49915785 => 0,
  );
  /* Filter criterion: Field: Category (field_category) */
  $handler->display->display_options['filters']['field_category_tid']['id'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['table'] = 'field_data_field_category';
  $handler->display->display_options['filters']['field_category_tid']['field'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['filters']['field_category_tid']['group'] = 1;
  $handler->display->display_options['filters']['field_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator_id'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['identifier'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    204923137 => 0,
    146522381 => 0,
    64780366 => 0,
    246010071 => 0,
    65280424 => 0,
    140330203 => 0,
    154199432 => 0,
    170478167 => 0,
    49915785 => 0,
  );
  $handler->display->display_options['filters']['field_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_category_tid']['vocabulary'] = 'category';
  /* Filter criterion: Field: Status (field_item_inventory_status) */
  $handler->display->display_options['filters']['field_item_inventory_status_value']['id'] = 'field_item_inventory_status_value';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['table'] = 'field_data_field_item_inventory_status';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['field'] = 'field_item_inventory_status_value';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['group'] = 1;
  $handler->display->display_options['filters']['field_item_inventory_status_value']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_item_inventory_status_value']['expose']['operator_id'] = 'field_item_inventory_status_value_op';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['expose']['label'] = 'Status';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['expose']['operator'] = 'field_item_inventory_status_value_op';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['expose']['identifier'] = 'field_item_inventory_status_value';
  $handler->display->display_options['filters']['field_item_inventory_status_value']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    30037204 => 0,
    204923137 => 0,
    146522381 => 0,
    64780366 => 0,
    246010071 => 0,
    65280424 => 0,
    140330203 => 0,
    154199432 => 0,
    170478167 => 0,
    49915785 => 0,
  );
  $handler->display->display_options['path'] = 'inventory';

  /* Display: Inventory */
  $handler = $view->new_display('page', 'Inventory', 'page_1');
  $handler->display->display_options['defaults']['exposed_form'] = FALSE;
  $handler->display->display_options['exposed_form']['type'] = 'better_exposed_filters';
  $handler->display->display_options['exposed_form']['options']['bef'] = array(
    'general' => array(
      'allow_secondary' => 0,
      'secondary_label' => 'Advanced options',
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'field_category_tid' => array(
      'bef_format' => 'default',
      'more_options' => array(
        'bef_select_all_none' => FALSE,
        'bef_collapsible' => 0,
        'is_secondary' => 0,
        'any_label' => '',
        'bef_filter_description' => '',
        'tokens' => array(
          'available' => array(
            0 => 'global_types',
            1 => 'vocabulary',
            'secondary_label' => NULL,
            'collapsible_label' => NULL,
            'combine_rewrite' => NULL,
            'reset_label' => NULL,
            'bef_filter_description' => NULL,
            'any_label' => NULL,
            'filter_rewrite_values' => NULL,
          ),
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
          'filter_rewrite_values' => NULL,
        ),
        'rewrite' => array(
          'filter_rewrite_values' => '',
          'secondary_label' => NULL,
          'collapsible_label' => NULL,
          'combine_rewrite' => NULL,
          'reset_label' => NULL,
          'bef_filter_description' => NULL,
          'any_label' => NULL,
        ),
        'secondary_label' => NULL,
        'collapsible_label' => NULL,
        'combine_rewrite' => NULL,
        'reset_label' => NULL,
        'filter_rewrite_values' => NULL,
      ),
      'secondary_label' => NULL,
      'collapsible_label' => NULL,
      'combine_rewrite' => NULL,
      'reset_label' => NULL,
      'bef_filter_description' => NULL,
      'any_label' => NULL,
      'filter_rewrite_values' => NULL,
    ),
    'secondary_label' => NULL,
    'collapsible_label' => NULL,
    'combine_rewrite' => NULL,
    'reset_label' => NULL,
    'bef_filter_description' => NULL,
    'any_label' => NULL,
    'filter_rewrite_values' => NULL,
  );
  $handler->display->display_options['exposed_form']['options']['input_required'] = 0;
  $handler->display->display_options['exposed_form']['options']['text_input_required_format'] = 'filtered_html';
  $handler->display->display_options['defaults']['relationships'] = FALSE;
  /* Relationship: Store: Inventory (field_inventory) */
  $handler->display->display_options['relationships']['field_inventory_value']['id'] = 'field_inventory_value';
  $handler->display->display_options['relationships']['field_inventory_value']['table'] = 'field_data_field_inventory';
  $handler->display->display_options['relationships']['field_inventory_value']['field'] = 'field_inventory_value';
  $handler->display->display_options['relationships']['field_inventory_value']['required'] = TRUE;
  $handler->display->display_options['relationships']['field_inventory_value']['delta'] = '-1';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Store: Id */
  $handler->display->display_options['fields']['id']['id'] = 'id';
  $handler->display->display_options['fields']['id']['table'] = 'eck_store';
  $handler->display->display_options['fields']['id']['field'] = 'id';
  $handler->display->display_options['fields']['id']['exclude'] = TRUE;
  /* Field: Field collection item: Item */
  $handler->display->display_options['fields']['field_item_autofill_1']['id'] = 'field_item_autofill_1';
  $handler->display->display_options['fields']['field_item_autofill_1']['table'] = 'field_data_field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill_1']['field'] = 'field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill_1']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['fields']['field_item_autofill_1']['label'] = 'id';
  $handler->display->display_options['fields']['field_item_autofill_1']['type'] = 'entityreference_entity_id';
  $handler->display->display_options['fields']['field_item_autofill_1']['settings'] = array(
    'link' => 0,
  );
  /* Field: Field collection item: Item */
  $handler->display->display_options['fields']['field_item_autofill']['id'] = 'field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['table'] = 'field_data_field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['field'] = 'field_item_autofill';
  $handler->display->display_options['fields']['field_item_autofill']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['fields']['field_item_autofill']['settings'] = array(
    'link' => 0,
  );
  /* Field: Field: Inventory unit of measurement */
  $handler->display->display_options['fields']['field_inventory_uom']['id'] = 'field_inventory_uom';
  $handler->display->display_options['fields']['field_inventory_uom']['table'] = 'field_data_field_inventory_uom';
  $handler->display->display_options['fields']['field_inventory_uom']['field'] = 'field_inventory_uom';
  $handler->display->display_options['fields']['field_inventory_uom']['relationship'] = 'field_inventory_value';
  /* Field: Field: Production unit of measurement */
  $handler->display->display_options['fields']['field_production_uom']['id'] = 'field_production_uom';
  $handler->display->display_options['fields']['field_production_uom']['table'] = 'field_data_field_production_uom';
  $handler->display->display_options['fields']['field_production_uom']['field'] = 'field_production_uom';
  $handler->display->display_options['fields']['field_production_uom']['relationship'] = 'field_inventory_value';
  /* Field: Field: Production Quantity */
  $handler->display->display_options['fields']['field_production_quantity']['id'] = 'field_production_quantity';
  $handler->display->display_options['fields']['field_production_quantity']['table'] = 'field_data_field_production_quantity';
  $handler->display->display_options['fields']['field_production_quantity']['field'] = 'field_production_quantity';
  $handler->display->display_options['fields']['field_production_quantity']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['fields']['field_production_quantity']['settings'] = array(
    'thousand_separator' => '',
    'prefix_suffix' => 1,
  );
  /* Field: Field: Category */
  $handler->display->display_options['fields']['field_category']['id'] = 'field_category';
  $handler->display->display_options['fields']['field_category']['table'] = 'field_data_field_category';
  $handler->display->display_options['fields']['field_category']['field'] = 'field_category';
  $handler->display->display_options['fields']['field_category']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['defaults']['arguments'] = FALSE;
  /* Contextual filter: Store: Id */
  $handler->display->display_options['arguments']['id']['id'] = 'id';
  $handler->display->display_options['arguments']['id']['table'] = 'eck_store';
  $handler->display->display_options['arguments']['id']['field'] = 'id';
  $handler->display->display_options['arguments']['id']['default_action'] = 'default';
  $handler->display->display_options['arguments']['id']['default_argument_type'] = 'node';
  $handler->display->display_options['arguments']['id']['summary']['number_of_records'] = '0';
  $handler->display->display_options['arguments']['id']['summary']['format'] = 'default_summary';
  $handler->display->display_options['arguments']['id']['summary_options']['items_per_page'] = '25';
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Store: store type */
  $handler->display->display_options['filters']['type']['id'] = 'type';
  $handler->display->display_options['filters']['type']['table'] = 'eck_store';
  $handler->display->display_options['filters']['type']['field'] = 'type';
  $handler->display->display_options['filters']['type']['value'] = array(
    'store' => 'store',
  );
  /* Filter criterion: Field: Category (field_category) */
  $handler->display->display_options['filters']['field_category_tid']['id'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['table'] = 'field_data_field_category';
  $handler->display->display_options['filters']['field_category_tid']['field'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['filters']['field_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator_id'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['label'] = 'Category';
  $handler->display->display_options['filters']['field_category_tid']['expose']['operator'] = 'field_category_tid_op';
  $handler->display->display_options['filters']['field_category_tid']['expose']['identifier'] = 'field_category_tid';
  $handler->display->display_options['filters']['field_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_category_tid']['vocabulary'] = 'category';
  /* Filter criterion: Field: Sub-category (field_sub_category) */
  $handler->display->display_options['filters']['field_sub_category_tid']['id'] = 'field_sub_category_tid';
  $handler->display->display_options['filters']['field_sub_category_tid']['table'] = 'field_data_field_sub_category';
  $handler->display->display_options['filters']['field_sub_category_tid']['field'] = 'field_sub_category_tid';
  $handler->display->display_options['filters']['field_sub_category_tid']['relationship'] = 'field_inventory_value';
  $handler->display->display_options['filters']['field_sub_category_tid']['exposed'] = TRUE;
  $handler->display->display_options['filters']['field_sub_category_tid']['expose']['operator_id'] = 'field_sub_category_tid_op';
  $handler->display->display_options['filters']['field_sub_category_tid']['expose']['label'] = 'Sub-category';
  $handler->display->display_options['filters']['field_sub_category_tid']['expose']['operator'] = 'field_sub_category_tid_op';
  $handler->display->display_options['filters']['field_sub_category_tid']['expose']['identifier'] = 'field_sub_category_tid';
  $handler->display->display_options['filters']['field_sub_category_tid']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
    4 => 0,
    5 => 0,
    6 => 0,
    7 => 0,
    8 => 0,
  );
  $handler->display->display_options['filters']['field_sub_category_tid']['type'] = 'select';
  $handler->display->display_options['filters']['field_sub_category_tid']['vocabulary'] = 'sub_category';
  $handler->display->display_options['path'] = 'inventory/%';
  $export['inventory'] = $view;

  return $export;
}
