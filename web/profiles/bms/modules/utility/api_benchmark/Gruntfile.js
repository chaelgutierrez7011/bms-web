module.exports = function(grunt) {
  // Project configuration.
  grunt.initConfig({
    pkg: grunt.file.readJSON('package.json'),

    api_benchmark: {
        bms_api: {
          options: {
            output: 'generated'
          },
          files: {
            'report.html': 'config.json'
          }
        }
      }
  });

  // Load grunt-api-benchmark
  grunt.loadNpmTasks('grunt-api-benchmark');
  grunt.registerTask('benchmark', ['api_benchmark']);
};
