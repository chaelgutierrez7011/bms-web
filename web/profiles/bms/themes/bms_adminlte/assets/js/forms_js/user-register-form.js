(function($) {
	'use strict';

	Drupal.behaviors.UserRegisterForm = {
		attach: function(context, settings) {
			$('#user-register-form #edit-close').click(function(e) {
				e.preventDefault();
				$('.ui-dialog-titlebar-close').trigger('click');
			});

			// Simply, trigger the search submit button whenever it
			// detected that the ajax call is finished

			// This some kind of an illusion that page is reloaded automatically
			// whenever you are adding a new crew user
			$(document).ajaxComplete(function(event, xhr, settings) {
				let condition = settings.url === '/system/ajax' && window.location.pathname === '/administer-employee',
					selector = '#views-exposed-form-manage-employee-manage-employee-franchisee #edit-submit-manage-employee';

				if (condition) {
					$(selector).trigger('click');
				}
			});
		}
	};
})(jQuery);

