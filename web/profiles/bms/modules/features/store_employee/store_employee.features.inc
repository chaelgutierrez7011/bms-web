<?php
/**
 * @file
 * store_employee.features.inc
 */

/**
 * Implements hook_views_api().
 */
function store_employee_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}
