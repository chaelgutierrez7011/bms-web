<?php

/**
 * @file
 * block--tab-menu.tpl.php
 * Template for Tab Menu block.
 */

?>

<div class="tab-navigations">
  <ul class="tabs-menu">

    <?php if ($tab_menu_type == 'attendance') : ?>
      
      <li class="current"><a href="#tab-0">Profile</a></li>
      <li><a href="#tab-1">Assigned Duty</a></li>
      <li><a href="#tab-2">Actual Duty Rendered</a></li>
      <li><a href="#tab-3">Break</a></li>

    <?php elseif ($tab_menu_type == 'inventory_summary') : ?>

      <li class="current"><a href="#tab-0">Beginning Inventory</a></li>
      <li><a href="#tab-1">Commissary Delivery</a></li>
      <li><a href="#tab-2">Commissary Pull-out</a></li>
      <li><a href="#tab-3">Store Trans-In</a></li>
      <li><a href="#tab-4">Store Trans-Out</a></li>
      <li><a href="#tab-5">Production</a></li>
      <li><a href="#tab-6">Wastage</a></li>
      <li><a href="#tab-7">Ending Inventory (Theo)</a></li>
      <li><a href="#tab-8">Ending Inventory (Actual Count)</a></li>
      <li><a href="#tab-9">Variance</a></li>
        
    <?php elseif ($tab_menu_type == 'production_summary') : ?>

      <li class="current"><a href="#tab-0">Beginning Inventory</a></li>
      <li><a href="#tab-1">Processed/Opened</a></li>
      <li><a href="#tab-2">Sales</a></li>
      <li><a href="#tab-3">Wastage</a></li>
      <li><a href="#tab-4">Ending Inventory (Theo)</a></li>
      <li><a href="#tab-5">Ending Inventory (Actual Count)</a></li>
      <li><a href="#tab-6">Variance</a></li>

    <?php elseif ($tab_menu_type == 'cash_management') : ?>

      <li class="current"><a href="#tab-0">Added to Vault</a></li>
      <li><a href="#tab-1">Sales</a></li>
      <li><a href="#tab-2">Pull Out</a></li>
      <li><a href="#tab-3">Deposit</a></li>
      <li><a href="#tab-4">Remaining Cash on Valut</a></li>

    <?php endif; ?>

  </ul>
</div>

