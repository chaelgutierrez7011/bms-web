<?php
/**
 * @file
 * bms_roles.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function bms_roles_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'administer menu_per_role'.
  $permissions['administer menu_per_role'] = array(
    'name' => 'administer menu_per_role',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'menu_per_role',
  );

  return $permissions;
}
