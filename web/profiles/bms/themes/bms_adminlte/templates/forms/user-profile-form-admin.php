<section class="left fields-visible">
  <?php print render($form['picture']); ?>
</section>

<section class="center fields-visible">

  <div class="fields-wrapper">
    <div class="field-row">
      <div class="form-item">
        <label>Admin User Type</label>
        <input type="text" class="form-control" id="admin-usertype-copy" value="<?php print $employee_role; ?>" disabled>
      </div>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_firstname']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_lastname']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['name']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['pass']); ?>
    </div>
    
  </div>
</section>

<section class="right fields-visible">

  <div class="fields-wrapper">
    <div class="field-row">
      <?php print render($form['field_account_employee_id']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_address']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['mail']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_mobile_number']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['roles']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['current_pass']); ?>
    </div>
  </div>
</section>

<?php if ($employee_role == 'franchisee' || $employee_role == 'operation' || $employee_role == 'general manager' || $employee_role == 'accounting' || $employee_role == 'hr' || $employee_role == 'commissary'): ?>
  <div class="store-assignments">
    <?php print render($form['profile_main']['field_store_franchised']); ?>
  </div>
<?php endif; ?>

<div class="hidden-container">
  <?php print render($form['account']['status']); ?>
  <?php print render($form['profile_main']['field_my_employee']); ?>
  <?php print render($form['profile_main']['field_manage_schedule']); ?>
  <?php print render($form['timezone']); ?>
  <?php print render($form['field_full_name']); ?>
  <?php print render($form['actions']['cancel']); ?>
</div>