<table id="table3" class="table table-bordered table-striped dataTable">
  <tr>
    <th colspan="5" scope="colgroup" style="background: #f78888;">Beginning Inventory</th>
    <th colspan="6" scope="colgroup" style="background: #a4d38b">Commisary Delivery/ Pull Out</th>
    <!-- <th colspan="2" scope="colgroup">Store/Trans in/Trans Out</th> -->
    <th colspan="5" scope="colgroup" style="background: #45bcd1">Ending Inventory (Theo)</th>
    <th colspan="5" scope="colgroup" style="background: #a482dd">Ending Inventory (Actual)</th>
    <th colspan="5" scope="colgroup" style="background: #e8a855">Variance</th>
  </tr>
  <tr>
    <th class="beggining-inventory" scope="col" style="background: #f78888;">Date</th>
    <th class="beggining-inventory" scope="col" style="background: #f78888;">Item</th>
    <!-- <th class="beggining-inventory" scope="col">Item Code</th> -->
    <th class="beggining-inventory" scope="col" style="background: #f78888;">Qty.</th>
    <th class="beggining-inventory" scope="col" style="background: #f78888;">Unit Price</th>
    <th class="beggining-inventory" scope="col" style="background: #f78888;">Total</th>
    <th class="commissary" scope="col" style="background: #a4d38b">Date</th>
    <th class="commissary" scope="col" style="background: #a4d38b">Time</th>
    <th class="commissary" scope="col" style="background: #a4d38b">Commissary no</th>
    <th class="commissary" scope="col" style="background: #a4d38b">Item</th>
    <!-- <th class="commissary" scope="col">Item Code</th> -->
    <th class="commissary" scope="col" style="background: #a4d38b">Qty.</th>
    <!-- <th class="commissary" scope="col">Unit Price</th> -->
    <th class="commissary" scope="col" style="background: #a4d38b">Total</th>
    <th class="ending_inventory_theoretical" scope="col" style="background: #45bcd1">Date</th>
    <th class="ending_inventory_theoretical" scope="col" style="background: #45bcd1">Item</th>
    <th class="ending_inventory_theoretical" scope="col" style="background: #45bcd1">Quantity</th>
    <th class="ending_inventory_theoretical" scope="col" style="background: #45bcd1">Unit Price</th>
    <th class="ending_inventory_theoretical" scope="col" style="background: #45bcd1">Total</th>

    <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Date</th>
    <!-- <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Time</th> -->
    <!-- <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Crew</th> -->
    <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Item</th>
    <!-- <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Item Code</th> -->
    <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Quantity</th>
    <!-- <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Unit of measurement</th> -->
    <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Unit Price</th>
    <th class="ending_inventory_actual" scope="col" style="background: #a482dd">Total</th>

    <th class="variance" scope="col" style="background: #e8a855">Date</th>
    <th class="variance" scope="col" style="background: #e8a855">Item</th>
    <!-- <th class="variance" scope="col" style="background: #e8a855">Item Code</th> -->
    <th class="variance" scope="col" style="background: #e8a855">Quantity</th>
    <!-- <th class="variance" scope="col" style="background: #e8a855">Unit of measurement</th> -->
    <th class="variance" scope="col" style="background: #e8a855">Unit Price</th>
    <th class="variance" scope="col" style="background: #e8a855">Total</th>
  </tr>
  <tbody>
    <?php foreach($items['content'] as $row): ?>
      <?php print $row; ?>
    <?php endforeach; ?>
  </tbody>
</table>
