<?php

/**
 * Implements hook_schema().
 */
function inventory_services_schema() {
  
  $schema['inventory_entry'] = array(
    'fields' => array(
      'eid' => array(
        'description' => 'The primary identifier for inventory entry',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'store_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Store id.',
      ),
      'store_name' => array(
        'description' => 'Store name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'uid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'User id',
      ),
      'crew_name' => array(
        'description' => 'Crew name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
    ),
    'indexes' => array(
      'created' => array('timestamp'),
    ),
    'unique keys' => array(
      'cs' => array('eid'),
    ),
    'primary key' => array('eid'),
  );

  $schema['beginning_inventory'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'bi' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['commissary_delivery'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'request_number' => array(
        'description' => 'Request Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'transaction_number' => array(
        'description' => 'Transaction Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'default' => 0,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'cd' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['commissary_pullout'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'request_number' => array(
        'description' => 'Request Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'transaction_number' => array(
        'description' => 'Transaction Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
      'reason' => array(
        'description' => 'Reason',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'cp' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['store_transin'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'store_source' => array(
        'description' => 'Store Source',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'request_number' => array(
        'description' => 'Request Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'transaction_number' => array(
        'description' => 'Transaction Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'si' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['store_transout'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'store_source' => array(
        'description' => 'Store Source',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'transaction_number' => array(
        'description' => 'Transaction Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'request_number' => array(
        'description' => 'Request Number',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'so' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['production'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'pd' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['wastage'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
      'reason' => array(
        'description' => 'Reason',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'wg' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['ending_inventory_theoretical'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'store_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Store id.',
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
      'type' => array(
        'description' => 'Type of process',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      )
    ),
    'indexes' => array(
      'item_name' => array('item'),
      'process_type' => array('type'),
    ),
    'unique keys' => array(
      'et' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['ending_inventory_actual'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'eia' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['inventory_variance'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for beginning inventory',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'eid' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'eid.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'category' => array(
        'description' => 'Category',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item id.',
      ),
      'item' => array(
        'description' => 'Item name',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'item_code' => array(
        'description' => 'Item code',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'quantity' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Item Quantity',
      ),
      'unit_of_measurement' => array(
        'description' => 'Unit of measurement',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'unit_price' => array(
        'description' => 'Unit price',
        'type' => 'float',
        'not null' => FALSE,
        'size' => 'medium',
      ),
    ),
    'indexes' => array(
      'item_name' => array('item')
    ),
    'unique keys' => array(
      'vai' => array('id'),
    ),
    'primary key' => array('id'),
  );

  $schema['actual_inventory_status'] = array(
    'fields' => array(
      'id' => array(
        'description' => 'The primary identifier for actual status',
        'type' => 'serial',
        'unsigned' => TRUE,
        'not null' => TRUE
      ),
      'store_id' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Store id.',
      ),
      'timestamp' => array(
        'type' => 'int',
        'not null' => TRUE,
        'default' => 0,
        'description' => 'Unix timestamp of when event occurred.',
      ),
      'type' => array(
        'description' => 'Process type',
        'type' => 'varchar',
        'length' => 255,
        'not null' => TRUE,
        'default' => ''
      ),
      'status' => array(
        'type' => 'int',
        'not null' => TRUE,
        'unsigned' => TRUE,
        'default' => 0,
        'description' => 'Ending inventory actual status',
      ),
    ),
    'indexes' => array(
      'type' => array('type')
    ),
    'primary key' => array('id'),
  );

  return $schema;
}