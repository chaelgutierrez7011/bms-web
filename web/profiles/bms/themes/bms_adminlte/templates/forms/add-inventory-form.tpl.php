<!-- Inventory -->
<?php if ($form['#bundle'] == 'product') : ?>
  <?php print render($form['title']); ?>
  <?php print render($form['field_item_code']); ?>
  <?php print render($form['field_category']); ?>
  <?php print render($form['field_item_inventory_status']); ?>

  <?php print render($form['uom_label']); ?>
  <?php print render($form['field_production_measurement']); ?>
  <?php print render($form['field_inventory_uom']); ?>
  <?php print render($form['field_production_quantity']); ?>
  <?php print render($form['field_production_uom']); ?>

  <?php print render($form['field_price']); ?>
  <?php print render($form['field_price_per_unit']); ?>

  <?php print render($form['field_accepted_reason']); ?>

  <div class="hidden-container">
    <?php print render($form['field_active']); ?>
    <?php print render($form['field_image']); ?>
    <?php print render($form['field_sub_category']); ?>
  </div>

<!-- Product -->
<?php elseif ($form['#bundle'] == 'cashier_items') : ?>
  <?php print render($form['title']); ?>
  <?php print render($form['field_product_code']); ?>

  <?php print render($form['field_category']); ?>
  <?php print render($form['field_selling_price_w_o_vat']); ?>

  <?php print render($form['field_vat']); ?>
  <?php print render($form['field_price']); ?>

  <?php print render($form['field_accepted_reason']); ?>

  <?php print render($form['field_components']); ?>
  <div class="hidden-container">
    <?php print render($form['field_product_name']); ?>
    <?php print render($form['field_selling_price_with_vat']); ?>
    <?php print render($form['field_component_count']); ?>
  </div>
<?php endif; ?>  

<?php print drupal_render_children($form); ?>