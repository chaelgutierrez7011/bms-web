<section class="left fields-visible">
  <?php if ($form['field_image']['und'][0]['#default_value']['fid'] == 0): ?>
   <div id="edit-field-image-preview">
	   <div class="store-image-preview">
		 <img src="/profiles/bms/themes/bms_adminlte/assets/images/no-image.jpg"/>
		</div>
   </div>
 <?php endif; ?>
  
  <?php print render($form['field_image']); ?>
</section>

<section class="right fields-visible">
  <?php print render($form['field_store_name']); ?>
  <?php hide($form['title']); ?>

  <?php print render($form['field_location']); ?>

  <?php print render($form['field_company']); ?>
  <?php print render($form['field_store_franchisee']); ?>
  <?php print render($form['field_store_ownership']); ?>

  <?php print render($form['field_type_of_store']); ?>
  <?php print render($form['field_area']); ?>
  <?php print render($form['field_contact_number']); ?>

  <?php print render($form['field_store_code']); ?>
  <?php print render($form['field_store_status']); ?>

  <?php print render($form['actions']); ?>
</section>

<div class="hidden-container">
  <?php print render($form['field_bank_name']); ?>
  <?php print render($form['field_inventory']); ?>
  <?php print render($form['field_inventory_placeholder']); ?>
  <?php print render($form['field_my_employee']); ?>
</div>

<?php print drupal_render_children($form); ?>
