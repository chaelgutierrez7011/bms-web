<inpu id="zkow" type="hidden" value="<?php print $item['owner_id']; ?>">
<div ng-app="starter">
  <form name="sales-by-store-form" ng-controller="ProductSalesBySystemWideCategoryAllCtrl">
  <div class="tab-navigations">
    <ul class="tabs-menu">
      <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Day Sales</a></li>
      <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Week to Date Sales</a></li>
      <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Month to Date Sales</a></li>
      <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Year to Date Sales</a></li>
    </ul>
  </div>

    <div class="body-tabs">
      <div id="tab-0" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
          <div ng-hide="!tabs[0].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Product Category</th>
                <th>Qty Sold</th>
                <th>Peso Total</th>
                <th>Volume %</th>
                <th>Peso %</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[0].content">
                  <td>{{ item.category }}</td>
                  <td>{{ item.quantity }}</td>
                  <td>{{ item.peso_total | currency:"₱":2}}</td>
                  <td>{{ item.volume_percentage | currency:"%":2}}</td>
                  <td>{{ item.peso_percentage | currency:"%":2}}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[0].content[0].total_quantity_sold }}</td>
                  <td>{{ tabs[0].content[0].total_peso_total | currency:"₱":2}}</td>
                  <td>{{ tabs[0].content[0].total_volume_percentage | currency:"%":2}}</td>
                  <td>{{ tabs[0].content[0].total_peso_percentage | currency:"%":2}}</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-1" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
          <div ng-hide="!tabs[1].isLoaded">
            <input type="week" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Product Category</th>
                <th>Qty Sold</th>
                <th>Peso Total</th>
                <th>Volume %</th>
                <th>Peso %</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[1].content">
                  <td>{{ item.category }}</td>
                  <td>{{ item.quantity }}</td>
                  <td>{{ item.peso_total | currency:"₱":2}}</td>
                  <td>{{ item.volume_percentage | currency:"%":2}}</td>
                  <td>{{ item.peso_percentage | currency:"%":2}}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[1].content[0].total_quantity_sold }}</td>
                  <td>{{ tabs[1].content[0].total_peso_total | currency:"₱":2}}</td>
                  <td>{{ tabs[1].content[0].total_volume_percentage | currency:"%":2}}</td>
                  <td>{{ tabs[1].content[0].total_peso_percentage | currency:"%":2}}</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-2" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
          <div ng-hide="!tabs[2].isLoaded">
            <input type="month" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Product Category</th>
                <th>Qty Sold</th>
                <th>Peso Total</th>
                <th>Volume %</th>
                <th>Peso %</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[2].content">
                  <td>{{ item.category }}</td>
                  <td>{{ item.quantity }}</td>
                  <td>{{ item.peso_total | currency:"₱":2}}</td>
                  <td>{{ item.volume_percentage | currency:"%":2}}</td>
                  <td>{{ item.peso_percentage | currency:"%":2}}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[2].content[0].total_quantity_sold }}</td>
                  <td>{{ tabs[2].content[0].total_peso_total | currency:"₱":2}}</td>
                  <td>{{ tabs[2].content[0].total_volume_percentage | currency:"%":2}}</td>
                  <td>{{ tabs[2].content[0].total_peso_percentage | currency:"%":2}}</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-3" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
          <div ng-hide="!tabs[3].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Product Category</th>
                <th>Qty Sold</th>
                <th>Peso Total</th>
                <th>Volume %</th>
                <th>Peso %</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[3].content">
                  <td>{{ item.category }}</td>
                  <td>{{ item.quantity }}</td>
                  <td>{{ item.peso_total | currency:"₱":2}}</td>
                  <td>{{ item.volume_percentage | currency:"%":2}}</td>
                  <td>{{ item.peso_percentage | currency:"%":2}}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[3].content[0].total_quantity_sold }}</td>
                  <td>{{ tabs[3].content[0].total_peso_total | currency:"₱":2}}</td>
                  <td>{{ tabs[3].content[0].total_volume_percentage | currency:"%":2}}</td>
                  <td>{{ tabs[3].content[0].total_peso_percentage | currency:"%":2}}</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
        </div>
      </div>
    </div>
  </form>
</div>
