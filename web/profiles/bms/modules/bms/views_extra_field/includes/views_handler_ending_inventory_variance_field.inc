<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_ending_inventory_variance_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {

    $beginning_inventory = $values->field_field_qty[0]['raw']['value'];
    $commissary_delivery = $values->field_field_qty_1[0]['raw']['value'];
    $commissary_pullout = $values->field_field_qty_2[0]['raw']['value'];
    $store_transin = $values->field_field_qty_3[0]['raw']['value'];
    $store_transout = $values->field_field_qty_4[0]['raw']['value'];
    $production = $values->field_field_qty_5[0]['raw']['value'];
    $wastage = $values->field_field_qty_6[0]['raw']['value'];

    $theo_quantity = $beginning_inventory + $commissary_delivery - $commissary_pullout + $store_transin - $store_transout - $production - $wastage;
    $actual = $values->field_field_qty_6[0]['raw']['value'];

    $variance_qty = $actual - $theo_quantity;

    return $variance_qty;
  }
}
