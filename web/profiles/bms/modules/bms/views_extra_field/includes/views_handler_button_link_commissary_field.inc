<?php

class views_handler_button_link_commissary_field extends views_handler_area {
  function render($empty = FALSE) {
      $path = current_path();
      $type = arg(1);
      $button_name  = '';

      switch($type) {
        case 'pullout':
          $button_name = t('Add Pull-Out');
          break;
        case 'order':
          $button_name = t('Add Order');
          break;
      }

      return '<a href="/' . $path . '/request">' . $button_name . '</a>';
  }
}