<inpu id="zkow" type="hidden" value="<?php print $item['owner_id']; ?>">
<div ng-app="starter">
  <form name="sales-by-store-form" ng-controller="ManageSalesBySystemWide">
  <div class="tab-navigations">
    <ul class="tabs-menu">
      <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Day Sales</a></li>
      <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Week to Date Sales</a></li>
      <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Month to Date Sales</a></li>
      <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Year to Date Sales</a></li>
    </ul>
  </div>

    <div class="body-tabs">
      <div id="tab-0" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
          <div ng-hide="!tabs[0].isLoaded">
            <div id="group-filter">
               <span class="input"><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required /></span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="ownership.value" ng-change="updateFilterOwnerShip(0, ownership.value)"
                    ng-options="ownershipType.value as ownershipType.title for ownershipType in ownership">
                    <option value="" selected="selected">Type of Store Ownership</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="area.value" ng-change="updateFilterAreaType(0, area.value)"
                    ng-options="areaType.value as areaType.title for areaType in area">
                    <option value="" selected="selected">Filter By Area</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Store</th>
                <th>Transaction Count</th>
                <th>Average Check</th>
                <th>Sales</th>
                <th>%</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[0].content">
                  <td>{{ item.store_name }}</td>
                  <td>{{ item.transaction_count }}</td>
                  <td>{{ item.average_check | currency:"₱":2}}</td>
                  <td>{{ item.sales | currency:"₱":2}}</td>
                  <td>{{ item.sales_percentage | currency:"%":2}}</td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[0].content[0].total_transaction_count }}</td>
                  <td>{{ tabs[0].content[0].total_average_check | currency:"₱":2}}</td>
                  <td>{{ tabs[0].content[0].total_sales | currency:"₱":2}}</td>
                  <td>{{ tabs[0].content[0].total_sales_percentage | currency:"%":2}}</td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-1" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
          <div ng-hide="!tabs[1].isLoaded">
            <div id="group-filter">
               <span><input type="week" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required /></span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="ownership.value" ng-change="updateFilterOwnerShip(1, ownership.value)"
                    ng-options="ownershipType.value as ownershipType.title for ownershipType in ownership">
                    <option value="" selected="selected">Type of Store Ownership</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="area.value" ng-change="updateFilterAreaType(1, area.value)"
                    ng-options="areaType.value as areaType.title for areaType in area">
                    <option value="" selected="selected">Filter By Area</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Store</th>
                <th>Transaction Count</th>
                <th>Average Check</th>
                <th>Sales</th>
                <th>%</th>
                <th>More Details</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[1].content">
                  <td>{{ item.store_name }}</td>
                  <td>{{ item.transaction_count }}</td>
                  <td>{{ item.average_check | currency:"₱":2}}</td>
                  <td>{{ item.sales | currency:"₱":2}}</td>
                  <td>{{ item.sales_percentage | currency:"%":2}}</td>
                  <td>
                    <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalSalesByWeek(1, item.store_id)">
                      View
                    </button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[1].content[0].total_transaction_count }}</td>
                  <td>{{ tabs[1].content[0].total_average_check | currency:"₱":2}}</td>
                  <td>{{ tabs[1].content[0].total_sales | currency:"₱":2}}</td>
                  <td>{{ tabs[1].content[0].total_sales_percentage | currency:"%":2}}</td>
                  <td></td>
                </tr>
                <tr class="second-footer">
                  <td>Average</td>
                  <td>{{ tabs[1].content[0].avg_transaction_count }}</td>
                  <td>{{ tabs[1].content[0].avg_total_sales/tabs[1].content[0].avg_transaction_count | currency:"₱":2}}</td>
                  <td>{{ tabs[1].content[0].avg_total_sales | currency:"₱":2}}</td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-2" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
          <div ng-hide="!tabs[2].isLoaded">
            <div id="group-filter">
               <span><input type="month" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required /></span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="ownership.value" ng-change="updateFilterOwnerShip(2, ownership.value)"
                    ng-options="ownershipType.value as ownershipType.title for ownershipType in ownership">
                    <option value="" selected="selected">Type of Store Ownership</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="area.value" ng-change="updateFilterAreaType(2, area.value)"
                    ng-options="areaType.value as areaType.title for areaType in area">
                    <option value="" selected="selected">Filter By Area</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Store</th>
                <th>Transaction Count</th>
                <th>Average Check</th>
                <th>Sales</th>
                <th>%</th>
                <th>More Details</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[2].content">
                  <td>{{ item.store_name }}</td>
                  <td>{{ item.transaction_count }}</td>
                  <td>{{ item.average_check | currency:"₱":2}}</td>
                  <td>{{ item.sales | currency:"₱":2}}</td>
                  <td>{{ item.sales_percentage | currency:"%":2}}</td>
                  <td>
                    <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalSalesByMonth(2, item.store_id)">
                      View
                    </button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[2].content[0].total_transaction_count }}</td>
                  <td>{{ tabs[2].content[0].total_average_check | currency:"₱":2}}</td>
                  <td>{{ tabs[2].content[0].total_sales | currency:"₱":2}}</td>
                  <td>{{ tabs[2].content[0].total_sales_percentage | currency:"%":2}}</td>
                  <td></td>
                </tr>
                <tr class="second-footer">
                  <td>Average</td>
                  <td>{{ tabs[2].content[0].avg_transaction_count }}</td>
                  <td>{{ tabs[2].content[0].avg_total_sales/tabs[2].content[0].avg_transaction_count | currency:"₱":2}}</td>
                  <td>{{ tabs[2].content[0].avg_total_sales | currency:"₱":2}}</td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-3" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
          <div ng-hide="!tabs[3].isLoaded">
            <div id="group-filter">
               <span><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required /></span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="ownership.value" ng-change="updateFilterOwnerShip(3, ownership.value)"
                    ng-options="ownershipType.value as ownershipType.title for ownershipType in ownership">
                    <option value="" selected="selected">Type of Store Ownership</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
               <span class="select">
                <select class="form-control" name="show-filter" ng-model="area.value" ng-change="updateFilterAreaType(3, area.value)"
                    ng-options="areaType.value as areaType.title for areaType in area">
                    <option value="" selected="selected">Filter By Area</option>
                </select>
                <div class="arrow"><b></b></div>
               </span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Store</th>
                <th>Transaction Count</th>
                <th>Average Check</th>
                <th>Sales</th>
                <th>%</th>
                <th>More Details</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[3].content">
                  <td>{{ item.store_name }}</td>
                  <td>{{ item.transaction_count }}</td>
                  <td>{{ item.average_check | currency:"₱":2}}</td>
                  <td>{{ item.sales | currency:"₱":2}}</td>
                  <td>{{ item.sales_percentage | currency:"%":2}}</td>
                  <td>
                    <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalSalesByYear(3, item.store_id)">
                      View
                    </button>
                  </td>
                </tr>
              </tbody>
              <tfoot>
                <tr>
                  <td>Total</td>
                  <td>{{ tabs[3].content[0].total_transaction_count }}</td>
                  <td>{{ tabs[3].content[0].total_average_check | currency:"₱":2}}</td>
                  <td>{{ tabs[3].content[0].total_sales | currency:"₱":2}}</td>
                  <td>{{ tabs[3].content[0].total_sales_percentage | currency:"%":2}}</td>
                  <td></td>
                </tr>
                <tr class="second-footer">
                  <td>Average</td>
                  <td>{{ tabs[3].content[0].avg_transaction_count }}</td>
                  <td>{{ tabs[3].content[0].avg_total_sales/tabs[3].content[0].avg_transaction_count | currency:"₱":2}}</td>
                  <td>{{ tabs[3].content[0].avg_total_sales | currency:"₱":2}}</td>
                  <td></td>
                  <td></td>
                </tr>
              </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
        </div>
      </div>
    </div>
  </form>
</div>
