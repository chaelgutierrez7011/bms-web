/**
 * @file table_trash.js
 *
 * Takes parameters set on the configuration page and invokes the DataTables JS
 * and DataTables-Responsive JS libraries.
 */
(function ($) {
  "use strict";
  Drupal.behaviors.table_trash_attach = {

    attach: function (context, settings) {
      var nots = [];
      $(settings.table_trash, context).each(function () {
        // settings.table_trash is an array of JSON objects indexed by selectors as
        // entered on the Table Trash config page. One selector per decoration.
        $.each(this, function (selector, params) {
          var tables = $(selector);

          if (selector !== 'table') {
            nots.push(selector);
          }
          else {
            tables = $(selector).not(nots.join(','));
          }

          if (tables.length > 0) {
            tables.DataTable(params);
          }
        });
      });
    }
  };
}) (jQuery);
