<?php
/**
 * @file
 * profile_type.features.inc
 */

/**
 * Implements hook_default_profile2_type().
 */
function profile_type_default_profile2_type() {
  $items = array();
  $items['main'] = entity_import('profile2_type', '{
    "userCategory" : true,
    "userView" : true,
    "type" : "main",
    "label" : "Main profile",
    "weight" : "-1",
    "data" : { "registration" : 1, "use_one_page" : 1, "use_page" : 0 },
    "rdf_mapping" : []
  }');
  return $items;
}
