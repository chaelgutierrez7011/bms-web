<?php

/**
 * Implement hook_views_query_alter().
 */
function bms_store_views_query_alter(&$view, &$query) {
  $parameter = arg(0);
  if($parameter == 'sales-by-hour') {
    $query->add_groupby('field_data_field_created_store_entity_type.store');
    $query->distinct = TRUE;
  }
}
