<!-- Bar chart -->
<div class="box box-primary">
  <div class="box-body">
    <div class="chart">
      <canvas id="bar-chart-monthly" style="height:230px"></canvas>
    </div>
  </div>
  <!-- /.box-body-->
</div>
<!-- /.box -->