<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_average_check_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {

    $date = explode(' ', $values->field_field_created[0]['raw']['value']);
    $sid = $values->_field_data['field_reference_store_id_eck_store_id']['entity']->field_store_id['und'][0]['value'];
    return $this->ge_average_check($date[0], $sid);
  }

  /**
   * Callback function getting average check
   */
   function ge_average_check($date, $sid) {
     $current_view = arg(0);
     
     switch ($current_view) {
       case 'sales-by-area':
        // By day
        $average_check = db_query("SELECT
        ROUND(SUM(wv.field_total_with_vat_value) / COUNT(id), 2)
        FROM
        eck_store eck
        INNER JOIN field_data_field_created fc ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si ON si.entity_id = eck.id
        INNER JOIN field_data_field_total_with_vat wv ON wv.entity_id = eck.id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')
        AND (field_reference_store_id_target_id = :sid)
        GROUP BY DATE(field_created_value)", array(
          ':sid' => $sid));
         break;
       case 'sales-by-system-wide':
        // By day
        $average_check = db_query("SELECT
        ROUND(SUM(wv.field_total_with_vat_value) / COUNT(id), 2)
        FROM
        eck_store eck
        INNER JOIN field_data_field_created fc ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si ON si.entity_id = eck.id
        INNER JOIN field_data_field_total_with_vat wv ON wv.entity_id = eck.id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')
        AND (field_reference_store_id_target_id = :sid)
        GROUP BY DATE(field_created_value)", array(
          ':sid' => $sid));
         break;
        case 'sales-by-store':
        // By day
        $average_check = db_query("SELECT
        ROUND(SUM(wv.field_total_with_vat_value) / COUNT(id), 2)
        FROM
        eck_store eck
        INNER JOIN field_data_field_created fc ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si ON si.entity_id = eck.id
        INNER JOIN field_data_field_total_with_vat wv ON wv.entity_id = eck.id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')
        AND (field_reference_store_id_target_id = :sid)
        GROUP BY DATE(field_created_value)", array(
          ':sid' => $sid));
          break;
       default:
         # code...
         break;
     }

     return $average_check->fetchField();
   }
}
