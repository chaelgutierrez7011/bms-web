<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Store Transfer Form</h3>
  </div><!-- /.box-header -->
  <div class="box-body">
    <?php print render($form['transaction']); ?>
    <?php print render($form['store_branch_from']); ?>
    <?php print render($form['store_branch_to']); ?>
    <?php print render($form['transfer_in']); ?>
    <?php print render($form['transfer_out']); ?>
    <?php print render($form['submit']); ?>
  </div><!-- /.box-body -->
</div>
<?php print drupal_render_children($form); ?>
