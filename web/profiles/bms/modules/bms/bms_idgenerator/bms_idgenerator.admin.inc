<?php

/**
 * Implement idgenerator_config_form
 */
function idgenerator_config_form($form, &$form_state) {

  $form['comp_abbr'] = array(
    '#title' => t('Company Abbreviation'),
    '#type' => 'textfield',
    '#default_value' => variable_get('comp_abbr', 'BMS')
  );

  $form['emp_number'] = array(
    '#title' => t('Employee number format'),
    '#type' => 'textfield',
    '#default_value' => variable_get('emp_number', '000'),
  );

  $form['order_number'] = array(
    '#title' => t('Order number format'),
    '#type' => 'textfield',
    '#default_value' => variable_get('order_number', '000'),
  );

  return system_settings_form($form);
}
