<?php
/**
 * @file
 * bms_image_styles.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function bms_image_styles_image_default_styles() {
  $styles = array();

  // Exported image style: square_400x400.
  $styles['square_400x400'] = array(
    'label' => 'Square 400x400',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 400,
          'height' => 400,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: square_45x45.
  $styles['square_45x45'] = array(
    'label' => 'square 45x45',
    'effects' => array(
      1 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 45,
          'height' => 45,
        ),
        'weight' => 1,
      ),
    ),
  );

  return $styles;
}
