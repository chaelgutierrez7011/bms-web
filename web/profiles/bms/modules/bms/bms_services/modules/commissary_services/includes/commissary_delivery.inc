<?php

/**
 * Implement commissary_services_deliver().
 */
function commissary_delivery_services($id, $data) {
  module_load_include('inc', 'bms_services', 'resources/store');
  $created = _datetime_format_UTC($data['created']);

  $store = entity_load_single('store', $id);

  $query = db_query("SELECT ie.eid FROM inventory_entry ie
  INNER JOIN commissary_delivery cd ON ie.eid = cd.eid
  WHERE FROM_UNIXTIME(ie.timestamp, '%Y-%m-%d') = :date_filter AND store_id = :sid",
  array(':date_filter' => date('Y-m-d', strtotime($created)), ':sid' => $id));

  $eid = $query->fetchColumn();

  if(!empty($eid)) {
    try {
      // If items is not empty it means that we need to populate it to complete data.
      if(!empty($data['items']) && empty($data['transaction_id'])) {
        // Insert none existing data.
        foreach($data['items'] as $delivery) {
          $product = entity_load_single('store', $delivery['item_id']);
          $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

          $commissary_delivery = new stdClass;
          $commissary_delivery->timestamp = strtotime($created);
          $commissary_delivery->eid = $eid;
          $commissary_delivery->request_number = $data['request_number'];
          $commissary_delivery->transaction_number = $data['transaction_number'];
          $commissary_delivery->category = $category->name;
          $commissary_delivery->item_id = $delivery['item_id'];
          $commissary_delivery->item = $delivery['item_name'];
          $commissary_delivery->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
          $commissary_delivery->quantity = $delivery['quantity'];
          $commissary_delivery->unit_of_measurement = $delivery['item_uom'];
          $commissary_delivery->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
          drupal_write_record('commissary_delivery', $commissary_delivery);
        }
        module_load_include('inc', 'bms_services', 'resources/store');
        // Record process status.
        record_ending_inventory_status($id, $created, 'commissary_delivery');
      }
      else {
        // Update the items
        if(!empty($data['transaction_id'])) {
          foreach($data['items'] as $delivery) {
            db_update('commissary_delivery')
            ->expression('quantity', 'quantity + :quantity', array(':quantity' => $delivery['quantity']))
            ->condition('item_id', $delivery['item_id'])
            ->condition('eid', $eid)
            ->execute();
          }
        }
      }
    }
    catch (Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }
  else {
    try {
      module_load_include('inc', 'bms_services', 'resources/store');
      // Record process status.
      record_ending_inventory_status($id, $created, 'commissary_delivery');

      $record = new stdClass;
      $record->timestamp = strtotime($created);
      $record->store_id = $id;
      $record->store_name = $store->title;

      $user = user_load($data['created_by_id']);

      $profile = profile2_load_by_user($user, $type_name = NULL);

      if(isset($first_name) && isset($last_name)) {
        $first_name = $profile['main']->field_firstname['und'][0]['value'];
        $last_name = $profile['main']->field_lastname['und'][0]['value'];

        $fullname = $first_name . ' ' . $last_name;
      }
      else {
        $fullname = $user->name;
      }
      $record->uid = $user->uid;
      $record->crew_name = $fullname;

      drupal_write_record('inventory_entry', $record);

      foreach($data['items'] as $delivery) {
        $product = entity_load_single('store', $delivery['item_id']);
        $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

        $commissary_delivery = new stdClass;
        $commissary_delivery->timestamp = strtotime($created);
        $commissary_delivery->eid = $record->eid;
        $commissary_delivery->request_number = $data['request_number'];
        $commissary_delivery->transaction_number = $data['transaction_number'];
        $commissary_delivery->category = $category->name;
        $commissary_delivery->item_id = $delivery['item_id'];
        $commissary_delivery->item = $delivery['item_name'];
        $commissary_delivery->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
        $commissary_delivery->quantity = $delivery['quantity'];
        $commissary_delivery->unit_of_measurement = $delivery['item_uom'];
        $commissary_delivery->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
        drupal_write_record('commissary_delivery', $commissary_delivery);
      }
    }
    catch (Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }

}
