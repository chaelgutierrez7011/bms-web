<?php

include_once 'includes/user-profile-helper.inc';

/**
 * Implements hook_preprocess_html().
 */
function bms_adminlte_preprocess_html(&$variables) {
  $variables['classes_array'][] = 'hold-transition skin-blue sidebar-mini';

  // For Stores Pages.
  $store_profile_view = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && empty(arg(3)));
  $store_profile_edit = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'edit');
  $store_profile_inventory = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'inventory');
  $store_profile_product = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'products');

  // Commissary Pages.
  $commissary_request_inner = (arg(0) == 'commissary' && arg(1) == 'order' && arg(2) == 'store');
  $commissary_pullout_inner = (arg(0) == 'commissary' && arg(1) == 'pullout' && arg(2) == 'store');

  // For Reports Pages.
  $store_transfers_inner = (arg(0) == 'store-transfers' && arg(1) == 'store' && is_numeric(arg(2)));
  $product_sales_by_store_inner = (arg(0) == 'product-sales' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'product-items' && arg(5) == 'by-store');
  $product_sales_bsw_inner = (arg(0) == 'product-sales' && arg(1) == 'system-wide' && arg(2) == 'component-item');
  $sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'sales-transaction');
  $sales_by_store_inner = (arg(0) == 'sales-by-store' && is_numeric(arg(1)));
  $sales_by_area_inner = (arg(0) == 'sales-by-area' && is_numeric(arg(1)));

  if ($store_profile_view) {
    $variables['classes_array'][] = 'store-basic-information';
  }
  elseif ($store_profile_edit) {
    $variables['classes_array'][] = 'store-edit';
  }
  elseif ($store_profile_inventory) {
    $variables['classes_array'][] = 'store-inventory';
  }
  elseif ($store_profile_product) {
    $variables['classes_array'][] = 'store-products';
  }
  elseif ($store_transfers_inner) {
    $variables['classes_array'][] = 'reports-store-transfers-inner';
  }

  if ($product_sales_by_store_inner || $product_sales_bsw_inner || $sales_transaction_inner || $sales_by_store_inner || $sales_by_area_inner) {
    $variables['classes_array'][] = 'reports-quicktabs-tables';
  }

  if ($commissary_request_inner || $commissary_pullout_inner) {
    $variables['classes_array'][] = 'commissary-quicktabs-tables';
  }

  // Separate Form Pages.
  $add_store = (request_uri() == '/store/add');
  $add_crew = (arg(0) == 'admin' && arg(1) == 'people' && arg(2) == 'create');
  $add_inventory = (request_uri() == '/raw-materials/add');
  $edit_inventory = (arg(0) == 'field-collection' && arg(1) == 'field-inventory' && arg(3) == 'edit');
  $inventory_forms = ($add_inventory || $edit_inventory);
  $add_product = (request_uri() == '/product/add');
  $edit_product = (arg(0) == 'admin' && arg(1) == 'structure' && arg(2) == 'entity-type' && arg(3) == 'store' && arg(4) == 'product' && arg(6) == 'edit');
  $product_forms = ($add_product || $edit_product);
  $edit_schedule = (arg(0) == 'add-schedule' && arg(2) == 'edit');
  $edit_undertime = (arg(0) == 'add-undertime' && arg(2) == 'edit');
  $edit_overtime = (arg(0) == 'add-overtime' && arg(2) == 'edit');
  $schedules = ($edit_schedule || $edit_undertime || $edit_overtime);
  $attendance_adr = (arg(0) == 'field-collection' && arg(1) == 'field-actual-duty-rendered' && arg(3) == 'edit');
  $attendance_br = (arg(0) == 'field-collection' && arg(1) == 'field-break-freezer' && arg(3) == 'edit');
  $attendances = ($attendance_adr || $attendance_br);
  $inventory_bi = (arg(0) == 'field-collection' && arg(1) == 'field-beginning-inventory' && arg(3) == 'edit');
  $inventory_cd = (arg(0) == 'field-collection' && arg(1) == 'field-commissary-delivery' && arg(3) == 'edit');
  $inventory_cp = (arg(0) == 'field-collection' && arg(1) == 'field-commissary-pullout' && arg(3) == 'edit');
  $inventory_sti = (arg(0) == 'field-collection' && arg(1) == 'field-store-transin' && arg(3) == 'edit');
  $inventory_sto = (arg(0) == 'field-collection' && arg(1) == 'field-store-transout' && arg(3) == 'edit');
  $inventory_ppo = (arg(0) == 'field-collection' && arg(1) == 'field-production-processed-open' && arg(3) == 'edit');
  $inventory_w = (arg(0) == 'field-collection' && arg(1) == 'field-wastages' && arg(3) == 'edit');
  $inventory_eia = (arg(0) == 'field-collection' && arg(1) == 'field-ending-inventory-actual' && arg(3) == 'edit');
  $inventory = ($inventory_bi || $inventory_cd || $inventory_cp || $inventory_sti || $inventory_sto || $inventory_ppo || $inventory_w || $inventory_eia);
  $production_po = (arg(0) == 'field-collection' && arg(1) == 'field-processed-opened' && arg(3) == 'edit');
  $production_w = (arg(0) == 'field-collection' && arg(1) == 'field-wastages' && arg(3) == 'edit');
  $production_eia = (arg(0) == 'field-collection' && arg(1) == 'field-ending-inventory-actual' && arg(3) == 'edit');
  $production = ($production_po || $production_w || $production_eia);
  $managesales = (arg(0) == 'manage-sales-transaction' && arg(1) == 'sales-transaction' && arg(2) == 'item');
  $cash_sales_atv = (arg(0) == 'manage-cash-sales' && arg(1) == 'store' && arg(3) == 'added-to-vault');
  $cash_sales_pull = (arg(0) == 'field-collection' && arg(1) == 'field-cash-management-pullout' && arg(3) == 'edit');
  $cash_sales_dep = (arg(0) == 'field-collection' && arg(1) == 'field-cash-management-deposit' && arg(3) == 'edit');
  $cash_sales = ($cash_sales_atv || $cash_sales_pull || $cash_sales_dep);
  $com_order_view = (arg(0) == 'commissary' && arg(1) == 'order' && arg(3) == 'view');
  $com_order_request = (arg(0) == 'commissary' && arg(1) == 'order' && arg(3) == 'request');
  $com_pullout_view = (arg(0) == 'commissary' && arg(1) == 'pullout' && arg(3) == 'view');
  $com_pullout_request = (arg(0) == 'commissary' && arg(1) == 'pullout' && arg(3) == 'request');
  $commissaries = ($com_order_view || $com_order_request || $com_pullout_view || $com_pullout_request);
  $store_transfer_in = (arg(0) == 'store-transfer' && arg(1) == 'transfer-in-request' && arg(3) == 'view');
  $store_transfer_out = (arg(0) == 'store-transfer' && arg(1) == 'transfer-out-request' && arg(3) == 'view');
  $store_transfers = ($store_transfer_in || $store_transfer_out);
  if ($add_store || $add_crew || $inventory_forms || $product_forms || $schedules || $attendances ||
      $inventory || $production || $managesales || $cash_sales || $commissaries || $store_transfers) {
    $variables['classes_array'][] = 'form-pages';
    $form_css = drupal_get_path('theme', 'bms_adminlte') . '/assets/css/components/forms-modals.css';
    drupal_add_css($form_css,
      array(
        'group' => CSS_THEME,
        'weight' => '9998',
      )
    );
  }
}

/**
 * Implements hook_init().
 */
function bms_adminlte_init() {
  $theme_path = drupal_get_path('theme', $GLOBALS['theme']);

  drupal_add_css('https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css', 'external');
  drupal_add_css('https://code.ionicframework.com/ionicons/2.0.1/css/ionicons.min.css', 'external');
  drupal_add_js($theme_path . '/dist/js/app.min.js', array('type' => 'file', 'scope' => 'footer'));
}

/**
 * Implements hook_js_alter().
 */
function bms_adminlte_js_alter(&$javascript) {
  $theme_path = drupal_get_path('theme', 'bms_adminlte');
  $store_profile_view = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && empty(arg(3)));
  $url = arg(0);

  if($url == 'manage-employee' || $url == 'administer-employee') {
    $javascript['profiles/bms/themes/bms_adminlte/plugins/jQuery/jquery-2.2.3.min.js']['group'] = -101;
    $javascript['profiles/bms/themes/bms_adminlte/plugins/jQuery/jquery-2.2.3.min.js']['weight'] = -19.999;
  }
  elseif ($store_profile_view) {

  }
  //unset($javascript['profiles/bms/themes/bms_adminlte/plugins/jQuery/jquery-2.2.3.min.js']);
  $javascript['profiles/bms/modules/contrib/autodialog/autodialog.js']['weight'] = 11;
  $javascript['profiles/bms/modules/contrib/autodialog/autodialog.js']['scope'] = 'footer';
}

/**
 * Implements hook_preprocess_page().
 */
function bms_adminlte_preprocess_page(&$vars, $hook) {
  global $user;
  global $base_url;
  $add_form_css = FALSE;
  $url = arg(0);

  $vars['front_page'] = $base_url . '/dashboard';
  $theme_path = drupal_get_path('theme', 'bms_adminlte');
  $module_path = drupal_get_path('module', 'bms_visualization');
  $vars['status_header'] = drupal_get_http_header("status");

  if($url == 'manage-employee' || $url == 'administer-employee') {
    drupal_add_js($theme_path . '/plugins/jQuery/jquery-2.2.3.min.js');
  }
  else {
    drupal_add_js($theme_path . '/plugins/jQuery/jquery-2.2.3.min.js', array('type' => 'file', 'scope' => 'footer', 'weight' => 11));
  }
  //drupal_add_js('https://code.jquery.com/jquery-1.10.2.min.js', array('type' => 'external', 'scope' => 'footer', 'weight' => 5));
  // Bootstrap 3.3.5
  drupal_add_js($theme_path . '/bootstrap/js/bootstrap.min.js', array('type' => 'file', 'scope' => 'footer', 'weight' => 11));
  // jQuery UI
  drupal_add_js('https://code.jquery.com/ui/1.11.4/jquery-ui.min.js', array('type' => 'external', 'scope' => 'footer'));
  // FastClick
  drupal_add_js($theme_path . '/plugins/fastclick/fastclick.min.js', array('type' => 'file', 'scope' => 'footer'));
  // AdminLTE App
  drupal_add_js($theme_path . '/dist/js/app.min.js', array('type' => 'file', 'scope' => 'footer', 'weight' => 12));
  // jQuery Knob
  drupal_add_js($theme_path . '/plugins/knob/jquery.knob.js', array('type' => 'file', 'scope' => 'footer', 'weight' => 11));
  // For Checkboxes.
  drupal_add_js($theme_path . '/assets/js/icheck.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_css($theme_path . '/assets/css/icheck.css', array('group' => CSS_THEME));
  // For select boxes.
  drupal_add_js($theme_path . '/assets/js/select2.min.js', array('type' => 'file', 'scope' => 'footer'));
  drupal_add_css($theme_path . '/assets/css/select2.min.css', array('group' => CSS_THEME));
  // Time Picker
  drupal_add_js($theme_path . '/plugins/timepicker/bootstrap-timepicker.min.js', array('type' => 'file', 'scope' => 'footer'));
  // Pace.
  //drupal_add_js($theme_path . '/assets/js/pace.min.js', array('type' => 'file', 'weight' => -10));
  //drupal_add_css($theme_path . '/assets/css/pace.css', array('group' => CSS_THEME));
  // Additional js for theme.
  drupal_add_js($theme_path . '/assets/js/script.js', array('type' => 'file', 'scope' => 'footer'));


  $vars['logout'] = '/user/logout';
  $vars['profile'] = drupal_get_path_alias('user/' . $user->uid);
  $roles = end($user->roles);
  $vars['role'] = ucfirst($roles);
  reset($user->roles);
  // Create custom variable for page tpl
  if(user_is_logged_in()) {
    $profile = profile2_load_by_user($user, 'main');
    $account = user_load($user->uid);

    $alt = t("@user's picture", array('@user' => format_username($user)));
    if (function_exists('identicon_identicon_image_path')) {
      $image_path = identicon_identicon_image_path($user);
      if(!empty($account->picture)) {
        $user_picture = theme('image_style', array('style_name' => 'square_400x400', 'path' => $account->picture->uri, 'alt' => $alt, 'title' => $alt, 'attributes' => array('class' => 'img-circle')));
        $user_picture_m = theme('image_style', array('style_name' => 'square_45x45', 'path' => $account->picture->uri, 'alt' => $alt, 'title' => $alt, 'attributes' => array('class' => 'user-image')));
      }
      else {
        $user_picture = theme('image_style', array('style_name' => 'square_400x400', 'path' => $image_path, 'alt' => $alt, 'title' => $alt, 'attributes' => array('class' => 'img-circle')));
        $user_picture_m = theme('image_style', array('style_name' => 'square_45x45', 'path' => $image_path, 'alt' => $alt, 'title' => $alt, 'attributes' => array('class' => 'user-image')));
      }

      $vars['avatar'] = $user_picture;
      $vars['avatarsm'] = $user_picture_m;
    }
    $vars['history'] = 'Member for ' . format_interval(time() - $user->created);
    $fullname = array(
      'firstname' => isset($profile->field_firstname['und']) ? $profile->field_firstname['und'][0]['value'] : '',
      'lastname' => isset($profile->field_lastname['und']) ? $profile->field_lastname['und'][0]['value'] : '',
    );

    $vars['fullname'] = implode($fullname, ' ');
  }

  // Additional css for common styles.
  drupal_add_css($theme_path . '/assets/css/main.css',
    array(
      'group' => CSS_THEME,
      'weight' => '9997',
    )
  );

  // Adding css for individual pages.
  $individual_css = '';

  // For Dashboard Page.
  if (drupal_is_front_page()) {
    $individual_css = '/assets/css/components/dashboard.css';
    $vars['title'] = 'Dashboard<small>Version 2.0</small>';
  }

  // For Settings Pages.
  $manage_employee = (arg(0) == 'manage-employee');
  $manage_product = (arg(0) == 'manage-product');
  $administer_employee = (arg(0) == 'administer-employee');
  $store_settings = (arg(0) == 'store-settings');
  $inventory = (arg(0) == 'inventory' && empty(arg(3)));
  if ($manage_employee || $manage_product || $administer_employee || $store_settings || $inventory) {
    $total_rows = $vars['page']['#views_contextual_links_info']['views_ui']['view']->total_rows;
    $individual_css = '/assets/css/components/settings.css';
    if ($administer_employee) {
      $vars['title'] = 'Crew Settings';
      $vars['title'] .= '<small>Total of ' . $total_rows . ' Crew</small>';
    }
    if ($manage_employee) {
      $vars['title'] = 'Admin User Settings';
      $vars['title'] .= '<small>Total of ' . $total_rows . ' Admin Users</small>';
    }
    if ($manage_product) {
      $vars['title'] = 'Product Settings';
      $vars['title'] .= '<small>Total of ' . $total_rows . ' Items</small>';
    }
    if ($inventory) {
      $vars['title'] = 'Inventory Settings';
      $vars['title'] .= '<small>Total of ' . $total_rows . ' Items</small>';
    }
    if ($store_settings) {
      $vars['title'] = 'Store Settings';
      $vars['title'] .= '<small>Total of ' . $total_rows . ' Stores</small>';
    }
  }

  // For User pages.
  $add_user_page = (arg(0) == 'admin' && arg(1) == 'people' && arg(2) == 'create');
  $user_profile = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == FALSE);
  $user_profile_edit = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'edit');
  $user_work_schedule = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'work-schedule');
  if ($user_profile || $user_profile_edit || $user_work_schedule || $add_user_page) {
    // Getting selected user full name.
    $employee_user = profile2_load_by_user(arg(1));
    if (isset($employee_user['main'])) {
      $employee_firstname = $employee_user['main']->field_firstname['und'][0]['value'];
      $employee_lastname = $employee_user['main']->field_lastname['und'][0]['value'];
      $vars['title'] = $employee_firstname . ' ' . $employee_lastname;
    }

    $employee_user_main = user_load(arg(1), $reset = FALSE);
    if (isset($employee_user_main->roles)) {
      if (!in_array('crew', $employee_user_main->roles)) {
        hide($vars['tabs']['#primary'][1]);
        hide($vars['page']['bottom_content']['bms_visualization_monthly_performance']);
        hide($vars['page']['bottom_content']['bms_others_store_assignments_block']);
        foreach ($employee_user_main->roles as $key => $value) {
          if ($value != 'authenticated user') {
            $vars['title'] .= '<small>' . $value . '</small>';
          }
        }

      }
    }

    if (!$add_user_page) {
      $individual_css = '/assets/css/components/user-profile.css';
    }

  }

  // For Stores Pages.
  $store_profile_view = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && empty(arg(3)));
  $store_profile_edit = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'edit');
  $store_profile = ($store_profile_view || $store_profile_edit);
  $store_profile_inventory = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'inventory');
  $store_profile_product = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'products');
  if ($store_profile || $store_profile_inventory || $store_profile_product) {
    $individual_css = '/assets/css/components/stores.css';
    $vars['title'] = 'Store Profile';
    if ($store_profile_view) {
      $store_details = entity_load_single('store', arg(2));

      foreach ($vars['page']['content']['system_main']['store'] as $key => $value) {
        // Get Image from store.
        $no_image_src = $base_url . '/' . $theme_path . '/assets/images/no-image.jpg';
        $markup = '<img src="' . $no_image_src . '">';
        if (isset($store_details->field_image['und'][0]['uri'])) {
          $store_image = image_style_url('square_400x400', $store_details->field_image['und'][0]['uri']);
          $markup = '<img src="' . $store_image . '">';
        }
        // Image Field.
        $vars['page']['content']['system_main']['store'][$key]['field_picture'] = array(
          '#weight' => 0,
          '#markup' => '<div class="store-image">' . $markup . '</div>',
        );
        // Name of Company Field.
        if (isset($store_details->field_company['und'][0]['value'])) {
          $store_company = $store_details->field_company['und'][0]['value'];
          $vars['page']['content']['system_main']['store'][$key]['field_company'] = array(
            '#theme' => 'field',
            '#title' => t('Name of Company'),
            '#weight' => 3,
            '#field_type' => 'text',
            '#items' => array(
              0 => array(
                'value' => $store_company,
              ),
            ),
            0 => array(
              '#markup' => $store_company,
            ),
          );
        }
        // Name of Franchisee Field.
        if (isset($store_details->field_store_franchisee['und'][0]['value'])) {
          $store_franchisee = $store_details->field_store_franchisee['und'][0]['value'];
          $vars['page']['content']['system_main']['store'][$key]['field_store_franchisee'] = array(
            '#theme' => 'field',
            '#title' => t('Name of Franchisee'),
            '#weight' => 4,
            '#field_type' => 'text',
            '#items' => array(
              0 => array(
                'value' => $store_franchisee,
              ),
            ),
            0 => array(
              '#markup' => $store_franchisee,
            ),
          );
        }
        // Arrange weight of fields in store profile.
        $vars['page']['content']['system_main']['store'][$key]['field_location']['#weight'] = 2;
        $vars['page']['content']['system_main']['store'][$key]['field_store_ownership']['#weight'] = 5;
        $vars['page']['content']['system_main']['store'][$key]['field_type_of_store']['#weight'] = 6;
        $vars['page']['content']['system_main']['store'][$key]['field_area']['#weight'] = 7;
        $vars['page']['content']['system_main']['store'][$key]['field_contact_number']['#weight'] = 8;
        $vars['page']['content']['system_main']['store'][$key]['field_store_code']['#weight'] = 9;
        $vars['page']['content']['system_main']['store'][$key]['field_store_status']['#weight'] = 10;

        // Change Labels.
        $vars['page']['content']['system_main']['store'][$key]['field_store_name']['#title'] = t('Store Name');
        $vars['page']['content']['system_main']['store'][$key]['field_location']['#title'] = t('Address');
        $vars['page']['content']['system_main']['store'][$key]['field_store_ownership']['#title'] = t('Type of Ownership');
        $vars['page']['content']['system_main']['store'][$key]['field_area']['#title'] = t('Area No');
        $vars['page']['content']['system_main']['store'][$key]['field_contact_number']['#title'] = t('Contact No');
        $vars['page']['content']['system_main']['store'][$key]['field_store_status']['#title'] = t('Status');

        // Group fields to different container divs.
        $picture_wrapper = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('picture-wrapper'),
          ),
          'field_picture' => $vars['page']['content']['system_main']['store'][$key]['field_picture'],
        );
        $fields_wrapper = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array('fields-wrapper'),
          ),
          'field_store_name' => $vars['page']['content']['system_main']['store'][$key]['field_store_name'],
          'field_location' => $vars['page']['content']['system_main']['store'][$key]['field_location'],
          'field_company' => $vars['page']['content']['system_main']['store'][$key]['field_company'],
          'field_store_franchisee' => $vars['page']['content']['system_main']['store'][$key]['field_store_franchisee'],
          'field_store_ownership' => $vars['page']['content']['system_main']['store'][$key]['field_store_ownership'],
          'field_type_of_store' => $vars['page']['content']['system_main']['store'][$key]['field_type_of_store'],
          'field_area' => $vars['page']['content']['system_main']['store'][$key]['field_area'],
          'field_contact_number' => $vars['page']['content']['system_main']['store'][$key]['field_contact_number'],
          'field_store_code' => $vars['page']['content']['system_main']['store'][$key]['field_store_code'],
          'field_store_status' => $vars['page']['content']['system_main']['store'][$key]['field_store_status'],
        );
        $vars['page']['content']['system_main']['store'][$key]['picture_wrapper'] = $picture_wrapper;
        $vars['page']['content']['system_main']['store'][$key]['fields_wrapper'] = $fields_wrapper;

        // Hide fields after grouping.
        hide($vars['page']['content']['system_main']['store'][$key]['field_picture']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_store_name']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_location']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_company']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_store_franchisee']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_store_ownership']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_type_of_store']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_area']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_contact_number']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_store_code']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_store_status']);
        hide($vars['page']['content']['system_main']['store'][$key]['field_bank_name']);
      }
    }
  }

  // For Reports pages.
  $attendance_outer = (arg(0) == 'attendance');
  $attendance_report = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'attendance');
  $attendance = ($attendance_outer || $attendance_report);
  $inventory_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'inventory-report');
  $inventory_summary = (arg(0) == 'inventory-summary' || $inventory_summary_detail);
  $production_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'production-report');
  $production_summary = (arg(0) == 'production-summary' || $production_summary_detail);
  $cash_management_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'cash-management');
  $cash_management = (arg(0) == 'cash-management' || $cash_management_detail);
  $order_delivery_outer = (arg(0) == 'commissary' && empty(arg(1)));
  $order_delivery_inner = (arg(0) == 'commissary' && arg(1) == 'store' && is_numeric(arg(2)));
  $order_delivery = ($order_delivery_inner || $order_delivery_outer);
  $product_sales_by_store = (arg(0) == 'product-sales' && empty(arg(1)));
  $product_sales_by_store_component = (arg(0) == 'product-sales' && arg(1) == 'store' && is_numeric(arg(2)));
  $product_sales_by_store_inner = (arg(0) == 'product-sales' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'product-items' && arg(5) == 'by-store');
  $product_sales_bsw = (arg(0) == 'product-sales' && arg(1) == 'by-system-wide' && empty(arg(2)));
  $product_sales_bsw_component = (arg(0) == 'product' && arg(1) == 'component-item' && is_numeric(arg(2)));
  $product_sales_bsw_inner = (arg(0) == 'product-sales' && arg(1) == 'by-system-wide' && arg(2) == 'product');
  $product_sales_bsw_all = (arg(0) == 'product-sales' && arg(1) == 'by-system-wide' && arg(2) == 'category' && arg(3) == 'all');
  $product_sales = ($product_sales_by_store_inner || $product_sales_by_store_component || $product_sales_by_store || $product_sales_bsw_component || $product_sales_bsw || $product_sales_bsw_inner || $product_sales_bsw_all);
  $sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'sales-transaction');
  $sales_transaction = (arg(0) == 'sales-transaction');
  $sales_by_store_inner = (arg(0) == 'sales-by-store' && is_numeric(arg(1)));
  $sales_by_store = (arg(0) == 'sales-by-store' && empty(arg(1)));
  $sales_by_area_inner = (arg(0) == 'sales-by-area' && is_numeric(arg(1)));
  $sales_by_area = (arg(0) == 'sales-by-area' && empty(arg(1)));
  $sales_by_system_wide = (arg(0) == 'sales-by-system-wide');
  $reports_sales = ($sales_transaction || $sales_by_store || $sales_by_area || $sales_by_system_wide || $sales_transaction_inner || $sales_by_store_inner || $sales_by_area_inner);
  $store_transfers_outer = (arg(0) == 'store-transfers');
  $store_transfers_inner = (arg(0) == 'store-transfers' && arg(1) == 'store' && is_numeric(arg(2)));
  $store_transfers = ($store_transfers_outer || $store_transfers_inner);
  if ($attendance || $inventory_summary || $production_summary || $cash_management || $sales_by_store || $store_transfers || $reports_sales || $product_sales || $order_delivery) {
    $individual_css = '/assets/css/components/reports.css';

    if ($attendance_report || $production_summary_detail || $inventory_summary_detail) {
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title;
        if ($attendance_report) {
          $vars['title'] .= '<small>Attendance Reports</small>';
        }
        elseif ($production_summary_detail) {
          $vars['title'] .= '<small>Production Reports</small>';
        }
        elseif ($inventory_summary_detail) {
          $vars['title'] .= '<small>Inventory Reports</small>';
        }
      }
    }
    elseif ($attendance_outer) {
      $vars['title'] = 'Attendance Reports';
    }
    elseif (arg(0) == 'inventory-summary') {
      $vars['title'] = 'Inventory Reports';
    }
    elseif (arg(0) == 'production-summary') {
      $vars['title'] = 'Production Reports';
    }
    elseif (arg(0) == 'cash-management') {
      $vars['title'] = 'Cash Sales Report';
    }
    elseif ($store_transfers_inner) {
      $store_details = entity_load_single('store', arg(2));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Store Transfers Reports</small>';
      }
      $add_form_css = TRUE;
    }
    elseif ($store_transfers_outer) {
      $add_form_css = TRUE;
    }
    elseif ($reports_sales) {
      $add_form_css = TRUE;
      if ($sales_transaction) {
        $vars['title'] = 'Sales by Transaction Reports';
      }
      elseif ($sales_by_store) {
        $vars['title'] = 'Sales by Store';
      }
      elseif ($sales_by_area) {
        $vars['title'] = 'Sales by Area';
      }
      elseif ($sales_by_system_wide) {
        $vars['title'] = 'System Wide Sales Report';
      }
      elseif ($sales_transaction_inner) {
        $store_details = entity_load_single('store', arg(1));
        if (isset($store_details->title)) {
          $vars['title'] = $store_details->title . '<small>Sales by Transaction Reports</small>';
        }
        $add_form_css = TRUE;
      }
      elseif ($sales_by_store_inner) {
        $store_details = entity_load_single('store', arg(1));
        if (isset($store_details->title)) {
          $vars['title'] = $store_details->title . '<small>Sales by Store Reports</small>';
        }
        $add_form_css = TRUE;
      }
      elseif ($sales_by_area_inner) {
        $area_details = taxonomy_term_load(arg(1));
        if (isset($area_details->name)) {
          $vars['title'] = $area_details->name . '<small>Sales by Area Report</small>';
        }
      }
    }
    elseif ($product_sales) {
      if ($product_sales_by_store) {
        $add_form_css = TRUE;
        $vars['title'] = 'Product Sales by Store Reports';
      }
      if ($product_sales_by_store_inner) {
        $store_details = entity_load_single('store', arg(2));
        if (isset($store_details->title)) {
          $store_name = $store_details->title;
          $vars['title'] = $store_name . ': ' . arg(6) . ' Sales Report';
        }
        $add_form_css = TRUE;
      }
      if ($product_sales_bsw || $product_sales_bsw_component) {
        $vars['title'] = 'Product Sales by System Wide Reports';
        $add_form_css = TRUE;
      }
      if ($product_sales_bsw_inner) {
        $store_details = entity_load_single('store', arg(3));
        $category = taxonomy_term_load($store_details->field_category['und'][0]['tid']);
        if (isset($category->name)) {
          $product_1 = ($category->name);
          if (isset($store_details->title)) {
            $product_2 = $store_details->title;
            $vars['title'] = $product_1 . ': ' . $product_2 . ' Sales Report';
          }
        }
        $add_form_css = TRUE;
      }
    }
    elseif ($order_delivery) {
      $add_form_css = TRUE;
      $store_details = entity_load_single('store', arg(2));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Order & Delivery Reports</small>';
      }
    }
    elseif ($cash_management_detail) {
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Cash Sales Report</small>';
      }
    }
  }

  // For Commissary pages.
  $commissary_order = (arg(0) == 'commissary' && arg(1) == 'order');
  $commissary_pullout = (arg(0) == 'commissary' && arg(1) == 'pullout');
  $commissary_order_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'order');
  $commissary_pullout_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'pullout');
  $commissary_order_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'request');
  $commissary_pullout_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'pullout');
  $commissary_admin = ($commissary_order_admin || $commissary_pullout_admin || $commissary_order_admin_inner || $commissary_pullout_admin_inner);
  if ($commissary_order || $commissary_pullout || $commissary_admin) {
    $individual_css = '/assets/css/components/commissary.css';
    $add_form_css = TRUE;
    $store_details = entity_load_single('store', arg(3));
    if (isset($store_details->title)) {
      if (arg(1) == 'order') {
        $vars['title'] = $store_details->title . '<small>Commissary Order Request</small>';
      }
      elseif (arg(1) == 'pullout' || arg(4) == 'pullout') {
        $vars['title'] = $store_details->title . '<small>Commissary Pull-Out Request</small>';
      }
    }
    elseif (arg(3) == 'all') {
      $vars['title'] = 'All Stores<small>Commissary Pull-Out Request</small>';
      if (arg(1) == 'order') {
        $vars['title'] = 'All Stores<small>Commissary Order Request</small>';
      }
    }
  }

  // Store Transfer pages.
  $store_transfer_in = (arg(0) == 'store-transfer' && arg(1) == 'transfer-in-request');
  $store_transfer_out = (arg(0) == 'store-transfer' && arg(1) == 'transfer-out-request');
  $store_transfers = ($store_transfer_in || $store_transfer_out);
  if ($store_transfer_in || $store_transfer_out) {
    $individual_css = '/assets/css/components/store-transfers.css';
    $add_form_css = TRUE;
    $store_details = entity_load_single('store', arg(3));
    if ($store_transfer_in) {
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Transfer In Request</small>';
      }
      elseif (arg(3) == 'all') {
        $vars['title'] = 'All Stores<small>Transfer In Request</small>';
      }
    }
    elseif ($store_transfer_out) {
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Transfer Out Request</small>';
      }
      elseif (arg(3) == 'all') {
        $vars['title'] = 'All Stores<small>Transfer Out Request</small>';
      }
    }
  }

  // For Store Operations Mgmt pages.
  $employee_schedule_outer = (arg(0) == 'manage-schedule');
  $employee_schedule_inner = (arg(0) == 'employee' && is_numeric(arg(1)) && arg(2) == 'schedule');
  $employee_schedule = ($employee_schedule_outer || $employee_schedule_inner);
  $manage_attendance_outer = (arg(0) == 'manage-attendance');
  $manage_attendance_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-attendance');
  $manage_attendance = ($manage_attendance_outer || $manage_attendance_inner);
  $manage_inventory_outer = (arg(0) == 'manage-inventory');
  $manage_inventory_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-inventory');
  $manage_inventory = ($manage_inventory_outer || $manage_inventory_inner);
  $manage_production_outer = (arg(0) == 'manage-production');
  $manage_production_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-production');
  $manage_production = ($manage_production_outer || $manage_production_inner);
  $manage_sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-sales-transaction');
  $manage_sales_transaction_outer = (arg(0) == 'manage-sales-transaction');
  $manage_sales_transaction = ($manage_sales_transaction_inner || $manage_sales_transaction_outer);
  $manage_cash_sales_outer = (arg(0) == 'manage-cash-management');
  $manage_cash_sales_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-cash-management');
  $manage_cash_sales = ($manage_cash_sales_outer || $manage_cash_sales_inner);
  if ($employee_schedule || $manage_attendance || $manage_inventory || $manage_production || $manage_sales_transaction || $manage_cash_sales) {
    $individual_css = '/assets/css/components/store-operations-mgmt.css';

    if ($employee_schedule_outer) {
      $vars['title'] = 'Manage Employee Schedule';
    }
    elseif ($manage_attendance_outer) {
      $vars['title'] = 'Manage Attendance';
    }
    elseif ($manage_attendance_inner) {
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Manage Attendance</small>';
      }
    }
    elseif ($employee_schedule_inner) {
      $add_form_css = TRUE;
    }
    elseif ($manage_production_outer) {
      $vars['title'] = 'Manage Production';
    }
    elseif ($manage_production_inner) {
      $add_form_css = TRUE;
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Manage Production</small>';
      }
    }
    elseif ($manage_sales_transaction_outer) {
      $vars['title'] = 'Manage Sales';
      $add_form_css = TRUE;
    }
    elseif ($manage_sales_transaction_inner) {
      $add_form_css = TRUE;
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $vars['title'] = $store_details->title . '<small>Manage Sales</small>';
      }
    }
    elseif ($manage_cash_sales) {
      $add_form_css = TRUE;
      if ($manage_cash_sales_inner) {
        $store_details = entity_load_single('store', arg(1));
        if (isset($store_details->title)) {
          $vars['title'] = $store_details->title . '<small>Manage Cash Sales</small>';
        }
      }
    }
    elseif ($manage_inventory) {
      $add_form_css = TRUE;
      if ($manage_inventory_inner) {
        $store_details = entity_load_single('store', arg(1));
        if (isset($store_details->title)) {
          $vars['title'] = $store_details->title . '<small>Manage Inventory</small>';
        }
      }
    }
  }

  if ($add_form_css == TRUE) {
    $form_css = drupal_get_path('theme', 'bms_adminlte') . '/assets/css/components/forms-modals.css';
    drupal_add_css($form_css,
      array(
        'group' => CSS_THEME,
        'weight' => '9998',
      )
    );
  }

  if (!$individual_css == '') {
    // Add css for individual pages.
    drupal_add_css($theme_path . $individual_css,
      array(
        'group' => CSS_THEME,
        'weight' => '9998',
      )
    );
  }

  // Separate Form Pages.
  $add_store = (request_uri() == '/store/add');
  $add_crew = (arg(0) == 'admin' && arg(1) == 'people' && arg(2) == 'create');
  $add_inventory = (request_uri() == '/raw-materials/add');
  $edit_inventory = (arg(0) == 'field-collection' && arg(1) == 'field-inventory' && arg(3) == 'edit');
  $inventory_forms = ($add_inventory || $edit_inventory);
  $add_product = (request_uri() == '/product/add');
  $edit_product = (arg(0) == 'admin' && arg(1) == 'structure' && arg(2) == 'entity-type' && arg(3) == 'store' && arg(4) == 'product' && arg(6) == 'edit');
  $product_forms = ($add_product || $edit_product);

  if ($add_store || $add_crew || $inventory_forms || $product_forms) {
    if ($add_crew) {
      $vars['title'] = t('Add Crew');
    }
    if ($add_inventory) {
      $vars['title'] = t('Add Inventory');
    }
    if ($edit_inventory) {
      $inventory = field_collection_item_load(arg(2));
      $entity_id = $inventory->field_item_autofill['und'][0]['target_id'];
      $inventory = entity_load_single('store', $entity_id);
      $vars['title'] = $inventory->title . '<small>Edit Inventory</small>';
    }
    if ($add_product) {
      $vars['title'] = t('Add Product');
    }
    if ($edit_product) {
      $product = entity_load_single('store', arg(5));
      $vars['title'] = $product->title . '<small>Edit Product</small>';
    }
  }

  if (drupal_get_path_alias() == 'inventory') {
    drupal_add_js(drupal_get_path('theme', 'bms_adminlte') . '/assets/js/forms_js/add-inventory-form.js');
  }

  if (drupal_get_path_alias() == 'manage-product') {
    drupal_add_js(drupal_get_path('theme', 'bms_adminlte') . '/assets/js/forms_js/edit-store-product-form.js');
  }

  if (drupal_get_path_alias() == 'administer-employee') {
    drupal_add_js(drupal_get_path('theme', 'bms_adminlte') . '/assets/js/forms_js/user-register-form.js');
  }

  if (drupal_get_path_alias() == 'store-settings') {
    drupal_add_js(drupal_get_path('theme', 'bms_adminlte') . '/assets/js/forms_js/add-store-form.js');
  }
}

/**
 * Implements theme_breadcrumb().
 */
function bms_adminlte_breadcrumb($variables) {
  $variables['breadcrumb'] =  array();

  // Settings Pages.
  $add_inventory = (request_uri() == '/raw-materials/add');
  $add_product = (request_uri() == '/product/add');
  $edit_inventory = (arg(0) == 'field-collection' && arg(1) == 'field-inventory' && arg(3) == 'edit');
  $edit_product = (arg(0) == 'admin' && arg(1) == 'structure' && arg(2) == 'entity-type' && arg(3) == 'store' && arg(4) == 'product' && arg(6) == 'edit');
  if ($add_inventory || $add_product || $edit_inventory || $edit_product) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/"><span>Settings</span></a>';
    if ($add_inventory) {
      $variables['breadcrumb'][] = '<a href="/inventory"><span>Inventory</span></a>';
      $variables['breadcrumb'][] = '<a href="/raw-materials/add"><span>Add Inventory</span></a>';
    }
    elseif ($add_product) {
      $variables['breadcrumb'][] = '<a href="/manage-product"><span>Product</span></a>';
      $variables['breadcrumb'][] = '<a href="/product/add"><span>Add Product</span></a>';
    }
    elseif ($edit_inventory) {
      $inventory = field_collection_item_load(arg(2));
      $entity_id = $inventory->field_item_autofill['und'][0]['target_id'];
      $inventory = entity_load_single('store', $entity_id);
      $variables['breadcrumb'][] = '<a href="/inventory"><span>Inventory</span></a>';
      $variables['breadcrumb'][] = '<a href="/field-collection/field-inventory/' . arg(2) . '/edit"><span>' . $inventory->title . '</span></a>';
    }
    elseif ($edit_product) {
      $product = entity_load_single('store', arg(5));
      $variables['breadcrumb'][] = '<a href="/manage-product"><span>Product</span></a>';
      $variables['breadcrumb'][] = '<a href="/admin/structure/entity-type/store/product/' . arg(5) . '/edit"><span>' . $product->title . '</span></a>';
    }
  }

  // User Pages.
  $add_crew  = (arg(0) == 'admin' && arg(1) == 'people' && arg(2) == 'create');
  $user_profile = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == FALSE);
  $user_profile_edit = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'edit');
  $user_work_schedule = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == 'work-schedule');
  $admin_user_settings = (arg(0) == 'manage-employee');
  if ($user_profile || $user_profile_edit || $user_work_schedule || $admin_user_settings || $add_crew) {

    if ($admin_user_settings) {
      $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
      $variables['breadcrumb'][] = '<a href="/"><span>Settings</span></a>';
      $variables['breadcrumb'][] = '<a href="/manage-employee"><span>Admin User</span></a>';
    }
    if ($add_crew) {
      $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
      $variables['breadcrumb'][] = '<a href="/"><span>Settings</span></a>';
      $variables['breadcrumb'][] = '<a href="/administer-employee"><span>Crew</span></a>';
      $variables['breadcrumb'][] = '<a href="/admin/people/create"><span>Add Crew</span></a>';
    }
    if ($user_work_schedule) {
      $form_css = drupal_get_path('theme', 'bms_adminlte') . '/assets/css/components/forms-modals.css';
      drupal_add_css($form_css,
        array(
          'group' => CSS_THEME,
          'weight' => '9998',
        )
      );
    }

    // Getting selected user full name.
    $employee_user = profile2_load_by_user(arg(1));
    if (isset($employee_user['main'])) {
      $employee_firstname = $employee_user['main']->field_firstname['und'][0]['value'];
      $employee_lastname = $employee_user['main']->field_lastname['und'][0]['value'];
      $title = $employee_firstname . ' ' . $employee_lastname;
    }

    $employee_user_main = user_load(arg(1), $reset = FALSE);
    if (isset($employee_user_main->roles)) {
      $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
      $variables['breadcrumb'][] = '<a href="/"><span>Settings</span></a>';
      if (!in_array('crew', $employee_user_main->roles)) {
        $variables['breadcrumb'][] = '<a href="/manage-employee"><span>Admin User</span></a>';
      }
      else {
        $variables['breadcrumb'][] = '<a href="/administer-employee"><span>Crew</span></a>';
      }
      if (isset($title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/user/' . arg(1) . '"><span>' . $title . '</span></a>';
      }
    }
  }

  // For Stores Pages.
  $store_profile_view = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)));
  $store_profile_edit = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'edit');
  $store_profile = ($store_profile_view || $store_profile_edit);
  $store_profile_inventory = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'inventory');
  $store_profile_product = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'product');
  $add_store = (request_uri() == '/store/add');
  if ($store_profile || $store_profile_inventory || $store_profile_product || $add_store) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/store-settings"><span>Settings</span></a>';
    $variables['breadcrumb'][] = '<a href="/store-settings"><span>Store</span></a>';
    $store_details = entity_load_single('store', arg(2));
    if (isset($store_details->title)) {
      $variables['breadcrumb'][] = '<a class="active" href="/store/store/' . arg(2) . '"><span>' . $store_details->title . '</span></a>';
    }
    if ($add_store) {
      $variables['breadcrumb'][] = '<a href="/store/add"><span>Add Store</span></a>';
    }
  }

  // For Commissary pages.
  $commissary_order = (arg(0) == 'commissary' && arg(1) == 'order');
  $commissary_pullout = (arg(0) == 'commissary' && arg(1) == 'pullout');
  $commissary_order_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'order');
  $commissary_pullout_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'pullout');
  $commissary_order_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'request');
  $commissary_pullout_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'pullout');
  $commissary_admin = ($commissary_order_admin || $commissary_pullout_admin || $commissary_order_admin_inner || $commissary_pullout_admin_inner);
  if ($commissary_order || $commissary_pullout || $commissary_admin) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/"><span>Commissary</span></a>';
    $store_details = entity_load_single('store', arg(3));

    if ($commissary_order || $commissary_order_admin || $commissary_order_admin_inner) {
      $variables['breadcrumb'][] = '<a href="/commissary/order"><span>Order Request</span></a>';

      if (isset($store_details->title)) {

        if ($commissary_order_admin_inner) {
          $variables['breadcrumb'][] = '<a class="active" href="/admin/commissary/store/' . arg(3) . '/request"><span>' . $store_details->title . '</span></a>';
        }
        else {
          $variables['breadcrumb'][] = '<a class="active" href="/commissary/order/store/' . arg(3) . '"><span>' . $store_details->title . '</span></a>';
        }

      }
      elseif (arg(3) === 'all') {
        $variables['breadcrumb'][] = '<a class="active" href="/commissary/order/store/0"><span>All Stores</span></a>';
      }
    }
    elseif ($commissary_pullout || $commissary_pullout_admin || $commissary_pullout_admin_inner) {
      $variables['breadcrumb'][] = '<a href="/commissary/pullout"><span>Pull-out Request</span></a>';
      if (isset($store_details->title)) {

        if ($commissary_pullout_admin_inner) {
          $variables['breadcrumb'][] = '<a class="active" href="/admin/commissary/store/' . arg(3) . '/pullout"><span>' . $store_details->title . '</span></a>';
        }
        else {
          $variables['breadcrumb'][] = '<a class="active" href="/commissary/pullout/store/' . arg(3) . '"><span>' . $store_details->title . '</span></a>';
        }

      }
      elseif (arg(3) === 'all') {
        $variables['breadcrumb'][] = '<a class="active" href="/commissary/pullout/store/0"><span>All Stores</span></a>';
      }
    }
  }

  // For Store Transfers pages.
  $store_transfer_in = (arg(0) == 'store-transfer' && arg(1) == 'transfer-in-request');
  $store_transfer_out = (arg(0) == 'store-transfer' && arg(1) == 'transfer-out-request');
  if ($store_transfer_in || $store_transfer_out) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/"><span>Store Transfers</span></a>';
    $store_details = entity_load_single('store', arg(3));
    if ($store_transfer_in) {
      $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-in-request"><span>Transfer In Request</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-in-request/store/' . arg(3) . '"><span>' . $store_details->title . '</span></a>';
      }
      elseif (arg(3) === 'all') {
        $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-in-request/store/0"><span>All Stores</span></a>';
      }
    }
    elseif ($store_transfer_out) {
      $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-out-request"><span>Transfer Out Request</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-out-request/store/' . arg(3) . '"><span>' . $store_details->title . '</span></a>';
      }
      elseif (arg(3) === 'all') {
        $variables['breadcrumb'][] = '<a href="/store-transfer/transfer-out-request/store/0"><span>All Stores</span></a>';
      }
    }
  }

  // For Store Operations Management Pages.
  $manage_schedule_outer = (arg(0) == 'manage-schedule');
  $manage_attendance_outer = (arg(0) == 'manage-attendance');
  $manage_attendance_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-attendance');
  $attendance_adr = (arg(0) == 'field-collection' && arg(1) == 'field-actual-duty-rendered' && arg(3) == 'edit');
  $attendance_br = (arg(0) == 'field-collection' && arg(1) == 'field-break-freezer' && arg(3) == 'edit');
  $attendances = ($attendance_adr || $attendance_br);
  $manage_attendance = ($manage_attendance_outer || $manage_attendance_inner || $attendances);
  $manage_inventory_outer = (arg(0) == 'manage-inventory');
  $manage_inventory_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-inventory');
  $inventory_bi = (arg(0) == 'field-collection' && arg(1) == 'field-beginning-inventory' && arg(3) == 'edit');
  $inventory_cd = (arg(0) == 'field-collection' && arg(1) == 'field-commissary-delivery' && arg(3) == 'edit');
  $inventory_cp = (arg(0) == 'field-collection' && arg(1) == 'field-commissary-pullout' && arg(3) == 'edit');
  $inventory_sti = (arg(0) == 'field-collection' && arg(1) == 'field-store-transin' && arg(3) == 'edit');
  $inventory_sto = (arg(0) == 'field-collection' && arg(1) == 'field-store-transout' && arg(3) == 'edit');
  $inventory_ppo = (arg(0) == 'field-collection' && arg(1) == 'field-production-processed-open' && arg(3) == 'edit');
  $inventory_w = (arg(0) == 'field-collection' && arg(1) == 'field-wastages' && arg(3) == 'edit');
  $inventory_eia = (arg(0) == 'field-collection' && arg(1) == 'field-ending-inventory-actual' && arg(3) == 'edit');
  $inventory = ($inventory_bi || $inventory_cd || $inventory_cp || $inventory_sti || $inventory_sto || $inventory_ppo || $inventory_w || $inventory_eia);
  $manage_inventory = ($manage_inventory_outer || $manage_inventory_inner || $inventory);
  $manage_production_outer = (arg(0) == 'manage-production');
  $manage_production_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-production');
  $production_po = (arg(0) == 'field-collection' && arg(1) == 'field-processed-opened' && arg(3) == 'edit');
  $production_wa = (arg(0) == 'field-collection' && arg(1) == 'field-wastages' && arg(3) == 'edit');
  $production_eia = (arg(0) == 'field-collection' && arg(1) == 'field-ending-inventory-actual' && arg(3) == 'edit');
  $productions = ($production_po || $production_wa || $production_eia);
  $manage_production = ($manage_production_outer || $manage_production_inner || $productions);
  $manage_sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-sales-transaction');
  $manage_sales_transaction_outer = (arg(0) == 'manage-sales-transaction');
  $manage_sales_transaction = ($manage_sales_transaction_inner || $manage_sales_transaction_outer);
  $manage_cash_sales_outer = (arg(0) == 'manage-cash-management');
  $manage_cash_sales_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-cash-management');
  $cash_sales_atv = (arg(0) == 'manage-cash-sales' && arg(1) == 'store' && arg(3) == 'added-to-vault');
  $cash_sales_pull = (arg(0) == 'field-collection' && arg(1) == 'field-cash-management-pullout' && arg(3) == 'edit');
  $cash_sales_dep = (arg(0) == 'field-collection' && arg(1) == 'field-cash-management-deposit' && arg(3) == 'edit');
  $cash_sales = ($cash_sales_atv || $cash_sales_pull || $cash_sales_dep);
  $manage_cash_sales = ($manage_cash_sales_outer || $manage_cash_sales_inner || $cash_sales);
  $edit_schedule = (arg(0) == 'add-schedule' && arg(2) == 'edit');
  $edit_undertime = (arg(0) == 'add-undertime' && arg(2) == 'edit');
  $edit_overtime = (arg(0) == 'add-overtime' && arg(2) == 'edit');
  $schedules = ($edit_schedule || $edit_undertime || $edit_overtime);
  if ($schedules || $manage_schedule_outer || $manage_attendance || $manage_production || $manage_sales_transaction || $manage_cash_sales || $manage_inventory) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/"><span>Store Operations Mgmt</span></a>';

    if ($manage_schedule_outer || $schedules) {
      $variables['breadcrumb'][] = '<a href="/manage-schedule"><span>Manage Employee Schedule</span></a>';
      if ($schedules) {
        if ($edit_schedule) {
          $variables['breadcrumb'][] = '<a href="/add-schedule/' . arg(1) . '/edit"><span>Edit Schedule</span></a>';
        }
        elseif ($edit_undertime) {
          $variables['breadcrumb'][] = '<a href="/add-undertime/' . arg(1) . '/edit"><span>Edit Undertime</span></a>';
        }
        elseif ($edit_overtime) {
          $variables['breadcrumb'][] = '<a href="/add-overtime/' . arg(1) . '/edit"><span>Edit Overtime</span></a>';
        }
      }
    }
    elseif ($manage_attendance) {
      $variables['breadcrumb'][] = '<a href="/manage-attendance"><span>Attendance</span></a>';
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/manage-attendance"><span>' . $store_details->title . '</span></a>';
      }
      if ($attendances) {
        $field = field_collection_item_load(arg(2));
        $field = entity_uuid_load('field_collection_item', array($field->uuid));
      }
    }
    elseif ($manage_production) {
      $variables['breadcrumb'][] = '<a href="/manage-Production"><span>Manage Production</span></a>';
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/manage-production"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($manage_inventory) {
      $variables['breadcrumb'][] = '<a href="/manage-inventory"><span>Manage Inventory</span></a>';
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/manage-inventory"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($manage_sales_transaction) {
      $variables['breadcrumb'][] = '<a href="/manage-sales-transaction"><span>Manage Sales</span></a>';
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/manage-sales-transaction"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($manage_cash_sales) {
      $variables['breadcrumb'][] = '<a href="/manage-cash-management"><span>Manage Cash Sales</span></a>';
      $store_details = entity_load_single('store', arg(1));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/manage-cash-management"><span>' . $store_details->title . '</span></a>';
      }
    }
  }

  // For Reports Pages.
  $attendance_report_outer = (arg(0) == 'attendance');
  $attendance_report_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'attendance');
  $attendance = ($attendance_report_outer || $attendance_report_inner);
  $inventory_summary_outer = (arg(0) == 'inventory-summary');
  $inventory_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'inventory-report');
  $inventory_summary = ($inventory_summary_outer || $inventory_summary_detail);
  $production_summary_outer = (arg(0) == 'production-summary');
  $production_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'production-report');
  $production_summary = ($production_summary_outer || $production_summary_detail);
  $cash_management_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'cash-management');
  $cash_management = (arg(0) == 'cash-management' || $cash_management_detail);
  $order_delivery_outer = (arg(0) == 'commissary' && empty(arg(1)));
  $order_delivery_inner = (arg(0) == 'commissary' && arg(1) == 'store' && is_numeric(arg(2)));
  $order_delivery = ($order_delivery_inner || $order_delivery_outer);
  $product_sales_by_store = (arg(0) == 'product-sales');
  $product_sales_by_store_component = (arg(0) == 'product-sales' && arg(1) == 'store' && is_numeric(arg(2)));
  $product_sales_bsw_component = (arg(0) == 'product' && arg(1) == 'component-item' && is_numeric(arg(2)));
  $product_sales_bsw_component_inner = (arg(0) == 'product-sales' && arg(1) == 'system-wide' && arg(2) == 'component-item' && is_numeric(arg(3)));
  $product_sales = ($product_sales_by_store || $product_sales_by_store_component || $product_sales_bsw_component || $product_sales_bsw_component_inner);
  $sales_transaction_outer = (arg(0) == 'sales-transaction');
  $sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'sales-transaction');
  $sales_transaction = ($sales_transaction_outer || $sales_transaction_inner);
  $sales_by_store_inner = (arg(0) == 'sales-by-store' && is_numeric(arg(1)));
  $sales_by_store_outer = (arg(0) == 'sales-by-store' && empty(arg(1)));
  $sales_by_store = ($sales_by_store_inner || $sales_by_store_outer);
  $sales_by_area_inner = (arg(0) == 'sales-by-area' && is_numeric(arg(1)));
  $sales_by_area_outer = (arg(0) == 'sales-by-area' && empty(arg(1)));
  $sales_by_area = ($sales_by_area_inner || $sales_by_area_outer);
  $sales_by_system_wide = (arg(0) == 'sales-by-system-wide');
  $reports_sales = ($sales_transaction || $sales_by_store || $sales_by_area || $sales_by_system_wide);
  $store_transfers_outer = (arg(0) == 'store-transfers');
  $store_transfers_inner = (arg(0) == 'store-transfers' && arg(1) == 'store' && is_numeric(arg(2)));
  $store_transfers = ($store_transfers_outer || $store_transfers_inner);
  if ($attendance || $inventory_summary || $production_summary || $cash_management || $store_transfers || $reports_sales || $product_sales || $order_delivery) {
    $variables['breadcrumb'][] = '<a href="/"><span>Home</span></a>';
    $variables['breadcrumb'][] = '<a href="/"><span>Reports</span></a>';
    $store_details = entity_load_single('store', arg(1));
    if ($attendance) {
      $variables['breadcrumb'][] = '<a href="/attendance"><span>Attendance</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/attendance"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($inventory_summary) {
      $variables['breadcrumb'][] = '<a href="/inventory-summary"><span>Inventory</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/inventory-report"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($production_summary) {
      $variables['breadcrumb'][] = '<a href="/production-summary"><span>Production</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/production-report"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($cash_management) {
      $variables['breadcrumb'][] = '<a href="/cash-management"><span>Cash Sales</span></a>';
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/cash-management"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($store_transfers) {
      $variables['breadcrumb'][] = '<a href="/store-transfers"><span>Store Transfers</span></a>';
      $store_details = entity_load_single('store', arg(2));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/store-transfers/store/' . arg(2) . '"><span>' . $store_details->title . '</span></a>';
      }
    }
    elseif ($product_sales) {
      $variables['breadcrumb'][] = '<a href="/product-sales"><span>Product Sales</span></a>';
      if ($product_sales_by_store) {
        if (arg(1) == 'by-system-wide') {
          $variables['breadcrumb'][] = '<a href="/product-sales/by-system-wide"><span>by System Wide</span></a>';
        }
        elseif (empty(arg(1))) {
          $variables['breadcrumb'][] = '<a href="/product-sales"><span>by Store</span></a>';
        }
        if ($product_sales_by_store_component) {
          $variables['breadcrumb'][] = '<a href="/product-sales"><span>by Store</span></a>';
          $store_details = entity_load_single('store', arg(2));
          if (isset($store_details->title)) {
            $variables['breadcrumb'][] = '<a class="active" href="/product-sales/store/' . arg(2) . '"><span>' . $store_details->title . '</span></a>';
            $last = arg(6);
            if ($last) {
              $variables['breadcrumb'][] = '<a class="active" href="/product-sales/store/' . arg(2) . '/product-items/' . arg(4) . '/by-store/' . $last . '"><span>' . $last . '</span></a>';
            }
          }
        }
        elseif ($product_sales_bsw_component_inner) {
          $store_details = entity_load_single('store', arg(3));
          $category = taxonomy_term_load($store_details->field_category['und'][0]['tid']);
          if (isset($store_details->title)) {
            $variables['breadcrumb'][] = '<a class="active" href="/product/component-item/' . arg(2) . '"><span>' . $category->name . '</span></a>';
            $store_details = entity_load_single('store', arg(3));
            if (isset($store_details->title)) {
              $variables['breadcrumb'][] = '<a class="active" href="/product-sales/system-wide/component-item/' . arg(3) . '"><span>' . $store_details->title . '</span></a>';
            }
          }
        }
      }
      elseif ($product_sales_bsw_component) {
        $variables['breadcrumb'][] = '<a href="/product-sales/by-system-wide"><span>by System Wide</span></a>';
        $store_details = entity_load_single('store', arg(2));
        if (isset($store_details->title)) {
          $variables['breadcrumb'][] = '<a class="active" href="/product/component-item/' . arg(2) . '"><span>' . $store_details->title . '</span></a>';
        }
      }
    }
    elseif ($reports_sales) {
      $variables['breadcrumb'][] = '<a href="/sales-transaction"><span>Sales</span></a>';
      if ($sales_transaction) {
        $variables['breadcrumb'][] = '<a href="/sales-transaction"><span>by Transaction</span></a>';
        $store_details = entity_load_single('store', arg(1));
        if (isset($store_details->title)) {
          $variables['breadcrumb'][] = '<a class="active" href="/store/' . arg(1) . '/sales-transaction"><span>' . $store_details->title . '</span></a>';
        }
      }
      elseif ($sales_by_store) {
        $variables['breadcrumb'][] = '<a href="/sales-by-store"><span>by Store</span></a>';
        if ($sales_by_store_inner) {
          $store_details = entity_load_single('store', arg(1));
          if (isset($store_details->title)) {
            $variables['breadcrumb'][] = '<a class="active" href="/sales-by-store/' . arg(1) . '"><span>' . $store_details->title . '</span></a>';
          }
        }
      }
      elseif ($sales_by_area) {
        $variables['breadcrumb'][] = '<a href="/sales-by-area"><span>by Area</span></a>';
        if ($sales_by_area_inner) {
          $area_details = taxonomy_term_load(arg(1));
          if (isset($area_details->name)) {
            $variables['breadcrumb'][] = '<a class="active" href="/sales-by-area/' . arg(1) . '"><span>' . $area_details->name . '</span></a>';
          }
        }
      }
      elseif ($sales_by_system_wide) {
        $variables['breadcrumb'][] = '<a href="/sales-by-system-wide"><span>by System Wide</span></a>';
      }
    }
    elseif ($order_delivery) {
      $variables['breadcrumb'][] = '<a href="/commissary"><span>Order & Delivery</span></a>';
      $store_details = entity_load_single('store', arg(2));
      if (isset($store_details->title)) {
        $variables['breadcrumb'][] = '<a class="active" href="/commissary/store/' . arg(2) . '"><span>' . $store_details->title . '</span></a>';
      }
    }
  }

  $breadcrumb = $variables['breadcrumb'];

  if (!empty($breadcrumb)) {
    $output = '<h2 class="element-invisible">' . t('You are here') . '</h2>';
    $output .= '<div class="breadcrumb">' . implode(' > ', $breadcrumb) . '</div>';
    return $output;
  }
}

/**
 * Implements hook_form_alter().
 */
function bms_adminlte_form_alter(&$form, &$form_state, $form_id) {
  $form_css = drupal_get_path('theme', 'bms_adminlte') . '/assets/css/components/forms-modals.css';
  if (!$form_id == 'user_profile_form') {
    $form['#attached']['css'] = array(
      $form_css => array(
        'group' => CSS_THEME,
        'weight' => '9999',
      ),
    );
  }
  switch ($form_id) {
    // Login Form
    case 'user_login_block':
      $form['name']['#title_display'] = 'invisible';
      $form['pass']['#title_display'] = 'invisible';
      $form['name']['#attributes']['placeholder'] = t('Username');
      $form['pass']['#attributes']['placeholder'] = t('Password');
      break;

    // For Views Exposed Filters.
    case 'views_exposed_form':
      // Change filter labels.
      $settings_admin_user = ($form['#id'] == 'views-exposed-form-manage-employee-manage-employee');
      $settings_store = ($form['#id'] == 'views-exposed-form-franchisee-store-settings');
      $settings_crew = ($form['#id'] == 'views-exposed-form-manage-employee-manage-employee-franchisee');
      $settings_inventory = ($form['#id'] == 'views-exposed-form-inventory-page');
      $settings_product = ($form['#id'] == 'views-exposed-form-manage-product-page');
      $com_request = ($form['#id'] == 'views-exposed-form-commissary-request-page');
      $com_request_inner_store = ($form['#id'] == 'views-exposed-form-commissary-request-commissary-order-request');
      $com_request_inner_com = ($form['#id'] == 'views-exposed-form-commissary-request-commissary-order-status');
      $com_pullout = ($form['#id'] == 'views-exposed-form-commissary-pull-out-page');
      $com_pullout_inner_store = ($form['#id'] == 'views-exposed-form-commissary-pull-out-store-pullout-status');
      $com_pullout_inner_com = ($form['#id'] == 'views-exposed-form-commissary-pull-out-commissary-pullout-status');
      $som_manage_employee_schedule = ($form['#id'] == 'views-exposed-form-manage-employee-page-1');
      $som_manage_attendance = ($form['#id'] == 'views-exposed-form-attendance-page-2');
      $som_manage_production = ($form['#id'] == 'views-exposed-form-production-summary-page-2');
      $som_manage_production_inner = ($form['#id'] == 'views-exposed-form-production-summary-manage-production');
      $som_manage_inventory = ($form['#id'] == 'views-exposed-form-inventory-summary-page-2');
      $som_manage_inventory_inner = ($form['#id'] == 'views-exposed-form-inventory-summary-manage-inventory');
      $som_manage_sales_transaction = ($form['#id'] == 'views-exposed-form-sales-transaction-page-2');
      $som_manage_cash_sales = ($form['#id'] == 'views-exposed-form-cash-management-page-2');
      $som_manage_cash_sales_inner = ($form['#id'] == 'views-exposed-form-cash-management-manage-cash-management');
      $store_transfers_in = ($form['#id'] == 'views-exposed-form-store-transfer-trans-in-store-transfer-in-store');
      $store_transfers_out = ($form['#id'] == 'views-exposed-form-store-transfer-trans-out-store-transfer-out-store');
      $store_transfers_in_inner = ($form['#id'] == 'views-exposed-form-store-transfer-trans-in-store-transfer-in');
      $store_transfers_out_inner = ($form['#id'] == 'views-exposed-form-store-transfer-trans-out-store-transfer-out');
      $reports_attendance = ($form['#id'] == 'views-exposed-form-attendance-page-1');
      $reports_inventory = ($form['#id'] == 'views-exposed-form-inventory-summary-page-1');
      $reports_inventory_inner = ($form['#id'] == 'views-exposed-form-inventory-summary-inventory-summary');
      $reports_order_delivery = ($form['#id'] ==  'views-exposed-form-commissary-page');
      $reports_production = ($form['#id'] == 'views-exposed-form-production-summary-page-1');
      $reports_production_inner = ($form['#id'] == 'views-exposed-form-production-summary-production-summary');
      $reports_cash_sales = ($form['#id'] == 'views-exposed-form-cash-management-page-1');
      $reports_cash_sales_inner = ($form['#id'] == 'views-exposed-form-cash-management-cash-management');
      $reports_store_transfer_in = ($form['#id'] == 'views-exposed-form-store-transfers-store-transfer-in');
      $reports_store_transfer_out = ($form['#id'] == 'views-exposed-form-store-transfers-store-transfer-out');
      $reports_store_transfers = ($form['#id'] == 'views-exposed-form-store-transfers-page');
      $reports_product_sales_store = ($form['#id'] == 'views-exposed-form-product-sales-product-sales-store');
      $reports_product_sales_bsw = ($form['#id'] == 'views-exposed-form-by-system-wide-product-sales-by-system-wide');
      $reports_sales_transaction = ($form['#id'] == 'views-exposed-form-sales-transaction-store-sales-transaction');
      $reports_sales_transaction_inner = ($form['#id'] == 'views-exposed-form-sales-transaction-sales-transaction');
      $reports_sales_by_store = ($form['#id'] == 'views-exposed-form-sales-by-store-page');
      $reports_sales_by_store_inner = ($form['#id'] == 'views-exposed-form-sales-by-store-day-sales');
      $reports_sales_by_area = ($form['#id'] == 'views-exposed-form-sales-by-area-sales-by-area');
      $reports_sales_by_area_inner = ($form['#id'] == 'views-exposed-form-sales-by-area-day-sales');
      $reports_sales_bsw_day = ($form['#id'] == 'views-exposed-form-sales-by-system-wide-day-sales');
      $reports_sales_bsw_week = ($form['#id'] == 'views-exposed-form-sales-by-system-wide-page-1');
      $com_com_order = ($form['#id'] == 'views-exposed-form-admin-commissary-request-page');
      $com_com_pullout = ($form['#id'] == 'views-exposed-form-admin-commissary-pull-out-page');
      $com_com_order_inner = ($form['#id'] == 'views-exposed-form-admin-commissary-request-commissary-order-status');
      $com_com_pullout_inner = ($form['#id'] == 'views-exposed-form-admin-commissary-pull-out-commissary-pullout-status');

      // Settings: Admin User.
      if ($settings_admin_user) {
        $form['name']['#attributes']['placeholder'] = t('Enter User ..');
        $form['rid_1']['#options']['All'] = t('Filter by Admin User Type');
      }
      // Settings: Store.
      elseif ($settings_store) {
        $form['store_title']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_status_value']['#options']['All'] = t('Filter by Status');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Settings: Crew.
      elseif ($settings_crew ) {
        $form['name']['#attributes']['placeholder'] = t('Enter Crew Name ..');
        $form['status_1']['#options']['All'] = t('Filter by Crew Status');
        foreach ($form['title']['#options'] as $key => $value) {
          if ($value == '- Select -') {
            $form['title']['#options'][$key] = t('Filter by Store');
          }
        }
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Settings: Inventory.
      elseif ($settings_inventory) {
        $form['field_store_name_value']['#attributes']['placeholder'] = t('Enter Item ..');
        $form['field_category_tid']['#options']['All'] = t('Filter by Category');
        $form['field_item_inventory_status_value']['#options']['All'] = t('Filter by Status');
      }
      // Settings: Product.
      elseif ($settings_product) {
        $form['title']['#attributes']['placeholder'] = t('Enter Product ..');
        $form['field_category_tid']['#options']['All'] = t('Filter by Category');
        $form['field_sub_category_tid']['#options']['All'] = t('Filter by Sub-category');
      }
      // Store Operations Mgmt: Manage Employee Schedule.
      elseif ($som_manage_employee_schedule) {
        $form['combine']['#attributes']['placeholder'] = t('Enter Crew ..');
        $form['status_1']['#options']['All'] = t('Filter by Crew Status');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
        foreach ($form['title']['#options'] as $key => $value) {
          if ($value == '- Select -') {
            $form['title']['#options'][$key] = t('Filter by Store');
          }
        }
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
      }
      // Store Operations Mgmt: Manage Attendance.
      // Store Operations Mgmt: Manage Production.
      // Store Operations Mgmt: Manage Cash Sales.
      // Store Operations Mgmt: Manage Inventory.
      // Reports: Attendance.
      // Reports: Inventory.
      // Reports: Production.
      // Reports: Cash Sales.
      elseif ($som_manage_attendance || $som_manage_production || $som_manage_cash_sales || $som_manage_inventory || $reports_attendance || $reports_inventory || $reports_production || $reports_cash_sales) {
        $form['store_name']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Store Operations Mgmt: Manage Production - Inner page.
      elseif ($som_manage_production_inner) {
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Date: Start');
        $form['field_created_value_1']['value']['#attributes']['placeholder'] = t('Date: End');
        $form['field_store_name_value']['#attributes']['placeholder'] = t('Enter Store ..');
      }
      // Reports: Inventory - Inner page.
      // Reports: Production - Inner Page.
      elseif ($reports_inventory_inner || $reports_production_inner) {
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Date: Start');
        $form['field_created_value_1']['value']['#attributes']['placeholder'] = t('Date: End');
      }
      // Reports: Cash Sales - Inner page.
      // Store Operations Mgmt: Manage Cash Sales - Inner page.
      elseif ($reports_cash_sales_inner || $som_manage_cash_sales_inner) {
        $form['date_filter']['value']['#attributes']['placeholder'] = t('Date: Start');
        $form['date_filter_1']['value']['#attributes']['placeholder'] = t('Date: End');
      }
      // Reports: Store Transfer in/out.
      elseif ($reports_store_transfer_in || $reports_store_transfer_out) {
        $form['field_request_no_value']['#attributes']['placeholder'] = t('Enter Keyword ..');
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Enter Date ..');
      }
      // Reports: Sales > by Transaction (Inner page).
      elseif ($reports_sales_transaction_inner) {
        $form['field_transaction_number_value']['#attributes']['placeholder'] = t('Enter Keyword ..');
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Enter Date ..');
      }
      // Reports: Store Transfers.
      // Reports: Product Sales (By Store).
      // Reports: Sales (by Store).
      elseif ($reports_store_transfers || $reports_product_sales_store || $reports_sales_by_store) {
        $form['title']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Reports: Sales (by Transaction).
      // Store Operations Mgmt: Manage Sales.
      elseif ($reports_sales_transaction || $reports_sales_by_store || $som_manage_sales_transaction) {
        $form['title']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Reports: Sales (by Area).
      elseif ($reports_sales_by_area) {
        $form['name']['#attributes']['placeholder'] = t('Enter Area No ..');
      }
      // Reports: Sales > by System Wide (Day Sales).
      // Reports: Sales > by System Wide (Week to Date Sales).
      elseif ($reports_sales_bsw_day || $reports_sales_bsw_week) {
        $form['title']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
      }
      // Reports: Sales > by Store (Inner page).
      elseif ($reports_sales_by_store_inner) {
        $form['title']['#attributes']['placeholder'] = t('Enter Keyword ..');
      }
      // Reports: Sales > by Area (Inner page).
      elseif ($reports_sales_by_area_inner) {
        $form['field_store_name_value']['#attributes']['placeholder'] = t('Enter Keyword ..');
      }
      // Commissary: Order Request.
      // Commissary: Pullout Request.
      // Store Transfers: Transfer In Request.
      // Store Transfers: Transfer Out Request.
      elseif ($com_request || $com_pullout || $store_transfers_in || $store_transfers_out) {
        $form['filter_store_name']['#attributes']['placeholder'] = t('Enter Store ..');
      }
      // Commissary: Order Request (Inner page).
      // Commissary: Pullout Request (Inner page).
      // Store Transfers: Transfer In Request (Inner page).
      // Store Transfers: Transfer Out Request (Inner page).
      elseif ($com_request_inner_com || $com_request_inner_store || $com_pullout_inner_com || $com_pullout_inner_store || $store_transfers_in_inner || $store_transfers_out_inner) {
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Select Date');
        $form['field_created_value_1']['value']['#attributes']['placeholder'] = t('Select Date');
      }
      // Store Operations Mgmt: Manage Inventory (Inner page).
      elseif ($som_manage_inventory_inner) {
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Date: Start');
        $form['field_created_value_1']['value']['#attributes']['placeholder'] = t('Date: End');
      }
      // Reports: Order & Delivery.
      elseif ($reports_order_delivery) {
        $form['filter_store_name']['#attributes']['placeholder'] = t('Enter Store ..');
        $form['field_store_ownership_value']['#options']['All'] = t('Type of Store Ownership');
        $form['field_area_tid']['#options']['All'] = t('Filter by Area');
      }
      // Commissary: Order Request (Commissary Role).
      // Commissary: Pullout Request (Commissary Role).
      if ($com_com_order || $com_com_pullout) {
        $form['filter_store_name']['#attributes']['placeholder'] = t('Enter Store ..');
      }
      // Commissary: Order Request Inner page (Commissary Role).
      // Commissary: Pullout Request Inner page (Commissary Role).
      elseif ($com_com_order_inner || $com_com_pullout_inner) {
        $form['field_created_value']['value']['#attributes']['placeholder'] = t('Enter Date ..');
      }
      // Reports: Product Sales by System Wide.
      elseif ($reports_product_sales_bsw) {
        $form['title']['#attributes']['placeholder'] = t('Enter Keyword ..');
      }
      break;

    // Add Store Form.
    case 'eck__entity__form_add_store_store':
      $form['title']['#title'] = t('Store Name');
      $form['field_location']['und'][0]['value']['#title'] = t('Address');
      $form['field_company']['und'][0]['value']['#title'] = t('Name of Company');
      $form['field_contact_number']['und'][0]['value']['#title'] = t('Contact No.');
      $form['field_store_franchisee']['und'][0]['value']['#title'] = t('Name of Franchisee');
      $form['field_area']['und']['#title'] = t('Area No.');
      $form['field_store_ownership']['und']['#title'] = t('Type of Ownership');
      $form['field_store_status']['und']['#title'] = t('Status');
      $form['field_store_name']['#attributes']['class'][] = 'field-hide';
      $form['actions']['submit']['#value'] = t('Add Store');
      break;

    // Edit Store Form.
    case 'eck__entity__form_edit_store_store':
      $form['field_store_name']['und'][0]['value']['#title'] = t('Store Name');
      $form['field_location']['und'][0]['value']['#title'] = t('Address');
      $form['field_company']['und'][0]['value']['#title'] = t('Name of Company');
      $form['field_contact_number']['und'][0]['value']['#title'] = t('Contact No.');
      $form['field_store_franchisee']['und'][0]['value']['#title'] = t('Name of Franchisee');
      $form['field_area']['und']['#title'] = t('Area No.');
      $form['field_store_ownership']['und']['#title'] = t('Type of Ownership');
      $form['field_store_status']['und']['#title'] = t('Status');
      $form['actions']['submit']['#value'] = 'Submit';
      break;

    // Settings: Store (Product Settings Form).
    case 'entity_store_product_tab':
      $form['product_name_1']['#title'] = '';
      $form['label_1'] = array(
        '#type' => 'item',
        '#markup' => '<h3>Select the items you want to activate</h3>',
        '#weight' => -2,
      );

      $form['label_2'] = array(
        '#type' => 'item',
        '#markup' => '<h2>Product Category</h2><h2>Product Items</h2>',
        '#weight' => -1,
      );
      $form['actions']['submit']['#value'] = t('Submit');
      break;

    // Settings: Store (Inventory Settings Form).
    case 'entity_store_inventory_tab':
      $form['inventory_product_name_1']['#title'] = '';
      $form['label_1'] = array(
        '#type' => 'item',
        '#markup' => '<h3>Select the items you want to activate</h3>',
        '#weight' => -2,
      );

      $form['label_2'] = array(
        '#type' => 'item',
        '#markup' => '<h2>Inventory Category</h2><h2>Inventory Items</h2>',
        '#weight' => -1,
      );
      $form['actions']['submit']['#value'] = t('Submit');
      break;

    // Settings: Inventory (Add Inventory Form).
    case 'eck__entity__form_add_store_product':
      $form['#attributes']['class'][] = 'add-inventory-form';
      $form['title']['#title'] = t('Inventory Item Name');
      $form['field_item_code']['und'][0]['value']['#title'] = t('Inventory Item Code');
      $form['field_category']['und']['#title'] = t('Inventory Category');
      $form['field_price']['und'][0]['value']['#title'] = t('Price per Pack');
      $form['field_price_per_unit']['und'][0]['value']['#title'] = t('Price per Unit');
      $form['field_price']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
      $form['field_price_per_unit']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
      $form['field_accepted_reason']['und']['#title'] = t('Acceptable Wastage Reason/s');
      $form['field_accepted_reason_commissary']['und']['#title'] = t('Acceptable Commissary Delivery Adjustment Reason');
      $form['actions']['submit']['#value'] = t('Add Item');
      $form['uom_label'] = array(
        '#type' => 'item',
        '#markup'=> '<div class="uom_equivalent"><label>Unit of Measurement(Equivalent)</label></div>',
      );

      break;

    // Settings: Add Product Form.
    case 'eck__entity__form_add_store_cashier_items':
      $form['#attributes']['class'][] = 'add-product-form';
      $form['title']['#title'] = t('Product Name');
      $form['field_accepted_reason']['und']['#title'] = t('Acceptable Wastage Reason/s');
      $form['actions']['submit']['#value'] = t('Add Product');
      $form['field_selling_price_w_o_vat']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
      $form['field_price']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
      $form['field_price']['und'][0]['value']['#title'] = t('Selling Price with Vat');
      $form['field_vat']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
      $form['field_selling_price_w_o_vat']['und'][0]['value']['#attributes']['placeholder'] = '0.00';
      $form['field_price']['und'][0]['value']['#attributes']['placeholder'] = '0.00';
      $form['field_vat']['und'][0]['value']['#attributes']['placeholder'] = '0.00';
      $form['actions']['submit']['#value'] = t('Add Product');

      foreach ($form['field_components']['und'] as $key => $value) {
        if (is_numeric($key)) {
          $form['field_components']['und'][$key]['field_item_autofill']['und'][0]['target_id']['#title'] = '';
          $form['field_components']['und'][$key]['field_item_autofill']['und'][0]['target_id']['#weight'] = '';
          $form['field_components']['und'][$key]['field_measurement']['und'][0]['value']['#title'] = '';
          $form['field_components']['und'][$key]['field_production_uom']['und']['#title'] = '';
          $form['field_components']['und'][$key]['field_category']['und']['#title'] = '';

          if ($key == 0) {
            $form['field_components']['und'][$key]['field_item_autofill']['und'][0]['target_id']['#title'] = t('Inventory Item');
            $form['field_components']['und'][$key]['field_item_autofill']['und'][0]['target_id']['#weight'] = 1;
            $form['field_components']['und'][$key]['field_measurement']['und'][0]['value']['#title'] = t('Qty');
            $form['field_components']['und'][$key]['field_production_uom']['und']['#title'] = t('Uom');
            $form['field_components']['und'][$key]['field_category']['und']['#title'] = t('Inventory Category');
          }
          hide($form['field_components']['und'][$key]['field_qty']['und']);
        }
      }
      $form['field_components']['und']['add_more']['#value'] = t('Add More Components');
      $form['field_components']['und']['add_more']['#prefix'] = '<div class="add-more"><span class="plus">+</span>';
      $form['field_components']['und']['add_more']['#suffix'] = '</div>';

      break;

    case 'field_collection_item_form':
      $som_mi_commissary_delivery = ($form['#bundle'] == 'field_commissary_delivery');
      $som_mi_commissary_pullout = ($form['#bundle'] == 'field_commissary_pullout');
      $som_mi_store_transin = ($form['#bundle'] == 'field_store_transin');
      $som_mi_store_transout = ($form['#bundle'] == 'field_store_transout');

      // Settings: Inventory (Edit Inventory Form).
      if ($form['#bundle'] == 'field_inventory') {
        $form['#attributes']['class'][] = 'edit-inventory-form';
        $form['field_item_autofill']['und'][0]['target_id']['#title'] = t('Inventory Item Name');
        $form['field_item_code']['und'][0]['value']['#title'] = t('Inventory Item Code');
        $form['field_category']['und']['#title'] = t('Inventory Category');
        $form['field_price']['und'][0]['value']['#title'] = t('Price per Pack');
        $form['field_price_per_unit']['und'][0]['value']['#title'] = t('Price per Unit');
        $form['field_price']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
        $form['field_price_per_unit']['und'][0]['value']['#prefix'] = '<span class="peso">Php</span>';
        $form['field_accepted_reason']['und']['#title'] = t('Acceptable Wastage Reason/s');
        $form['field_accepted_reason_commissary']['und']['#title'] = t('Acceptable Commissary Delivery Adjustment Reason');
        $form['uom_label'] = array(
          '#type' => 'item',
          '#markup'=> '<div class="uom_equivalent"><label>Unit of Measurement(Equivalent)</label></div>',
        );
        $form['field_item_autofill']['#weight'] = -20;
        $form['field_item_code']['#weight'] = -19;
        $form['field_category']['#weight'] = -18;
        $form['field_item_inventory_status']['#weight'] = -17;
        $form['uom_label']['#weight'] = -16;
        $form['field_initial_quantity']['#weight'] = -15;
        $form['field_inventory_uom']['#weight'] = -14;
        $form['field_production_quantity']['#weight'] = -13;
        $form['field_production_uom']['#weight'] = -12;
        $form['field_price']['#weight'] = -11;
        $form['field_price_per_unit']['#weight'] = -10;
        $form['field_accepted_reason']['#weight'] = -9;
        $form['field_accepted_reason_commissary']['#weight'] = -8;
        $form['field_sub_category']['#attributes']['class'][] = 'field-hide';
        $form['field_active']['#attributes']['class'][] = 'field-hide';
        $form['field_store_franchised']['#attributes']['class'][] = 'field-hide';
        $form['field_item']['#attributes']['class'][] = 'field-hide';
      }
      // Actual Duty Rendered Edit Form.
      elseif ($form['#bundle'] == 'field_actual_duty_rendered') {
        $form['field_timein']['#attributes']['class'][] = 'field-hide';
        $form['field_timeout']['#attributes']['class'][] = 'field-hide';
        $form['field_no_of_hours_rendered']['#attributes']['class'][] = 'field-hide';
        $form['field_ot']['#attributes']['class'][] = 'field-hide';
        $form['field_total_hours_rendered']['#attributes']['class'][] = 'field-hide';
        $form['field_ut']['#attributes']['class'][] = 'field-hide';
        $form['field_variance']['#attributes']['class'][] = 'field-hide';
        $form['field_late']['#attributes']['class'][] = 'field-hide';
        $form['field_absent']['#attributes']['class'][] = 'field-hide';
      }
      // Break/Freezer Edit Form.
      elseif ($form['#bundle'] == 'field_break_freezer') {
        $form['field_freezer_temp']['#attributes']['class'][] = 'field-hide';
        $form['field_total_break']['#attributes']['class'][] = 'field-hide';
        $form['field_total_no_of_breaks_minutes']['#attributes']['class'][] = 'field-hide';
        $form['field_break_in']['#attributes']['class'][] = 'field-hide';
        $form['field_break_out']['#attributes']['class'][] = 'field-hide';
      }
      // Processed/Open Edit Form.
      elseif ($form['#bundle'] == 'field_processed_opened') {
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_crew']['#attributes']['class'][] = 'field-hide';
        $form['field_item']['#attributes']['class'][] = 'field-hide';
        $form['field_item_code']['#attributes']['class'][] = 'field-hide';
        $form['field_total']['#attributes']['class'][] = 'field-hide';
        $form['field_unit_price']['#attributes']['class'][] = 'field-hide';
        $form['field_uom']['#attributes']['class'][] = 'field-hide';
        $form['field_item_id']['#attributes']['class'][] = 'field-hide';
        $form['field_category']['#attributes']['class'][] = 'field-hide';
        $form['actions']['cancel'] = array(
          '#type' => 'submit',
          '#value' => t('Cancel'),
          '#attributes' => array(
            'class' => array(
              'left-save',
            ),
          ),
        );
      }
      // Wastages Edit Form.
      elseif ($form['#bundle'] == 'field_wastages') {
        $form['field_qty']['und'][0]['value']['#title'] = t('Converted Quantity');
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_crew']['#attributes']['class'][] = 'field-hide';
        $form['field_item']['#attributes']['class'][] = 'field-hide';
        $form['field_item_code']['#attributes']['class'][] = 'field-hide';
        $form['field_total']['#attributes']['class'][] = 'field-hide';
        $form['field_unit_price']['#attributes']['class'][] = 'field-hide';
        $form['field_uom']['#attributes']['class'][] = 'field-hide';
        $form['field_item_id']['#attributes']['class'][] = 'field-hide';
        $form['field_category']['#attributes']['class'][] = 'field-hide';
        $item_name = $form['field_item']['und'][0]['value']['#default_value'];
        drupal_set_title('Edit: ' . $item_name);
      }
      // Ending Inventory (Actual) Edit Form.
      elseif ($form['#bundle'] == 'field_ending_inventory_actual') {
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_crew']['#attributes']['class'][] = 'field-hide';
        $form['field_item']['#attributes']['class'][] = 'field-hide';
        $form['field_item_code']['#attributes']['class'][] = 'field-hide';
        $form['field_total']['#attributes']['class'][] = 'field-hide';
        $form['field_unit_price']['#attributes']['class'][] = 'field-hide';
        $form['field_uom']['#attributes']['class'][] = 'field-hide';
        $form['field_item_id']['#attributes']['class'][] = 'field-hide';
        $form['field_category']['#attributes']['class'][] = 'field-hide';
        $item_name = $form['field_item']['und'][0]['value']['#default_value'];
        drupal_set_title('Edit: ' . $item_name);
      }
      // Cash Pullout Edit form.
      elseif ($form['#bundle'] == 'field_cash_management_pullout') {
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_pulled_out_by']['#attributes']['class'][] = 'field-hide';
        $form['field_total_pulled_out_cash']['#attributes']['class'][] = 'field-hide';
        $form['field_requested_by']['und'][0]['value']['#title'] = t('Requestor');
        $form['#attributes']['class'][] = 'edit-cash-pullout-form';
      }
      // Cash Deposit Edit form.
      elseif ($form['#bundle'] == 'field_cash_management_deposit') {
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_deposited_by']['#attributes']['class'][] = 'field-hide';
        $form['field_total_deposited_cash']['#attributes']['class'][] = 'field-hide';
        $form['field_bank_transaction_number']['und'][0]['value']['#title'] = t('Transaction No.');
        $form['#attributes']['class'][] = 'edit-cash-deposit-form';
      }
      // Commissary Delivery Edit form.
      // Commissary Pullout Edit form.
      // Store Transin Edit form.
      // Store Transout Edit form.
      elseif ($som_mi_commissary_delivery || $som_mi_commissary_pullout || $som_mi_store_transin || $som_mi_store_transout) {
        $form['#attributes']['class'][] = 'mi-columns';
        $item_id = $form['field_item_id']['und'][0]['value']['#default_value'];
        $item_details = entity_load_single('store', $item_id);
        $item_tid = ($item_details->field_category['und'][0]['tid']);
        $item_category = taxonomy_term_load($item_tid);
        $form['item_original_category'] = array(
          '#type' => 'container',
          '#attributes' => array(
            'class' => array(
              'field-type-text',
              'field-widget-text-textfield',
            ),
          ),
          'und' => array(
            0 => array(
              'value' => array(
                '#type' => 'textfield',
                '#title' => 'Category',
                '#default_value' => $item_category->name,
                '#disabled' => TRUE,
              ),
            ),
          ),
          '#tree' => TRUE,
          '#access' => TRUE,
          '#weight' => 0,
        );
        $form['field_commissary_delivery_number']['#attributes']['class'][] = 'field-hide';
        $form['field_commissary_pull_out_number']['#attributes']['class'][] = 'field-hide';
        $form['field_transfer_in_number']['#attributes']['class'][] = 'field-hide';
        $form['field_transfer_out_number']['#attributes']['class'][] = 'field-hide';
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_item_code']['#attributes']['class'][] = 'field-hide';
        $form['field_total']['#attributes']['class'][] = 'field-hide';
        $form['field_unit_price']['#attributes']['class'][] = 'field-hide';
        $form['field_item_id']['#attributes']['class'][] = 'field-hide';
        $form['field_category']['#attributes']['class'][] = 'field-hide';
        $form['field_qty']['und'][0]['value']['#title'] = 'Qty';
        $form['field_item']['und'][0]['value']['#disabled'] = TRUE;
        $form['field_uom']['und'][0]['value']['#disabled'] = TRUE;
        $form['field_uom']['und'][0]['value']['#title'] = '';
        if ($som_mi_commissary_delivery) {
          $item_no = $form['field_commissary_delivery_number']['und'][0]['value']['#default_value'];
        }
        if ($som_mi_commissary_pullout) {
          $item_no = $form['field_commissary_pull_out_number']['und'][0]['value']['#default_value'];
        }
        if ($som_mi_store_transin) {
          $item_no = $form['field_transfer_in_number']['und'][0]['value']['#default_value'];
        }
        if ($som_mi_store_transout) {
          $item_no = $form['field_transfer_out_number']['und'][0]['value']['#default_value'];
        }
        drupal_set_title('Edit: ' . $item_no);
      }
      // Beginning Inventory Edit form.
      elseif ($form['#bundle'] == 'field_beginning_inventory' || $form['#bundle'] == 'field_production_processed_open') {
        $form['field_qty']['und'][0]['value']['#title'] = 'Qty';
        $form['field_item']['#attributes']['class'][] = 'field-hide';
        $form['field_created']['#attributes']['class'][] = 'field-hide';
        $form['field_item_code']['#attributes']['class'][] = 'field-hide';
        $form['field_total']['#attributes']['class'][] = 'field-hide';
        $form['field_unit_price']['#attributes']['class'][] = 'field-hide';
        $form['field_item_id']['#attributes']['class'][] = 'field-hide';
        $form['field_uom']['#attributes']['class'][] = 'field-hide';
        $form['field_category']['#attributes']['class'][] = 'field-hide';
        $item_name = $form['field_item']['und'][0]['value']['#default_value'];
        $form['actions']['cancel'] = array(
          '#type' => 'submit',
          '#value' => t('Cancel'),
          '#attributes' => array(
            'class' => array(
              'left-save',
            ),
          ),
        );
        drupal_set_title('Edit: ' . $item_name);
      }
      $form['actions']['submit']['#value'] = t('Save Changes');
      break;

    // Store Operations Mgmt: Manage Employee Schedule (edit undertime form).
    case 'add_undertime_edit_form':
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = $form['submit'];
      hide($form['submit']);
      break;

    // Store Operations Mgmt: Manage Employee (edit schedule form).
    case 'add_schedule_edit_form':
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = $form['submit'];
      hide($form['submit']);
      break;

    // Store Operations Mgmt: Manage Employee (edit overtime form).
    case 'add_overtime_edit_form':
      $form['actions']['#type'] = 'actions';
      $form['actions']['submit'] = $form['submit'];
      hide($form['submit']);
      break;

    // Commissary: Order Request (Add Order form).
    case 'commissary_order_form':
      $form['form_content']['targe_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Commissary: Order Request (Add Pullout form).
    case 'commissary_pullout_form':
      $form['form_content']['targe_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Store Transfer: Transfer in (Trans in add form).
    case 'store_transfer_in_form':
      $form['form_content']['targe_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Store Transfer: Transfer in (View form).
    case 'store_transfer_in_view_form':
      $form['form_top']['right']['target_transfer_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Store Transfer: Transfer out (Trans out add form).
    case 'store_transfer_out_form':
      $form['form_content']['targe_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Store Transfer: Transfer out (View form).
    case 'store_transfer_out_view_form':
      $form['form_top']['right']['target_transfer_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    // Commissary Pullout: Store Pullout Status View form.
    case 'order_pullout_view_form':
      $form['form_top']['right']['target_transfer_date']['#attributes']['placeholder'] = t('Select Date');
      break;

    default:
      break;
  }
}

/**
 * Implements template_preprocess_user_profile().
 */
function bms_adminlte_preprocess_user_profile(&$variables) {
  global $base_url;
  $theme_path = $base_url . '/' . drupal_get_path('theme', 'bms_adminlte');

  $variables['user_image'] = '';

  // Determine Employee Role (User type).
  foreach ($variables['elements']['#account']->roles as $role_key => $role) {
    if ($role != 'authenticated user') {
      $variables['employee_role'] = $role;
    }
  }

  // Set Employee Picture.
  if (isset($variables['elements']['#account']->picture->uri)) {
    $image = $variables['elements']['#account']->picture->uri;
    $variables['user_image'] = image_style_url('square_400x400', $image);
  }
  else {
    $variables['user_image'] = $theme_path . '/assets/images/no-image.jpg';
  }

  // Set Employee Status.
  if ($variables['elements']['#account']->status == 1) {
    $variables['employee_status'] = 'Active';
  }
  else {
    $variables['employee_status'] = 'Inactive';
  }

  // Check if Employee is Crew or Admin User.
  if (in_array('crew', $variables['elements']['#account']->roles)) {
    $variables['employee_type'] = 'crew';
  }
  else {
    $variables['employee_type'] = 'admin';
    $variables['user_profile']['field_account_employee_id']['#title'] = 'User#';

    // For Access Information blocks.
    $role_operation = ($variables['employee_role'] == 'operation');
    $role_general_manager = ($variables['employee_role'] == 'general manager');
    $role_accounting = ($variables['employee_role'] == 'accounting');
    $role_franchisee = ($variables['employee_role'] == 'franchisee');
    $role_hr = ($variables['employee_role'] == 'hr');
    $role_commissary = ($variables['employee_role'] == 'commissary');

    if ($role_operation || $role_general_manager) {
      $column_1 = array();
      $column_2 = array();
      $column_1[] = '<li><a href="#"><span>Dashboard</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Settings</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Store Operations Management</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Commissary Order/Pull-out Request</span></a></li>';
      $column_2[] = '<li><a href="#"><span>Store Transfer</span></a></li>';
      $column_2[] = '<li><a href="#"><span>Reports</span></a></li>';
      $variables['column_1'] = $column_1;
      $variables['column_2'] = $column_2;
    }
    elseif ($role_accounting ) {
      $column_1 = array();
      $column_1[] = '<li><a href="#"><span>Dashboard</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Reports</span></a></li>';
      $variables['column_1'] = $column_1;
    }
    elseif ($role_franchisee) {
      $column_1 = array();
      $column_2 = array();
      $column_3 = array();
      $column_1[] = '<li><a href="#"><span>Dashboard</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Settings - Crew</span></a></li>';
      $column_2[] = '<li><a href="#"><span>Store Operations</span></a></li>';
      $column_2[] = '<li><a href="#"><span>Commissary</span></a></li>';
      $column_3[] = '<li><a href="#"><span>Store Transfer</span></a></li>';
      $column_3[] = '<li><a href="#"><span>Reports</span></a></li>';
      $variables['column_1'] = $column_1;
      $variables['column_2'] = $column_2;
      $variables['column_3'] = $column_3;
    }
    elseif ($role_hr) {
      $column_1 = array();
      $column_1[] = '<li><a href="#"><span>Dashboard</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Reports - Attendance</span></a></li>';
      $variables['column_1'] = $column_1;
    }
    elseif ($role_commissary) {
      $column_1 = array();
      $column_1[] = '<li><a href="#"><span>Dashboard</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Commissary</span></a></li>';
      $column_1[] = '<li><a href="#"><span>Settings - Inventory (Limited to raw material pricing adjustment)</span></a></li>';
      $variables['column_1'] = $column_1;
    }
  }

  hide($variables['user_profile']['summary']);
  if ($variables['employee_role'] == 'crew') {
    $variables['user_profile']['field_account_employee_id']['#title'] = t("Store Crew No");
  }
}

/**
 * Implements hook_preprocess_form_id().
 */
function bms_adminlte_preprocess_user_profile_form(&$variables, $hook) {
  global $user;
  global $base_url;
  $theme = $base_url . '/' . drupal_get_path('theme', 'bms_adminlte');

  // Changing field labels.
  $variables['form']['account']['#title'] = '';
  $variables['form']['profile_main']['field_birthdate']['und'][0]['#title'] = '';
  $variables['form']['picture']['#title'] = '';
  $variables['form']['profile_main']['field_birthdate']['und'][0]['value']['date']['#title'] = 'Birthdate';
  $variables['form']['profile_main']['field_date_of_hire']['und'][0]['value']['date']['#title'] = 'Date of Hire';
  $variables['form']['profile_main']['field_end_of_contract']['und'][0]['value']['date']['#title'] = 'End of Contract';
  $variables['form']['account']['roles']['#title'] = 'User Type';
  $variables['form']['account']['mail']['#title'] = 'Email';
  $variables['form']['profile_main']['field_mobile_number']['und'][0]['value']['#title'] = 'Mobile #';
  $variables['form']['account']['roles']['#title'] = t('Position');

  foreach ($variables['form']['#user']->roles as $role_key => $role) {
    if ($role != 'authenticated user') {
      $variables['employee_role'] = $role;
    }
  }

  // Check if User is Crew or not.
  if (in_array('crew', $variables['form']['#user']->roles)) {
    $variables['employee_type'] = 'crew';
    $variables['form']['field_store']['und'][0]['target_id']['#title'] = t('Store Assignments');
    $variables['form']['field_account_employee_id']['und'][0]['value']['#title'] = t('Store Crew No');
  }
  else {
    $variables['employee_type'] = 'admin';
    $variables['form']['field_account_employee_id']['und'][0]['value']['#title'] = 'User #';
    $variables['form']['profile_main']['field_gender']['#access'] = FALSE;
    $variables['form']['profile_main']['field_birthdate']['#access'] = FALSE;
    $variables['form']['profile_main']['field_marital_status']['#access'] = FALSE;
    $variables['form']['profile_main']['field_date_of_hire']['#access'] = FALSE;
    $variables['form']['profile_main']['field_date_of_hire']['und'][0]['#element_validate'] = 0;
    $variables['form']['profile_main']['field_end_of_contract']['#access'] = FALSE;
    $variables['form']['field_store']['#access'] = FALSE;
  }
}

/**
 * Implements hook_preprocess_form_id().
 */
function bms_adminlte_preprocess_user_register_form(&$variables, $hook) {
  global $base_url;
  $theme = $base_url . '/' . drupal_get_path('theme', 'bms_adminlte');
  $variables['form']['account']['#title'] = '';
  $variables['form']['profile_main']['field_birthdate']['und'][0]['#title'] = '';
  $variables['form']['picture']['#title'] = '';
  $variables['form']['profile_main']['field_birthdate']['und'][0]['value']['date']['#title'] = 'Birthdate';
  $variables['form']['profile_main']['field_date_of_hire']['und'][0]['value']['date']['#title'] = 'Date of Hire';
  $variables['form']['profile_main']['field_end_of_contract']['und'][0]['value']['date']['#title'] = 'End of Contract';
  $variables['form']['field_account_employee_id']['und'][0]['value']['#title'] = t('Store Crew No');
  $variables['form']['field_store']['und']['#title'] = t('Store Assignments');
  $variables['form']['account']['roles']['#attributes']['class'][] = 'field-hide';
  $variables['form']['account']['roles']['#title'] = '';
  $variables['form']['account']['status']['#attributes']['class'][] = 'field-hide';
  $variables['form']['account']['status']['#title'] = '';
}

/**
 * Implements hook_theme().
 */
function bms_adminlte_theme($existing, $type, $theme, $path) {
  $items['user_profile_form'] = array(
    'render element' => 'form',
    'template' => 'user-profile-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['user_register_form'] = array(
    'render element' => 'form',
    'template' => 'user-register-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['eck__entity__form_add_store_product'] = array(
    'render element' => 'form',
    'template' => 'add-inventory-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['eck__entity__form_edit_store_product'] = array(
    'render element' => 'form',
    'template' => 'add-inventory-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['eck__entity__form_add_store_cashier_items'] = array(
    'render element' => 'form',
    'template' => 'add-cashier-items-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['eck__entity__form_add_store_store'] = array(
    'render element' => 'form',
    'template' => 'add-store-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );
  $items['eck__entity__form_edit_store_store'] = array(
    'render element' => 'form',
    'template' => 'edit-store-form',
    'path' => drupal_get_path('theme', 'bms_adminlte') . '/templates/forms',
  );

  return $items;
}

/**
 * Implement theme_button().
 */
function bms_adminlte_button($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'submit';
  element_set_attributes($element, array('id', 'name', 'value'));

  $element['#attributes']['class'][] = 'form-' . $element['#button_type'];
  if(!in_array('btn btn-block btn-default btn-sm', $element['#attributes']['class'])) {
    $element['#attributes']['class'][] = 'btn btn-block btn-primary';
  }

  if (!empty($element['#attributes']['disabled'])) {
    $element['#attributes']['class'][] = 'form-button-disabled';
  }

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Implement theme_textfield().
 */
function bms_adminlte_textfield($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'text';
  element_set_attributes($element, array('id', 'name', 'value', 'size', 'maxlength'));
  _form_set_class($element, array('form-text', 'form-control'));

  $extra = '';
  if ($element['#autocomplete_path'] && !empty($element['#autocomplete_input'])) {
    drupal_add_library('system', 'drupal.autocomplete');
    $element['#attributes']['class'][] = 'form-autocomplete';

    $attributes = array();
    $attributes['type'] = 'hidden';
    $attributes['id'] = $element['#autocomplete_input']['#id'];
    $attributes['value'] = $element['#autocomplete_input']['#url_value'];
    $attributes['disabled'] = 'disabled';
    $attributes['class'][] = 'autocomplete';
    $extra = '<input' . drupal_attributes($attributes) . ' />';
  }

  $output = '<input' . drupal_attributes($element['#attributes']) . ' />';

  return $output . $extra;
}

/**
 * Implement theme_password().
 */
function bms_adminlte_password($variables) {
  $element = $variables['element'];
  $element['#attributes']['type'] = 'password';
  element_set_attributes($element, array('id', 'name', 'size', 'maxlength'));
  _form_set_class($element, array('form-text', 'form-control'));

  return '<input' . drupal_attributes($element['#attributes']) . ' />';
}

/**
 * Implement theme_preprocess_views_view_table().
 */
function bms_adminlte_preprocess_views_view_table(&$vars) {

  // Reporting grouping of fields in tabs.
  // Attendance.
  $attendance_display_1 = ($vars['view']->current_display  == 'attendance_report_summary');
  $attendance_display_2 = ($vars['view']->current_display  == 'manage_attendance');
  if ($vars['view']->name == 'attendance' && ($attendance_display_1 || $attendance_display_2)) {
    $vars['profile'] = array(
      //'field_store_name',
      //'field_store_code',
      'employee_full_name',
      'field_employee_no',
      'field_username',
      //'field_date_filter'
    );
    $vars['assigned_duty'] = array(
      'field_date_filter',
      'employee_full_name',
      'field_assigned_time_in',
      'field_assigned_time_out',
      'field_no_hours_assigned',
      'field_ot_request',
      'field_ut_request',
      'field_expected_timeout',
      'field_total_hours_assigned'
    );
    $vars['actual_duty_rendered'] = array(
      'employee_full_name',
      'field_actual_duty_time_in',
      'field_img_time_in',
      'field_actual_duty_time_out',
      'field_img_time_out',
      'field_no_of_hours_rendered',
      'field_late',
      'field_ot',
      'field_ut',
      'field_total_hours_rendered',
      'field_variance',
      'field_actual_duty_rendered',
    );

    $vars['break_freezer'] = array(
      'employee_full_name',
      'field_attendance_break_out',
      'field_attendance_break_in',
      'field_total_no_of_breaks_minutes',
      'attendance_total_breaks_field',
      //'field_freezer_temp',
      'field_break_freezer'
    );
  }

  // Inventory Summary.
  $inventory_display_1 = ($vars['view']->current_display  == 'inventory_summary');
  $inventory_display_2 = ($vars['view']->current_display  == 'manage_inventory');
  if ($vars['view']->name == 'inventory_summary' && ($inventory_display_1 || $inventory_display_2)) {
    $vars['beginning_inventory'] = array(
      'field_category',
      'field_item',
      'field_item_code',
      'field_qty',
      'field_uom',
      'field_unit_price',
      'field_total',
      'field_beginning_inventory',
    );
    $vars['commissary_delivery'] = array(
      'field_created',
      'field_category_1',
      'field_item_1',
      'field_item_code_1',
      'field_qty_1',
      'field_uom_1',
      'field_unit_price_1',
      'field_total_1',
      'field_commissary_delivery',
    );
    $vars['commissary_pullout'] = array(
      'field_created_1',
      'field_category_2',
      'field_item_2',
      'field_item_code_2',
      'field_qty_2',
      'field_uom_2',
      'field_unit_price_9',
      'field_total_2',
      'field_commissary_pullout',
    );
    $vars['store_trans_in'] = array(
      'field_category_3',
      'field_item_3',
      'field_item_code_3',
      'field_qty_3',
      'field_uom_3',
      'field_unit_price_2',
      'field_total_3',
      'field_store_transin',
    );
    $vars['store_trans_out'] = array(
      'field_category_4',
      'field_item_4',
      'field_item_code_4',
      'field_qty_4',
      'field_uom_4',
      'field_unit_price_3',
      'field_total_4',
      'field_store_transout',
    );
    $vars['production'] = array(
      'field_category_5',
      'field_item_5',
      'field_item_code_5',
      'field_qty_5',
      'field_uom_5',
      'field_unit_price_4',
      'field_total_5',
      'field_production_processed_open'
    );
    $vars['wastage'] = array(
      'field_crew',
      'field_category_6',
      'field_item_6',
      'field_item_code_6',
      'field_qty_6',
      'field_uom_6',
      'field_reason',
      'field_unit_price_5',
      'field_total_6',
      'field_wastages',
    );
    $vars['ending_inventory_theoretical'] = array(
      'field_category_7',
      'field_item_7',
      'field_item_code_7',
      //'field_qty_7',
      'ending_inventory_theoretical_qty',
      'field_uom_7',
      'field_unit_price_6',
      'field_total_7',
    );
    $vars['ending_inventory_actual_count'] = array(
      'field_crew_1',
      'field_category_8',
      'field_item_8',
      'field_item_code_8',
      'field_qty_8',
      'field_uom_8',
      'field_unit_price_7',
      'field_total_8',
      'field_ending_inventory_actual',
    );
    $vars['variance'] = array(
      'field_category_9',
      'field_item_9',
      'field_item_code_9',
      //'field_qty_9',
      'ending_inventory_variance_qty',
      'field_uom_9',
      'field_unit_price_8',
      'field_total_9',
    );
  }

  // Production Summary.
  $production_display_1 = ($vars['view']->current_display  == 'production_summary');
  $production_display_2 = ($vars['view']->current_display  == 'manage_production');
  if ($vars['view']->name == 'production_summary' && ($production_display_1 || $production_display_2)) {
    $vars['beginning_inventory'] = array(
      'field_category',
      'field_item',
      'field_item_code',
      'field_qty',
      'field_uom',
      'field_unit_price',
      'field_total',
    );
    $vars['processed_opened'] = array(
      'field_created',
      //'field_crew',
      'field_category_1',
      'field_item_1',
      'field_item_code_1',
      'field_qty_1',
      'field_uom_1',
      'field_unit_price_1',
      'field_total_1',
      'field_processed_opened',
    );
    $vars['sales'] = array(
      'field_category_2',
      'field_item_2',
      'field_item_code_2',
      'field_qty_2',
      'field_uom_2',
      'field_unit_price_2',
      'field_total_2',
    );
    $vars['wastage'] = array(
      'field_created_1',
      'field_category_4',
      'field_crew_1',
      'field_item_3',
      'field_item_code_3',
      'field_qty_3',
      'field_uom_3',
      'field_unit_price_3',
      'field_total_3',
      'field_reason',
      'field_wastages',
    );
    $vars['ending_inventory_theoretical'] = array(
      'field_category_3',
      'field_item_4',
      'field_item_code_4',
      //'field_qty_4',
      'ending_inventory_production_theoretical_qty',
      'field_uom_4',
      'field_unit_price_4',
      'field_total_4',
    );
    $vars['ending_inventory_actual_count'] = array(
      //'field_crew_3',
      'field_category_5',
      'field_item_5',
      'field_item_code_5',
      'field_qty_5',
      'field_uom_5',
      'field_unit_price_5',
      'field_total_5',
      'field_ending_inventory_actual'
    );
    $vars['variance'] = array(
      'field_category_6',
      'field_item_6',
      'field_item_code_6',
      //'field_qty_6',
      'ending_inventory_production_variance_qty',
      'field_uom_6',
      'field_unit_price_6',
      'field_total_6',
    );
  }

  // Order & Delivery.
  $order_delivery_request = ($vars['view']->current_display  == 'commissary_order_reports');
  $order_delivery_pullout = ($vars['view']->current_display  == 'commissary_pullout_reports');
  if ($vars['view']->name == 'commissary' && $order_delivery_request) {
    $vars['request'] = array(
      'field_created',
      'field_requested_by',
      'field_request_no',
      'field_category',
      'field_item',
      'field_item_code',
      'field_qty',
      'field_uom',
      'field_unit_price',
      'field_total',
      'field_status',
    );
    $vars['commissary'] = array(
      'field_created_1',
      'field_received_by',
      'field_delivery_no',
      'field_category_1',
      'field_item_1',
      'field_item_code_1',
      'field_delivery_quantity',
      'field_uom_1',
      'field_unit_price_1',
      'field_total_1',
      'field_status_1',
    );
    $vars['variance'] = array(
      'field_created_2',
      'field_delivery_no_1',
      'field_request_no_1',
      'field_category_2',
      'field_item_2',
      'field_item_code_2',
      'field_qty_2',
      'field_uom_2',
      'field_unit_price_2',
      'field_total_2',
    );
  }
  elseif ($vars['view']->name == 'commissary' && $order_delivery_pullout) {
    $vars['request'] = array(
      'field_created',
      'field_requested_by',
      'field_pullout_request_no',
      'field_reason',
      'field_item',
      'field_item_code',
      'field_qty',
      'field_uom',
      'field_unit_price',
      'field_total',
    );
    $vars['commissary'] = array(
      'field_created_1',
      'field_released_by',
      'field_pullout_no',
      'field_item_1',
      'field_item_code_1',
      'field_qty_1',
      'field_uom_1',
      'field_unit_price_1',
      'field_total_1',
    );
    $vars['variance'] = array(
      'field_created_2',
      'field_pullout_no_1',
      'field_item_2',
      'field_item_code_2',
      'field_qty_2',
      'field_uom_2',
      'field_unit_price_2',
      'field_total_2',
    );
  }

  // Store Transfers.
  $store_transfer_in = ($vars['view']->current_display  == 'store_transfer_in');
  $store_transfer_out = ($vars['view']->current_display  == 'store_transfer_out');
  if ($vars['view']->name == 'store_transfers' && ($store_transfer_in || $store_transfer_out)) {
    if ($store_transfer_in) {
      $vars['transfer_in_request'] = array(
        'field_created',
        'field_store_source',
        'field_requested_by',
        'field_request_no',
        'field_item',
        'field_item_code',
        'field_qty',
        'field_uom',
        'field_unit_price',
        'field_total',
        'field_status',
      );
      $vars['transfer_in_delivery'] = array(
        'field_created_1',
        'field_received_by',
        'field_transfer_in_number',
        'field_store_source_1',
        'field_transfer_out_number',
        'field_item_1',
        'field_item_code_1',
        'field_qty_1',
        'field_uom_1',
        'field_unit_price_1',
        'field_total_1',
        'field_status_1',
      );
      $vars['variance'] = array(
        'field_trans_in_no',
        'field_request_no_1',
        'field_item_2',
        'field_item_code_2',
        'field_qty_2',
        'field_uom_2',
        'field_unit_price_2',
        'field_total_2',
      );
    }
    elseif ($store_transfer_out) {
      $vars['transfer_in_request'] = array(
        'field_created',
        'field_requested_by',
        'field_request_no',
        'field_receiving_store',
        'field_item',
        'field_item_code',
        'field_qty',
        'field_uom',
        'field_unit_price',
        'field_total',
      );
      $vars['transfer_in_delivery'] = array(
        'field_created_1',
        'field_released_by',
        'field_receiving_store_1',
        'field_receiving_trans_in_no',
        'field_item_1',
        'field_item_code_1',
        'field_qty_1',
        'field_uom_1',
        'field_unit_price_1',
        'field_total_1',
        'field_status',
      );
      $vars['variance'] = array(
        'field_request_no_1',
        'field_trans_out_no',
        'field_item_2',
        'field_item_code_2',
        'field_qty_2',
        'field_uom_2',
        'field_unit_price_2',
        'field_total_2',
      );
    }
  }

  // Cash Management.
  $cash_management_display_1 = ($vars['view']->current_display  == 'cash_management');
  $cash_management_display_2 = ($vars['view']->current_display  == 'manage_cash_management');
  if ($vars['view']->name == 'cash_management' && $cash_management_display_1) {
    $vars['vault'] = array(
      'field_cashier',
      'field_created_6',
      'field_amount_1',
      'field_total',
    );
    $vars['sales'] = array(
      'field_cash_sales',
      'field_total_sales',
      'field_gc_sales',
      'field_total_sales_for_the_day',
      'field_sales_variance',
    );
    $vars['pullout'] = array(
      'field_created_1',
      'field_created_3',
      'field_pulled_out_by',
      'field_requested_by',
      'field_amount',
      'field_total_pulled_out_cash',
      'field_reason',
    );
    $vars['deposit'] = array(
      'field_created_4',
      'field_created_5',
      'field_deposited_by',
      'field_bank',
      'field_bank_transaction_number',
      'field_amount_2',
      'field_total_deposited_cash',
    );
    $vars['other'] = array(
      'field_remaining_cash_on_vault',
    );
  }
  elseif ($vars['view']->name == 'cash_management' && $cash_management_display_2) {
    $vars['vault'] = array(
      'field_cashier',
      'field_created_6',
      'field_amount_1',
      'field_total',
      'nothing',
    );
    $vars['sales'] = array(
      'field_cash_sales',
      'field_gc_sales',
      'field_total_sales',
      'field_total_sales_for_the_day',
      'field_sales_variance',
    );
    $vars['pullout'] = array(
      //'field_created_1',
      'field_created_3',
      'field_pulled_out_by',
      'field_requested_by',
      'field_amount',
      'field_total_pulled_out_cash',
      'field_reason',
      'field_cash_management_pullout',
    );
    $vars['deposit'] = array(
      //'field_created_4',
      'field_created_5',
      'field_deposited_by',
      'field_bank',
      'field_bank_transaction_number',
      'field_amount_2',
      'field_total_deposited_cash',
      'field_cash_management_deposit',
    );
    $vars['other'] = array(
      'field_remaining_cash_on_vault',
    );
  }

  $vars['extra_fields'] = array();
  // Sales by system wide
  if($vars['view']->name == 'sales_by_system_wide' && $vars['view']->current_display == 'day_sales' ||
     $vars['view']->name == 'sales_by_area' && $vars['view']->current_display == 'day_sales' ||
     $vars['view']->name == 'sales_by_store' && $vars['view']->current_display == 'day_sales') {
     // Init default variable.
     $vars['extra_fields']['transaction_count'] = 0;
     $vars['extra_fields']['average_check'] = 0;
     $vars['extra_fields']['amount'] = 0;
     $vars['extra_fields']['percentage_to_sales'] = 0;

    foreach($vars['rows'] as $key => $item) {
      if ($vars['view']->name == 'sales_by_store' && $vars['view']->current_display == 'day_sales') {
        $hour = $vars['rows'][$key]['hourly_range_field'];
        $times = explode('-', $hour);
        $vars['rows'][$key]['hourly_range_field'] = $times[0];
        $vars['rows'][$key]['nothing_1'] = $times[1];
      }

      $percentage_to_sales += $item['percentage_to_sales_field'];
      $vars['extra_fields']['transaction_count'] += $item['transaction_count_field'];
      $vars['extra_fields']['average_check'] += number_format($item['average_check_field'], 2);
      $vars['extra_fields']['amount'] += number_format($item['amount_field'], 2);
      $vars['extra_fields']['percentage_to_sales'] = number_format($percentage_to_sales, 2) . '%';

    }
    if ($vars['view']->name == 'sales_by_store' && $vars['view']->current_display == 'day_sales') {
      $inserted = array('extra' => ' ');
      array_splice($vars['extra_fields'], 0, 0, $inserted);
    }
  }
  // Sales by transaction
  if($vars['view']->name == 'sales_transaction' && $vars['view']->current_display == 'sales_transaction') {
    // Init default variable.
    $vars['extra_fields']['crew'] = '';
    $vars['extra_fields']['transaction_number'] = '';
    $vars['extra_fields']['or_number'] = '';
    $vars['extra_fields']['gross_sales'] = 0;
    $vars['extra_fields']['sc_discount'] = 0;
    $vars['extra_fields']['pwd_discount'] = 0;
    $vars['extra_fields']['ee_discount'] = 0;
    $vars['extra_fields']['evat'] = 0;
    $vars['extra_fields']['net_sales'] = 0;
    $vars['extra_fields']['nothing'] = '';

    foreach($vars['rows'] as $item) {
      $vars['extra_fields']['gross_sales'] += number_format($item['field_sub_total'], 2);
      $vars['extra_fields']['sc_discount'] += number_format($item['field_sc_discount'], 2);
      $vars['extra_fields']['pwd_discount'] += number_format($item['field_pwd_discount'], 2);
      $vars['extra_fields']['ee_discount'] += number_format($item['field_employee_discount'], 2);
      $vars['extra_fields']['evat'] += number_format($item['field_vat'], 2);
      $vars['extra_fields']['net_sales'] += number_format($item['field_total_amount'], 2);
    }
  }
  // Store Operation Management
  if($vars['view']->name == 'sales_transaction' && $vars['view']->current_display == 'manage_sales_transaction') {

    // Init default variable.
    $vars['extra_fields']['crew'] = '';
    $vars['extra_fields']['transaction_number'] = '';
    $vars['extra_fields']['or_number'] = '';
    $vars['extra_fields']['gross_sales'] = 0;
    $vars['extra_fields']['sc_discount'] = 0;
    $vars['extra_fields']['pwd_discount'] = 0;
    $vars['extra_fields']['ee_discount'] = 0;
    $vars['extra_fields']['evat'] = 0;
    $vars['extra_fields']['net_sales'] = 0;
    $vars['extra_fields']['nothing'] = '';

    foreach($vars['rows'] as $item) {
      $vars['extra_fields']['gross_sales'] += number_format($item['field_sub_total'], 2);
      $vars['extra_fields']['sc_discount'] += number_format($item['field_sc_discount'], 2);
      $vars['extra_fields']['pwd_discount'] += number_format($item['field_pwd_discount'], 2);
      $vars['extra_fields']['ee_discount'] += number_format($item['field_employee_discount'], 2);
      $vars['extra_fields']['evat'] += number_format($item['field_vat'], 2);
      $vars['extra_fields']['net_sales'] += number_format($item['field_total_amount'], 2);
    }
  }
  // Product Sales by store and system wide.
  if($vars['view']->name == 'product_sales' && $vars['view']->current_display == 'product_day_sales' ||
    $vars['view']->name == 'product_sales_by_system_wide_reports' && $vars['view']->current_display == 'system_wide_day_sales') {
    $vars['extra_fields']['qty_sold'] = 0;
    $vars['extra_fields']['unit_price'] = 0;
    $vars['extra_fields']['peso_total'] = 0;
    $vars['extra_fields']['volume'] = 0;
    $vars['extra_fields']['peso'] = 0;

    foreach($vars['rows'] as $item) {
      $vars['extra_fields']['qty_sold'] += $item['product_sales_qty'];
      $vars['extra_fields']['peso_total'] += $item['product_sales_total'];
      $vars['extra_fields']['volume'] += $item['product_sales_volume'];
      $vars['extra_fields']['peso'] += $item['product_sales_percentage'];
    }
    $vars['extra_fields']['volume'] = round($vars['extra_fields']['volume']);
    $vars['extra_fields']['peso'] = round($vars['extra_fields']['peso']);
  }

}

/**
 * Implement theme_select().
 */
function bms_adminlte_select($variables) {
  $element = $variables['element'];
  element_set_attributes($element, array('id', 'name', 'size'));
  _form_set_class($element, array('form-select', 'form-control'));

  return '<select' . drupal_attributes($element['#attributes']) . '>' . form_select_options($element) . '</select>';
}

/**
 * Customize specific menu - bms_menu.
 */
function bms_adminlte_menu_tree__menu_bms_menu($variables) {
  global $level;
  global $user;

  if(in_array('accounting', $user->roles) || in_array('general manager', $user->roles)) {

    $class = ($level == 1) ? 'sidebar-menu' : 'treeview-menu';
    $main_navigation = '';

    $variables['tree'] = '<li class="first leaf active-trail active"><a href="/dashboard"><i class="fa fa-dashboard active-trail active"></i><span>Dashboard</span></a></li>
<li class="treeview"><a href="/"><i class="fa fa-table active"></i><span>Reports</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu"><li class="first leaf"><a href="/attendance"><i class="fa fa-circle-o"></i><span>Attendance</span></a></li>
<li class="treeview"><a href="/"><i class="fa fa-circle-o active"></i><span>Sales</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu"><li class="first leaf"><a href="/sales-transaction"><i class="fa fa-circle-o"></i><span>by Transaction</span></a></li>
<li class="leaf"><a href="/sales-by-store"><i class="fa fa-circle-o"></i><span>by Store</span></a></li>
<li class="leaf"><a href="/sales-by-area"><i class="fa fa-circle-o"></i><span>by Area</span></a></li>
<li class="last leaf"><a href="/sales-by-system-wide"><i class="fa fa-circle-o"></i><span>by System Wide</span></a></li>
</ul></li>
<li class="treeview"><a href="/"><i class="fa fa-circle-o active"></i><span>Product Sales</span><i class="fa fa-angle-left pull-right"></i></a><ul class="treeview-menu"><li class="first leaf"><a href="/product-sales"><i class="fa fa-circle-o"></i><span>by Store</span></a></li>
<li class="last leaf"><a href="/product-sales/by-system-wide"><i class="fa fa-circle-o"></i><span>by System Wide</span></a></li>
</ul></li>
</ul></li>';

    return '<ul class="' . $class . '">' . $main_navigation . $variables['tree'] . '</ul>';
  }
  else {
    $class = ($level == 1) ? 'sidebar-menu' : 'treeview-menu';
    $main_navigation = ($level == 1) ? '<li class="header">MAIN NAVIGATION</li>' : '';

    return '<ul class="' . $class . '">' . $main_navigation . $variables['tree'] . '</ul>';
  }
}

/**
 * Implement theme_menu_link().
 */
function bms_adminlte_menu_link(array $variables) {

  // Separate Form Pages.
  $add_store = (request_uri() == '/store/add');
  $add_crew = (arg(0) == 'admin' && arg(1) == 'people' && arg(2) == 'create');
  $add_inventory = (request_uri() == '/raw-materials/add');
  $edit_inventory = (arg(0) == 'field-collection' && arg(1) == 'field-inventory' && arg(3) == 'edit');
  $inventory_forms = ($add_inventory || $edit_inventory);
  $add_product = (request_uri() == '/product/add');
  $edit_product = (arg(0) == 'admin' && arg(1) == 'structure' && arg(2) == 'entity-type' && arg(3) == 'store' && arg(4) == 'product' && arg(6) == 'edit');
  $product_forms = ($add_product || $edit_product);
  $form_pages = ($add_store || $add_crew || $inventory_forms || $product_forms);

  // Settings pages.
  $user_profile = (arg(0) == 'user' && is_numeric(arg(1)) && arg(2) == FALSE);
  $store_profile_view = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)));
  $store_profile_edit = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'edit');
  $store_profile = ($store_profile_view || $store_profile_edit);
  $store_profile_inventory = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'inventory');
  $store_profile_product = (arg(0) == 'store' && arg(1) == 'store' && is_numeric(arg(2)) && arg(3) == 'product');
  $store_settings_pages = ($store_profile || $store_profile_inventory || $store_profile_product);
  $settings_inner_pages = ($user_profile || $store_profile || $store_profile_inventory || $store_profile_product);

  // For Commissary pages.
  $commissary_order_inner_add = (arg(0) == 'commissary' && arg(1) == 'order' && arg(4) == 'request');
  $commissary_pullout_inner_add = (arg(0) == 'commissary' && arg(1) == 'pullout' && arg(4) == 'request');
  $commissary_order_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'order');
  $commissary_pullout_admin = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'pullout');
  $commissary_order_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'request');
  $commissary_pullout_admin_inner = (arg(0) == 'admin' && arg(1) == 'commissary' && arg(2) == 'store' && arg(4) == 'pullout');
  $commissary_admin = ($commissary_order_admin || $commissary_pullout_admin || $commissary_order_admin_inner || $commissary_pullout_admin_inner);
  $commissary_pages = ($commissary_order_inner_add || $commissary_pullout_inner_add || $commissary_admin);

  // For Store Transfer pages.
  $store_transfers_in_inner = (arg(0) == 'store-transfer' && arg(1) == 'transfer-in-request');
  $store_transfers_out_inner = (arg(0) == 'store-transfer' && arg(1) == 'transfer-out-request');
  $store_transfers_inner_pages = ($store_transfers_in_inner || $store_transfers_out_inner);

  // Store Operations Mgmt pages.
  $employee_schedule_inner = (arg(0) == 'employee' && is_numeric(arg(1)) && arg(2) == 'schedule');
  $manage_attendance_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-attendance');
  $manage_inventory_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-inventory');
  $manage_production_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-production');
  $manage_sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-sales-transaction');
  $manage_cash_sales_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-cash-management');
  $edit_schedule = (arg(0) == 'add-schedule' && arg(2) == 'edit');
  $edit_undertime = (arg(0) == 'add-undertime' && arg(2) == 'edit');
  $edit_overtime = (arg(0) == 'add-overtime' && arg(2) == 'edit');
  $schedules = ($edit_schedule || $edit_undertime || $edit_overtime);
  $store_operations_mgmt_inner_pages = ($employee_schedule_inner || $manage_attendance_inner || $manage_inventory_inner || $manage_production_inner || $manage_sales_transaction_inner || $manage_cash_sales_inner || $schedules);

  // Reports Pages.
  $attendance_report = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'attendance');
  $inventory_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'inventory-report');
  $production_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'production-report');
  $cash_management_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'cash-management');
  $store_transfers_inner = (arg(0) == 'store-transfers' && arg(1) == 'store' && is_numeric(arg(2)));
  $product_sales_bsw_component = (arg(0) == 'product' && arg(1) == 'component-item' && is_numeric(arg(2)));
  $product_sales_bsw_component_inner = (arg(0) == 'product-sales' && arg(1) == 'system-wide' && arg(2) == 'component-item' && is_numeric(arg(3)));
  $product_sales_bsw_component_product = (arg(0) == 'product-sales' && arg(1) == 'by-system-wide' && arg(2) == 'product');
  $product_sales_bsw_component_all = (arg(0) == 'product-sales' && arg(1) == 'by-system-wide' && arg(2) == 'category' && arg(3) == 'all');
  $product_sales_by_store_component = (arg(0) == 'product-sales' && arg(1) == 'store' && is_numeric(arg(2)));
  $product_sales = ($product_sales_bsw_component || $product_sales_bsw_component_inner || $product_sales_by_store_component || $product_sales_bsw_component_all || $product_sales_bsw_component_product);
  $sales_transaction_outer = (arg(0) == 'sales-transaction');
  $sales_transaction_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'sales-transaction');
  $sales_transaction = ($sales_transaction_outer || $sales_transaction_inner);
  $reports_inner_pages = ($attendance_report || $inventory_summary_detail || $production_summary_detail || $cash_management_detail || $store_transfers_inner || $product_sales || $sales_transaction);

  $element = $variables['element'];
  $sub_menu = '';
  global $level;
  // Add treeview class
  if ($element['#below']) {
    $element['#attributes']['class'][0] = 'treeview';
    if ($settings_inner_pages || $form_pages) {
      if ($element['#title'] == 'Settings') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($store_operations_mgmt_inner_pages) {
      if ($element['#title'] == 'Store Operations Mgmt') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($reports_inner_pages) {
      if ($element['#title'] == 'Reports') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($product_sales_bsw_component) {
      if ($element['#title'] == 'Product Sales') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($product_sales_bsw_component_all || $product_sales_bsw_component_product) {
      if ($element['#title'] == 'Product Sales') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($product_sales_by_store_component) {
      if ($element['#title'] == 'Product Sales') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($sales_transaction_inner) {
      if ($element['#title'] == 'Sales') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($commissary_pages) {
      if ($element['#title'] == 'Commissary') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }
    if ($store_transfers_inner_pages) {
      if ($element['#title'] == 'Store Transfer') {
        $element['#attributes']['class'][0] = 'treeview active';
      }
    }

    unset($element['#attributes']['class'][1]);
    $sub_menu = drupal_render($element['#below']);
    $level = 1;
  }
  else {
    $level = $element['#original_link']['depth'];
  }
  // Add active class in selected menu
  if($element['#original_link']['in_active_trail'] == TRUE) {
    $element['#attributes']['class'][] = 'active';
  }
  if ($element['#href'] == 'cash-management') {
    if ($cash_management_detail) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'store-transfers') {
    if ($store_transfers_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'inventory-summary') {
    if ($inventory_summary_detail) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'production-summary') {
    if ($production_summary_detail) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'attendance') {
    if ($attendance_report) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-sales-transaction') {
    if ($manage_sales_transaction_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-production') {
    if ($manage_production_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-attendance') {
    if ($manage_attendance_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-schedule') {
    if ($employee_schedule_inner || $schedules) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'administer-employee') {
    if ($user_profile) {
      $employee_user_main = user_load(arg(1), $reset = FALSE);
      if (isset($employee_user_main->roles)) {
        if (in_array('crew', $employee_user_main->roles)) {
          $element['#attributes']['class'][] = 'active';
        }
      }
    }
    if ($add_crew) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-employee') {
    if ($user_profile) {
      $employee_user_main = user_load(arg(1), $reset = FALSE);
      if (isset($employee_user_main->roles)) {
        if (!in_array('crew', $employee_user_main->roles)) {
          $element['#attributes']['class'][] = 'active';
        }
      }
    }
  }
  if ($element['#href'] == 'store-settings') {
    if ($store_settings_pages || $add_store) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-cash-management') {
    if ($manage_cash_sales_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'product-sales/by-system-wide') {
    if ($product_sales_bsw_component || $product_sales_bsw_component_inner || $product_sales_bsw_component_all || $product_sales_bsw_component_product) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'product-sales') {
    if ($product_sales_bsw_component_inner) {
      $element['#attributes']['class'][] = 'active';
      foreach ($element['#attributes']['class'] as $key => $value) {
        if ($value == 'active') {
          unset($element['#attributes']['class'][$key]);
        }
      }
    }
    if ($product_sales_by_store_component) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'sales-transaction') {
    if ($sales_transaction_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'commissary/order' || $element['#href'] == 'admin/commissary/order') {
    if ($commissary_order_inner_add || $commissary_order_admin || $commissary_order_admin_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'commissary/pullout' || $element['#href'] == 'admin/commissary/pullout') {
    if ($commissary_pullout_inner_add || $commissary_pullout_admin || $commissary_pullout_admin_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-inventory') {
    if ($manage_inventory_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'store-transfer/transfer-in-request') {
    if ($store_transfers_in_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'store-transfer/transfer-out-request') {
    if ($store_transfers_out_inner) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'inventory') {
    if ($inventory_forms) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  if ($element['#href'] == 'manage-product') {
    if ($product_forms) {
      $element['#attributes']['class'][] = 'active';
    }
  }
  $icon = '';

  if($level == 1 && $element['#href'] == '<front>') {
    $icon = '<i class="fa fa-angle-left pull-right"></i>';
  }

  //$output = l($element['#title'], $element['#href'], $element['#localized_options']);
  $output = '<a href="' . check_plain(url($element['#href'], $element['#original_link']['options'])) . '">' . '<i' . drupal_attributes($element['#original_link']['options']['attributes']) . '></i><span>' . $element['#title'] . '</span>' . $icon . '</a>';
  return '<li' . drupal_attributes($element['#attributes']) . '>' . $output . $sub_menu . "</li>\n";

}

/**
 * Implement theme_link().
 */
function bms_adminlte_link($variables) {
  $title = ($variables['options']['html'] ? $variables['text'] : check_plain($variables['text']));

  if (isset($variables['options']['attributes']['class'])) {
    $autodialog_class = ($variables['options']['attributes']['class'][0] == 'autodialog');
  }
  else {
    $autodialog_class = FALSE;
  }

  if ($autodialog_class) {
    $links = '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '" ' . drupal_attributes($variables['options']['attributes']) . ' data-dialog-ajax="true" data-dialog-ajax-disable-redirect="true">' . $title . '</a>';
  }
  else {
    $links = '<a href="' . check_plain(url($variables['path'], $variables['options'])) . '"' . drupal_attributes($variables['options']['attributes']) . '>' . $title . '</a>';
  }

  return $links;
}

/**
 * Implements hook_menu_alter().
 */
function bms_adminlte_menu_alter(&$item) {
  // Hide the "Shortcuts" tab.
  $item['user/%/shortcuts']['type'] = MENU_CALLBACK;
  // Hide the "Devel" tab.
  $item['user/%/devel']['type'] = MENU_CALLBACK;
}

/**
 * Implements theme_menu_local_tasks().
 */
function bms_adminlte_menu_local_tasks(&$variables) {
  $output = '';

  if (!empty($variables['primary'])) {
    $variables['primary']['#prefix'] = '<h2 class="element-invisible">' . t('Primary tabs') . '</h2>';
    $variables['primary']['#prefix'] .= '<ul class="nav nav-tabs">';
    $variables['primary']['#suffix'] = '</ul>';
    foreach ($variables['primary'] as $key => $link) {
      if (is_array($link)) {
        if ($link['#link']['path'] == 'user/%/view' || $link['#link']['path'] == 'store/store/%/view') {
          $variables['primary'][$key]['#link']['title'] = 'Basic Information';
        }
        if ($link['#link']['path'] == 'user/%/edit') {
          $variables['primary'][$key]['#link']['title'] = 'Settings';
        }
        if ($link['#link']['path'] == 'store/store/%/edit') {
          $variables['primary'][$key]['#link']['title'] = 'Store Settings';
        }
        if ($link['#link']['path'] == 'store/store/%/inventory') {
          $variables['primary'][$key]['#link']['title'] = 'Inventory Settings';
        }
        if ($link['#link']['path'] == 'store/store/%/products') {
          $variables['primary'][$key]['#link']['title'] = 'Product Settings';
        }
        if ($link['#link']['path'] == 'store/store/%/delete') {
          hide($variables['primary'][$key]);
        }
      }
    }
    $output .= drupal_render($variables['primary']);
  }
  if (!empty($variables['secondary'])) {
    $variables['secondary']['#prefix'] = '<h2 class="element-invisible">' . t('Secondary tabs') . '</h2>';
    $variables['secondary']['#prefix'] .= '<ul class="tabs secondary">';
    $variables['secondary']['#suffix'] = '</ul>';
    $output .= drupal_render($variables['secondary']);
  }
  return $output;
}

/**
 * Implement theme_status_messages().
 */
function bms_adminlte_status_messages($variables) {
  $display = $variables['display'];
  $output = '';

  $status_heading = array(
    'status' => t('Status message'),
    'error' => t('Error message'),
    'warning' => t('Warning message'),
  );

  foreach (drupal_get_messages($display) as $type => $messages) {

    switch ($type) {
      case 'error':
        $class = 'alert alert-danger alert-dismissible';
        $icon = '<i class="icon fa fa-ban"></i>';
        break;
      case 'status':
        $class = 'alert alert-success alert-dismissible';
        $icon = '<i class="icon fa fa-check"></i>';
        break;
      case 'warning':
        $class = 'alert alert-warning alert-dismissible';
        $icon = '<i class="icon fa fa-warning"></i>';
        break;
      default:
        # code here
        break;
    }

    $output .= "<div class='box-body'>";
    $output .= "<div class=\"$class\">\n";
    if (!empty($status_heading[$type])) {
      $output .= '<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>';
      $output .= '<h2 class="element-invisible">' . $status_heading[$type] . "</h2>\n";
      //$output .= '<h4>' . $icon . ' ' . $status . '</h4>';
    }
    if (count($messages) > 1) {
      $output .= " <div>\n";
      foreach ($messages as $message) {
        $output .= '  <span>' . $icon . ' ' . $message . "</span>\n";
      }
      $output .= " </div>\n";
    }
    else {
      $output .= $icon;
      $output .= reset($messages);
    }
    $output .= "</div>";
    $output .= "</div>\n";
  }
  return $output;
}

/**
 * Implements preprocess_table__field_collection_table().
 */
function bms_adminlte_preprocess_table__field_collection_table(&$vars) {
  // Modify created > date
  if(arg(1) == 'request-item') {
    $vars['header'][0]['data'] = 'Date';
  }

  // Add a new (blank) header
  array_unshift($vars['header'], '&nbsp;');
  $vars['attributes']['class'][0] = 'views-table cols-24 table table-bordered table-striped dataTable';
  // Add an incremental count to each row
  $count = 1;
  foreach ($vars['rows'] as $key => &$row) {
    array_unshift($row['data'], $count++);
  }
}

/*
function bms_adminlte_preprocess_entity(&$variables) {
  $bundle = $variables['elements']['#entity']->type;
  if($bundle == 'commissary') {
    $variables['title'] = '';
    foreach($variables['theme_hook_suggestions'] as &$suggestion) {
      $suggestion = 'entity__' . $suggestion;
      dpm($suggestion);
    }
  }
}
*/

function bms_adminlte_table($variables) {
  $header = $variables['header'];
  $rows = $variables['rows'];
  $attributes = $variables['attributes'];
  $caption = $variables['caption'];
  $colgroups = $variables['colgroups'];
  $sticky = $variables['sticky'];
  $empty = $variables['empty'];

  // Add sticky headers, if applicable.
  if (count($header) && $sticky) {
    drupal_add_js('misc/tableheader.js');
    // Add 'sticky-enabled' class to the table to identify it for JS.
    // This is needed to target tables constructed by this function.
    $attributes['class'][] = 'sticky-enabled';
  }

  if(current_path() == 'manage-employee' || current_path() == 'administer-employee') {
    $attributes = $variables['attributes'] = array('class' => 'cols-24 table table-bordered table-striped dataTable');
  }

  $output = '<table' . drupal_attributes($attributes) . ">\n";

  if (isset($caption)) {
    $output .= '<caption>' . $caption . "</caption>\n";
  }

  // Format the table columns:
  if (count($colgroups)) {
    foreach ($colgroups as $number => $colgroup) {
      $attributes = array();

      // Check if we're dealing with a simple or complex column
      if (isset($colgroup['data'])) {
        foreach ($colgroup as $key => $value) {
          if ($key == 'data') {
            $cols = $value;
          }
          else {
            $attributes[$key] = $value;
          }
        }
      }
      else {
        $cols = $colgroup;
      }

      // Build colgroup
      if (is_array($cols) && count($cols)) {
        $output .= ' <colgroup' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cols as $col) {
          $output .= ' <col' . drupal_attributes($col) . ' />';
        }
        $output .= " </colgroup>\n";
      }
      else {
        $output .= ' <colgroup' . drupal_attributes($attributes) . " />\n";
      }
    }
  }

  // Add the 'empty' row message if available.
  if (!count($rows) && $empty) {
    $header_count = 0;
    foreach ($header as $header_cell) {
      if (is_array($header_cell)) {
        $header_count += isset($header_cell['colspan']) ? $header_cell['colspan'] : 1;
      }
      else {
        $header_count++;
      }
    }
    $rows[] = array(array('data' => $empty, 'colspan' => $header_count, 'class' => array('empty', 'message')));
  }

  // Format the table header:
  if (count($header)) {
    $ts = tablesort_init($header);
    // HTML requires that the thead tag has tr tags in it followed by tbody
    // tags. Using ternary operator to check and see if we have any rows.
    $output .= (count($rows) ? ' <thead><tr>' : ' <tr>');
    foreach ($header as $cell) {
      $cell = tablesort_header($cell, $header, $ts);
      $output .= _theme_table_cell($cell, TRUE);
    }
    // Using ternary operator to close the tags based on whether or not there are rows
    $output .= (count($rows) ? " </tr></thead>\n" : "</tr>\n");
  }
  else {
    $ts = array();
  }

  // Format the table rows:
  if (count($rows)) {
    $output .= "<tbody>\n";
    $flip = array('even' => 'odd', 'odd' => 'even');
    $class = 'even';
    foreach ($rows as $number => $row) {
      // Check if we're dealing with a simple or complex row
      if (isset($row['data'])) {
        $cells = $row['data'];
        $no_striping = isset($row['no_striping']) ? $row['no_striping'] : FALSE;

        // Set the attributes array and exclude 'data' and 'no_striping'.
        $attributes = $row;
        unset($attributes['data']);
        unset($attributes['no_striping']);
      }
      else {
        $cells = $row;
        $attributes = array();
        $no_striping = FALSE;
      }
      if (count($cells)) {
        // Add odd/even class
        if (!$no_striping) {
          $class = $flip[$class];
          $attributes['class'][] = $class;
        }

        // Build row
        $output .= ' <tr' . drupal_attributes($attributes) . '>';
        $i = 0;
        foreach ($cells as $cell) {
          $cell = tablesort_cell($cell, $header, $ts, $i++);
          $output .= _theme_table_cell($cell);
        }
        $output .= " </tr>\n";
      }
    }
    $output .= "</tbody>\n";
  }

  $output .= "</table>\n";
  return $output;
}

/**
 * Implement hook_preprocess_views_view_fields().
 */
function bms_adminlte_preprocess_views_view_fields(&$variables) {
  $view = $variables['view'];
  if ($view->name == 'manage_employee' && $view->current_display == 'manage_employee') {
    $variables['uid'] = $variables['fields']['uid']->raw;
    unset($variables['fields']['uid']);
  }
  elseif ($view->name == 'franchisee') {
    $store_id = $variables['row']->eck_store_field_data_field_store_franchised_id;
    $variables['inventory_settings_link'] = '/store/store/' . $store_id . '/inventory';
    $variables['product_settings_link'] = '/store/store/' . $store_id . '/products';
  }
}

/**
 * Implement hook_preprocess_views_view().
 */
function bms_adminlte_preprocess_views_view(&$variables) {
  global $user;

  $function_name = __FUNCTION__ . '__' . $variables['view']->name;
  if (function_exists($function_name)) {
    $function_name($variables);
  }
  $add_form_css = FALSE;
  $views_page = views_get_page_view();
  // Add variable checking for current views display
  if (is_object($variables['view'])) {
    $variables['current_view'] = $variables['view']->name;
  }
  if (is_object($views_page)) {
    switch ($variables['view']->current_display) {
      case 'page':
        if ($variables['view']->name == 'inventory') {
          $add_form_css = TRUE;
          if (is_array($user->roles) && in_array('admin', array_values($user->roles)) || is_array($user->roles) && in_array('operation', array_values($user->roles))) {
            $variables['is_admin'] = TRUE;
          }
        }
        elseif ($variables['view']->name == 'manage_product') {
          $add_form_css = TRUE;
          if (is_array($user->roles) && in_array('admin', array_values($user->roles)) || is_array($user->roles) && in_array('operation', array_values($user->roles))) {
            $variables['is_admin'] = TRUE;
          }
        }
        break;

      case 'page_1':
        $manage_employee_page = ($variables['view']->name == 'manage_employee');
        $attendance_page = ($variables['view']->name == 'attendance');
        $inventory_summary_page = ($variables['view']->name == 'inventory_summary');
        $production_summary_page = ($variables['view']->name == 'production_summary');
        $cash_management = ($variables['view']->name == 'cash_management');
        if ($manage_employee_page || $attendance_page || $inventory_summary_page || $production_summary_page || $cash_management) {
          $add_form_css = TRUE;
        }
        break;

      case 'page_2':
        if ($variables['view']->name == 'attendance' || $variables['view']->name == 'production_summary') {
          $add_form_css = TRUE;
        }
        break;

      case 'manage_attendance':
        if ($variables['view']->name == 'attendance') {
          $add_form_css = TRUE;
        }
        break;

      case 'attendance_report_summary':
        $add_form_css = TRUE;
        break;

      case 'store_settings':
        $add_form_css = TRUE;
        break;

      case 'manage_employee':
        $add_form_css = TRUE;
        if (is_array($user->roles) && in_array('administrator', array_values($user->roles))) {
          $variables['is_admin'] = TRUE;
        }
        break;

      case 'administer-employee':
        if (is_array($user->roles) && in_array('administrator', array_values($user->roles))) {
          $variables['is_admin'] = TRUE;
        }
        break;

      case 'manage_employee_franchisee':
        if (is_array($user->roles) && in_array('operation', array_values($user->roles)) || is_array($user->roles) && in_array('franchisee', array_values($user->roles))) {
          $variables['is_admin'] = TRUE;
        }
        $add_form_css = TRUE;
        break;

      case 'inventory_settings':
        if (is_array($user->roles) && in_array('operation', array_values($user->roles))) {
          $variables['is_admin'] = TRUE;
        }
        break;

      case 'inventory_summary':
        $add_form_css = TRUE;
        break;

      case 'product_settings':
        if (is_array($user->roles) && in_array('operation', array_values($user->roles))) {
          $variables['is_admin'] = TRUE;
        }
        break;

      case 'production_summary':
        $add_form_css = TRUE;
        break;

      case 'cash_management':
        $add_form_css = TRUE;
        break;

      default:
        break;
    }
  }

  // Enable order request in operation and franchisee
  if ($variables['view']->current_display == 'commissary_order_request' || $variables['view']->current_display == 'store_pullout_status') {
    if ($variables['view']->name == 'commissary_request' || $variables['view']->name == 'commissary_pull_out') {
      if (is_array($user->roles) && in_array('operation', array_values($user->roles)) || is_array($user->roles) && in_array('franchisee', array_values($user->roles))) {
        $variables['is_admin'] = TRUE;
      }
    }
  }
  // Enable store transfer in operation and franchisee
  if ($variables['view']->current_display == 'store_transfer_in' || $variables['view']->current_display == 'store_transfer_out') {
    if ($variables['view']->name == 'store_transfer_trans_in' || $variables['view']->name == 'store_transfer_trans_out') {
      if (is_array($user->roles) && in_array('operation', array_values($user->roles)) || is_array($user->roles) && in_array('franchisee', array_values($user->roles))) {
        $variables['is_admin'] = TRUE;
      }
    }
  }

  if ($variables['view']->current_display == 'commissary_order_reports' || $variables['view']->current_display == 'commissary_pullout_reports') {
    $variables['is_admin'] = TRUE;
  }
  if ($variables['view']->current_display == 'store_transfer_in' || $variables['view']->current_display == 'store_transfer_out') {
    $variables['is_admin'] = TRUE;
  }

  if ($variables['view']->name == 'commissary_request' && $variables['view']->current_display == 'page') {
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="/commissary/order/store/all"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($variables['view']->name == 'commissary_pull_out' && $variables['view']->current_display == 'page') {
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="/commissary/pullout/store/all"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($variables['view']->name == 'by_system_wide' && $variables['view']->current_display == 'page') {
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="/product-sales/by-system-wide/category/all"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($variables['view']->name == 'store_transfer_trans_out' && $variables['view']->current_display == 'store_transfer_out_store') {
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="/store-transfer/transfer-out-request/store/all"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($variables['view']->name == 'store_transfer_trans_in' && $variables['view']->current_display == 'store_transfer_in_store') {
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="/store-transfer/transfer-in-request/store/all"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($variables['view']->name == 'by_system_wide' && $variables['view']->current_display == 'product_sales_by_system_wide') {
    $all_category_id = arg(2);
    $all_link = '/product-sales/by-system-wide/product/category/' . $all_category_id . '/all';
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="' . $all_link . '"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  // Product Sales By Store
  if ($variables['view']->name == 'by_system_wide' && $variables['view']->current_display == 'by_store') {
    $all_store_id = arg(2);
    $all_link = '/product-sales/store/' . $all_store_id . '/all';
    $variables['rows'] = '<div class="views-row"><div class="views-field-title"><span class="field-content"><a href="' . $all_link . '"><span>All</span></a></span></div></div>' . $variables['rows'];
  }
  if ($add_form_css == TRUE) {
    $form_css = drupal_get_path('theme', 'bms_adminlte') . '/assets/css/components/forms-modals.css';
    drupal_add_css($form_css,
      array(
        'group' => CSS_THEME,
        'weight' => '9998',
      )
    );
  }
}

/**
 * Convert time to AM/PM format.
 */
function _bms_adminlte_ampm($time) {
  $formatted_time = date("h:i A", strtotime($time));
  return $formatted_time;
}

/**
 * Convert time to 12hrs without AM/PM format.
 */
function _bms_adminlte_w_ampm($time) {
  $formatted_time = date("h:i", strtotime($time));
  return $formatted_time;
}


/**
 * Return Category Name.
 */
function _bms_adminlte_get_category_name($item_id) {
  $item_details = entity_load_single('store', $item_id);
  $item_tid = $item_details->field_category['und'][0]['tid'];
  $item_category = taxonomy_term_load($item_tid);
  return $item_category->name;
}

/**
 * Implement theme_views_pre_render().
 */
function bms_adminlte_views_pre_render(&$view) {

  switch($view->name) {
    case 'cash_management':
      if(arg(2) == 'cash-management' || arg(2) == 'manage-cash-management') {
        // array of nodes to prevent duplicates
        $eids = array();
        // manipulated results
        $entity = array();
        // check each node of the result array on it's nid
        foreach($view->result as $key => $result) {
          $eid =  $result->field_collection_item_field_data_field_cash_management_vault;
          // if this node isn't a duplicate
          if (!in_array($eid, $eids)) {
            // add it to the manipulated results
            $entity[] = $view->result[$key];
            // mark this nid as in results to prevent duplicates from now on
            $eids[$key] = $eid;
          };
        }
        // replace the old results with the results without duplicates
        $view->result = $entity;
      }
      break;

    case 'attendance':
      if (arg(2) == 'attendance' || arg(2) == 'manage-attendance') {
        // Trim down not important fields
        $eids = array();
        foreach($view->result as $key => $result) {
          $eid = $result->field_collection_item_field_data_field_profile_item_id;
          if (!in_array($eid, $eids)) {
            $eids[$key] = $eid;
          }
          else {
            $result->field_field_store_name = array();
            $result->field_field_store_code = array();
            $result->field_field_img_time_in = array();
            $result->field_field_actual_duty_time_in = array();
            $result->field_field_actual_duty_time_out = array();
            $result->field_field_img_time_out = array();
            $result->field_field_username = array();
            $result->field_field_employee_no  = array();
            $result->field_field_assigned_time_in  = array();
            $result->field_field_assigned_time_out = array();
            $result->field_field_no_hours_assigned  = array();
            $result->field_created = array();
            $result->field_field_ot_request = array();
            $result->field_field_ut_request = array();
            $result->field_field_expected_timeout  = array();
            $result->field_field_total_hours_assigned  = array();
            $result->field_field_timein  = array();
            $result->field_field_timeout = array();
            $result->field_field_no_of_hours_rendered  = array();
            $result->field_field_late  = array();
            $result->field_field_ot  = array();
            $result->field_field_ut = array();
            $result->field_field_total_hours_rendered = array();
            $result->field_field_variance = array();
            $result->attendance_total_breaks_field = array();
            $result->field_field_freezer_temp = array();
            $result->field_field_actual_duty_rendered = array();
          }
        }
      }
      if ($view->current_display == 'attendance_report_summary' || $view->current_display == 'manage_attendance') {
        foreach ($view->result as $key => $row) {
          // Assigned Duty.
          if(!empty($row->field_field_assigned_time_in[0]['raw']['value'])) {
            $field_field_assigned_time_in = $row->field_field_assigned_time_in[0]['raw']['value'];
            $view->result[$key]->field_field_assigned_time_in[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_assigned_time_in);
          }

          if(!empty($row->field_field_assigned_time_out[0]['raw']['value'])) {
            $field_field_assigned_time_out = $row->field_field_assigned_time_out[0]['raw']['value'];
            $view->result[$key]->field_field_assigned_time_out[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_assigned_time_out);
          }
          /*
          if(!empty($row->field_field_no_hours_assigned[0]['raw']['value'])) {
            $no_hours_assigned = explode(':', $row->field_field_no_hours_assigned[0]['raw']['value']);
            if($no_hours_assigned[0] > 0) {
              $field_field_no_hours_assigned = $row->field_field_no_hours_assigned[0]['raw']['value'];
              $view->result[$key]->field_field_no_hours_assigned[0]['rendered']['#markup'] = _bms_adminlte_w_ampm($field_field_no_hours_assigned);
            }
            else {
              $field_field_no_hours_assigned = $row->field_field_no_hours_assigned[0]['raw']['value'];
              $view->result[$key]->field_field_no_hours_assigned[0]['rendered']['#markup'] = $field_field_no_hours_assigned;
            }
          }
          */
          if(!empty($row->field_field_expected_timeout[0]['raw']['value'])) {
            $field_field_expected_timeout = $row->field_field_expected_timeout[0]['raw']['value'];
            $view->result[$key]->field_field_expected_timeout[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_expected_timeout);
          }

          if(!empty($row->field_field_total_hours_assigned[0]['raw']['value'])) {
              $field_field_total_hours_assigned = $row->field_field_total_hours_assigned[0]['raw']['value'];
              $view->result[$key]->field_field_total_hours_assigned[0]['rendered']['#markup'] = $field_field_total_hours_assigned;
          }
          // Actual Duty Rendered.
          if(!empty($row->field_field_actual_duty_time_in[0]['raw']['value'])) {
            $field_field_actual_duty_time_in = $row->field_field_actual_duty_time_in[0]['raw']['value'];
            $view->result[$key]->field_field_actual_duty_time_in[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_actual_duty_time_in);
          }

          if(!empty($row->field_field_actual_duty_time_out[0]['raw']['value'])) {
            $field_field_actual_duty_time_out = $row->field_field_actual_duty_time_out[0]['raw']['value'];
            $view->result[$key]->field_field_actual_duty_time_out[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_actual_duty_time_out);
          }

          if(!empty($row->field_field_no_of_hours_rendered[0]['raw']['value'])) {
            $no_of_hours_rendered = explode(':', $row->field_field_no_of_hours_rendered[0]['raw']['value']);
            if($no_of_hours_rendered[0] > 0) {
              $field_field_no_of_hours_rendered = $row->field_field_no_of_hours_rendered[0]['raw']['value'];
              $view->result[$key]->field_field_no_of_hours_rendered[0]['rendered']['#markup'] = _bms_adminlte_w_ampm($field_field_no_of_hours_rendered);
            }
            else {
              $field_field_no_of_hours_rendered = $row->field_field_no_of_hours_rendered[0]['raw']['value'];
              $view->result[$key]->field_field_no_of_hours_rendered[0]['rendered']['#markup'] = $field_field_no_of_hours_rendered;
            }
          }
          /*
          if(!empty($row->field_field_total_hours_rendered[0]['raw']['value'])) {
            $field_field_total_hours_rendered = $row->field_field_total_hours_rendered[0]['raw']['value'];
            $view->result[$key]->field_field_total_hours_rendered[0]['rendered']['#markup'] = _bms_adminlte_w_ampm($field_field_total_hours_rendered);
          }
          */
          if(!empty($row->field_field_variance[0]['raw']['value'])) {
            $variance = explode(':', $row->field_field_variance[0]['raw']['value']);
            if($variance[0] > 0) {
              $field_field_variance = $row->field_field_variance[0]['raw']['value'];
              $view->result[$key]->field_field_variance[0]['rendered']['#markup'] = _bms_adminlte_w_ampm($field_field_variance);
            }
            else {
              $field_field_variance = $row->field_field_variance[0]['raw']['value'];
              $view->result[$key]->field_field_variance[0]['rendered']['#markup'] = $field_field_variance;
            }
          }

          // Break/Freezer.
          if(!empty($row->field_field_attendance_break_in[0]['raw']['value'])) {
            $field_field_attendance_break_in = $row->field_field_attendance_break_in[0]['raw']['value'];
            $view->result[$key]->field_field_attendance_break_in[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_attendance_break_in);
          }

          if(!empty($row->field_field_attendance_break_out[0]['raw']['value'])) {
            $field_field_attendance_break_out = $row->field_field_attendance_break_out[0]['raw']['value'];
            $view->result[$key]->field_field_attendance_break_out[0]['rendered']['#markup'] = _bms_adminlte_ampm($field_field_attendance_break_out);
          }
        }
      }

      break;

    case 'commissary':
      if ($view->current_display == 'commissary_order_reports') {
        /*
        foreach($view->result as $key => $result) {
          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id[0]['raw']['value']);
          $view->result[$key]->field_field_category[0]['rendered']['#markup'] = $cat_name;

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_1[0]['raw']['value']);
          $view->result[$key]->field_field_category_1[0]['rendered']['#markup'] = $cat_name;

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_2[0]['raw']['value']);
          $view->result[$key]->field_field_category_2[0]['rendered']['#markup'] = $cat_name;
        }
        */
      }
      break;
    case 'production_summary':
      /*
      if ($view->current_display == 'manage_production') {
        foreach($view->result as $key => $result) {
          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id[0]['raw']['value']);
          $view->result[$key]->field_field_category[0]['rendered']['#markup'] = $cat_name;

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_1[0]['raw']['value']);
          $view->result[$key]->field_field_category_1[0]['rendered']['#markup'] = t($cat_name);

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_2[0]['raw']['value']);
          $view->result[$key]->field_field_category_2[0]['rendered']['#markup'] = t($cat_name);

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_3[0]['raw']['value']);
          $view->result[$key]->field_field_category_3[0]['rendered']['#markup'] = t($cat_name);

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_4[0]['raw']['value']);
          $view->result[$key]->field_field_category_4[0]['rendered']['#markup'] = t($cat_name);

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_5[0]['raw']['value']);
          $view->result[$key]->field_field_category_5[0]['rendered']['#markup'] = t($cat_name);

          $cat_name = _bms_adminlte_get_category_name($result->field_field_item_id_6[0]['raw']['value']);
          $view->result[$key]->field_field_category_6[0]['rendered']['#markup'] = t($cat_name);
        }
      }
      */
      break;

    case 'sales_by_store':
      if ($view->current_display == 'day_sales') {
      }
      break;

    case 'work_schedule':
        foreach($view->result as $key => $row) {
          if(isset($row->field_field_schedule_time_in[0]['raw']['value'])) {
            $view->result[$key]->field_field_schedule_time_in[0]['rendered']['#markup'] = _bms_adminlte_ampm($row->field_field_schedule_time_in[0]['rendered']['#markup']);
          }
          if(isset($row->field_field_schedule_time_out[0]['raw']['value'])) {
            $view->result[$key]->field_field_schedule_time_out[0]['rendered']['#markup'] = _bms_adminlte_ampm($row->field_field_schedule_time_out[0]['rendered']['#markup']);
          }
          if(isset($row->field_field_expected_timeout[0]['raw']['value'])) {
            $view->result[$key]->field_field_expected_timeout[0]['rendered']['#markup'] = _bms_adminlte_ampm($row->field_field_expected_timeout[0]['rendered']['#markup']);
          }
        }
      break;

    case 'store_transfer_trans_in':
        $args = arg(3);
        if(isset($args) && $args != 'all') {
          // Unset specific store source.
          unset($view->field['field_store_name']);
        }
      break;
    case 'by_system_wide':
      if ($view->current_display == 'by_store') {
        $store_id = arg(2);
        foreach($view->result as $key => $item) {
          $category = $item->field_field_category[0]['raw']['tid'];
          $view->result[$key]->field_field_category[0]['rendered']['#href'] = '/product-sales/store/' . $store_id . '/category/' . $category;
        }
      }
      break;
    default:
      // Code here
      break;
  }
}
