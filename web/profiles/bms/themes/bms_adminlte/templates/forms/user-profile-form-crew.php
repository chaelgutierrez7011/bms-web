<section class="left fields-visible">
  <?php print render($form['picture']); ?>
  <input type="submit" id="edit-field-image-und-0-upload-button" name="field_image_und_0_upload_button" value="Upload" class="form-submit btn btn-block btn-primary">
</section>

<section class="center fields-visible">

  <h1 class="h1-bi">Basic Information</h1>

  <div class="fields-wrapper">
    <div class="field-row">
      <?php print render($form['profile_main']['field_firstname']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_lastname']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_address']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_mobile_number']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['mail']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_gender']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_birthdate']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_marital_status']); ?>
    </div>
  </div>
</section>

<section class="right fields-visible">
  <div class="fields-wrapper">
    <div class="field-row">
      <?php print render($form['account']['name']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['pass']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['roles']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['field_account_employee_id']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['profile_main']['field_date_of_hire']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['status']); ?>
    </div>
    <div class="field-row">
      <?php print render($form['account']['current_pass']); ?>
    </div>
  </div>
</section>

<div class="store-assignments">
  <?php print render($form['field_store']); ?>
</div>

<div class="hidden-container">
  <?php print render($form['profile_main']['field_store_franchised']); ?>
  <?php print render($form['profile_main']['field_my_employee']); ?>
  <?php print render($form['profile_main']['field_manage_schedule']); ?>
  <?php print render($form['timezone']); ?>
  <?php print render($form['field_full_name']); ?>
  <?php print render($form['actions']['cancel']); ?>
  <?php print render($form['profile_main']['field_end_of_contract']); ?>
</div>
