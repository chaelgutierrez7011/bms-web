<?php
/**
 * Style plugin to render each item as a column in a table.
 *
 * @ingroup views_style_plugins
 */
class views_advance_formatter_plugin_style_table extends views_plugin_style_table {
  function option_definition() {
    $options = parent::option_definition();

    $options['merge_table_all_table'] = array('default' => TRUE);

    return $options;
  }

  /**
   * Render the given style.
   */
  function options_form(&$form, &$form_state) {
    parent::options_form($form, $form_state);
    unset($form['grouping']);
    $form['merge_table_all_table'] = array(
      '#type' => 'checkbox',
      '#title' => t("Merge grouped field table into one"),
      '#default_value' => $this->options['merge_table_all_table'],
      '#description' => t("Merge grouping field table into one table."),
    );
  }
}
