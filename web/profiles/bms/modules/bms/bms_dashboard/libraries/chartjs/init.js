(function ($) {
  Drupal.behaviors.bmsDashboard = {
    attach: function (context, settings) {
    var pie = settings.bms_dashboard.pie;
    var productPie = settings.bms_dashboard.productPie;
    var peso_sales_by_hourly = settings.bms_dashboard.peso_sales_hourly;
    var peso_sales_by_weekly = settings.bms_dashboard.peso_sales_weekly;
    var peso_sales_by_monthly = settings.bms_dashboard.peso_sales_monthly;
    var peso_sales_by_yearly = settings.bms_dashboard.peso_sales_yearly;

    //-------------
    //- PIE CHART -
    //-------------
    // Get context with jQuery - using jQuery's .get() method.
    var pieChartCanvas = $("#pieChart").get(0).getContext("2d");
    var pieChart = new Chart(pieChartCanvas);
    var PieData = pie;

    var pieOptions = {
      //Boolean - Whether we should show a stroke on each segment
      //segmentShowStroke: true,
      //String - The colour of each segment stroke
      //segmentStrokeColor: "#fff",
      //Number - The width of each segment stroke
      segmentStrokeWidth: 0,
      //Number - The percentage of the chart that we cut out of the middle
      percentageInnerCutout: 50, // This is 0 for Pie charts
      //Number - Amount of animation steps
      animationSteps: 100,
      //String - Animation easing effect
      animationEasing: "easeOutBounce",
      //Boolean - Whether we animate the rotation of the Doughnut
      animateRotate: true,
      //Boolean - Whether we animate scaling the Doughnut from the centre
      animateScale: false,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true,
      // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
    };
    //Create pie or douhnut chart
    // You can switch between pie and douhnut using the method below.
    pieChart.Doughnut(PieData, pieOptions);

    // Generate piechart dynamically.
    productPie.forEach(function(item, key){
      //console.log(item['items']);
      var pieObj = {};
      var pieObjData = {};
      var pieObjOptions = {};

      pieObj["pieChart" + key] = new Chart($("#" + item.pieId).get(0).getContext("2d"));
      pieObjData["PieData" + key] = item['items'];

      pieObjOptions["pieData" + key] = {
        //Boolean - Whether we should show a stroke on each segment
        //segmentShowStroke: true,
        //String - The colour of each segment stroke
        //segmentStrokeColor: "#fff",
        //Number - The width of each segment stroke
        segmentStrokeWidth: 0,
        //Number - The percentage of the chart that we cut out of the middle
        percentageInnerCutout: 65, // This is 0 for Pie charts
        //Number - Amount of animation steps
        animationSteps: 100,
        //String - Animation easing effect
        animationEasing: "easeOutBounce",
        //Boolean - Whether we animate the rotation of the Doughnut
        animateRotate: true,
        //Boolean - Whether we animate scaling the Doughnut from the centre
        animateScale: false,
        //Boolean - whether to make the chart responsive to window resizing
        responsive: true,
        // Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
        maintainAspectRatio: true,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<segments.length; i++){%><li><span style=\"background-color:<%=segments[i].fillColor%>\"></span><%if(segments[i].label){%><%=segments[i].label%><%}%></li><%}%></ul>"
      }
      pieObj["pieChart" + key].Doughnut(pieObjData["PieData" + key], pieObjOptions["pieData" + key]);
    });

    /* jQueryKnob */

    $(".knob").knob({
      draw: function () {

        // "tron" case
        if (this.$.data('skin') == 'tron') {

          var a = this.angle(this.cv)  // Angle
              , sa = this.startAngle          // Previous start angle
              , sat = this.startAngle         // Start angle
              , ea                            // Previous end angle
              , eat = sat + a                 // End angle
              , r = true;

          this.g.lineWidth = this.lineWidth;

          this.o.cursor
          && (sat = eat - 0.3)
          && (eat = eat + 0.3);

          if (this.o.displayPrevious) {
            ea = this.startAngle + this.angle(this.value);
            this.o.cursor
            && (sa = ea - 0.3)
            && (ea = ea + 0.3);
            this.g.beginPath();
            this.g.strokeStyle = this.previousColor;
            this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sa, ea, false);
            this.g.stroke();
          }

          this.g.beginPath();
          this.g.strokeStyle = r ? this.o.fgColor : this.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth, sat, eat, false);
          this.g.stroke();

          this.g.lineWidth = 2;
          this.g.beginPath();
          this.g.strokeStyle = this.o.fgColor;
          this.g.arc(this.xy, this.xy, this.radius - this.lineWidth + 1 + this.lineWidth * 2 / 3, 0, 2 * Math.PI, false);
          this.g.stroke();

          return false;
        }
      }
    });
    /* END JQUERY KNOB */

    /*
     * BAR CHART WEEKLY
     * ---------
     */
      var byWeekBarData = {
        labels: peso_sales_by_weekly.label,
        datasets: [
          {
            label: "Digital Goods",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgb(0, 155, 82)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: peso_sales_by_weekly.data
          }
        ]
      };

      var byWeekbarChartCanvas = $("#bar-chart-weekly").get(0).getContext("2d");
      var byWeekbarChart = new Chart(byWeekbarChartCanvas);
      var byWeekbarChartData = byWeekBarData;

      byWeekbarChartData.datasets[0].fillColor = "#00a65a";
      var byWeekbarChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      byWeekbarChartOptions.datasetFill = false;
      byWeekbarChart.Bar(byWeekbarChartData, byWeekbarChartOptions);
    /* END BAR CHART WEEKLY*/

    /*
     * BAR CHART MONTHLY
     * ---------
     */
      var byMonthlyBarData = {
        labels: peso_sales_by_monthly.label,
        datasets: [
          {
            label: "Digital Goods",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgb(239, 148, 11)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: peso_sales_by_monthly.data
          }
        ]
      };

      var byMonthlybarChartCanvas = $("#bar-chart-monthly").get(0).getContext("2d");
      var byMonthlybarChart = new Chart(byMonthlybarChartCanvas);
      var byMonthlybarChartData = byMonthlyBarData;

      byMonthlybarChartData.datasets[0].fillColor = "#F09B15";
      var byMonthlybarChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      byMonthlybarChartOptions.datasetFill = false;
      byMonthlybarChart.Bar(byMonthlybarChartData, byMonthlybarChartOptions);
    /* END BAR CHART MONTHLY*/

    /*
     * BAR CHART YEARLY
     * ---------
     */
      var byYearlyBarData = {
        labels: peso_sales_by_yearly.label,
        datasets: [
          {
            label: "Digital Goods",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgb(221, 49, 39)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: peso_sales_by_yearly.data
          }
        ]
      };

      var byYearlybarChartCanvas = $("#bar-chart-yearly").get(0).getContext("2d");
      var byYearlybarChart = new Chart(byYearlybarChartCanvas);
      var byYearlybarChartData = byYearlyBarData;

      byYearlybarChartData.datasets[0].fillColor = "#DC443A";
      var byYearlybarChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      byYearlybarChartOptions.datasetFill = false;
      byYearlybarChart.Bar(byYearlybarChartData, byYearlybarChartOptions);
    /* END BAR CHART YEARLY*/

    /*
     * LINE CHART PESO SALES BY HOUR
     * ----------
     */
    var pesoSalesByHourChartData = {
      labels: peso_sales_by_hourly.hour_range,
      datasets: [
        {
          label: "Digital Goods",
          fillColor: "rgba(60,141,188,0.9)",
          strokeColor: "rgba(60,141,188,0.8)",
          pointColor: "#3b8bba",
          pointStrokeColor: "rgba(60,141,188,1)",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(60,141,188,1)",
          data: peso_sales_by_hourly.data
        }
      ]
    };

    var pesoSalesByHourChartOptions = {
      //Boolean - If we should show the scale at all
      showScale: true,
      //Boolean - Whether grid lines are shown across the chart
      scaleShowGridLines: false,
      //String - Colour of the grid lines
      scaleGridLineColor: "rgba(0,0,0,.05)",
      //Number - Width of the grid lines
      scaleGridLineWidth: 1,
      //Boolean - Whether to show horizontal lines (except X axis)
      scaleShowHorizontalLines: true,
      //Boolean - Whether to show vertical lines (except Y axis)
      scaleShowVerticalLines: true,
      //Boolean - Whether the line is curved between points
      bezierCurve: true,
      //Number - Tension of the bezier curve between points
      bezierCurveTension: 0.3,
      //Boolean - Whether to show a dot for each point
      pointDot: false,
      //Number - Radius of each point dot in pixels
      pointDotRadius: 4,
      //Number - Pixel width of point dot stroke
      pointDotStrokeWidth: 1,
      //Number - amount extra to add to the radius to cater for hit detection outside the drawn point
      pointHitDetectionRadius: 20,
      //Boolean - Whether to show a stroke for datasets
      datasetStroke: true,
      //Number - Pixel width of dataset stroke
      datasetStrokeWidth: 2,
      //Boolean - Whether to fill the dataset with a color
      datasetFill: true,
      //String - A legend template
      legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].lineColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
      //Boolean - whether to maintain the starting aspect ratio or not when responsive, if set to false, will take up entire container
      maintainAspectRatio: true,
      //Boolean - whether to make the chart responsive to window resizing
      responsive: true
    };

    //-------------
    //- LINE CHART -
    //--------------
    var pesoSalesByHourlineChartCanvas = $("#line-chart-by-hour").get(0).getContext("2d");
    var pesoSalesByHourlineChart = new Chart(pesoSalesByHourlineChartCanvas);
    var pesoSalesByHourlineChartOptions = pesoSalesByHourChartOptions;
    pesoSalesByHourlineChartOptions.datasetFill = false;
    pesoSalesByHourlineChart.Line(pesoSalesByHourChartData, pesoSalesByHourlineChartOptions);

    }
  };
})(jQuery);
