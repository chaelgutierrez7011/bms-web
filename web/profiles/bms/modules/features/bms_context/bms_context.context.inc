<?php
/**
 * @file
 * bms_context.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function bms_context_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'add_inventory_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'raw-materials/add' => 'raw-materials/add',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['add_inventory_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'add_product_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'product/add' => 'product/add',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['add_product_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'add_store_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/add' => 'store/add',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['add_store_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'calendar_edit_schedule_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-manage-schedule/*/edit' => 'field-collection/field-manage-schedule/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['calendar_edit_schedule_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'commissary_order_block';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'commissary_order_page' => 'commissary_order_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-commissary_request' => array(
          'module' => 'quicktabs',
          'delta' => 'commissary_request',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['commissary_order_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'commissary_order_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'commissary/order/store/*' => 'commissary/order/store/*',
        '~commissary/order/store/*/request' => '~commissary/order/store/*/request',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['commissary_order_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'commissary_pullout_block';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'commissary_pullout_page' => 'commissary_pullout_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-commissary_pullout' => array(
          'module' => 'quicktabs',
          'delta' => 'commissary_pullout',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['commissary_pullout_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'commissary_pullout_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'commissary/pullout/store/*' => 'commissary/pullout/store/*',
        '~commissary/pullout/store/*/request' => '~commissary/pullout/store/*/request',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['commissary_pullout_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_actual_duty_rendered_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-actual-duty-rendered/*/edit' => 'field-collection/field-actual-duty-rendered/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_actual_duty_rendered_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_break_freezer_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-break-freezer/*/edit' => 'field-collection/field-break-freezer/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_break_freezer_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_ending_inventory_actual_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-ending-inventory-actual/*/edit' => 'field-collection/field-ending-inventory-actual/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_ending_inventory_actual_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_inventory_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-inventory/*/edit' => 'field-collection/field-inventory/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_inventory_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_processed_opened_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-processed-opened/*/edit' => 'field-collection/field-processed-opened/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_processed_opened_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_product_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/structure/entity-type/store/product/*/edit' => 'admin/structure/entity-type/store/product/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_product_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'edit_wastages_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-wastages/*/edit' => 'field-collection/field-wastages/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['edit_wastages_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'front';
  $context->description = '';
  $context->tag = 'front';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        '<front>' => '<front>',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'user-login' => array(
          'module' => 'user',
          'delta' => 'login',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('front');
  $export['front'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'global';
  $context->description = '';
  $context->tag = 'global';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'menu-menu-bms-menu' => array(
          'module' => 'menu',
          'delta' => 'menu-bms-menu',
          'region' => 'sidebar_first',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('global');
  $export['global'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'inventory_settings_block';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'inventory_settings_page' => 'inventory_settings_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['inventory_settings_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'inventory_settings_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'inventory' => 'inventory',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['inventory_settings_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_sales';
  $context->description = '';
  $context->tag = 'sales';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'product-sales/store/*/product-items/*/by-store/*' => 'product-sales/store/*/product-items/*/by-store/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-prod' => array(
          'module' => 'quicktabs',
          'delta' => 'prod',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sales');
  $export['product_sales'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'product_sales_by_system_wide';
  $context->description = '';
  $context->tag = 'sales';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'product-sales/system-wide/component-item/*' => 'product-sales/system-wide/component-item/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'quicktabs-product_sales_by_system_wide' => array(
          'module' => 'quicktabs',
          'delta' => 'product_sales_by_system_wide',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('sales');
  $export['product_sales_by_system_wide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_cash_sales_inner_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'reports_cash_sales_inner_page' => 'reports_cash_sales_inner_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['reports_cash_sales_inner_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_cash_sales_inner_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/cash-management' => 'store/*/cash-management',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['reports_cash_sales_inner_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_inventory_inner_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'reports_inventory_inner_page' => 'reports_inventory_inner_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['reports_inventory_inner_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_inventory_inner_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/inventory-summary' => 'store/*/inventory-summary',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['reports_inventory_inner_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_production_inner_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'reports_production_inner_page' => 'reports_production_inner_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['reports_production_inner_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'reports_production_inner_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/production-summary' => 'store/*/production-summary',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['reports_production_inner_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_cash_sales_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'store_operations_mgmt_manage_cash_sales_page' => 'store_operations_mgmt_manage_cash_sales_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['store_operations_mgmt_manage_cash_sales_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_cash_sales_modals';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-cash-management-deposit/*/edit' => 'field-collection/field-cash-management-deposit/*/edit',
        'field-collection/field-cash-management-pullout/*/edit' => 'field-collection/field-cash-management-pullout/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_cash_sales_modals'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_cash_sales_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/manage-cash-management' => 'store/*/manage-cash-management',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_cash_sales_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_beginning_inventory_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-beginning-inventory/*/edit' => 'field-collection/field-beginning-inventory/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_beginning_inventory_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'store_operations_mgmt_manage_inventory_page' => 'store_operations_mgmt_manage_inventory_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['store_operations_mgmt_manage_inventory_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_commissary_delivery_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-commissary-delivery/*/edit' => 'field-collection/field-commissary-delivery/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_commissary_delivery_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_commissary_pullout_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-commissary-pullout/*/edit' => 'field-collection/field-commissary-pullout/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_commissary_pullout_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/manage-inventory' => 'store/*/manage-inventory',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_production_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-production-processed-open/*/edit' => 'field-collection/field-production-processed-open/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_production_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_store_transin_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-store-transin/*/edit' => 'field-collection/field-store-transin/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_store_transin_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_inventory_store_transout_edit';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'field-collection/field-store-transout/*/edit' => 'field-collection/field-store-transout/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_inventory_store_transout_edit'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_production_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'store_operations_mgmt_manage_production_page' => 'store_operations_mgmt_manage_production_page',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['store_operations_mgmt_manage_production_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_operations_mgmt_manage_production_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/manage-production' => 'store/*/manage-production',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_operations_mgmt_manage_production_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_profile_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'store_profile_page' => 'store_profile_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bms_visualization-bar_graph_monthly_performance' => array(
          'module' => 'bms_visualization',
          'delta' => 'bar_graph_monthly_performance',
          'region' => 'content',
          'weight' => '9',
        ),
        'views-my_store_employee-block' => array(
          'module' => 'views',
          'delta' => 'my_store_employee-block',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['store_profile_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'store_profile_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'store/*/view' => 'store/*/view',
        'store/store/*' => 'store/store/*',
        '~store/store/*/edit' => '~store/store/*/edit',
        '~store/store/*/inventory' => '~store/store/*/inventory',
        '~store/store/*/products' => '~store/store/*/products',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['store_profile_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_edit_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'user/*/edit' => 'user/*/edit',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['user_edit_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_profile_blocks';
  $context->description = '';
  $context->tag = 'Blocks';
  $context->conditions = array(
    'context' => array(
      'values' => array(
        'user_profile_page' => 'user_profile_page',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bms_visualization-monthly_performance' => array(
          'module' => 'bms_visualization',
          'delta' => 'monthly_performance',
          'region' => 'bottom_content',
          'weight' => '-10',
        ),
        'bms_others-store_assignments_block' => array(
          'module' => 'bms_others',
          'delta' => 'store_assignments_block',
          'region' => 'bottom_content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Blocks');
  $export['user_profile_blocks'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_profile_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'user_page' => array(
      'values' => array(
        'view' => 'view',
      ),
      'options' => array(
        'mode' => 'all',
      ),
    ),
  );
  $context->reactions = array();
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['user_profile_page'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'user_register_page';
  $context->description = '';
  $context->tag = 'Location';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'admin/people/create' => 'admin/people/create',
      ),
    ),
  );
  $context->reactions = array(
    'active_theme' => array(
      'theme' => 'bms_adminlte',
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Location');
  $export['user_register_page'] = $context;

  return $export;
}
