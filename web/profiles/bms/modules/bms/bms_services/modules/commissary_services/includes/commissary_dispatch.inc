<?php

/**
 * Implement commissary_services_dispatch().
 */
function commissary_services_dispatch($id, $data) {
  module_load_include('inc', 'bms_services', 'resources/store');
  $created = _datetime_format_UTC($data['created']);
  
  $store = entity_load_single('store', $id);

  $query = db_query("SELECT ie.eid FROM inventory_entry ie 
  INNER JOIN commissary_pullout cd ON ie.eid = cd.eid 
  WHERE FROM_UNIXTIME(ie.timestamp, '%Y-%m-%d') = :date_filter AND store_id = :sid", 
  array(':date_filter' => date('Y-m-d', strtotime($created)), ':sid' => $id));

  $eid = $query->fetchColumn();

  if(!empty($eid)) {
    try {
      // If items is not empty it means that we need to populate it to complete data.
      if(!empty($data['items']) && empty($data['transaction_id'])) {
        foreach($data['items'] as $pullout) {
          $product = entity_load_single('store', $pullout['item_id']);
          $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

          $commissary_pullout = new stdClass;
          $commissary_pullout->timestamp = strtotime($created);
          $commissary_pullout->eid = $eid;
          $commissary_pullout->request_number = $data['request_number'];
          $commissary_pullout->transaction_number = $data['transaction_number'];
          $commissary_pullout->category = $category->name;
          $commissary_pullout->item_id = $pullout['item_id'];
          $commissary_pullout->item = $pullout['item_name'];
          $commissary_pullout->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
          $commissary_pullout->quantity = $pullout['quantity'];
          $commissary_pullout->unit_of_measurement = $pullout['item_uom'];
          $commissary_pullout->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
          $commissary_pullout->reason = $pullout['reason'];
          drupal_write_record('commissary_pullout', $commissary_pullout);
        }
        module_load_include('inc', 'bms_services', 'resources/store');
        // Record process status.
        record_ending_inventory_status($id, $created, 'order_pullout');
      }
      else {
        // Update the items
        if(!empty($data['transaction_id'])) {
          foreach($data['items'] as $delivery) {
            db_update('commissary_pullout')
            ->expression('quantity', 'quantity + :quantity', array(':quantity' => $delivery['quantity']))
            ->condition('item_id', $delivery['item_id'])
            ->condition('eid', $eid)
            ->execute();
          }
        }
      }
    }
    catch (Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }
  else {
    try {
      $record = new stdClass;
      $record->timestamp = strtotime($created);
      $record->store_id = $id;
      $record->store_name = $store->title;
      $user = user_load($data['created_by_id']);

      $profile = profile2_load_by_user($user, $type_name = NULL);
      
      if(isset($first_name) && isset($last_name)) {
        $first_name = $profile['main']->field_firstname['und'][0]['value'];
        $last_name = $profile['main']->field_lastname['und'][0]['value'];
        
        $fullname = $first_name . ' ' . $last_name;
      }
      else {
        $fullname = $user->name;
      }
      
      $record->crew_name = $fullname;

      drupal_write_record('inventory_entry', $record);

      foreach($data['items'] as $pullout) {
        $product = entity_load_single('store', $pullout['item_id']);
        $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

        $commissary_pullout = new stdClass;
        $commissary_pullout->timestamp = strtotime($created);
        $commissary_pullout->eid = $record->eid;
        $commissary_pullout->request_number = $data['request_number'];
        $commissary_pullout->transaction_number = $data['transaction_number'];
        $commissary_pullout->category = $category->name;
        $commissary_pullout->item_id = $pullout['item_id'];
        $commissary_pullout->item = $pullout['item_name'];
        $commissary_pullout->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
        $commissary_pullout->quantity = $pullout['quantity'];
        $commissary_pullout->unit_of_measurement = $pullout['item_uom'];
        $commissary_pullout->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
        $commissary_pullout->reason = isset($pullout['reason']) ? $pullout['reason'] : '-';
        drupal_write_record('commissary_pullout', $commissary_pullout);
      }
      module_load_include('inc', 'bms_services', 'resources/store');
      // Record process status.
      record_ending_inventory_status($id, $created, 'order_pullout');
    }
    catch (Exception $e) {
      return services_error("Something went wrong please contact your adminstrator.", 500, $e->getMessage());
    }
  }
}

/**
 * Bridge commissary dispatch order to theoretical inventory
 */
function theoretical_commissary_pullout_dispatch($id, $data) {
  $created = _datetime_format_UTC($data['created']);
  $store = entity_load_single('store', $id);

  $record = new stdClass;
  $record->timestamp = strtotime($created);
  $record->store_id = $id;
  $record->store_name = $store->title;
  $user = user_load($data['created_by_id']);

  $profile = profile2_load_by_user($user, $type_name = NULL);
  
  if(isset($first_name) && isset($last_name)) {
    $first_name = $profile['main']->field_firstname['und'][0]['value'];
    $last_name = $profile['main']->field_lastname['und'][0]['value'];

    $fullname = $first_name . ' ' . $last_name;
  }
  else {
    $fullname = $user->name;
  }
  
  $record->crew_name = $fullname;

  drupal_write_record('inventory_entry', $record);

  foreach($data['items'] as $pullout) {
    $product = entity_load_single('store', $pullout['item_id']);
    $category = taxonomy_term_load($product->field_category['und'][0]['tid']);

    $commissary_pullout = new stdClass;
    $commissary_pullout->timestamp = strtotime($created);
    $commissary_pullout->store_id = $id;
    $commissary_pullout->eid = $record->eid;
    $commissary_pullout->category = $category->name;
    $commissary_pullout->item_id = $pullout['item_id'];
    $commissary_pullout->item = $pullout['item_name'];
    $commissary_pullout->item_code = isset($product->field_item_code['und']) ? $product->field_item_code['und'][0]['value'] : '';
    $commissary_pullout->quantity = $pullout['quantity'];
    $commissary_pullout->unit_of_measurement = $pullout['item_uom'];
    $commissary_pullout->unit_price = isset($product->field_price_per_unit['und']) ? $product->field_price_per_unit['und'][0]['value'] : 0;
    $commissary_pullout->type = 'commissary_pullout';
    drupal_write_record('ending_inventory_theoretical', $commissary_pullout);
  }
}