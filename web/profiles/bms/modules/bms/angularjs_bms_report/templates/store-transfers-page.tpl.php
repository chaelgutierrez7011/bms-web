<div ng-app="starter" ng-controller="StoreTransferCtrl">
    <!-- Custom Tabs -->
    <div class="nav-tabs-custom">
      <ul class="nav nav-tabs">
        <li class="active"><a href="#tab_1" data-toggle="tab">Store Transfer In</a></li>
        <li><a href="#tab_2" data-toggle="tab">Store Transfer Out</a></li>
      </ul>
      <div class="tab-content">
        <div class="tab-pane active" id="tab_1">
          <div class="tab-navigations">
              <ul class="tabs-menu">
                  <li ng-class="{'active': activeTabOne == 0}"><a href="" ng-click="setActiveTabOne(0)">Transfer In Request</a></li>
                  <li ng-class="{'active': activeTabOne == 1}"><a href="" ng-click="setActiveTabOne(1)">Transfer In Delivery</a></li>
                  <li ng-class="{'active': activeTabOne == 2}"><a href="" ng-click="setActiveTabOne(2)">Variance</a></li>
              </ul>
          </div>
          <div class="tab-content">
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabOne === 0, 'tab-pane' : activeTabOne !== 0}" data-ng-init="setActiveTabOne(0)">
                  <div ng-hide="!tabs[0].isLoaded">
                    <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required />
                    <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                        <th>Time</th>
                        <th>Store Source</th>
                        <th>Requested By</th>
                        <th>Store Trans-In Request No.</th>
                        <th>Inventory Category</th>
                        <th>Inventory Item</th>
                        <th>Inventory Item Code</th>
                        <th>QTY</th>
                        <th>UOM</th>
                        <th>Unit Price</th>
                        <th>Total</th>
                        <th>Status</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="item in tabs[0].content">
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.created }}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_source}}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.requested_by }}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no }}</td>
                          <td>{{ item.category }}</td>
                          <td>{{ item.item }}</td>
                          <td>{{ item.item_code }}</td>
                          <td>{{ item.qty }}</td>
                          <td>{{ item.uom }}</td>
                          <td>{{ item.unit_price | currency:"₱":2}}</td>
                          <td>{{ item.total | currency:"₱":2}}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.status }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
                </div>
            </div>
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabOne === 1, 'tab-pane' : activeTabOne !== 1}">
                <div ng-hide="!tabs[1].isLoaded">
                  <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required />
                  <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                  <thead>
                      <th>Time</th>
                      <th>Received By</th>
                      <th>Store Trans-In Request No.</th>
                      <th>Store Source</th>
                      <th>Store Trans-Out No.</th>
                      <th>Inventory Category</th>
                      <th>Inventory Item</th>
                      <th>Inventory Item Code</th>
                      <th>QTY</th>
                      <th>UOM</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                      <th>Status</th>
                    </thead>
                    <tbody>
                      <tr ng-repeat="item in tabs[1].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.created }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.received_by }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_source}}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transout_no }}</td>
                        <td>{{ item.category }}</td>
                        <td>{{ item.item }}</td>
                        <td>{{ item.item_code }}</td>
                        <td>{{ item.qty }}</td>
                        <td>{{ item.uom }}</td>
                        <td>{{ item.unit_price | currency:"₱":2}}</td>
                        <td>{{ item.total | currency:"₱":2}}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.status }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
              </div>
            </div>
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabOne === 2, 'tab-pane' : activeTabOne !== 2}">
                <div ng-hide="!tabs[2].isLoaded">
                  <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required />
                  <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                  <thead>
                      <th>Store Trans-In Request No.</th>
                      <th>Store Trans-In No.</th>
                      <th>Inventory Category</th>
                      <th>Inventory Item</th>
                      <th>Inventory Item Code</th>
                      <th>QTY Variance</th>
                      <th>UOM</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                    </thead>
                    <tbody>
                      <tr ng-repeat="item in tabs[2].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no}}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transout_no }}</td>
                        <td>{{ item.category }}</td>
                        <td>{{ item.item }}</td>
                        <td>{{ item.item_code }}</td>
                        <td>{{ item.qty }}</td>
                        <td>{{ item.uom }}</td>
                        <td>{{ item.unit_price | currency:"₱":2}}</td>
                        <td>{{ item.qty * item.unit_price | currency:"₱":2}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.tab-pane -->
        <div class="tab-pane" id="tab_2">
          <div class="tab-navigations">
              <ul class="tabs-menu">
                  <li ng-class="{'active': activeTabTwo == 3}"><a href="" ng-click="setActiveTabTwo(3)">Transfer Out Request</a></li>
                  <li ng-class="{'active': activeTabTwo == 4}"><a href="" ng-click="setActiveTabTwo(4)">Transfer Out Delivery</a></li>
                  <li ng-class="{'active': activeTabTwo == 5}"><a href="" ng-click="setActiveTabTwo(5)">Variance</a></li>
              </ul>
          </div>
          <div class="tab-content">
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabTwo === 3, 'tab-pane' : activeTabTwo !== 3}" data-ng-init="setActiveTabTwo(3)">
                  <div ng-hide="!tabs[3].isLoaded">
                    <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required />
                    <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                        <th>Time</th>
                        <th>Requested By</th>
                        <th>Store Trans-Out Request No.</th>
                        <th>Receiving Store</th>
                        <th>Inventory Category</th>
                        <th>Inventory Item</th>
                        <th>Inventory Item Code</th>
                        <th>QTY</th>
                        <th>UOM</th>
                        <th>Unit Price</th>
                        <th>Total</th>
                        <th>Status</th>
                      </thead>
                      <tbody>
                        <tr ng-repeat="item in tabs[3].content">
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.created }}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.requested_by }}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no }}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.receiving_store }}</td>
                          <td>{{ item.category }}</td>
                          <td>{{ item.item }}</td>
                          <td>{{ item.item_code }}</td>
                          <td>{{ item.qty }}</td>
                          <td>{{ item.uom }}</td>
                          <td>{{ item.unit_price | currency:"₱":2}}</td>
                          <td>{{ item.total | currency:"₱":2}}</td>
                          <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.status }}</td>
                        </tr>
                      </tbody>
                    </table>
                  </div>
                  <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
                </div>
            </div>
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabTwo === 4, 'tab-pane' : activeTabTwo !== 4}">
                <div ng-hide="!tabs[4].isLoaded">
                  <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(4)" placeholder="yyyy-MM-dd" required />
                  <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                  <thead>
                      <th>Time</th>
                      <th>Received By</th>
                      <th>Trans-Out No.</th>
                      <th>Receiving Store</th>
                      <th>Store Trans-In Request No.</th>
                      <th>Inventory Category</th>
                      <th>Inventory Item</th>
                      <th>Inventory Item Code</th>
                      <th>QTY</th>
                      <th>UOM</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                      <th>Status</th>
                    </thead>
                    <tbody>
                      <tr ng-repeat="item in tabs[4].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.created }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.received_by }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_source}}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transout_no }}</td>
                        <td>{{ item.category }}</td>
                        <td>{{ item.item }}</td>
                        <td>{{ item.item_code }}</td>
                        <td>{{ item.qty }}</td>
                        <td>{{ item.uom }}</td>
                        <td>{{ item.unit_price | currency:"₱":2}}</td>
                        <td>{{ item.total | currency:"₱":2}}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.status }}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div ng-hide="tabs[4].isLoaded"><loading></loading></div>
              </div>
            </div>
            <div class="tab-content">
                <div ng-class="{'tab-pane active' : activeTabTwo === 5, 'tab-pane' : activeTabTwo !== 5}">
                <div ng-hide="!tabs[5].isLoaded">
                  <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(5)" placeholder="yyyy-MM-dd" required />
                  <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                  <thead>
                      <th>Request No</th>
                      <th>Store Trans-Out No.</th>
                      <th>Inventory Category</th>
                      <th>Inventory Item</th>
                      <th>Inventory Item Code</th>
                      <th>QTY Variance</th>
                      <th>UOM</th>
                      <th>Unit Price</th>
                      <th>Total</th>
                    </thead>
                    <tbody>
                      <tr ng-repeat="item in tabs[5].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.request_number }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.store_transin_request_no }}</td>
                        <td>{{ item.category }}</td>
                        <td>{{ item.item }}</td>
                        <td>{{ item.item_code }}</td>
                        <td>{{ item.qty }}</td>
                        <td>{{ item.uom }}</td>
                        <td>{{ item.unit_price | currency:"₱":2}}</td>
                        <td>{{ item.qty * item.unit_price | currency:"₱":2}}</td>
                      </tr>
                    </tbody>
                  </table>
                </div>
                <div ng-hide="tabs[5].isLoaded"><loading></loading></div>
                </div>
            </div>
          </div>
        </div>
        <!-- /.tab-pane -->
        </div>
      </div>
      <!-- /.tab-content -->
    </div>
    <!-- nav-tabs-custom -->
</div>