<?php print render($form['field_store_name']); ?>
<?php print render($form['title']); ?>

<?php print render($form['field_location']); ?>

<?php print render($form['field_company']); ?>
<?php print render($form['field_store_franchisee']); ?>
<?php print render($form['field_store_ownership']); ?>

<?php print render($form['field_type_of_store']); ?>
<?php print render($form['field_area']); ?>
<?php print render($form['field_contact_number']); ?>

<?php print render($form['field_store_code']); ?>
<?php print render($form['field_store_status']); ?>


<div class="hidden-container">
  <?php print render($form['field_bank_name']); ?>
  <?php print render($form['field_inventory']); ?>
  <?php print render($form['field_inventory_placeholder']); ?>
  <?php print render($form['field_my_employee']); ?>
  <?php print render($form['field_image']); ?>
</div>

<?php print drupal_render_children($form); ?>