<div id="manage-sales-transaction" ng-app="starter" ng-cloak ng-controller="ManageSalesTransactionCtrl">
<input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter()" placeholder="yyyy-MM-dd" required />
  <table datatable="ng" class="row-border hover" data-ng-init="manageSales()" dt-options="dtOptions">
    <thead>
      <tr>
        <th class="time-header">Time</th>
        <th class="crew-header">Crew</th>
        <th class="transaction-no-header">Transaction No.</th>
        <th class="or-header">OR #</th>
        <th class="gross-sales-header">Gross Sales</th>
        <th class="sc-discount-header">SC Discount</th>
        <th class="pwd-discount-header">PWD Discount</th>
        <th class="ee-discount-header">EE Discount</th>
        <th class="e-vat-header">E-VAT</th>
        <th class="net-sales-header">Net Sales</th>
        <th class="action-header">Actions</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="item in sales" data-item-number="{{item.transaction_id}}">
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.created }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.crew }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.transaction_no }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.or_no }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.gross_sales | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.senior_citizen_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.pwd_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.employee_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.e_vat | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'">{{ item.net_sales | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" class="sales-wrapper">
			<button class="btn btn-block btn-default" id="transaction-view" ng-click="showModal(item.transaction_id)">
				View
			</button>
			<button class="btn btn-block btn-default" id="transaction-void" ng-click="voidTransaction(item.transaction_id)">
				Void
			</button>
		</td>
      </tr>
    </tbody>
    <tfoot>
      <tr ng-repeat="item in total">
        <td>Total</td>
        <td></td>
        <td></td>
        <td></td>
        <td>{{ item.total_gross | currency:"₱":2 }}</td>
        <td>{{ item.total_sc_discount | currency:"₱":2}}</td>
        <td>{{ item.total_pwd_discount | currency:"₱":2}}</td>
        <td>{{ item.ee_discount | currency:"₱":2}}</td>
        <td>{{ item.total_evat | currency:"₱":2}}</td>
        <td>{{ item.total_net_sales | currency:"₱":2}}</td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</div>

