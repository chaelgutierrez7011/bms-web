var bmsServices = angular.module('bmsServices', ['ngResource']);
/*
bmsServices.factory('Commissary', ['$resource',
function($resource){
  return $resource('https://api.github.com/users/umandalroald/gists', {}, {
    query: {method: 'GET', isArray:true}
  });
}]);
*/
bmsServices.factory('ProductIds', ['$resource',
function($resource){
  return $resource('/api/store/:store_id/inventory', {store_id: '@store_id'}, {
    query: {method: 'GET', isArray:true}
  });
}]);

// Store Inventory Reports Simplified
bmsServices.factory('InventorySimplified', ['$resource', 
function($resource){
  return $resource('/store/:store_id/simplified-inventory/json/:selectedDate', {store_id: '@store_id', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
  })
}]);

// Store Inventory Reports
bmsServices.factory('Inventory', ['$resource', 
function($resource){
  return $resource('/store/:store_id/inventory/:tabToSet/json/:selectedDate/:tid/:eid', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', tid: '@tid', eid: '@eid'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
    update: {method: 'PUT'},
  })
}]);

// Store Production Reports Simplified
bmsServices.factory('ProductionSimplified', ['$resource', 
function($resource){
  return $resource('/store/:store_id/simplified-production/json/:selectedDate', {store_id: '@store_id', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
  })
}]);

// Manage Store Production Inventory
bmsServices.factory('Production', ['$resource', 
function($resource){
  return $resource('/store/:store_id/production/:tabToSet/json/:selectedDate/:tid', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', tid: '@tid'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
    update: {method: 'PUT'},
  })
}]);

bmsServices.factory('manageSalesTransaction', ['$resource', 
function($resource){
	return $resource(
		'/store/:store_id/sales-transaction/json/:selectedDate/:tid', 
		{
			store_id: '@store_id',
			selectedDate: '@selectedDate', 
			tid: '@tid'
		}, 
		{
    		query: {
				method: 'GET', 
				isArray: false
			},
    		update: {
				method: 'PUT'
			},
    		delete: {
				method: 'DELETE'
			}
  		}
	)
}]);

// Sales By Store
bmsServices.factory('SalesByStore', ['$resource', 
function($resource){
  return $resource('/store/:store_id/sales-by-store/:tabToSet/json/:selectedDate/:selectedDateEnd', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', selectedDateEnd: '@selectedDateEnd'}, {
    query: {method: 'GET', isArray:true},
  })
}]);

// Sales By Area
bmsServices.factory('SalesByArea', ['$resource', 
function($resource){
  return $resource('/store/:store_id/sales-by-area/:tabToSet/json/:selectedDate', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate'}, {
    query: {method: 'GET', isArray:true},
  })
}]);

// System Wide
bmsServices.factory('SalesBySystemWide', ['$resource', 
function($resource){
  return $resource('/owner/:owner_id/sales-by-system-wide/:tabToSet/json/:selectedDate/:ownership/:area', {owner_id: '@owner_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', ownership: '@ownership', area: '@area'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// System Wide

// Product By Store
bmsServices.factory('ProductSalesByStore', ['$resource', 
function($resource){
  return $resource('/store/:store_id/product-sales/:tabToSet/json/:selectedDate/:category', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', category: '@category'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Product By Store All
bmsServices.factory('ProductSalesByStoreAll', ['$resource', 
function($resource){
  return $resource('/store/:store_id/product-sales/:tabToSet/json/:selectedDate', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Product By System Wide All
bmsServices.factory('ProductSalesBySystemWideProductAll', ['$resource', 
function($resource){
  return $resource('/owner/:owner_id/product-sales-by-system-wide/:tabToSet/json/:selectedDate/:category', {owner_id: '@owner_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', category: '@category'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Product By SystemWide category All
bmsServices.factory('ProductSalesBySystemWideCategoryAll', ['$resource', 
function($resource){
  return $resource('/owner/:owner_id/by-system-wide-all/:tabToSet/json/:selectedDate', {owner_id: '@owner_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Product Sales System Wide
bmsServices.factory('ProductSalesBySystemWide', ['$resource', 
function($resource){
  return $resource('/owner/:owner_id/product-sales-by-system-wide/:tabToSet/json/:selectedDate/:product_id/:category', {owner_id: '@owner_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', product_id: '@product_id', category: '@category'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Store Transfer Reports
bmsServices.factory('StoreTransfer', ['$resource', 
function($resource){
  return $resource('/store/:store_id/store-transfers/:tabToSet/json/:selectedDate', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Commissary Reports
bmsServices.factory('Commissary', ['$resource', 
function($resource){
  return $resource('/store/:store_id/commissary/:tabToSet/json/:selectedDate', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray:true},
  })
}]);

// Cash Sales
bmsServices.factory('CashSales', ['$resource', 
function($resource){
  return $resource('/store/:store_id/cash-sales/:tabToSet/json/:selectedDate/:tid/:eid', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', tid: '@tid', eid: '@eid'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
    update: {method: 'PUT'},
  })
}]);

// Attendance
bmsServices.factory('Attendance', ['$resource', 
function($resource){
  return $resource('/store/:store_id/attendance/:tabToSet/json/:selectedDate/:eid', {store_id: '@store_id', tabToSet: '@tabToSet', selectedDate: '@selectedDate', eid: '@eid'}, {
    get: { method: 'GET'},
    query: {method: 'GET', isArray: true},
    update: {method: 'PUT'},
  })
}]);