(function ($, Drupal, window, document, undefined) {


// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.bms_core = {
  attach: function(context, settings) {
    var qty = $('#edit-field-qty-und-0-value');
    var unit_price = $('#edit-field-unit-price-und-0-value');

    $('#edit-field-qty-und-0-value, #edit-field-unit-price-und-0-value').change(function() {
      var total = qty.val() * unit_price.val();
      $('#edit-field-total-und-0-value').val(total);
    });
  }
};


}(jQuery, Drupal, this, this.document));
