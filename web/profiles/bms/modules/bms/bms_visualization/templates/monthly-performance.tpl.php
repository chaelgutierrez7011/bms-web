      <div class="row">
        <div class="box box-solid">
          <!-- /.box-header -->
          <div class="box-body">
            <div class="row">
              <div class="col-xs-3 col-sm-3 col-md-3 text-center">
                <span class="knob-box bg-light-blue"><?php print $data['late']; ?></span>
                <div class="knob-label">Lates <span>(h:m)</span></div>
              </div>
              <!-- ./col -->
              <div class="col-xs-3 col-sm-3 col-md-3 text-center">
                <span class="knob-box bg-green absent"><?php print $data['absent']; ?></span>
                <div class="knob-label">Absences <span>(days)</span></div>
              </div>
              <!-- ./col -->
              <div class="col-xs-3 col-sm-3 col-md-3 text-center">
                <span class="knob-box bg-yellow"><?php print $data['ut']; ?></span>
                <div class="knob-label">Undertime <span>(h:m)</span></div>
              </div>
              <!-- ./col -->
              <div class="col-xs-3 col-sm-3 col-md-3 text-center">
                <span class="knob-box bg-red"><?php print $data['ot']; ?></span>
                <div class="knob-label">Overtime <span>(h:m)</span></div>
              </div>
              <!-- ./col -->
            </div>
            <!-- /.row -->
          </div>
          <!-- /.box-body -->
        </div>
        <!-- /.box -->
      </div>
      <!-- /.row -->