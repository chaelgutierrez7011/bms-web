<?php
/**
 * @file
 * entity_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function entity_type_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_eck_bundle_info().
 */
function entity_type_eck_bundle_info() {
  $items = array(
    'store_attendance' => array(
      'machine_name' => 'store_attendance',
      'entity_type' => 'store',
      'name' => 'attendance',
      'label' => 'Attendance',
      'config' => array(),
    ),
    'store_cash_management' => array(
      'machine_name' => 'store_cash_management',
      'entity_type' => 'store',
      'name' => 'cash_management',
      'label' => 'Cash Management',
      'config' => array(),
    ),
    'store_cashier' => array(
      'machine_name' => 'store_cashier',
      'entity_type' => 'store',
      'name' => 'cashier',
      'label' => 'Cashier',
      'config' => array(),
    ),
    'store_cashier_items' => array(
      'machine_name' => 'store_cashier_items',
      'entity_type' => 'store',
      'name' => 'cashier_items',
      'label' => 'Cashier Items',
      'config' => array(),
    ),
    'store_commissary' => array(
      'machine_name' => 'store_commissary',
      'entity_type' => 'store',
      'name' => 'commissary',
      'label' => 'Commissary',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'uid' => 0,
          'created' => 0,
        ),
      ),
    ),
    'store_commissary_pullout' => array(
      'machine_name' => 'store_commissary_pullout',
      'entity_type' => 'store',
      'name' => 'commissary_pullout',
      'label' => 'Commissary Pullout',
      'config' => array(),
    ),
    'store_discount' => array(
      'machine_name' => 'store_discount',
      'entity_type' => 'store',
      'name' => 'discount',
      'label' => 'Discount',
      'config' => array(),
    ),
    'store_inventory' => array(
      'machine_name' => 'store_inventory',
      'entity_type' => 'store',
      'name' => 'inventory',
      'label' => 'Inventory',
      'config' => array(),
    ),
    'store_inventory_placeholder' => array(
      'machine_name' => 'store_inventory_placeholder',
      'entity_type' => 'store',
      'name' => 'inventory_placeholder',
      'label' => 'Inventory Placeholder',
      'config' => array(),
    ),
    'store_product' => array(
      'machine_name' => 'store_product',
      'entity_type' => 'store',
      'name' => 'product',
      'label' => 'Product',
      'config' => array(),
    ),
    'store_production' => array(
      'machine_name' => 'store_production',
      'entity_type' => 'store',
      'name' => 'production',
      'label' => 'Production',
      'config' => array(),
    ),
    'store_schedule' => array(
      'machine_name' => 'store_schedule',
      'entity_type' => 'store',
      'name' => 'schedule',
      'label' => 'Schedule',
      'config' => array(
        'managed_properties' => array(
          'title' => 'title',
          'uid' => 0,
          'created' => 0,
        ),
      ),
    ),
    'store_store' => array(
      'machine_name' => 'store_store',
      'entity_type' => 'store',
      'name' => 'store',
      'label' => 'Store',
      'config' => array(
        'managed_properties' => array(
          'title' => 0,
          'uid' => 0,
          'created' => 0,
        ),
      ),
    ),
    'store_store_transfer' => array(
      'machine_name' => 'store_store_transfer',
      'entity_type' => 'store',
      'name' => 'store_transfer',
      'label' => 'Store Transfer',
      'config' => array(),
    ),
    'store_store_transfer_pullout' => array(
      'machine_name' => 'store_store_transfer_pullout',
      'entity_type' => 'store',
      'name' => 'store_transfer_pullout',
      'label' => 'Store Transfer Pullout',
      'config' => array(),
    ),
  );
  return $items;
}

/**
 * Implements hook_eck_entity_type_info().
 */
function entity_type_eck_entity_type_info() {
  $items = array(
    'store' => array(
      'name' => 'store',
      'label' => 'Store',
      'properties' => array(
        'title' => array(
          'label' => 'Title',
          'type' => 'text',
          'behavior' => 'title',
        ),
        'uid' => array(
          'label' => 'Author',
          'type' => 'integer',
          'behavior' => 'author',
        ),
        'created' => array(
          'label' => 'Created',
          'type' => 'integer',
          'behavior' => 'created',
        ),
      ),
    ),
  );
  return $items;
}
