<section class="left fields-visible">
  <div class="photo-container">
    <img src="<?php print $user_image; ?>">
  </div>
</section>

<section class="center fields-visible">
  <?php if (isset($profile['field_firstname']) || isset($profile['field_lastname'])) : ?>
    <div class="field field-row">
      <div class="field-label">Full Name:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php print $profile['field_firstname'][0]['#markup']; ?>
          <?php print $profile['field_lastname'][0]['#markup']; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_address'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_address']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_mobile_number'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_mobile_number']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($elements['#account']->mail)) : ?>
    <div class="field field-row">
      <div class="field-label">Email:</div>
      <div class="field-items">
        <div class="field-item even email-word">
          <?php print $elements['#account']->mail; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_gender'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_gender']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_birthdate'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_birthdate']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_marital_status'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_marital_status']); ?>
    </div>
  <?php endif; ?>

</section>

<section class="right fields-visible ">

  <?php if (isset($elements['#account']->name)) : ?>
    <div class="field field-row">
      <div class="field-label">Username:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php print $elements['#account']->name; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($elements['#account']->roles)) : ?>
    <div class="field field-row">
      <div class="field-label">Position:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php foreach ($elements['#account']->roles as $role_key => $role): ?>
            <?php if ($role != 'authenticated user'): ?>
              <?php print $role; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($user_profile['field_account_employee_id'])) : ?>
    <div class="field-row">
      <?php print render($user_profile['field_account_employee_id']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_date_of_hire'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_date_of_hire']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_end_of_contract'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_end_of_contract']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($user_profile['field_store'])) : ?>
    <div class="field-row">
      <?php hide($user_profile['field_store']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($employee_status)) : ?>
    <div class="field field-row">
      <div class="field-label">Status</div>
      <div class="field-items">
        <div class="field-item even">
          <?php print $employee_status; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

</section>
