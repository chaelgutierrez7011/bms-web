<?php

/**
 * @file
 * block--store-assignments.tpl.php
 * Template for Store Assignments block.
 */

?>

<h2>Store Assignments</h2>

<?php if (isset($stores)) : ?>
  <div class="stores-wrapper">
    <?php foreach ($stores as $key => $store) : ?>
      <div class="store-item">
        <span><?php print $store; ?></span>
      </div>
    <?php endforeach; ?>
  </div>
<?php endif; ?>

  
  