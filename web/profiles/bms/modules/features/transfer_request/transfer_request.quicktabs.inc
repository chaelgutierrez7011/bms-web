<?php
/**
 * @file
 * transfer_request.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function transfer_request_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'commissary_pullout';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Commissary PullOut';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'commissary_pull_out',
      'display' => 'store_pullout_status',
      'args' => '%3',
      'title' => 'Store Pull-Out Status',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'commissary_pull_out',
      'display' => 'commissary_pullout_status',
      'args' => '%3',
      'title' => 'Commissary Pull-Out Status',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Commissary Pull-Out Status');
  t('Commissary PullOut');
  t('Store Pull-Out Status');

  $export['commissary_pullout'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'commissary_request';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Commissary Request';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'commissary_request',
      'display' => 'commissary_order_request',
      'args' => '%3',
      'title' => 'Store Order Status',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'commissary_request',
      'display' => 'commissary_order_status',
      'args' => '%3',
      'title' => 'Commissary Order Status',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Commissary Order Status');
  t('Commissary Request');
  t('Store Order Status');

  $export['commissary_request'] = $quicktabs;

  return $export;
}
