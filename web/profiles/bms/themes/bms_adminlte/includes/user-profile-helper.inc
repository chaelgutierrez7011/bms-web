<?php

/**
 * Helper function to retrieve the Full name of a user
 *
 * @param $username
 *   The given username of a user
 * 
 * @return <Full Name>
 *   If retrieval is successfull
 * @return <username>
 *   fallback to username if user detail is not possible to retrieve
 */
function get_user_details($username) {
  $lang = LANGUAGE_NONE;

  $user_details = user_load_by_name($username);
  
  if (!module_exists('profile2')) {
    // fall back to username if profile detail is not possible to retrieve
    return $username;
  }

  $profile = profile2_by_uid_load($user_details->uid, $type_name = NULL);

  if (isset($profile['main']->field_firstname[$lang])) {
    $firstname = $profile['main']->field_firstname[$lang][0]['safe_value'];
  }

  if (isset($profile['main']->field_lastname[$lang])) {
    $lastname = $profile['main']->field_lastname[$lang][0]['safe_value'];
  }

  // make a fall back here as well if the first name
  // and last name are just blank
  if ($firstname == '' && $lastname == '') {
    return $username;
  }
  else {
    return $firstname . ' ' . $lastname;
  }
}

