<?php
/**
 * @file
 * bms_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function bms_roles_user_default_roles() {
  $roles = array();

  // Exported role: accounting.
  $roles['accounting'] = array(
    'name' => 'accounting',
    'weight' => 6,
    'machine_name' => 'accounting',
  );

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
    'machine_name' => 'administrator',
  );

  // Exported role: commissary.
  $roles['commissary'] = array(
    'name' => 'commissary',
    'weight' => 7,
    'machine_name' => 'commissary',
  );

  // Exported role: crew.
  $roles['crew'] = array(
    'name' => 'crew',
    'weight' => 3,
    'machine_name' => 'crew',
  );

  // Exported role: franchisee.
  $roles['franchisee'] = array(
    'name' => 'franchisee',
    'weight' => 11,
    'machine_name' => 'franchisee',
  );

  // Exported role: general manager.
  $roles['general manager'] = array(
    'name' => 'general manager',
    'weight' => 9,
    'machine_name' => 'general_manager',
  );

  // Exported role: hr.
  $roles['hr'] = array(
    'name' => 'hr',
    'weight' => 5,
    'machine_name' => 'hr',
  );

  // Exported role: maintenance.
  $roles['maintenance'] = array(
    'name' => 'maintenance',
    'weight' => 12,
    'machine_name' => 'maintenance',
  );

  // Exported role: operation.
  $roles['operation'] = array(
    'name' => 'operation',
    'weight' => 8,
    'machine_name' => 'operation',
  );

  return $roles;
}
