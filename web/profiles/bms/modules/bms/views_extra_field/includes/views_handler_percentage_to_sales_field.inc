<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_percentage_to_sales_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {
    $date = explode(' ', $values->field_field_created[0]['raw']['value']);
    $sid = $values->_field_data['field_reference_store_id_eck_store_id']['entity']->field_store_id['und'][0]['value'];
    return $this->get_percentage_to_sales($date[0], $sid);
  }

  /**
   * Callback handler
   * Return percentage to sales
   */
  function get_percentage_to_sales($date, $sid) {
    $current_view = arg(0);
    $area = arg(1);

    switch ($current_view) {
      case 'sales-by-area':

        $sub_query = db_query("SELECT 
        SUM(wv.field_total_with_vat_value)AS Amount 
        FROM eck_store eck 
        INNER JOIN field_data_field_total_with_vat wv 
        ON wv.entity_id = eck.id 
        INNER JOIN field_data_field_created fc 
        ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si 
        ON si.entity_id = eck.id
        INNER JOIN field_data_field_area fa
        ON si.field_reference_store_id_target_id = fa.entity_id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')
        AND (field_area_tid = $area)");  

        $get_total = $sub_query->fetchField();
        
        $result = db_query("SELECT 
        CONCAT(ROUND(SUM(wv.field_total_with_vat_value) / $get_total * 100, 2), '%') AS Amount 
        FROM eck_store eck 
        INNER JOIN field_data_field_total_with_vat wv 
        ON wv.entity_id = eck.id 
        INNER JOIN field_data_field_created fc 
        ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si 
        ON si.entity_id = eck.id 
        WHERE (field_created_value LIKE '%$date%')
        AND (field_reference_store_id_target_id = :sid)", array(
          ':sid' => $sid
        ));

        break;
      case 'sales-by-system-wide':
        $sub_query = db_query("SELECT 
        SUM(wv.field_total_with_vat_value)AS Amount 
        FROM eck_store eck 
        INNER JOIN field_data_field_total_with_vat wv 
        ON wv.entity_id = eck.id 
        INNER JOIN field_data_field_created fc 
        ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si 
        ON si.entity_id = eck.id
        INNER JOIN field_data_field_area fa
        ON si.field_reference_store_id_target_id = fa.entity_id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')");  

        $get_total = $sub_query->fetchField();

        // By day
        $result = db_query("SELECT
        CONCAT(ROUND(SUM(wv.field_total_with_vat_value) / $get_total * 100, 2), '%')
        FROM
        {eck_store} eck
        INNER JOIN {field_data_field_reference_store_id} si ON si.entity_id = eck.id
        INNER JOIN field_data_field_created fc ON fc.entity_id = eck.id
        INNER JOIN field_data_field_total_with_vat wv ON wv.entity_id = eck.id
        WHERE  (type = 'cashier')
        AND field_created_value LIKE '%$date%'
        AND field_reference_store_id_target_id = :sid", array(
          ':sid' => $sid));
        break;
      case 'sales-by-store':
        $sub_query = db_query("SELECT 
        SUM(wv.field_total_with_vat_value)AS Amount 
        FROM eck_store eck 
        INNER JOIN field_data_field_total_with_vat wv 
        ON wv.entity_id = eck.id 
        INNER JOIN field_data_field_created fc 
        ON fc.entity_id = eck.id
        INNER JOIN field_data_field_reference_store_id si 
        ON si.entity_id = eck.id
        INNER JOIN field_data_field_area fa
        ON si.field_reference_store_id_target_id = fa.entity_id
        WHERE (type = 'cashier')
        AND (field_created_value LIKE '%$date%')
        AND field_reference_store_id_target_id = :sid", array(
          ':sid' => $sid));

        $get_total = $sub_query->fetchField();

        // By day
        $result = db_query("SELECT
        CONCAT(ROUND(SUM(wv.field_total_with_vat_value) / $get_total * 100, 2), '%')
        FROM
        {eck_store} eck
        INNER JOIN {field_data_field_reference_store_id} si ON si.entity_id = eck.id
        INNER JOIN field_data_field_created fc ON fc.entity_id = eck.id
        INNER JOIN field_data_field_total_with_vat wv ON wv.entity_id = eck.id
        WHERE  (type = 'cashier')
        AND field_created_value LIKE '%$date%'
        AND field_reference_store_id_target_id = :sid", array(
          ':sid' => $sid));
        break;
      default:
        # code...
        break;
    }

    return $result->fetchField();
  }
}
