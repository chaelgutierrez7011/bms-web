<div class="box box-solid">
    <div class="box-header with-border">
        <h4 class="box-title">Add Undertime</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
        <?php print render($form['undertime']); ?>
        <!-- /.input group -->
        </div>
        <div class="form-group">
        <?php print render($form['store_assigment']); ?>
        <?php print render($form['uid']); ?>
        </div>
        <?php print render($form['submit']); ?>
    </div>
</div>