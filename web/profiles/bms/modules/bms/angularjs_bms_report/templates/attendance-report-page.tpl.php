<div ng-app="starter">
  <form name="attendance-form" ng-controller="AttendanceCtrl">
  <div class="tab-navigations">
    <ul class="tabs-menu">
      <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Profile</a></li>
      <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Assigned Duty</a></li>
      <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Actual Duty rendered</a></li>
      <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)"><?php print !empty($items['break_status']) ? 'Break' : 'Break / Freezer'; ?></a></li>
    </ul>
  </div>
    <div class="body-tabs">
      <div id="tab-0" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
          <div ng-hide="!tabs[0].isLoaded">
            <div id="group-filter">
               <span class="input"><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required /></span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Crew Name</th>
                <th>Employee No.</th>
                <th>Username</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[0].content">
                  <td>{{ item.fullname }}</td>
                  <td>{{ item.employee_no }}</td>
                  <td>{{ item.username }}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-1" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
          <div ng-hide="!tabs[1].isLoaded">
            <div id="group-filter">
               <span><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required /></span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Crew Name</th>
                <th>Time-In</th>
                <th>Time-Out</th>
                <th>No of Hrs. Assigned</th>
                <th>Overtime Request</th>
                <th>Undertime Request</th>
                <th>Expected Time Out</th>
                <th>Total Hrs. Assigned</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[1].content">
                  <td>{{ item.fullname }}</td>
                  <td>{{ item.time_in | date: 'hh:mm a'}}</td>
                  <td>{{ item.time_out | date: 'hh:mm a'}}</td>
                  <td>{{ item.no_hours_assigned }}</td>
                  <td>{{ item.ot_request }}</td>
                  <td>{{ item.ut_request }}</td>
                  <td>{{ item.expected_timeout }}</td>
                  <td>{{ item.total_hours_assigned }}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-2" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
          <div ng-hide="!tabs[2].isLoaded">
            <div id="group-filter">
               <span><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required /></span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Crew Name</th>
                <th>Time-In</th>
                <th>Photo</th>
                <th>Time-Out</th>
                <th>Photo</th>
                <th>Lates</th>
                <th>Overtime</th>
                <th>Undertime</th>
                <th>Total Hrs. Rendered</th>
                <th>Variance (Assigned vs Rendered)</th>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[2].content">
                  <td>{{ item.fullname }}</td>
                  <td>{{ item.time_in | date: 'hh:mm a'}}</td>
                  <td>
                    <a ng-click="openLightboxModal($index, 0)">
                      <img ng-src="{{item.timein_photo}}" class="img-thumbnail">
                    </a>
                  </td>
                  <td>{{ item.time_out | date: 'hh:mm a'}}</td>
                  <td>
                    <a ng-click="openLightboxModal($index, 1)">
                      <img ng-src="{{item.timeout_photo}}" class="img-thumbnail">
                    </a>
                  </td>
                  <td>{{ item.late }}</td>
                  <td>{{ item.ot }}</td>
                  <td>{{ item.ut }}</td>
                  <td>{{ item.total_hours_rendered | date: 'hh:mm'}}</td>
                  <td>{{ item.variance }}</td>
                </tr>
              </tbody>
            </table>
          </div>
          <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div id="tab-3" class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
          <div ng-hide="!tabs[3].isLoaded">
            <div id="group-filter">
               <span><input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required /></span>
            </div>
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
              <thead>
                <th>Crew Name</th>
                <th>Break-Out</th>
                <th>Break-In</th>
                <th>Total No. of Break Mins</th>
                <th>Total Breaks</th>
                <?php print !empty($items['break_status']) ? '' : '<th>Freezer Temp.</th>'; ?>
              </thead>
              <tbody>
                <tr ng-repeat="item in tabs[3].content">
                  <td>{{ item.fullname }}</td>
                  <td>{{ item.break_out | date: 'hh:mm a'}}</td>
                  <td>{{ item.break_in | date: 'hh:mm a'}}</td>
                  <td>{{ item.breaks_min}}</td>
                  <td>{{ item.total_break }}</td>
                  <?php print !empty($items['break_status']) ? '' : '<td>{{ item.freezer_tmp }}</td>'; ?>
                </tr>
              </tbody>
            </table>
          </div>
          <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
        </div>
      </div>
    </div>
  </form>
</div>
