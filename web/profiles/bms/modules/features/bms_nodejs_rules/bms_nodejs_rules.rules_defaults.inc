<?php
/**
 * @file
 * bms_nodejs_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function bms_nodejs_rules_default_rules_configuration() {
  $items = array();
  $items['rules_attendance_after_update'] = entity_import('rules_config', '{ "rules_attendance_after_update" : {
      "LABEL" : "Attendance",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:10:\\u0022attendance\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:10:\\u0022attendance\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_cash_management'] = entity_import('rules_config', '{ "rules_cash_management" : {
      "LABEL" : "Cash Management",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck", "profile2" ],
      "ON" : { "store_insert" : [], "profile2_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:15:\\u0022cash_management\\u0022;s:10:\\u0022display_id\\u0022;s:15:\\u0022cash_management\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:15:\\u0022cash_management\\u0022;s:10:\\u0022display_id\\u0022;s:15:\\u0022cash_management\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_commissary'] = entity_import('rules_config', '{ "rules_commissary" : {
      "LABEL" : "Commissary",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : {
                "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022commissary_report\\u0022;s:10:\\u0022display_id\\u0022;s:16:\\u0022commissary_order\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022commissary_report\\u0022;s:10:\\u0022display_id\\u0022;s:16:\\u0022commissary_order\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022commissary_report\\u0022;s:10:\\u0022display_id\\u0022;s:18:\\u0022commissary_pullout\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022commissary_report\\u0022;s:10:\\u0022display_id\\u0022;s:18:\\u0022commissary_pullout\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:10:\\u0022commissary\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:10:\\u0022commissary\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_1\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_1\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_2\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_2\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_3\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022commissary_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_3\\u0022;}"
              }
            }
          }
        }
      ]
    }
  }');
  $items['rules_inventory'] = entity_import('rules_config', '{ "rules_inventory" : {
      "LABEL" : "Inventory",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022inventory_summary\\u0022;s:10:\\u0022display_id\\u0022;s:17:\\u0022inventory_summary\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022inventory_summary\\u0022;s:10:\\u0022display_id\\u0022;s:17:\\u0022inventory_summary\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_production'] = entity_import('rules_config', '{ "rules_production" : {
      "LABEL" : "Production",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022production_summary\\u0022;s:10:\\u0022display_id\\u0022;s:18:\\u0022production_summary\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:18:\\u0022production_summary\\u0022;s:10:\\u0022display_id\\u0022;s:18:\\u0022production_summary\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_sales_summary'] = entity_import('rules_config', '{ "rules_sales_summary" : {
      "LABEL" : "Sales Summary",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : { "a:2:{s:4:\\u0022name\\u0022;s:13:\\u0022sales_summary\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:13:\\u0022sales_summary\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}" } } } }
      ]
    }
  }');
  $items['rules_store_transfer'] = entity_import('rules_config', '{ "rules_store_transfer" : {
      "LABEL" : "Store Transfer",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "TAGS" : [ "BMS" ],
      "REQUIRES" : [ "rules", "views_nodejs", "eck" ],
      "ON" : { "store_insert" : [], "store_update" : [] },
      "IF" : [ { "entity_is_of_type" : { "entity" : [ "store" ], "type" : "store" } } ],
      "DO" : [
        { "views_nodejs" : { "views" : { "value" : {
                "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022store_transfer_in\\u0022;s:10:\\u0022display_id\\u0022;s:11:\\u0022transfer_in\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022store_transfer_in\\u0022;s:10:\\u0022display_id\\u0022;s:11:\\u0022transfer_in\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022store_transfer_in\\u0022;s:10:\\u0022display_id\\u0022;s:12:\\u0022transfer_out\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:17:\\u0022store_transfer_in\\u0022;s:10:\\u0022display_id\\u0022;s:12:\\u0022transfer_out\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:4:\\u0022page\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_1\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_1\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_2\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_2\\u0022;}",
                "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_3\\u0022;}" : "a:2:{s:4:\\u0022name\\u0022;s:16:\\u0022transfer_request\\u0022;s:10:\\u0022display_id\\u0022;s:6:\\u0022page_3\\u0022;}"
              }
            }
          }
        }
      ]
    }
  }');
  return $items;
}
