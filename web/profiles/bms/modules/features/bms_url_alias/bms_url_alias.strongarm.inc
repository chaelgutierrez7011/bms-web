<?php
/**
 * @file
 * bms_url_alias.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function bms_url_alias_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_profile2_main_pattern';
  $strongarm->value = 'profile/[profile2:field-firstname]-[profile2:field-lastname]';
  $export['pathauto_profile2_main_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_profile2_pattern';
  $strongarm->value = '';
  $export['pathauto_profile2_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_store_commissary_pattern';
  $strongarm->value = 'commissary/order-request/[store:id]';
  $export['pathauto_store_commissary_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_store_commissary_pullout_pattern';
  $strongarm->value = 'commissary/pullout-request/[store:id]';
  $export['pathauto_store_commissary_pullout_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_store_store_transfer_pattern';
  $strongarm->value = 'transfer-item/[store:id]';
  $export['pathauto_store_store_transfer_pattern'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'pathauto_store_store_transfer_pullout_pattern';
  $strongarm->value = 'transfer-item/[store:id]';
  $export['pathauto_store_store_transfer_pullout_pattern'] = $strongarm;

  return $export;
}
