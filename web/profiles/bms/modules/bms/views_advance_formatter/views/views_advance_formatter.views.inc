<?php

/**
 * Implementation of hook_views_plugins().
 */
function views_advance_formatter_views_plugins() {
  $base_path = drupal_get_path('module', 'views_advance_formatter');

  return array(
    'style' => array(
      'advance_table' => array(
        'title' => t('Advance Table'),
        'help' => t('Display table with multiple comlumn rows'),
        'handler' => 'views_advance_formatter_plugin_style_table',
        //'parent' => 'table',
        'uses row plugin' => FALSE,
        'uses fields' => TRUE,
        'uses options' => TRUE,
        'type' => 'normal',
        'theme' => 'views_advance_formatter_results_table',
        'theme path' => $base_path . '/views',
      ),
    ),
  );
}
