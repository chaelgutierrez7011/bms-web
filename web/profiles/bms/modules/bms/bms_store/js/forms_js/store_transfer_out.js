(function($) {
	'use strict';

	Drupal.behaviors.StoreTransferOut = {
		attach: function(context, settings) {
			let formTable = '#store-transfer-out-view-form table',
				subTotal = '#edit-form-content-foot .form-item-subtotal input[type="text"]',
				payableVat = '#edit-form-content-foot .form-item-vat input[type="text"]',
				payableTotal = '#edit-form-content-foot .form-item-total input[type="text"]',
				refObject = Drupal.settings.StoreTransferOut;

			$(formTable).find('input[type="text"]').bind('keyup change', function(e) {
				let $self = $(this),
					itemCode = $self.parent().parent().prev().html(),
					total = parseInt(refObject.totalRefs[itemCode].price) * parseInt($self.val());

				refObject.totalRefs[itemCode].total = total;
				refObject.subTotal = _recalSubtotal(refObject);
				refObject.vatPayable = _recalVatPayable(refObject);
				refObject.totalPayable = _recalTotalPayable(refObject);

				// I am sure there is a better way for this selectors
				// but for the mean time, please forgive me :D
				// by any chance, please refactor as you may
				$self.parent().parent().next().next().html('₱' + numeral(total.toFixed(2)).format('0,0.00'));

				// reflect the new calculation here...
				$(subTotal).val('₱' + numeral(refObject.subTotal.toFixed(2)).format('0,0.00'));
				$(payableVat).val('₱' + numeral(refObject.vatPayable.toFixed(2)).format('0,0.00'));
				$(payableTotal).val('₱' + numeral(refObject.totalPayable.toFixed(2)).format('0,0.00'));
			});
		}
	};

	function _recalSubtotal(refObject) {
		let t = 0;

		Object.keys(refObject.totalRefs).forEach(function(key) {
			t += parseInt(refObject.totalRefs[key].total);
		});

		return t;
	}

	function _recalVatPayable(refObject) {
		return refObject.subTotal * 0.12;
	}

	function _recalTotalPayable(refObject) {
		return refObject.subTotal + refObject.vatPayable;
	}
})(jQuery);
