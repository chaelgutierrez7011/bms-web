<?php

/**
 * @file
 * Define this Export UI plugin for decorations.
 */

$plugin = array(
  'schema' => 'table_trash',
  'access' => 'configure table decorations',
  'menu' => array(
    'menu item' => 'table_trash',
    'menu title' => 'Table Trash',
    'menu description' => 'Administer Table Trash decorations.',
    'menu prefix' => 'admin/config/content',
  ),

  'title singular' => t('decoration'),
  'title plural' => t('decorations'),
  'title singular proper' => t('Table Trash decoration'),
  'title plural proper' => t('Table Trash decorations'),

  'form' => array(
    'settings' => 'table_trash_ctools_export_ui_form',
    'submit' => 'table_trash_ctools_export_ui_form_submit',
  ),
);

/**
 * Add an edit form for decorations.
 */
function table_trash_ctools_export_ui_form(&$form, &$form_state) {
  ctools_include('export');

  // Retain tree-hierarchy in values, prevent the form from being flattened.
  $form['#tree'] = TRUE;

  $form['#attached']['css'] = array(
    drupal_get_path('module', 'table_trash') . '/css/table_trash.admin.css',
  );

  // Load the config.
  $decoration = array();
  $deco_config = array();
  $selectors = array();
  $responsive = array();

  if (isset($form['info']['machine_name']['#value'])) {
    $decoration = ctools_export_crud_load('table_trash', $form['info']['machine_name']['#value']);
    $deco_config = $decoration->data['decoration_config'];
    $selectors = $decoration->data['selectors'];
    $responsive = $deco_config['responsive_config'];
  }
  elseif (isset($form['info']['machine_name']['#default_value'])) {
    $decoration = $form_state['item'];
    $deco_config = $decoration->data['decoration_config'];
    $selectors = $decoration->data['selectors'];
    $responsive = $deco_config['responsive_config'];
  }

  $form['notes'] = array(
    '#markup' => 'See the <a href="https://www.datatables.net/download/compatibility">DataTables compatibility</a> table for questions regarding how settings interact with each other.'
  );

  $form['decoration_config'] = array(
    '#type' => 'container',
    '#attributes' => array(
      'class' => array(
        'decoration-config',
      ),
    ),
  );

  // Searching.
  $form['decoration_config']['searching'] = array(
    '#type' => 'checkbox',
    '#title' => t('Display a search box'),
    '#default_value' => isset($deco_config['searching']) ? $deco_config['searching'] : TRUE,
    '#description' => t('The searching allows the visitor to filter the table by keywords they enter.'),
  );

  // Column Reordering.
  $form['decoration_config']['col_reorder'] = array(
    '#type' => 'checkbox',
    '#title' => t('Reorder Columns'),
    '#default_value' => isset($deco_config['col_reorder']) ? $deco_config['col_reorder'] : TRUE,
    '#description' => t('Reposition columns by clicking and dragging the headers.'),
  );

  // Button Exports.
  $form['decoration_config']['buttons_export'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Add export buttons'),
    '#description' => t('Allow visitors to export data in these formats.'),
    '#options' => array(
      'copy' => 'Copy',
      'csv' => 'Csv',
      'excel' => 'Excel',
      'pdf' => 'Pdf',
      'print' => 'Print',
    ),
    '#default_value' => isset($deco_config['buttons_export']) ? array_keys($deco_config['buttons_export']) : array(),
  );

  // Retrieve.
  $form['decoration_config']['retrieve'] = array(
    '#type' => 'checkbox',
    '#title' => t('Allow AJAX updates of targeted tables'),
    '#default_value' => isset($deco_config['retrieve']) ? $deco_config['retrieve'] : FALSE,
    '#description' => t('Tick if you get error messages using Exposed Filters on tabular Views with <strong>Use AJAX: Yes</strong>. Do not tick if you do not have any issues, as there is a small performance penalty to pay.'),
  );

  // Paging.
  $form['decoration_config']['paging_type'] = array(
    '#type' => 'select',
    '#multiple' => FALSE,
    '#title' => t('Pager style'),
    '#options' => array(
      '' => t('No pager'),
      'numbers' => t("Page number buttons only"),
      'simple' => t("'Previous' and 'Next' buttons only"),
      'simple_numbers' => t("'Previous' and 'Next' buttons, plus page numbers"),
      'full' => t("'First', 'Previous', 'Next' and 'Last' buttons"),
      'numbers' => t('Page number buttons only'),
      'full_numbers' => t("'First', 'Previous', 'Next' and 'Last' buttons, plus page numbers"),
    ),
    '#default_value' => isset($deco_config['paging_type']) ? $deco_config['paging_type'] : '',
    '#description' => t('If you use this client-side pager, it is recommended you switch off the server-side pager (as provided by Views).'),
  );

  // Paging.
  $form['decoration_config']['page_length'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 4,
    '#title' => t('Page height'),
    '#default_value' => isset($deco_config['page_length']) ? $deco_config['page_length'] : '',
    '#description' => t('If paged, number of rows per page.'),
  );

  // Column Sorting.
  $form['decoration_config']['not_orderable'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#title' => t('Columns NOT sortable'),
    '#default_value' => isset($deco_config['not_orderable']) ? $deco_config['not_orderable'] : '',
    '#description' => t('All columns are sortable by default. Enter a comma-separated list of column numbers for which client-side sorting is to be <em>disabled</em>. The leftmost column is number 1. Enter <strong>0</strong> to switch off column-sorting altogether.'),
  );

  // Horizontal Scrolling.
  $form['decoration_config']['scrollX'] = array(
    '#type' => 'checkbox',
    '#size' => 4,
    '#title' => t('Horizontal Scroll'),
    '#default_value' => isset($deco_config['scrollX']) ? $deco_config['scrollX'] : '',
    '#description' => t('Choose if the table should scroll horizontally'),
  );

  // Vertical Scrolling.
  $form['decoration_config']['scrollY'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#title' => t('Vertical Scroll'),
    '#default_value' => isset($deco_config['scrollY']) ? $deco_config['scrollY'] : '',
    '#description' => t('Enter the desired height of the table. May be expressed as a number or any valid CSS unit including vh.'),
  );

  // Vertical Collapsing.
  $form['decoration_config']['scrollCollapse'] = array(
    '#type' => 'checkbox',
    '#title' => t('Vertical Scroll Collapse'),
    '#default_value' => isset($deco_config['scrollCollapse']) ? $deco_config['scrollCollapse'] : '',
    '#description' => t('Determine if the table height should collapse when there is a small number of rows'),
  );

  // Fixed Columns Left.
  $form['decoration_config']['fixed_columns_left'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 2,
    '#title' => t('Fix left column(s)'),
    '#default_value' => isset($deco_config['fixed_columns_left']) ? $deco_config['fixed_columns_left'] : '',
    '#description' => t('Works in conjunction with <em>Horizontal Scrolling</em>. Enter the number of left columns to fix in position when scrolling horizontally. Example: <strong>1</strong>'),
  );

  // Fixed Columns Right.
  $form['decoration_config']['fixed_columns_right'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#maxlength' => 2,
    '#title' => t('Fix right column(s)'),
    '#default_value' => isset($deco_config['fixed_columns_right']) ? $deco_config['fixed_columns_right'] : '',
    '#description' => t('Works in conjunction with <em>Horizontal Scrolling</em>. Enter the number of right columns to fix in position when scrolling horizontally. Example: <strong>1</strong>'),
  );

  // Fixed Header.
  $form['decoration_config']['fixed_header'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fix table header on scroll'),
    '#default_value' => isset($deco_config['fixed_header']) ? $deco_config['fixed_header'] : FALSE,
    '#description' => t('Fix the header to the top of the window when vertically scrolling tall tables. Cannot be used with <em>Horizontal Scrolling</em> or AJAX.'),
  );

  // Fixed Footer.
  $form['decoration_config']['fixed_footer'] = array(
    '#type' => 'checkbox',
    '#title' => t('Fix table footer on scroll'),
    '#default_value' => isset($deco_config['fixed_header']) ? $deco_config['fixed_header'] : FALSE,
    '#description' => t('Fix the footer to the bottom of the window when vertically scrolling tall tables. Cannot be used with <em>Horizontal Scrolling</em> or AJAX.'),
  );

  // Responsive Extension.
  $form['decoration_config']['responsive_config'] = array(
    '#type' => 'fieldset',
    '#collapsible' => TRUE,
    '#collapsed' => TRUE,
    '#title' => t('Responsive tables feature'),
    '#description' => t('Responsively hides selected columns on small windows. Hidden cell content is revealed when the "expand" icon is clicked. Does not work in combination with <em>Oversize and scroll horizontally</em>.'),
  );

  $form['decoration_config']['responsive_config']['responsive_control_column'] = array(
    '#type' => 'textfield',
    '#size' => 4,
    '#title' => t('"Expand" column'),
    '#default_value' => isset($responsive['responsive_control_column']) ? $responsive['responsive_control_column'] : '',
    '#description' => t("Typically you'd pick the title column for this. The leftmost column is number 1. Leave empty to disallow tables from responding to small window-sizes."),
  );

  $form['decoration_config']['responsive_config']['responsive_phone_columns'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#maxsize' => 256,
    '#title' => t('Columns to hide when width of window is phone-size'),
    '#default_value' => isset($responsive['responsive_phone_columns']) ? $responsive['responsive_phone_columns'] : '',
  );

  $form['decoration_config']['responsive_config']['responsive_tablet_columns'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#maxsize' => 256,
    '#title' => t('Columns to hide when width of window is tablet-size'),
    '#default_value' => isset($responsive['responsive_tablet_columns']) ? $responsive['responsive_tablet_columns'] : '',
    '#description' => t('This is usually a subset of the columns specified for phone-sized windows.'),
  );

  $form['selectors'] = array(
    '#type' => 'markup',
    '#prefix' => '<div class="selectors">',
    '#suffix' => '</div>',
  );

  $form['selectors']['included_pages'] = array(
    '#type' => 'textarea',
    '#rows' => 2,
    '#title' => t('Pages to be adorned with all of the above'),
    '#default_value' => isset($selectors['included_pages']) ? $selectors['included_pages'] : TABLE_TRASH_DEFAULT_PAGE_INCLUSIONS,
    '#description' => t("Enter relative paths, one per line. Do not start with a slash. You may use path aliases. <em>&lt;front&gt;</em> means the front page. The asterisk <em>*</em> is the wildcard character, i.e. <em>admin/*</em> denotes all pages that have a path starting with <em>admin/</em>"),
    '#required' => TRUE,
  );

  $form['selectors']['excluded_pages'] = array(
    '#type' => 'textarea',
    '#rows' => 3,
    '#title' => t('Exceptions: pages excluded from wildcards on the left'),
    '#default_value' => isset($selectors['excluded_pages']) ? $selectors['excluded_pages'] : TABLE_TRASH_DEFAULT_PAGE_EXCLUSIONS,
    '#description' => t('One relative path per line.'),
  );

  $form['selectors']['included_css_tables'] = array(
    '#type' => 'textfield',
    '#size' => 32,
    '#title' => t('CSS-selector for tables targeted'),
    '#default_value' => isset($selectors['included_css_tables']) ? $selectors['included_css_tables'] : '',
    '#description' => t('Empty defaults to <strong>@default-selector</strong> and will usually be ok. If you want to decorate say a <a href="@views" target="views">Views</a> block and a Views attachment, but no other tables on that page, then target the individual tables. Like this: <strong>.view-display-id-block_1&nbsp;table, .view-display-id-attachment_1&nbsp;table</strong>', array(
        '@views' => url('http://drupal.org/project/views'),
        '@default-selector' => TABLE_TRASH_DEFAULT_TABLE_SELECTOR,
      )
    ),
  );
}

/**
 * Submit handler for the preset edit form.
 */
function table_trash_ctools_export_ui_form_submit($form, &$form_state) {
  $decoration = &$form_state['item'];
  $values = $form_state['values'];

  // Remove disabled export buttons.
  foreach (array_keys($values['decoration_config']['buttons_export'], 0, TRUE) as $button_disabled) {
    unset($values['decoration_config']['buttons_export'][$button_disabled]);
  }

  $decoration->name = $values['info']['name'];
  $decoration->machine_name = $values['info']['machine_name'];
  $decoration->description = $values['info']['description'];
  $decoration->data = array('decoration_config' => $values['decoration_config'], 'selectors' => $values['selectors']);
  table_trash_process_decoration_config($decoration);
}
