angular.module(['starter.controllers'], ['datatables', 'datatables.buttons', 'ui.bootstrap', 'bootstrapLightbox'])
.controller('CashManagementCtrl', function($scope, $http) {
  $http({method: 'GET', url: '/sample/store/424/manage-cash-management/json'}).success(function(data) {
    data.forEach(function(entry) {
      $scope.add_to_vault = entry.add_to_vault;
      $scope.sales = entry.sales;
      $scope.pullout = entry.pullout;
      $scope.deposit = entry.deposit;
      $scope.remaining_cash_on_vault = entry.remaining_cash_on_vault;
    });

    $scope.cash_management = data;
  });
    $scope.activeTab = 1;

    $scope.setActiveTab = function(tabToSet) {
      $scope.activeTab = tabToSet;
    }
}).config(function (LightboxProvider) {
  LightboxProvider.fullScreenMode = true;
})
.directive('loading', function () {
  var host = location.protocol + '//' + location.host + '/';
      return {
        restrict: 'E',
        replace:true,
        template: '<div class="loading"><div class="sk-folding-cube"><div class="sk-cube1 sk-cube"></div><div class="sk-cube2 sk-cube"></div><div class="sk-cube4 sk-cube"></div><div class="sk-cube3 sk-cube"></div></div>',
        link: function (scope, element, attr) {
              scope.$watch('loading', function (val) {
                  if (val)
                      $(element).show();
                  else
                      $(element).hide();
              });
        }
      }
})
.directive('yearDrop',function(){
   	function getYears(offset, range){
        var currentYear = new Date().getFullYear();
        var years = [];
        for (var i = 0; i < range + 1; i++){
            years.push(currentYear + offset + i);
        }
        return years;
    }
    return {
        link: function(scope,element,attrs){
            scope.years = getYears(+attrs.offset, +attrs.range);
            scope.selected = scope.years[0];
        },
        template: '<select ng-model="selected" ng-options="y for y in years"></select>',
        controller: 'ManageSalesByArea'
    }
})
.controller('CommissaryReportsCtrl', function($scope, $http, $timeout, $filter, Commissary, DTOptionsBuilder) {
  var store_id = location.pathname.split('/')[3];

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  $scope.tabs = [
    { title: "Order Commissary Request", content:[], isLoaded:false, active:true },
    { title: "Commissary Delivery", content:[], isLoaded:false },
    { title: "Commissary Order Variance", content:[], isLoaded:false },
    { title: "Commissary Pull-Out Request", content:[], isLoaded:false, active:true },
    { title: "Commissary Pull-Out", content:[], isLoaded:false },
    { title: "Commissary Pull-Out Variance", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var commissary = Commissary.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

    commissary.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  $scope.activeTabOne = 0;
  $scope.setActiveTabOne = function(tabToSet) {
    $scope.activeTabOne = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var commissary = Commissary.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      commissary.$promise.then(function(data) {
        commissary.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      });
    }, 2000);
  }

  $scope.activeTabTwo = 3;
  $scope.setActiveTabTwo = function(tabToSet) {
    $scope.activeTabTwo = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var commissary = Commissary.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      commissary.$promise.then(function(data) {
        commissary.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      });
    }, 2000);
  }
})
.controller('OrderCommissaryCtrl', function($scope, ProductIds, $routeParams, $location, $parse) {
  var store_id = location.pathname.split('/')[4];
  var product = ProductIds.query({store_id: store_id});

  product.$promise.then(function(data) {
    data.forEach(function(entry) {
      // init variable.
      var price = parseFloat(entry.price);
      var amount = {id: 'amount' + entry.id};
      var total = {id: 'total' + entry.id};

      // Assigned amount entered.
      if( amount.id != undefined) {
        var model = $parse(amount.id);
        model.assign($scope, 0);
      }

      // Assigned total value.
      if( total.id != undefined) {
        var total = $parse(total.id);

        // Individual total.
        total.assign($scope, function() {
          return $scope[amount.id] * price;
        });
      }
    });
    // Get subtotal
    $scope.subtotal = function() {
      total = 0;
      data.forEach(function(entry){
        var amount = {id: 'amount' + entry.id};
        var price = parseFloat(entry.price);
        // Assigned amount entered.
        if( amount.id != undefined) {
          var model = $parse(amount.id);
          model.assign($scope, $scope[amount.id]);
        }
        total += ($scope[amount.id] * price) / 1.12;
      });
      return total;
    }

    // Get vat
    $scope.vat = function() {
      total = 0;
      data.forEach(function(entry){
        var amount = {id: 'amount' + entry.id};
        var price = parseFloat(entry.price);
        // Assigned amount entered.
        if( amount.id != undefined) {
          var model = $parse(amount.id);
          model.assign($scope, $scope[amount.id]);
        }
        total += (($scope[amount.id] * price) / 1.12) * 0.12;
      });
      return total;
    }

    // Get vat
    $scope.grandtotal = function() {
      grandtotal = 0;
      vat = 0;
      data.forEach(function(entry){
        var amount = {id: 'amount' + entry.id};
        var price = parseFloat(entry.price);
        // Assigned amount entered.
        if( amount.id != undefined) {
          var model = $parse(amount.id);
          model.assign($scope, $scope[amount.id]);
        }
        grandtotal += ($scope[amount.id] * price) / 1.12;
        vat += (($scope[amount.id] * price) / 1.12) * 0.12;
      });

      return grandtotal + vat;
    }
  });
})
.controller('CashSalesCtrl', function($scope, $http, $timeout, $filter, CashSales, DTOptionsBuilder, $uibModal) {
    var store_id = location.pathname.split('/')[2];

    // Store tabs information.
    $scope.tabs = [
      { title: "Added to Vault", content:[], isLoaded:false, active:true},
      { title: "Sales", content:[], isLoaded:false},
      { title: "Pull Out", content:[], isLoaded:false},
      { title: "Deposit", content:[], isLoaded:false},
      { title: "Remaining Cash on Vault", content:[], isLoaded:false},
    ];

    $scope.dateFilter = {
      value: new Date()
    }

    // Show modal event cash sales handler
    $scope.showModalCashSales = function(Id, tabToSet) {
      $scope.opts = {
        backdrop: true,
        backdropClick: true,
        dialogFade: false,
        keyboard: true,
        templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-cash-sales.html',
        controller: 'ModalCashSalesCtrl',
        resolve: {}
      };

      $scope.opts.resolve.cashsales = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        var cashsales = CashSales.get({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, tid: Id});

        return cashsales;
      }
      // Return selected date.
      $scope.opts.resolve.selectDate = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        return selectedDate;
      }

      // Tab index
      $scope.opts.resolve.tabIndex = function() {
        return tabToSet;
      }

      // Return store id
      $scope.opts.resolve.store_id = function() {
        return store_id;
      }
      // Return transasction id
      $scope.opts.resolve.tid = function () {
        return Id;
      }

      var modalInstance = $uibModal.open($scope.opts);

    }

    // Show modal event cash sales edit handler
    $scope.showModalCashSalesEdit = function(Id, tabToSet) {
      $scope.opts = {
        backdrop: true,
        backdropClick: true,
        dialogFade: false,
        keyboard: true,
        templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-cash-sales.html',
        controller: 'ModalCashSalesEditCtrl',
        resolve: {}
      };

      $scope.opts.resolve.cashsales = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        var cashsales = CashSales.get({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, tid: Id});

        return cashsales;
      }
      // Return selected date.
      $scope.opts.resolve.selectDate = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        return selectedDate;
      }

      // Tab index
      $scope.opts.resolve.tabIndex = function() {
        return tabToSet;
      }

      // Return store id
      $scope.opts.resolve.store_id = function() {
        return store_id;
      }
      // Return transasction id
      $scope.opts.resolve.tid = function () {
        return Id;
      }

      var modalInstance = $uibModal.open($scope.opts);

    }

    // Show modal event cash sales pullout handler
    $scope.showModalPullOutEditCashSales = function(Id, tabToSet ,index) {
      $scope.opts = {
        backdrop: true,
        backdropClick: true,
        dialogFade: false,
        keyboard: true,
        templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-cash-sales-pullout.html',
        controller: 'ModalEditPulloutCtrl',
        resolve: {}
      };

      $scope.opts.resolve.pullout = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        var pullout = CashSales.get({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, tid: Id});

        return pullout;
      }

      //Single Data Editing
      $scope.opts.resolve.editPullout = function() {
      return $scope.tabs[tabToSet].content[index];
      }

      //Multiple Update for computation
      $scope.opts.resolve.editPulloutAllData = function() {
      return $scope.tabs[tabToSet].content;
      }

      // Return selected date.
      $scope.opts.resolve.selectDate = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        return selectedDate;
      }

      // Tab index
      $scope.opts.resolve.tabIndex = function() {
        return tabToSet;
      }

      // Return store id
      $scope.opts.resolve.store_id = function() {
        return store_id;
      }
      // Return transasction id
      $scope.opts.resolve.tid = function () {
        return Id;
      }

      var modalInstance = $uibModal.open($scope.opts);

    }

    // Show modal event cash sales deposit handler
    $scope.showModalDepositEditCashSales = function(Id, tabToSet ,index) {
      $scope.opts = {
        backdrop: true,
        backdropClick: true,
        dialogFade: false,
        keyboard: true,
        templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-cash-sales-deposit.html',
        controller: 'ModalEditDepositCtrl',
        resolve: {}
      };

      $scope.opts.resolve.deposit = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        var deposit = CashSales.get({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, tid: Id});

        return deposit;
      }
      // Return selected date.
      $scope.opts.resolve.selectDate = function() {
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        return selectedDate;
      }
      //Edit single Deposit Data
      $scope.opts.resolve.editDeposit  = function(){
        return $scope.tabs[tabToSet].content[index];
      };

      //All Deposit Data for Calculating Total
      $scope.opts.resolve.editDepositAllData =  function(){
        return $scope.tabs[tabToSet].content;
      };

      // Tab index
      $scope.opts.resolve.tabIndex = function() {
        return tabToSet;
      }

      // Return store id
      $scope.opts.resolve.store_id = function() {
        return store_id;
      }
      // Return transasction id
      $scope.opts.resolve.tid = function () {
        return Id;
      }

      var modalInstance = $uibModal.open($scope.opts);

    }

    // Update views when filter is change.
    $scope.updateFilter = function(index) {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var cash = CashSales.query({store_id: store_id, tabToSet: index, selectedDate: selectedDate});
      cash.$promise.then(function(data) {
        $scope.tabs[index].content = data;
        $scope.tabs[index].isLoaded = true;
      });
    }

    // Add datatable expor plugins
    $scope.dtOptionsManageCashSales = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', false)
      .withOption('bLengthChange', false);

    // Add datatable expor plugins
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', false)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

    // Set default tab to 1.
    $scope.activeTab = 0;
    $scope.setActiveTab = function(tabToSet) {
      // Set tab
      $scope.activeTab = tabToSet;
      $scope.loading = true;
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

      if($scope.tabs[tabToSet].isLoaded) {
        return
      }

      $timeout(function(){
        var cash = CashSales.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
        cash.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      }, 2000);
    }
})
.controller('StoreInventorySimplifiedCtrl', function($scope, InventorySimplified, $filter,  DTOptionsBuilder, $timeout) {
   var store_id = location.pathname.split('/')[2];

    $scope.dateFilter = {
      value: new Date()
    }

    // Add datatable expor plugins
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', false)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true,
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9]
            }
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    // Update views when filter is change.
    $scope.updateFilter = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var inventory = InventorySimplified.query({store_id: store_id, selectedDate: selectedDate});
      inventory.$promise.then(function(data) {
        $scope.items = data;
      });
    }

    $scope.isLoaded = { spinner: false };
    $scope.loading = true;

    $timeout(function(){
      var inventory = InventorySimplified.query({store_id: store_id, selectedDate: selectedDate});
      inventory.$promise.then(function(data) {
        $scope.items = data;
        $scope.isLoaded.spinner = true;
      });
    }, 1000);
})
.controller('StoreProductionSimplifiedCtrl', function($scope, ProductionSimplified, $filter,  DTOptionsBuilder, $timeout) {
   var store_id = location.pathname.split('/')[2];

    $scope.dateFilter = {
      value: new Date()
    }

    // Add datatable expor plugins
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', false)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true,
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9]
            }
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    // Update views when filter is change.
    $scope.updateFilter = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var production = ProductionSimplified.query({store_id: store_id, selectedDate: selectedDate});
      production.$promise.then(function(data) {
        $scope.items = data;
      });
    }

    $scope.isLoaded = { spinner: false };
    $scope.loading = true;

    $timeout(function(){
      var production = ProductionSimplified.query({store_id: store_id, selectedDate: selectedDate});
      production.$promise.then(function(data) {
        $scope.items = data;
        $scope.isLoaded.spinner = true;
      });
    }, 1000);
})
.controller('StoreInventoryCtrl', function($scope, $http, $timeout, $filter, Inventory,  DTOptionsBuilder, DTColumnBuilder, $q, $uibModal) {
   var store_id = location.pathname.split('/')[2];
   var inventoryPath = location.pathname.split('/')[3];

  // Store tabs information.
  $scope.tabs = [
    { title: "Beginning Inventory", content:[], isLoaded:false, active:true },
    { title: "Commissary Delivery", content:[], isLoaded:false },
    { title: "Commissary Pull-Out", content:[], isLoaded:false },
    { title: "Store Trans-In", content:[], isLoaded:false },
    { title: "Store Trans-Out", content:[], isLoaded:false },
    { title: "Production", content:[], isLoaded:false },
    { title: "Wastage", content:[], isLoaded:false },
    { title: "Ending Inventory (Theo)", content:[], isLoaded:false },
    { title: "Ending Inventory (Actual)", content:[], isLoaded:false },
    { title: "Variance", content:[], isLoaded:false }
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal event beginning inventory handler
  $scope.showModalBeginning = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-beginning-inventory.html',
      controller: 'ModalInventoryCtrl as mic',
	  scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.beginning = function() {
	  return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal Commissary Delivery
  $scope.showModalCommissaryDelivery = function(Id, tabToSet, eid, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-commissary-delivery.html',
      controller: 'ModalDeliveryCtrl as mdc',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.delivery = function() {
		return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    // Return eid
    $scope.opts.resolve.eid = function () {
      return eid;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal commissary pullout
  $scope.showModalCommissaryPullout = function(Id, tabToSet, eid, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-commissary-pullout.html',
      controller: 'ModalPulloutCtrl as mpc',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.pullout = function() {
	     return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    // Return eid
    $scope.opts.resolve.eid = function () {
      return eid;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store trans in
  $scope.showModalStoreTransIn = function(Id, tabToSet, eid, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-store-trans-in.html',
      controller: 'ModalTransInCtrl as mtic',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.transferin = function() {
		    return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
		return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    // Return eid
    $scope.opts.resolve.eid = function () {
      return eid;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store trans out
  $scope.showModalStoreTransOut = function(Id, tabToSet, eid, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-store-trans-out.html',
      controller: 'ModalTransOutCtrl as mtoc',
	  scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.transferout = function() {
      return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    // Return eid
    $scope.opts.resolve.eid = function () {
      return eid;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store production
  $scope.showModalStoreProduction = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-inventory-production.html',
      controller: 'ModalProductionCtrl as mpc',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.production = function() {
		  return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store wastage
  $scope.showModalStoreWastage = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-inventory-wastage.html',
      controller: 'ModalInventoryWastageCtrl as miwc',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.wastage = function() {
	     return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store actual
  $scope.showModalStoreActual = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-inventory-actual.html',
      controller: 'ModalInventoryActualCtrl as miac',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.actual = function() {
		return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Add datatable expor plugins
  if(inventoryPath == 'manage-inventory') {
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', true)
      .withOption('aaSorting', false)
      .withOption('bLengthChange', false);
  }
  else if (inventoryPath == 'inventory-report') {
    
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', true)
      .withOption('aaSorting', false)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var inventory = Inventory.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
    inventory.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Set active tab
  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function(){
      var inventory = Inventory.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      inventory.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 1000);
  }
})
.controller('StoreProductionCtrl', function($scope, $http, $timeout, $filter, Production, DTOptionsBuilder, DTColumnBuilder, $uibModal) {
  var store_id = location.pathname.split('/')[2];
  var productionPath = location.pathname.split('/')[3];

  $scope.tabs = [
    { title: "Beginning Inventory", content:[], isLoaded:false, active:true },
    { title: "Processed/Opened", content:[], isLoaded:false },
    { title: "Sales", content:[], isLoaded:false },
    { title: "Wastage", content:[], isLoaded:false },
    { title: "Ending Inventory (Theoretical)", content:[], isLoaded:false },
    { title: "Ending Inventory (Actual)", content:[], isLoaded:false },
    { title: "Variance", content:[], isLoaded:false }
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal store opened
  $scope.showModalProductionOpened = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-production-opened.html',
      controller: 'ModalProductionOpenedCtrl as mpoc',
	    scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.opened = function() {
		return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store wastage
  $scope.showModalProductionWastage = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-production-wastage.html',
      controller: 'ModalProductionWastageCtrl as mpwc',
	  scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.wastage = function() {
		return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal store actual
  $scope.showModalProductionActual = function(Id, tabToSet, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-production-actual.html',
      controller: 'ModalProductionActualCtrl as mpac',
	  scope: $scope,
      resolve: {}
    };

    $scope.opts.resolve.actual = function() {
		return $scope.tabs[tabToSet].content[index];
    }

    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      return $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    }

    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }

    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }
    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Add datatable expor plugins
  if(productionPath == 'manage-production') {
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('aaSorting', false)
      .withPaginationType('full_numbers')
      .withOption('bFilter', true)
      .withOption('bLengthChange', false);
  }
  else if (productionPath == 'production-report') {
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withOption('aaSorting', false)
      .withPaginationType('full_numbers')
      .withOption('bFilter', true)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');
  }


  // Update views when filter is change.
  $scope.updateFilter = function(index) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var production = Production.query({store_id: store_id, tabToSet: index, selectedDate: selectedDate});
    production.$promise.then(function(data) {
      $scope.tabs[index].content = data;
      $scope.tabs[index].isLoaded = true;
    });
  }

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var production = Production.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      production.$promise.then(function(data) {
        production.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      });
    }, 2000);
  }
})
.controller('ManageSalesTransactionCtrl', function($scope, manageSalesTransaction, $filter, $uibModal, DTOptionsBuilder) {
  var store_id = location.pathname.split('/')[2];

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal event handler
  $scope.showModal = function(Id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-sales-transaction.html',
      controller: 'ModalInstanceCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var salesPerTransaction = manageSalesTransaction.query({store_id: store_id, selectedDate: selectedDate, tid: Id});

      return salesPerTransaction;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }
    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  /**
   * Void event handler
   *
   * @param Id
   *   The transaction id, represented as tid column
   *   from the database
   */
  $scope.voidTransaction = function(Id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'sm',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/void-sales-transaction.html',
      controller: 'ModalVoidCtrl',
      resolve: {}
    };

	$scope.opts.resolve.selectDate = function() {
		var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
		return selectedDate;
    }

    $scope.opts.resolve.store_id = function() {
      return store_id;
    }

    $scope.opts.resolve.tid = function () {
      return Id;
    }

    $scope.opts.resolve.voidtransaction = function() {}
	$scope.opts.resolve.tid = function() {
		return Id;
	}

    var voidInsance = $uibModal.open($scope.opts);
  }

  $scope.manageSales = function() {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var salesTransaction = manageSalesTransaction.query({store_id: store_id, selectedDate: selectedDate});
      salesTransaction.$promise.then(function(item) {
        $scope.sales = item.data;
        $scope.total = item.total;

        if(item.data != undefined) {
          item.data.forEach(function(entry) {
            $scope.checkVoid = function(evoid) {
              if(evoid > 0) {
                return false;
              }
              else {
                return true;
              }
            }
          });
        }
      });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withOption('bFilter', false)
    .withOption('bLengthChange', false);

  // Update views when filter is change.
  $scope.updateFilter = function() {
    $scope.manageSales();
  }
})
.controller('SalesTransactionCtrl', function($scope, manageSalesTransaction, $filter, DTOptionsBuilder, $uibModal) {
  var store_id = location.pathname.split('/')[2];

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal event handler
  $scope.showModal = function(Id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-transaction.html',
      controller: 'SalesReportModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var salesPerTransaction = manageSalesTransaction.query({store_id: store_id, selectedDate: selectedDate, tid: Id});

      return salesPerTransaction;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }
    // Return transasction id
    $scope.opts.resolve.tid = function () {
      return Id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

    // Add datatable expor plugins
    $scope.dtOptions = DTOptionsBuilder.newOptions()
      .withPaginationType('full_numbers')
      .withOption('bFilter', false)
      .withOption('bLengthChange', false)
      .withButtons([
          {
            extend: 'print',
            className: 'print-button',
            text: '<i class="fa fa-fw fa-print"></i> Print Report',
            footer: true,
            exportOptions: {
              columns: [0,1,2,3,4,5,6,7,8,9]
            }
          },
          {
            extend: 'collection',
            text: '<i class="fa fa-fw fa-download"></i> Download as',
            buttons: [
              {
                extend: 'csv',
                text: 'CSV File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              },
              {
                extend: 'excelHtml5',
                text: 'Excel File',
                footer: true,
                exportOptions: {
                  columns: [0,1,2,3,4,5,6,7,8,9]
                }
              }
            ]
          }
      ]);

    angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
    angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.manageSales = function() {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var salesTransaction = manageSalesTransaction.query({store_id: store_id, selectedDate: selectedDate});

    salesTransaction.$promise.then(function(item) {
      $scope.sales = item.data;
      $scope.total = item.total

      if(item.data != undefined) {
        item.data.forEach(function(entry) {
          $scope.checkVoid = function(evoid) {
            if(evoid > 0) {
              return false;
            }
            else {
              return true;
            }
          }
        });
      }
    });
  }

  // Update views when filter is change.
  $scope.updateFilter = function() {
    $scope.manageSales();
  }
})
.controller('ManageSalesByProduct', function($scope, $http, $timeout, $filter, SalesByStore, DTOptionsBuilder){
  var store_id = location.pathname.split('/')[2];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  $scope.dateFilterEnd = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }
    var sales_by_product = SalesByStore.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

    sales_by_product.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('aaSorting', false)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {

      switch(tabToSet) {
        case 0:
          var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
          break;
        case 1:
          var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
          break;
        case 2:
          var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
          break;
        case 3:
          var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
          break
      }

      var sales_by_product = SalesByStore.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

      sales_by_product.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ManageSalesByArea', function($scope, $http, $timeout, $filter, $uibModal, SalesByArea, SalesByStore, DTOptionsBuilder, $attrs){
  var store_id = location.pathname.split('/')[2];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.ownership = [
    {"title": 'Franchisee', "value": 'franchisee'},
    {"title": 'Company Owned', "value": 'company owned'},
  ]

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal event handler
  $scope.showModalSalesByWeek = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-week.html',
      controller: 'SalesByAreaModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      var sales_by_area = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  $scope.showModalSalesByMonth = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-month.html',
      controller: 'SalesByAreaMonthModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area_month = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
      var sales_by_area_month = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area_month;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  $scope.showModalSalesByYear = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-year.html',
      controller: 'SalesByAreaYearModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area_year = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var sales_by_area_year = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area_year;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_area = SalesByArea.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

    sales_by_area.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }


  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var sales_by_area = SalesByArea.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

      sales_by_area.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ManageSalesBySystemWide', function($scope, $http, $timeout, $filter, $uibModal, SalesByStore, SalesBySystemWide, DTOptionsBuilder, $attrs) {
  var owner_id = angular.element("#zkow")[0].attributes[2].value;

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.ownership = [
    {"title": 'Franchisee', "value": 'Franchise'},
    {"title": 'Company Owned', "value": 'Company Owned'},
  ]

  $http({
    method: 'GET',
    url: '/store/area/list'
  }).then(function successCallback(response) {
    $scope.area = response.data;
  });

  $scope.dateFilter = {
    value: new Date()
  }

  $scope.dateFilterEnd = {
    value: new Date()
  }

  // Show modal event handler
  $scope.showModalSalesByWeek = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-week.html',
      controller: 'SalesByAreaModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      var sales_by_area = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  $scope.showModalSalesByMonth = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-month.html',
      controller: 'SalesByAreaMonthModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area_month = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
      var sales_by_area_month = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area_month;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  $scope.showModalSalesByYear = function(tabToSet, id) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'lg',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/view-sales-by-area-year.html',
      controller: 'SalesByAreaYearModalCtrl',
      resolve: {}
    };

    $scope.opts.resolve.sales_by_area_year = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      var sales_by_area_year = SalesByStore.query({store_id: id, tabToSet: tabToSet, selectedDate: selectedDate});

      return sales_by_area_year;
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
      return selectedDate;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return id;
    }

    var modalInstance = $uibModal.open($scope.opts);
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_system_wide = SalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, ownership: 'all', area: 'all'});

    sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }
  // Filter Ownership
  $scope.updateFilterOwnerShip = function(tabToSet, item) {

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_system_wide = SalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, ownership: item, area: 'all'});

    sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Filter Area Type
  $scope.updateFilterAreaType = function(tabToSet, item) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_system_wide = SalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, ownership: 'all', area: item});

    sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var sales_by_system_wide = SalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, ownership: 'all', area: 'all'});

      sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('SampleCtrl', function($scope, Inventory, DTOptionsBuilder, DTColumnBuilder, $http, $q){
  var vm = this;

  vm.dtOptions = DTOptionsBuilder.fromFnPromise(function() {
    var defer = $q.defer();
    var inventory = Inventory.query({store_id: 13435, tabToSet: 1, selectedDate: '2016-10-23'});
    inventory.$promise.then(function(data) {
        defer.resolve(data);
    });
    return defer.promise;
  }).withPaginationType('full_numbers');

  vm.dtColumns = [
      DTColumnBuilder.newColumn('id').withTitle('ID'),
      DTColumnBuilder.newColumn('item').withTitle('Item'),
      DTColumnBuilder.newColumn('category').withTitle('Category').notVisible()
  ];
})

.controller('ModalInstanceCtrl', function($scope, $uibModalInstance, $uibModal, sales, manageSalesTransaction, selectDate, tid, store_id, $parse, $timeout) {
  $scope.transaction = [];
  $scope.transaction_total = [];
  $scope.quantity;

  $scope.checkVoid = function(evoid) {
    if(evoid > 0) {
      return true;
    }
    else {
      return false;
    }
  }

  sales.$promise.then(function(item) {
    $scope.transaction.push(item.data);
    $scope.transaction_total.push(item.total);

    // Get subtotal
    $scope.subtotal = function() {
      total = 0;
      item.data.forEach(function(entry){
        var amount = entry.quantity;
        var price = parseFloat(entry.unit_price);

        total += (amount * price) / 1.12;
      });
      return total;
    }

    // Get vat
    $scope.vat = function() {
      total = 0;
      subtotal = 0;
      item.data.forEach(function(entry){
        var amount = entry.quantity;
        var price = parseFloat(entry.unit_price);

        subtotal += (amount * price) / 1.12;
      });
      total = subtotal * .12;
      return total;
    }

    // Discount
    $scope.discount = function() {
      discount = 0;
      subtotal = 0;
      item.data.forEach(function(entry){
       var quantity = entry.quantity;
       var price = parseFloat(entry.unit_price);
       subtotal += (quantity * price) / 1.12;
       vat = subtotal * .12;

       switch(entry.discount_type) {
         case 'PWD Discount':
           discount = (subtotal * .20);
           break;
         case 'Senior Citizen':
           discount = (subtotal * .20);
           break;
         case 'Employee Discount':
           item.total.forEach(function(i) {
             discount = i.less;
             return discount;
           });
           break;
       }
      });

      return discount;
    }

    // Get total
    $scope.grandtotal = function() {
      grandtotal = 0;
      vat = 0;
      subtotal = 0;
      discount = 0;
      total = 0;

      item.data.forEach(function(entry){
       var quantity = entry.quantity;
       var price = parseFloat(entry.unit_price);
       subtotal += (quantity * price) / 1.12;
       total += (quantity * price);
       vat = subtotal * .12;

       switch(entry.discount_type) {
         case 'PWD Discount':
           discount = (subtotal * .20) + vat;
           break;
         case 'Senior Citizen':
           discount = (subtotal * .20) + vat;
           break;
         case 'Employee Discount':
           item.total.forEach(function(i) {
             discount = i.less;
             return discount;
           });
           break;
       }
      });

      grandtotal = total - discount;

      return grandtotal;
    }
  });

  $scope.save = function(transaction) {
    $uibModalInstance.close();
    manageSalesTransaction.get({store_id: store_id, selectedDate: selectDate, tid: tid});
    manageSalesTransaction.update({store_id: store_id, selectedDate: selectDate, tid: tid}, transaction);
  }

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('ModalVoidCtrl', function($scope, $uibModalInstance, $uibModal, manageSalesTransaction, selectDate, tid, store_id, voidtransaction) {
  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }

  $scope.voidtransaction = function() {
	$uibModalInstance.close();

	manageSalesTransaction.delete({
		store_id: store_id,
		selectedDate: selectDate,
		tid: tid
	});

	angular.element('tr[data-item-number="' + tid + '"]').remove();
  }
})
.controller('SalesReportModalCtrl', function($scope, $uibModalInstance, $uibModal, sales, manageSalesTransaction, selectDate, tid, store_id, $parse, $timeout) {
  $scope.transaction = [];
  $scope.transaction_total = [];

  sales.$promise.then(function(item) {
    $scope.transaction.push(item.data);
    $scope.transaction_total.push(item.total);

     // Get subtotal
    $scope.subtotal = function() {
      total = 0;
      item.data.forEach(function(entry){
        var amount = entry.quantity;
        var price = parseFloat(entry.unit_price);

        total += (amount * price) / 1.12;
      });
      return total;
    }

    // Get vat
    $scope.vat = function() {
      total = 0;
      subtotal = 0;
      item.data.forEach(function(entry){
        var amount = entry.quantity;
        var price = parseFloat(entry.unit_price);

        subtotal += (amount * price) / 1.12;
      });
      total = subtotal * .12;
      return total;
    }

    // Discount
    $scope.discount = function() {
      discount = 0;
      subtotal = 0;
      item.data.forEach(function(entry){
       var quantity = entry.quantity;
       var price = parseFloat(entry.unit_price);
       subtotal += (quantity * price) / 1.12;
       vat = subtotal * .12;

       switch(entry.discount_type) {
         case 'PWD Discount':
           discount = (subtotal * .20);
           break;
         case 'Senior Citizen':
           discount = (subtotal * .20);
           break;
         case 'Employee Discount':
           item.total.forEach(function(i) {
             discount = i.less;
             return discount;
           });
           break;
       }
      });

      return discount;
    }

    // Get total
    $scope.grandtotal = function() {
      grandtotal = 0;
      vat = 0;
      subtotal = 0;
      discount = 0;
      total = 0;

      item.data.forEach(function(entry){
       var quantity = entry.quantity;
       var price = parseFloat(entry.unit_price);
       subtotal += (quantity * price) / 1.12;
       total += (quantity * price);
       vat = subtotal * .12;

       switch(entry.discount_type) {
         case 'PWD Discount':
           discount = (subtotal * .20) + vat;
           break;
         case 'Senior Citizen':
           discount = (subtotal * .20) + vat;
           break;
         case 'Employee Discount':
           item.total.forEach(function(i) {
             discount = i.less;
             return discount;
           });
           break;
       }
      });

      grandtotal = total - discount;

      return grandtotal;
    }
  });

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('ModalInventoryCtrl', function($scope, $uibModalInstance, $uibModal, beginning, Inventory, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = beginning;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * parseInt(vm.items.unit_price);
    }

	vm.save = function() {
		$uibModalInstance.close();
		Inventory.update({store_id: store_id, tabToSet: tabIndex, selectedDate: selectDate, tid: tid}, vm.items);
	}

	vm.cancel = function() {
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalDeliveryCtrl', function($scope, $uibModalInstance, $uibModal, delivery, Inventory, selectDate, tid, eid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = delivery;

  var prevDataQuantity = delivery.qty;
  var prevDataTotal = delivery.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid,
			eid: eid
		}, vm.items);
	}

	vm.cancel = function() {
    delivery.qty = prevDataQuantity;
    delivery.total = prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalPulloutCtrl', function($scope, $uibModalInstance, $uibModal, pullout, Inventory, selectDate, tid, eid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = pullout;

  var prevDataQuantity = pullout.qty;
  var prevDataTotal = pullout.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid,
			eid: eid
		}, vm.items);
	}

	vm.cancel = function() {
    pullout.qty = prevDataQuantity;
    pullout.total = prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalTransInCtrl', function($scope, $uibModalInstance, $uibModal, transferin, Inventory, selectDate, tid, eid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = transferin;

  var prevDataQuantity = transferin.qty;
  var prevDataTotal = transferin.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid,
			eid: eid
		}, vm.items);
	}

	vm.cancel = function() {
    transferin.qty = prevDataQuantity;
    transferin.total =prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalTransOutCtrl', function($scope, $uibModalInstance, $uibModal, transferout, Inventory, selectDate, tid, eid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = transferout;

  var prevDataQuantity = transferout.qty;
  var prevDataTotal = transferout.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid,
			eid: eid
		}, vm.items);
	}

	vm.cancel = function() {
    transferout.qty = prevDataQuantity;
    transferout.total =prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalProductionCtrl', function($scope, $uibModalInstance, $uibModal, production, Inventory, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = production;

  var prevDataQuantity = production.qty;
  var prevDataTotal = production.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * parseInt(vm.items.unit_price);
	}

	vm.save = function() {
		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid
		}, vm.items);
	}

	vm.cancel = function() {
    production.qty = prevDataQuantity;
    production.total =prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalInventoryWastageCtrl', function($http, $scope, $uibModalInstance, $uibModal, wastage, Inventory, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = wastage;

  var prevDataQuantity = wastage.qty;
  var prevDataTotal = wastage.total;
  var prevDataReason = wastage.reason;

	$http({
		method: 'GET',
		url: '/api/store/' + store_id + '/reason'
	})
	.success(function(data) {
		vm.reasons = data;
  	});

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
 		$uibModalInstance.close();

		Inventory.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid
		}, vm.items);
	}

	vm.cancel = function() {
    wastage.qty = prevDataQuantity;
    wastage.total = prevDataTotal;
    wastage.reason = prevDataReason;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalInventoryActualCtrl', function($scope, $uibModalInstance, $uibModal, actual, Inventory, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = actual;

  var prevDataQuantity = actual.qty;
  var prevDataTotal = actual.total;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();
		Inventory.update({store_id: store_id, tabToSet: tabIndex, selectedDate: selectDate, tid: tid}, vm.items);
	}

	vm.cancel = function() {
    actual.qty = prevDataQuantity;
    actual.total = prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalProductionOpenedCtrl', function($scope, $uibModalInstance, $uibModal, opened, Production, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = opened;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.converted_qty) * vm.items.unit_price;
	}

  var prevDataTotal = opened.total;
  var prevDataQuantity = opened.converted_qty;

	vm.save = function() {
		$uibModalInstance.close();

		Production.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid
		}, vm.items);
	}

	vm.cancel = function() {
    opened.total = prevDataTotal;
    opened.converted_qty = prevDataQuantity;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalProductionWastageCtrl', function($http, $scope, $uibModalInstance, $uibModal, wastage, Production, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
  vm.items = wastage;

	$http({
		method: 'GET',
		url: '/api/store/' + store_id + '/reason'
	})
	.success(function(data) {
		vm.reasons = data;
	});

  var prevDataQuantity = wastage.converted_qty;
  var prevDataTotal = wastage.total;
  var prevDataReason = wastage.reason;

	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.converted_qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();

		Production.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid
		}, vm.items);
	}

	vm.cancel = function() {
    wastage.converted_qty = prevDataQuantity;
    wastage.total = prevDataTotal;
    wastage.reason= prevDataReason;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalProductionActualCtrl', function($scope, $uibModalInstance, $uibModal, actual, Production, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
	let vm = this;
	vm.items = actual;

  var prevDataQuantity = actual.converted_qty;
  var prevDataTotal = actual.total;
	vm.updateTotal = function() {
		vm.items.total = parseInt(vm.items.converted_qty) * vm.items.unit_price;
	}

	vm.save = function() {
		$uibModalInstance.close();
		Production.update({
			store_id: store_id,
			tabToSet: tabIndex,
			selectedDate: selectDate,
			tid: tid
		}, vm.items);
	}

	vm.cancel = function() {
    actual.converted_qty = prevDataQuantity;
    actual.total = prevDataTotal;
		$uibModalInstance.dismiss('cancel');
	}
})
.controller('ModalCashSalesCtrl', function($scope, $uibModalInstance, $uibModal, cashsales, CashSales, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
  cashsales.$promise.then(function(item) {
      $scope.items = item;
      $scope.tabindex = tabIndex;
      total = 0;
      item.data.forEach(function(s) {
        total += s.denomination * s.quantity
      });
      $scope.total = total;
  });

  $scope.save = function(transaction, tabToSet) {
    $uibModalInstance.close();
    var arr = [];
    transaction.forEach(function(item) {
      arr.push({
        'id': item.id,
        'item' : item.category,
        'item_id': item.item_id,
        'quantity': item.quantity
      });
    });

    CashSales.update({store_id: store_id, tabToSet: tabToSet, selectedDate: selectDate, tid: tid}, transaction);
  }

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('ModalCashSalesEditCtrl', function($scope, $uibModalInstance, $uibModal, cashsales, CashSales, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
  cashsales.$promise.then(function(item) {
      $scope.items = item;
      $scope.tabindex = tabIndex;

      $scope.total = function() {
        total = 0;
        item.data.forEach(function(s) {
          total += s.denomination * s.quantity
        });
        return total;
      }
  });

  $scope.save = function(transaction, tabToSet) {
    $uibModalInstance.close();
    var arr = [];
    transaction.forEach(function(item) {
      arr.push({
        'id': item.id,
        'item' : item.category,
        'item_id': item.item_id,
        'quantity': item.quantity
      });
    });

    CashSales.update({store_id: store_id, tabToSet: tabToSet, selectedDate: selectDate, tid: tid}, transaction);
  }

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('ModalEditPulloutCtrl', function(editPulloutAllData, editPullout, $scope, $uibModalInstance, $uibModal, pullout, CashSales, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
  pullout.$promise.then(function(item) {
      $scope.items = item;
      $scope.tabindex = tabIndex;
  });

  $scope.save = function(transaction, tabToSet) {
    $uibModalInstance.close();
    var arr = [];
    var totalValue = 0;

    editPullout.requestor = transaction[0].requestor;
    editPullout.amount = transaction[0].amount;
    editPullout.reason = transaction[0].reason;

    angular.forEach(editPulloutAllData, function(value,key){
        totalValue = parseInt(value.amount) + totalValue;

    });

    angular.forEach(editPulloutAllData, function(value,key){
        value.total_pulled_out_cash = totalValue;
    });

    transaction.forEach(function(item) {
      arr.push({
        'id': item.id,
        'requestor' : item.requestor,
        'amount': item.amount,
        'reason': item.reason
      });
    });

    CashSales.update({store_id: store_id, tabToSet: tabToSet, selectedDate: selectDate, tid: tid}, transaction);
  }

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('ModalEditDepositCtrl', function(editDeposit ,editDepositAllData,$scope, $uibModalInstance, $uibModal, deposit, CashSales, selectDate, tid, store_id, tabIndex, $parse, $timeout) {
  deposit.$promise.then(function(item) {
      $scope.items = item;
      $scope.tabindex = tabIndex;
  });

  $scope.save = function(transaction, tabToSet) {
    $uibModalInstance.close();
    var arr = [];
    var totalValue = 0;

    editDeposit.bank = transaction[0].bank;
    editDeposit.amount = transaction[0].amount;
    editDeposit.transaction_number = transaction[0].transaction_no;

    angular.forEach(editDepositAllData,function(value,key){
        totalValue = parseInt(value.amount) + totalValue;
    });

    angular.forEach(editDepositAllData,function(value,key){
        value.deposited_cash = totalValue;
    });

    transaction.forEach(function(item) {
      arr.push({
        'id': item.id,
        'requestor' : item.bank,
        'amount': item.amount,
        'reason': item.amount
      });
    });

    CashSales.update({store_id: store_id, tabToSet: tabToSet, selectedDate: selectDate, tid: tid}, transaction);
  }

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('StoreTransferCtrl', function($scope, $http, $timeout, $filter, StoreTransfer, DTOptionsBuilder) {
  var store_id = location.pathname.split('/')[3];

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  $scope.tabs = [
    { title: "Transfer In Request", content:[], isLoaded:false, active:true },
    { title: "Transfer In Delivery", content:[], isLoaded:false },
    { title: "Transfer In Variance", content:[], isLoaded:false },
    { title: "Transfer Out Request", content:[], isLoaded:false, active:true },
    { title: "Transfer Out Delivery", content:[], isLoaded:false },
    { title: "Transfer Out Variance", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var transfer = StoreTransfer.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

    transfer.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  $scope.activeTabOne = 0;
  $scope.setActiveTabOne = function(tabToSet) {
    $scope.activeTabOne = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var transfer = StoreTransfer.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      transfer.$promise.then(function(data) {
        transfer.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      });
    }, 2000);
  }

  $scope.activeTabTwo = 3;
  $scope.setActiveTabTwo = function(tabToSet) {
    $scope.activeTabTwo = tabToSet;
    $scope.loading = true;
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var transfer = StoreTransfer.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
      transfer.$promise.then(function(data) {
        transfer.$promise.then(function(data) {
          $scope.tabs[tabToSet].content = data;
          $scope.tabs[tabToSet].isLoaded = true;
        });
      });
    }, 2000);
  }
})
.controller('ManageProductionSalesByStoreCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesByStore, DTOptionsBuilder, $attrs) {
  var store_id = location.pathname.split('/')[3];
  var category = location.pathname.split('/')[5];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_system_wide = ProductSalesByStore.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, category: category});

    sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var sales_by_system_wide = ProductSalesByStore.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate, category: category});

      sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ManageProductionSalesByStoreAllCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesByStoreAll, DTOptionsBuilder, $attrs) {
  var store_id = location.pathname.split('/')[3];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var sales_by_system_wide = ProductSalesByStoreAll.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

    sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var sales_by_system_wide = ProductSalesByStoreAll.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

      sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ProductSalesBySystemWideCategoryAllCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesBySystemWideCategoryAll, DTOptionsBuilder, $attrs) {
  var owner_id = angular.element("#zkow")[0].attributes[2].value;

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var product_sales_by_store_all = ProductSalesBySystemWideCategoryAll.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate});

    product_sales_by_store_all.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var product_sales_by_store_all = ProductSalesBySystemWideCategoryAll.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate});

      product_sales_by_store_all.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ProductSalesBySystemWideCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesBySystemWide, DTOptionsBuilder) {
  var owner_id = angular.element("#zkow")[0].attributes[2].value;
  var product_id = location.pathname.split('/')[4];
  var category = location.pathname.split('/')[6];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var product_sales_by_system_wide = ProductSalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, product_id: product_id, category: category});

    product_sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var product_sales_by_system_wide = ProductSalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, product_id: product_id, category: category});

      product_sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('ProductSalesBySystemWideProductAllCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesBySystemWideProductAll, DTOptionsBuilder) {
  var owner_id = angular.element("#zkow")[0].attributes[2].value;
  var category = location.pathname.split('/')[5];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var product_sales_by_system_wide = ProductSalesBySystemWideProductAll.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, category: category});

    product_sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var product_sales_by_system_wide = ProductSalesBySystemWideProductAll.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, category: category});

      product_sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('AllProductCategorySalesCtrl', function($scope, $http, $timeout, $filter, $uibModal, ProductSalesBySystemWide, DTOptionsBuilder) {
  var owner_id = angular.element("#zkow")[0].attributes[2].value;
  var product_id = location.pathname.split('/')[3];
  var category = location.pathname.split('/')[5];

  $scope.tabs = [
    { title: "Day Sales", content:[], isLoaded:false, active:true },
    { title: "Week to Date Sales", content:[], isLoaded:false },
    { title: "Month to Date Sales", content:[], isLoaded:false },
    { title: "Year to Date Sales", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    var product_sales_by_system_wide = ProductSalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, product_id: product_id, category: category});

    product_sales_by_system_wide.$promise.then(function(data) {
      $scope.tabs[tabToSet].content = data;
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    switch(tabToSet) {
      case 0:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break;
      case 1:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
        break;
      case 2:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'MM');
        break;
      case 3:
        var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
        break
    }

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var product_sales_by_system_wide = ProductSalesBySystemWide.query({owner_id: owner_id, tabToSet: tabToSet, selectedDate: selectedDate, product_id: product_id, category: category});

      product_sales_by_system_wide.$promise.then(function(data) {
        $scope.tabs[tabToSet].content = data;
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('SalesByAreaModalCtrl', function($scope, $uibModalInstance, $uibModal, sales_by_area, SalesByStore, selectDate, store_id, $parse, $timeout) {
  sales_by_area.$promise.then(function(item) {
      $scope.items = item;
  });

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('SalesByAreaMonthModalCtrl', function($scope, $uibModalInstance, $uibModal, sales_by_area_month, SalesByStore, selectDate, store_id, $parse, $timeout) {
  sales_by_area_month.$promise.then(function(item) {
      $scope.items = item;
  });

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('SalesByAreaYearModalCtrl', function($scope, $uibModalInstance, $uibModal, sales_by_area_year, SalesByStore, selectDate, store_id, $parse, $timeout) {
  sales_by_area_year.$promise.then(function(item) {
      $scope.items = item;
  });

  $scope.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
})
.controller('StoreOperationAttendanceCtrl', function($scope, $http, $timeout, $filter, $uibModal, Attendance, DTOptionsBuilder, Lightbox) {
  var store_id = location.pathname.split('/')[2];

  $scope.tabs = [
    { title: "Profile", content:[], isLoaded:false, active:true },
    { title: "Assigned Duty", content:[], isLoaded:false },
    { title: "Assigned Duty Rendered", content:[], isLoaded:false },
    { title: "Break / Freezer", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Show modal event handler
  $scope.showModalAttendanceEdit = function(tabToSet, id, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'sm',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-attendance-actual.html',
      controller: 'AttendanceActualModalCtrl as aam',
      resolve: {}
    };

    $scope.opts.resolve.attendance_actual = function() {
      return $scope.tabs[tabToSet].content[index];
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      return selectedDate;
    }
    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }
    // Return attendance id
    $scope.opts.resolve.attendance_id = function() {
      return id;
    }
    var modalInstance = $uibModal.open($scope.opts);
  }

  // Show modal event handler
  $scope.showModalAttendanceBreakEdit = function(tabToSet, id, index) {
    $scope.opts = {
      backdrop: true,
      backdropClick: true,
      dialogFade: false,
      keyboard: true,
      size: 'sm',
      templateUrl: '/profiles/bms/modules/bms/angularjs_bms_report/templates/edit-attendance-breaks.html',
      controller: 'AttendanceBreaksModalCtrl as abm',
      resolve: {}
    };

    $scope.opts.resolve.attendance_break = function() {
      return $scope.tabs[tabToSet].content[index];
    }
    // Return selected date.
    $scope.opts.resolve.selectDate = function() {
      var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyyww');
      return selectedDate;
    }
    // Tab index
    $scope.opts.resolve.tabIndex = function() {
      return tabToSet;
    }
    // Return store id
    $scope.opts.resolve.store_id = function() {
      return store_id;
    }
    // Return attendance id
    $scope.opts.resolve.attendance_id = function() {
      return id;
    }
    var modalInstance = $uibModal.open($scope.opts);
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var attendance_record = Attendance.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
    var dutyRendered = [];

    attendance_record.$promise.then(function(data) {
      angular.forEach(data, function(value, key) {
        // Convert date string to date obj.
        if(value.time_in != null){
            //Check weather the value is null for placeholder
            value.time_in = new Date(value.time_in);
        }
        if(value.time_out !== null){
            //Check weather the value is null for placeholder
            value.time_out = new Date(value.time_in);
        }
        value.break_out = new Date(value.break_out);
        value.break_in = new Date(value.break_in);
        value.total_hours_rendered = new Date(value.total_hours_rendered);
        //value.variance = new Date(value.variance);
        //value.breaks_min = new Date(value.breaks_min);
        dutyRendered[key] = value;
      });

      $scope.tabs[tabToSet].content = dutyRendered;
      // Open image in modal
      $scope.openLightboxModal = function (index, id) {
        Lightbox.openModal($scope.tabs[tabToSet].content[index], id);
      };
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var attendance_record = Attendance.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

      attendance_record.$promise.then(function(data) {

        angular.forEach(data, function(value, key) {
          // Convert date string to date obj.
          if(data[key].time_in != null){
              //Check weather the value is null for placeholder
             data[key].time_in = new Date(data[key].time_in);
          }
          if(data[key].time_out != null){
            //Check weather the value is null for placeholder
             data[key].time_out = new Date(data[key].time_out);
          }
          data[key].break_out = new Date(data[key].break_out);
          data[key].break_in = new Date(data[key].break_in);
          data[key].total_hours_rendered = new Date(data[key].total_hours_rendered);
          //data[key].variance = new Date(data[key].variance);
          //data[key].breaks_min = new Date(data[key].breaks_min);

        });

        $scope.tabs[tabToSet].content = data;
        // Open image in modal
        $scope.openLightboxModal = function (index, id) {
          Lightbox.openModal($scope.tabs[tabToSet].content[index], id);
        };
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }
})
.controller('AttendanceCtrl', function($scope, $http, $timeout, $filter, $uibModal, Attendance, DTOptionsBuilder, Lightbox) {
  var store_id = location.pathname.split('/')[2];

  $scope.tabs = [
    { title: "Profile", content:[], isLoaded:false, active:true },
    { title: "Assigned Duty", content:[], isLoaded:false },
    { title: "Assigned Duty Rendered", content:[], isLoaded:false },
    { title: "Break / Freezer", content:[], isLoaded:false },
  ];

  $scope.dateFilter = {
    value: new Date()
  }

  // Update views when filter is change.
  $scope.updateFilter = function(tabToSet) {
    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');
    var attendance_record = Attendance.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});
    var dutyRendered = [];

    attendance_record.$promise.then(function(data) {
      angular.forEach(data, function(value, key) {
        // Convert date string to date obj.
        value.time_in = new Date(value.time_in);
        value.time_out = new Date(value.time_out);
        value.break_out = new Date(value.break_out);
        value.break_in = new Date(value.break_in);
        value.total_hours_rendered = new Date(value.total_hours_rendered);
        //value.variance = new Date(value.variance);
        //value.breaks_min = new Date(value.breaks_min);
        dutyRendered[key] = value;
      });
      $scope.tabs[tabToSet].content = dutyRendered;
      // Open image in modal
      $scope.openLightboxModal = function (index, id) {
        Lightbox.openModal($scope.tabs[tabToSet].content[index], id);
      };
      $scope.tabs[tabToSet].isLoaded = true;
    });
  }

  // Add datatable expor plugins
  $scope.dtOptions = DTOptionsBuilder.newOptions()
    .withPaginationType('full_numbers')
    .withDisplayLength(25)
    .withOption('bFilter', false)
    .withOption('bLengthChange', false)
    .withButtons([
        {
          extend: 'print',
          className: 'print-button',
          text: '<i class="fa fa-fw fa-print"></i> Print Report',
          footer: true
        },
        {
          extend: 'collection',
          text: '<i class="fa fa-fw fa-download"></i> Download as',
          buttons: [
            {
              extend: 'csv',
              text: 'CSV File',
              footer: true
            },
            {
              extend: 'excelHtml5',
              text: 'Excel File',
              footer: true
            }
          ]
        }
    ]);

  angular.element('a.dt-button.buttons-collection').prepend('<i class="fa fa-fw fa-download"></i>');
  angular.element('a.dt-button.buttons-print.print-button').prepend('<i class="fa fa-fw fa-print"></i>');

  $scope.activeTab = 0;
  $scope.setActiveTab = function(tabToSet) {
    $scope.activeTab = tabToSet;
    $scope.loading = true;

    var selectedDate = $filter('date')($scope.dateFilter.value, 'yyyy-MM-dd');

    if($scope.tabs[tabToSet].isLoaded) {
      return
    }

    $timeout(function() {
      var attendance_record = Attendance.query({store_id: store_id, tabToSet: tabToSet, selectedDate: selectedDate});

      attendance_record.$promise.then(function(data) {
        angular.forEach(data, function(value, key) {
          // Convert date string to date obj.
          data[key].time_in = new Date(data[key].time_in);
          data[key].time_out = new Date(data[key].time_out);
          data[key].break_out = new Date(data[key].break_out);
          data[key].break_in = new Date(data[key].break_in);
          data[key].total_hours_rendered = new Date(data[key].total_hours_rendered);
          //data[key].variance = new Date(data[key].variance);
          //data[key].breaks_min = new Date(data[key].breaks_min);

        });

        $scope.tabs[tabToSet].content = data;
        // Open image in modal
        $scope.openLightboxModal = function (index, id) {
          Lightbox.openModal($scope.tabs[tabToSet].content[index], id);
        };
        $scope.tabs[tabToSet].isLoaded = true;
      });
    }, 2000);
  }

})
.controller('AttendanceActualModalCtrl', function($scope, $uibModalInstance, $uibModal, attendance_actual, selectDate, store_id, attendance_id, Attendance, tabIndex, $parse, $timeout){
  let vm = this;
  vm.items = attendance_actual;
  vm.items.time_in = new Date(vm.items.time_in);
  vm.items.time_out = new Date(vm.items.time_out);
  vm.items.assigned_timein = new Date(vm.items.assigned_timein);
  vm.items.assigned_timeout = new Date(vm.items.assigned_timeout);
  vm.items.expected_timeout = new Date(vm.items.expected_timeout);
  vm.items.total_hours_assigned = new Date(vm.items.total_hours_assigned);
  vm.items.total_hours_rendered = new Date(vm.items.total_hours_rendered);

	vm.updateMisc = function() {
    // Crew late computation
    if(vm.items.time_in >= vm.items.assigned_timein) {
      var difference_late = vm.items.time_in.getTime() - vm.items.assigned_timein.getTime();
      var total_minutes = Math.round(difference_late / 60000);
      var hours = Math.floor(total_minutes / 60);
      var minutes = total_minutes % 60;
      vm.items.late = hours + ':' + minutes;
    }
    // Crew undertime computation
    if(vm.items.time_out <= vm.items.assigned_timeout){
      var difference_ut = vm.items.assigned_timeout.getTime() - vm.items.time_out;
      var total_minutes_ut = Math.round(difference_ut / 60000);
      var hours_ut = Math.floor(total_minutes_ut / 60);
      var minutes_ut = total_minutes_ut % 60;
      vm.items.ut = hours_ut + ':' + minutes_ut;
    }
    else {
      if(vm.items.time_out <= vm.items.expected_timeout) {
        var difference_ut = vm.items.expected_timeout.getTime() - vm.items.time_out;
        var total_minutes_ut = Math.round(difference_ut / 60000);
        var hours_ut = Math.floor(total_minutes_ut / 60);
        var minutes_ut = total_minutes_ut % 60;
        vm.items.ut = hours_ut + ':' + minutes_ut;
      }
    }
    // Crew overtime computation
    if(vm.items.assigned_overtime != null) {
      if(vm.items.time_out >= vm.items.expected_timeout) {
        var difference_ot = vm.items.expected_timeout.getTime() - vm.items.assigned_timeout;
        var total_minutes_ot = Math.round(difference_ot / 60000);
        var hours_ot = Math.floor(total_minutes_ot / 60);
        var minutes_ot = total_minutes_ot % 60;
        vm.items.ot = hours_ot + ':' + minutes_ot;
      }
      else {
        var difference_ot = vm.items.assigned_timeout.getTime() - vm.items.time_out.getTime();
        var total_minutes_ot = Math.round(difference_ot / 60000);
        var hours_ot = Math.floor(total_minutes_ot / 60);
        var minutes_ot = total_minutes_ot % 60;
        vm.items.ot = hours_ot + ':' + minutes_ot;
      }
    }

    /**
     * Crew total rendered hours computation
     * Crew assigned vs variance
     * Initialize variables
     */
    var schedule_details = {
      'schedule': vm.items.schedule,
      'time_in' : vm.items.time_in,
      'time_out' : vm.items.time_out,
      'late' : vm.items.late,
      'assigned_timein' : vm.items.assigned_timein,
      'assigned_timeout' : vm.items.assigned_timeout,
      'expected_timeout' : vm.items.expected_timeout,
      'total_hours_assigned': vm.items.total_hours_assigned
    }
    /**
     * Initialize rendered time
     */
    var renderedtime = rendered_time(schedule_details, vm.items.ot, vm.items.ut);
    vm.items.total_hours_rendered = new Date(renderedtime);
    /**
     * Get attendance variance
     */
    var difference_variance = schedule_details.total_hours_assigned.getTime() - vm.items.total_hours_rendered.getTime();
    var total_minutes_difference_variance = Math.round(difference_variance  / 60000);
    var hours_difference_variance = Math.floor(total_minutes_difference_variance / 60);
    var minutes_difference_variance = total_minutes_difference_variance % 60;
    vm.items.variance = hours_difference_variance + ':' + minutes_difference_variance;

	}
  /**
   * Get rendered time of crew.
   */
  function rendered_time(schedule_details, ot, ut) {
    // Lets change the value if its equal to this format 00:00
    if(ot == '00:00') {
      var ot = 0;
    }

    if(ot == 0 && schedule_details.time_in >= schedule_details.assigned_timein && schedule_details.time_out <= schedule_details.assigned_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.time_in.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(ot == 0 && schedule_details.time_in <= schedule_details.assigned_timein && schedule_details.time_out >= schedule_details.assigned_timeout) {
      var difference_rendered_time =  schedule_details.time_out.getTime() - schedule_details.assigned_timein.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(ot == 0 && schedule_details.time_in >= schedule_details.assigned_timein && schedule_details.time_out <= schedule_details.assigned_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.time_in.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(ot == 0 && schedule_details.time_in >= schedule_details.assigned_timein && schedule_details.time_out >= schedule_details.expected_timeout) {
      var difference_rendered_time = schedule_details.expected_timeout.getTime() - schedule_details.time_in.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(ot == 0 && schedule_details.time_in <= schedule_details.assigned_timein && schedule_details.time_out <= schedule_details.assigned_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.assigned_timein.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(!ot && ot.length > 0 && schedule_details.time_in > schedule_details.assigned_timein && schedule_details.time_out < schedule_details.expected_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.time_in.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(!ot && ot.length > 0 && schedule_details.time_in <= schedule_details.assigned_timein && schedule_details.time_out >= schedule_details.expected_timeout) {
      var difference_rendered_time = schedule_details.expected_timeout.getTime() - schedule_details.assigned_timein.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(!ot && ot.length > 0 && schedule_details.time_in >= schedule_details.assigned_timein && schedule_details.time_out <= schedule_details.expected_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.time_in.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    else if(!ot && ot.length > 0 && schedule_details.time_in <= schedule_details.assigned_timein && schedule_details.time_out < schedule_details.expected_timeout) {
      var difference_rendered_time = schedule_details.time_out.getTime() - schedule_details.assigned_timein.getTime();
      var total_minutes_rendered_time = Math.round(difference_rendered_time / 60000);
      var hours_rendered_time = Math.floor(total_minutes_rendered_time / 60);
      var minutes_rendered_time = total_minutes_rendered_time % 60;

      var rendered_time = new Date(schedule_details.schedule + ' ' + hours_rendered_time + ':' + minutes_rendered_time);
    }
    return rendered_time;
  }

  vm.save = function() {
		$uibModalInstance.close();
    Attendance.update({store_id: store_id, tabToSet: tabIndex, selectedDate: selectDate, eid: attendance_id}, vm.items);
  }

  vm.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
}).controller('AttendanceBreaksModalCtrl', function($scope, $uibModalInstance, $uibModal, attendance_break, selectDate, store_id, attendance_id, Attendance, tabIndex, $parse, $timeout){
  let vm = this;
  vm.items = attendance_break;
  vm.items.break_out = new Date(vm.items.break_out);
  vm.items.break_in = new Date(vm.items.break_in);

  vm.updateMisc = function() {
    var difference_breaks = vm.items.break_in.getTime() - vm.items.break_out.getTime();
    var total_minutes_break = Math.round(difference_breaks / 60000);
    var hours_rendered_break = Math.floor(total_minutes_break / 60);
    var minutes_break = total_minutes_break % 60;

    vm.items.breaks_min = hours_rendered_break + ':' + minutes_break;
    vm.items.total_break = vm.items.breaks_min;
  }

  vm.save = function() {
		$uibModalInstance.close();
    Attendance.update({store_id: store_id, tabToSet: tabIndex, selectedDate: selectDate, eid: attendance_id}, vm.items);
  }

  vm.cancel = function() {
    $uibModalInstance.dismiss('cancel');
  }
});
