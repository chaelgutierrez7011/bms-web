(function($) {
	'use strict';

	/**
	 * This behavious is being used to reload the page automatically
	 * in /manage-product. The idea is from when the add product modal
	 * submits, it will try to track when the ajax call is done. 
	 * When it's done, it will trigger the submit button from the views filter
	 * thus, making an illusion that view is refreshed.
	 * There is actually a Drupal way on refreshing a view programmatically
	 * I haven't got that to work, so this trick will do for now.
	 */
	Drupal.behaviors.EditStoreProduct = {
		attach: function(context, settings) {
			$(document).ajaxComplete(function(event, xhr, ajaxSettings) {
				let condition = ajaxSettings.url === '/system/ajax' && window.location.pathname === '/manage-product',
					selector = '.views-exposed-form .views-submit-button input';

				if (condition) {
					$(selector).trigger('click');
				}
			});
		}
	};
})(jQuery);
