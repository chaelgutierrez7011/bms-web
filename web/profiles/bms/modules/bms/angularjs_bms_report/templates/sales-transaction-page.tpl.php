<div id="sales-transaction" ng-app="starter" ng-controller="SalesTransactionCtrl">
<input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter()" placeholder="yyyy-MM-dd" required />
  <table datatable="ng" dt-options="dtOptions" class="row-border hover" data-ng-init="manageSales()">
    <thead>
      <tr>
        <th>Time</th>
        <th>Crew</th>
        <th>Transaction No.</th>
        <th>OR #</th>
        <th>Gross Sales</th>
        <th>SC Discount</th>
        <th>PWD Discount</th>
        <th>EE Discount</th>
        <th>E-VAT</th>
        <th>Net Sales</th>
        <th>View</th>
      </tr>
    </thead>
    <tbody>
      <tr ng-repeat="item in sales">
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.created }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.crew }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.transaction_no }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.or_no }}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.gross_sales | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.senior_citizen_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.pwd_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.employee_discount | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.e_vat | currency:"₱":2}}</td>
        <td ng-class="checkVoid(item.void) ? 'even-row' : 'voided'" ng-cloak>{{ item.net_sales | currency:"₱":2}}</td>
        <td class="sales-wrapper"><button class="btn btn-block btn-default" id="transaction-view" ng-click="showModal(item.transaction_id)">View</button></td>
      </tr>
    </tbody>
    <tfoot>
      <tr ng-repeat="item in total">
        <td>Total</td>
        <td></td>
        <td></td>
        <td></td>
        <td ng-cloak>{{ item.total_gross | currency:"₱":2}}</td>
        <td ng-cloak>{{ item.total_sc_discount | currency:"₱":2}}</td>
        <td ng-cloak>{{ item.total_pwd_discount | currency:"₱":2}}</td>
        <td ng-cloak>{{ item.ee_discount | currency:"₱":2}}</td>
        <td ng-cloak>{{ item.total_evat | currency:"₱":2}}</td>
        <td ng-cloak>{{ item.total_net_sales | currency:"₱":2}}</td>
        <td></td>
      </tr>
    </tfoot>
  </table>
</div>