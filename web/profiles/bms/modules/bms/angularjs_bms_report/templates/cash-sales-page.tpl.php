<div ng-app="starter" ng-controller="CashSalesCtrl">
    <div class="tab-navigations">
      <ul class="tabs-menu">
        <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Added to Vault</a></li>
        <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Sales</a></li>
        <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Pull Out</a></li>
        <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Deposit</a></li>
        <li ng-class="{'active': activeTab == 4}"><a href="" ng-click="setActiveTab(4)">Remaining Cash on Vault</a></li>
      </ul>
    </div>
    <div class="tab-content">
      <div class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
          <div ng-hide="!tabs[0].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required />
            <table  datatable="ng" class="row-border hover" dt-options="dtOptions">
                <thead>
                  <th><?php print t('Cashier'); ?></th>
                  <th><?php print t('Time'); ?></th>
                  <th><?php print t('Amount') ?></th>
                  <th ng-hide="true">Total</th>
                  <th><?php print t('Operations'); ?></th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[0].content">
                    <td ng-cloak>{{ item.cashier }}</td>
                    <td ng-cloak>{{ item.time }}</td>
                    <td ng-cloak>{{ item.amount | currency:"₱":2}}</td>
                    <td ng-hide="true">{{ item.total | currency:"₱":2}}</td>
                    <td>
                      <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalCashSales(item.individual_item, 0)">
                        View
                      </button>
                    </td>
                  </tr>
                </tbody>
                <tfoot>
                    <td><?php print t('Total'); ?></td>
                    <td></td>
                    <td ng-cloak>{{ tabs[0].content[0].total | currency:'₱':2 }}</td>
                    <td></td>
                </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
          <div ng-hide="!tabs[1].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required />
            <table  datatable="ng" class="row-border hover" dt-options="dtOptions">
                <thead>
                  <th><?php print t('Cash Sales'); ?></th>
                  <th><?php print t('GC Sales'); ?></th>
                  <th><?php print t('Total Sales'); ?></th>
                  <th><?php print t('Cash Count'); ?></th>
                  <th><?php print t('GC Count'); ?></th>
                  <th><?php print t('Total Count'); ?></th>
                  <th><?php print t('Variance Total Sales vs Total Count'); ?></th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[1].content">
                    <td ng-cloak>{{ item.cash_sales | currency:"₱":2}}</td>
                    <td ng-cloak>{{ item.gc_sales | currency:"₱":2}}</td>
                    <td ng-cloak>{{ item.total_sales | currency:"₱":2}}</td>
                    <td ng-cloak ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.cash_count | currency:"₱":2}}</td>
                    <td ng-cloak ng-class="{ 'hide-data': (item.class.length > 0) }">{{ item.gc_count | currency:"₱":2}}</td>
                    <td ng-cloak ng-class="{ 'hide-data': (item.class.length > 0) }">{{ (item.cash_count * 1) + (item.gc_count * 1) | currency:"₱":2}}</td>
                    <td ng-cloak>{{ item.total - ((item.cash_count * 1) + (item.gc_count * 1)) | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
          <div ng-hide="!tabs[2].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required />
            <table  datatable="ng" class="row-border hover" dt-options="dtOptions">
                <thead>
                  <th><?php print t('Time'); ?></th>
                  <th><?php print t('Pulled Out By'); ?></th>
                  <th><?php print t('Requestor'); ?></th>
                  <th><?php print t('Amount'); ?></th>
                  <th ng-hide="true">Total Pulled Out Cash</th>
                  <th><?php print t('Reason'); ?></th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[2].content">
                    <td ng-cloak>{{ item.time }}</td>
                    <td ng-cloak>{{ item.pulled_out_by }}</td>
                    <td ng-cloak>{{ item.requestor }}</td>
                    <td ng-cloak>{{ item.amount | currency:"₱":2}}</td>
                    <td ng-hide="true">{{ item.total_pulled_out_cash | currency:"₱":2}}</td>
                    <td ng-cloak>{{ item.reason }}</td>
                  </tr>
                </tbody>
                <tfoot>
                    <td><?php print t('Total'); ?></td>
                    <td></td>
                    <td></td>
                    <td ng-cloak>{{ tabs[2].content[0].total_pulled_out_cash | currency:"₱":2 }}</td>
                    <td></td>
                </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
          <div ng-hide="!tabs[3].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required />
            <table  datatable="ng" class="row-border hover" dt-options="dtOptions">
                <thead>
                  <th><?php print t('Time'); ?></th>
                  <th><?php print t('Deposited By'); ?></th>
                  <th><?php print t('Bank'); ?></th>
                  <th><?php print t('Transaction No.'); ?></th>
                  <th>Amount</th>
                  <th ng-hide="true">Total Deposited Cash</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[3].content">
                    <td ng-cloak>{{ item.time }}</td>
                    <td ng-cloak>{{ item.deposited_by }}</td>
                    <td ng-cloak>{{ item.bank }}</td>
                    <td ng-cloak>{{ item.transaction_number }}</td>
                    <td ng-cloak>{{ item.amount | currency:"₱":2}}</td>
                    <td ng-hide="true">{{ item.deposited_cash | currency:"₱":2}}</td>
                  </tr>
                </tbody>
                <tfoot>
                    <td><?php print t('Total'); ?></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td ng-cloak>{{ tabs[3].content[0].deposited_cash | currency:'₱':2 }}</td>
                </tfoot>
            </table>
          </div>
          <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
        </div>
      </div>
      <div class="tab-content">
        <div ng-class="{'tab-pane active' : activeTab === 4, 'tab-pane' : activeTab !== 4}">
          <div ng-hide="!tabs[4].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(4)" placeholder="yyyy-MM-dd" required />
            <table  datatable="ng" class="row-border hover" dt-options="dtOptions">
                <thead>
                  <th><?php print t('Remaining Cash on Vault'); ?></th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[4].content">
                    <td ng-cloak>{{ item.remaining_cash_on_vault | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[4].isLoaded"><loading></loading></div>
        </div>
      </div>
    </div>
</div>
