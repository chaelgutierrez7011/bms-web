(function($) {
	'use strict';

	Drupal.behaviors.ProductEditForm = {
		attach: function(context, settings) {
			let parent = Drupal.behaviors.ProductEditForm;

			if (!$('body').hasClass('page-manage-product')) {
				return;
			}			

			$('#delete_inventory').click(parent.deleteProduct);
		},

		deleteProduct: function(e) {
			let parent = Drupal.behaviors.ProductEditForm;

			e.preventDefault();

			if (typeof Drupal.settings.productEntityId === 'undefined') {
				console.log('Cannot delete this product because product id is not present');
				return;
			}

			$.ajax({
				type: 'POST',
				url: '?q=bms_core/delete/product',
				data: 'productEntityId=' + Drupal.settings.productEntityId,
				success: function(data) {
					if (!data.error) {
						parent.triggerReload(parent);						
					}	
				}
			});
		},

		/**
		 * Method to trigger the reload on the page
		 * The page reload is done by triggering the click event
		 * for the filter form's submit button.
		 *
		 * @param parent
		 *   represents the drupal behavior object of this commit
		 */
		triggerReload: function(parent) {
			// just making sure that the element we wanted to trigger
			// is definitely in the page
			if ($('#edit-submit-manage-product').length) {
				$('#edit-submit-manage-product').trigger('click');
			}

			// if the dialog window is around, we trigger it's close button
			if ($('.ui-icon-closethick').length) {
				$('.ui-icon-closethick').trigger('click');
			}
		}
	};
})(jQuery);

