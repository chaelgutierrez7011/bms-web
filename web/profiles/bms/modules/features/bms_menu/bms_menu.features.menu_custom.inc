<?php
/**
 * @file
 * bms_menu.features.menu_custom.inc
 */

/**
 * Implements hook_menu_default_menu_custom().
 */
function bms_menu_menu_default_menu_custom() {
  $menus = array();

  // Exported menu: menu-bms-menu.
  $menus['menu-bms-menu'] = array(
    'menu_name' => 'menu-bms-menu',
    'title' => 'BMS Menu',
    'description' => '',
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('BMS Menu');

  return $menus;
}
