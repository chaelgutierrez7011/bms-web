<?php
/**
 * @file
 * entity_type.features.uuid_term.inc
 */

/**
 * Implements hook_uuid_features_default_terms().
 */
function entity_type_uuid_features_default_terms() {
  $terms = array();

  $terms[] = array(
    'name' => 'Excess',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '11897691-8c43-4cc6-92ec-bc436876a589',
    'vocabulary_machine_name' => 'reason',
  );
  $terms[] = array(
    'name' => 'Spiled',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '159e2ebe-ca3d-494c-b822-753f290bbc5c',
    'vocabulary_machine_name' => 'reason',
  );
  $terms[] = array(
    'name' => 'Sample Reason 1',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '2feee21d-68c6-4955-8c2c-b9890b7095ca',
    'vocabulary_machine_name' => 'commissary_reason',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Drink',
    'description' => '',
    'format' => 'plain_text',
    'weight' => 0,
    'uuid' => '3593b0f9-9021-44c4-a238-b6aed566ef44',
    'vocabulary_machine_name' => 'category',
    'path' => array(
      'pathauto' => 1,
    ),
  );
  $terms[] = array(
    'name' => 'Choc Bars',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '36184395-2e2d-44e3-a288-13534a3e7f4a',
    'vocabulary_machine_name' => 'sub_category',
  );
  $terms[] = array(
    'name' => 'Area 4',
    'description' => NULL,
    'format' => NULL,
    'weight' => 0,
    'uuid' => '37bf9c55-500c-46a3-aab6-4e20aa05390c',
    'vocabulary_machine_name' => 'area',
  );
  $terms[] = array(
    'name' => 'Food',
    'description' => '',
    'format' => 'plain_text',
    'weight' => 0,
    'uuid' => '5972c318-2603-4160-bc98-f23ccb5f3b96',
    'vocabulary_machine_name' => 'category',
    'path' => array(
      'pathauto' => 0,
    ),
  );
  return $terms;
}
