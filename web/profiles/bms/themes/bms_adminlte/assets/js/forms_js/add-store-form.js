(function($) {
	'use strict';

	Drupal.behaviors.AddStoreForm = {
		attach: function(context, settings) {
			$(document).ajaxComplete(function(event, xhr, ajaxSettings) {
				let condition = ajaxSettings.url === '/system/ajax' && window.location.pathname === '/store-settings',
					selector = '.views-exposed-form #edit-store-title-wrapper #edit-store-title';

				if (condition) {
					console.log('Trigerring view refresh');
					$(selector).trigger('change');
				}
			});
		}
	};
})(jQuery);
