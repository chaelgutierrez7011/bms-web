<?php global $user; ?>

<?php if(in_array('commissary', $user->roles)): ?>

  <!-- Main content -->
  <section class="content">
    <div class="row">

      <div class="box box-sync">
        <div class="box-header with-border">
          <h3 class="box-title">Sync Table</h3>
        </div>
        <!-- /.box-header -->
        <div class="box-body">
          <div class="col-md-6 col-sm-6 col-xs-12">
            <!-- Inventory -->
            <div class="progress-group">
              <span class="progress-text">Inventory</span>
              <span class="progress-number"><b><?php print $inventory['count']; ?></b>/<?php print $store; ?></span>

              <div class="progress sm">
                <div class="progress-bar progress-bar-yellow" style="width: <?php print $inventory['percent']; ?>%"></div>
              </div>
            </div>
            <!-- /Inventory -->
          </div><!-- /.col -->

          <div class="col-md-6 col-sm-6 col-xs-12">
            <!-- Commissary -->
            <div class="progress-group">
              <span class="progress-text">Commissary</span>
              <span class="progress-number"><b><?php print $commissary['count']; ?></b>/<?php print $store; ?></span>

              <div class="progress sm">
                <div class="progress-bar progress-bar-red" style="width: <?php print $commissary['percent']; ?>%"></div>
              </div>
            </div>
            <!-- /Commissary -->
          </div><!-- /.col -->
          <!-- End Third -->
        </div>
      </div>

      <div class="box-com-st-request">
        <div class="col-md-6">
          <h3 class="outside-title">Commissary Request</h3>
          <div class="info-box bg-green">
            <span class="info-box-icon"><i class="ion ion-document-text"></i></span>
            <div class="info-box-content">
              <div class="info">
                <div class="inner">
                  <span class="info-box-text">PENDING</span>
                  <span class="progress-description">Commissary Order Request</span>
                </div>
              </div>
              <div class="number">
                <span class="info-box-number"><?php print $role_commissary_status; ?></span>
              </div> 
            </div>
            <!-- /.info-box-content -->
          </div>
        </div><!-- /.col (LEFT) -->

        <div class="col-md-6">
          <h3 class="outside-title">Commissary Pullout</h3>
          <div class="info-box bg-aqua">
            <span class="info-box-icon"><i class="ion ion-ios-paper"></i></span>
            <div class="info-box-content">
              <div class="info">
                <div class="inner">
                  <span class="info-box-text">PENDING</span>
                  <span class="progress-description">Commissary Pull-Out Request</span>
                </div>
              </div>
              <div class="number">
                <span class="info-box-number"><?php print $role_commissary_pullout_status; ?></span>
              </div> 
            </div>
            <!-- /.info-box-content -->
          </div>
        </div><!-- /.col (RIGHT) -->
      </div>

        
    </div><!-- /.row -->
  </section><!-- /.content -->

<?php else: ?>

  <?php if(in_array('administrator', $user->roles) || in_array('operation', $user->roles) || in_array('franchisee', $user->roles)): ?>
  
    <!-- Main content -->
    <section class="content">
      <div class="row">

        <div class="box box-sync">
          <div class="box-header with-border">
            <h3 class="box-title">Sync</h3>
          </div>
          <!-- /.box-header -->
          <div class="box-body">

            <div class="col-md-4 col-sm-6 col-xs-12">
              <!-- Attendance -->
              <div class="progress-group">
                <span class="progress-text">Attendance</span>
                <span class="progress-number"><b><?php print $attendance['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-yellow" style="width: <?php print $attendance['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Attendance -->

              <!-- Transfer -->
              <div class="progress-group">
                <span class="progress-text">Transfer</span>
                <span class="progress-number"><b><?php print $transfer['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-aqua" style="width: <?php print $transfer['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Transfer -->

              <!-- Cashier -->
              <div class="progress-group">
                <span class="progress-text">Cashier</span>
                <span class="progress-number"><b><?php print $cashier['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: <?php print $cashier['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Cashier -->
            </div><!-- /.col -->
            <!-- /End firstt -->

            <div class="col-md-4 col-sm-6 col-xs-12">
              <!-- Cash Management -->
              <div class="progress-group">
                <span class="progress-text">Cash Management</span>
                <span class="progress-number"><b><?php print $cash_management['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-green" style="width: <?php print $cash_management['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Cash Management -->

              <!-- Inventory -->
              <div class="progress-group">
                <span class="progress-text">Inventory</span>
                <span class="progress-number"><b><?php print $inventory['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-yellow" style="width: <?php print $inventory['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Inventory --> 
            </div><!-- /.col -->
            <!-- /End Second -->

            <div class="col-md-4 col-sm-6 col-xs-12">
              <!-- Commissary -->
              <div class="progress-group">
                <span class="progress-text">Commissary</span>
                <span class="progress-number"><b><?php print $commissary['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-red" style="width: <?php print $commissary['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Commissary -->

              <!-- Production -->
              <div class="progress-group">
                <span class="progress-text">Production</span>
                <span class="progress-number"><b><?php print $production['count']; ?></b>/<?php print $store; ?></span>

                <div class="progress sm">
                  <div class="progress-bar progress-bar-green" style="width: <?php print $production['percent']; ?>%"></div>
                </div>
              </div>
              <!-- /Production -->  
            </div><!-- /.col -->
            <!-- End Third --> 
          </div>
        </div>

        <div class="box-com-st-request">
          <div class="col-md-6">
            <h3 class="outside-title">Commissary Request</h3>
            <div class="info-box bg-yellow">
              <span class="info-box-icon"><i class="ion ion-document-text"></i></span>
              <div class="info-box-content">
                <div class="info">
                  <div class="inner">
                    <span class="info-box-text">PENDING</span>
                    <span class="progress-description">Commissary Order Request</span>
                  </div>
                </div>
                <div class="number">
                  <span class="info-box-number"><?php print $commissary_status; ?></span>
                </div>           
              </div>
              <!-- /.info-box-content -->
            </div>

            <div class="info-box bg-aqua">
              <span class="info-box-icon"><i class="ion ion-ios-paper"></i></span>
              <div class="info-box-content">
                <div class="info">
                  <div class="inner">
                    <span class="info-box-text">PENDING</span>
                    <span class="progress-description">Commissary Pull-Out Request</span>
                  </div>
                </div>
                <div class="number">
                  <span class="info-box-number"><?php print $store_pullout; ?></span>
                </div>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div><!-- /.col (LEFT) -->

          <div class="col-md-6">
            <h3 class="outside-title">Store Transfer Request</h3>
            <div class="info-box bg-green">
              <span class="info-box-icon"><i class="ion ion-clipboard"></i></span>
              <div class="info-box-content">
                <div class="info">
                  <div class="inner">
                    <span class="info-box-text">PENDING</span>
                    <span class="progress-description">Store Trans-In Request</span>
                  </div>
                </div>
                <div class="number">
                  <span class="info-box-number"><?php print $store_transin; ?></span>
                </div>
              </div>
              <!-- /.info-box-content -->
            </div>

            <div class="info-box bg-red">
              <span class="info-box-icon"><i class="ion ion-ios-list-outline"></i></span>
              <div class="info-box-content">
                <div class="info">
                  <div class="inner">
                    <span class="info-box-text">PENDING</span>
                    <span class="progress-description">Store Trans-Out Request</span>
                  </div>
                </div>
                <div class="number">
                  <span class="info-box-number"><?php print $store_transout; ?></span>
                </div>
              </div>
              <!-- /.info-box-content -->
            </div>
          </div><!-- /.col (RIGHT) -->
        </div> 

        <div class="box-graphs">
          <div class="col-md-6">
            <!-- Peso Sales: Hourly -->
            <?php if(!empty($peso_sales_by_hourly)): ?>
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Peso Sales: Per day</h3>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="line-chart-by-hour" style="height:230px"></canvas>
                  </div>
                </div>
                <!-- /.box-body-->
              </div>
            <?php endif; ?>
            <!-- /.box -->

            <?php if(!empty($peso_sales_weekly)): ?>
              <!-- Peso Sales: Weekly -->
              <div class="box box-primary">
                <div class="box-header with-border">
                  <h3 class="box-title">Peso Sales: Per week</h3>
                </div>
                <div class="box-body">
                  <div class="chart">
                    <canvas id="bar-chart-weekly" style="height:230px"></canvas>
                  </div>
                </div>
                <!-- /.box-body-->
                </div>
              <!-- End Peso Sales: Weekly -->
              </div>
            <?php endif; ?>

          <div class="col-md-6">
            <?php if(!empty($peso_sales_monthly)): ?>
            <!-- Peso Sales: Monthly -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Peso Sales: Monthly</h3>
              </div>
              <div class="box-body">
                  <div class="chart">
                    <canvas id="bar-chart-monthly" style="height:230px"></canvas>
                  </div>
              </div>
              <!-- /.box-body-->
            </div>
            <!-- Peso Sales: Monthly -->
            <?php endif; ?>
            
            <?php if(!empty($peso_sales_yearly)): ?>
            <!-- Peso Sales: Yearly -->
            <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Peso Sales: Yearly</h3>
              </div>
              <div class="box-body">
                <div class="chart">
                  <canvas id="bar-chart-yearly" style="height:230px"></canvas>
                </div>
              </div>
              <!-- /.box-body-->
            </div>
            <!-- Peso Sales: Yearly -->
            <?php endif; ?>
          </div>
        </div>

        <?php if(!empty($peso_sales)): ?>

          <div class="col-md-12 box-by-product-cat">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Peso Sales By Product Category</h3>
              </div>

              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <div class="col-md-6">
                    <!-- DONUT CHART -->
                    <div class="box-body">
                      <div class="center-title">
                        <h1>System Wide</h1>
                        <h3>Product Category</h3>
                      </div>
                      <canvas id="pieChart" height="220"></canvas>
                    </div>
                    <!-- /.box-body -->
                  </div>
                  <div class="col-md-6">
                    <div class="box-body no-padding">
                      <table class="table">
                        <?php foreach($peso_sales as $key => $item): ?>
                          <tr>
                            <td><small class="label pull-right" style="background-color:<?php print $item['color']; ?>">&nbsp</small></td>
                            <td><?php print $item['label']; ?></td>
                            <td>₱<?php print $item['total']; ?></td>
                            <td>%<?php print $item['value']; ?></td>
                          </tr>
                        <?php endforeach; ?>
                      </table>
                    </div>
                  </div>
                  <!-- /.col -->
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
            </div>
          </div>

        <?php else: ?>
            <div class="empty" style="display:none;">
                <canvas id="pieChart" height="220"></canvas>
            </div>
        <?php endif; ?>

        <?php if(!empty($product_sales)): ?>

          <div class="col-md-12 box-by-product">
            <div class="box">
              <div class="box-header with-border">
                <h3 class="box-title">Peso Sales By Product</h3>
              </div>
              <!-- /.box-header -->
              <div class="box-body">
                <div class="row">
                  <?php foreach($product_sales as $key => $product): ?>
                    <div class="col-md-4 col-sm-6 col-xs-12 row-pies">
                      <!-- DONUT CHART -->
                      <div class="box-body">
                        <div class="center-title">
                          <h1><?php print $product['category']; ?></h1>
                        </div>
                        <canvas id="<?php print $product['pieId']; ?>" style="height:250px"></canvas>
                      </div>
                        <!-- /.box-body -->
                      <div class="box-body no-padding">
                        <table class="table">
                          <?php foreach($product['items'] as $item): ?>
                            <tr>
                              <td><small class="label pull-right" style="background-color:<?php print $item['color']; ?>">&nbsp</small></td>
                              <td><?php print $item['label']; ?></td>
                              <td>₱<?php print $item['total']; ?></td>
                              <td>%<?php print $item['value']; ?></td>
                            </tr>
                          <?php endforeach; ?>
                        </table>
                      </div>
                    </div><!-- /.col -->
                  <?php endforeach; ?>
                </div>
                <!-- /.row -->
              </div>
              <!-- ./box-body -->
            </div>
            <!-- /.box -->
          </div>

        <?php endif; ?>
      </div><!-- /.row -->
    </section><!-- /.content -->

  <?php else: ?>

    <section class="content"></section>

  <?php endif; ?>
<?php endif; ?>

