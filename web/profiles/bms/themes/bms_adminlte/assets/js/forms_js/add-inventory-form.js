(function($) {
	'use strict';

	Drupal.behaviors.AddInventoryForm = {
		attach: function(context, settings) {
			$(document).ajaxComplete(function(event, xhr, settings) {
				let condition = settings.url === '/system/ajax' && window.location.pathname === '/inventory';
				if (condition) {
					// Been working this out: https://www.drupal.org/node/1781988
					// but it seems not working. I thought that making a trigger
					// on the pager would work, but I realize it will not always 
					// work, the fact that the list of inventory items are sorted
					// by inventory code in ascending order meaning, you cannot 
					// always assume that the newly added inventory will always be 
					// added to the last page. So, just to make sure that view is 
					// reloaded by means of AJAX (not the hard page reload), this 
					// one should do the trick. 

					// Just a CAVEAT, there will be some time that the newly added 
					// inventory item will not appear on the first page, specially 
					// if the items on page is more than 25 items already. That 
					// would mean, it has been added to the next/last page. 
					$('#edit-field-item-value').trigger('change');
				}
			});
		}
	};
})(jQuery);
