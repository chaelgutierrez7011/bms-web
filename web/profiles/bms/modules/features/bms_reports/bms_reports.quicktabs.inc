<?php
/**
 * @file
 * bms_reports.quicktabs.inc
 */

/**
 * Implements hook_quicktabs_default_quicktabs().
 */
function bms_reports_quicktabs_default_quicktabs() {
  $export = array();

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'commissary_reports';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Commissary Reports';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'commissary',
      'display' => 'commissary_order_reports',
      'args' => '%2',
      'title' => 'Commissary Order',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'commissary',
      'display' => 'commissary_pullout_reports',
      'args' => '%2',
      'title' => 'Commissary Pull-Out',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Commissary Order');
  t('Commissary Pull-Out');
  t('Commissary Reports');

  $export['commissary_reports'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'prod';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Product Sales by Store';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'product_sales',
      'display' => 'product_day_sales',
      'args' => '%2/%4',
      'title' => 'Day Sales',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'path' => 'empty_page',
      'title' => 'Week to Date Sales',
      'weight' => '-99',
      'type' => 'callback',
    ),
    2 => array(
      'path' => 'empty_page',
      'title' => 'Month to Date Sales',
      'weight' => '-98',
      'type' => 'callback',
    ),
    3 => array(
      'path' => 'empty_page',
      'title' => 'Year to Date Sales',
      'weight' => '-97',
      'type' => 'callback',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Day Sales');
  t('Month to Date Sales');
  t('Product Sales by Store');
  t('Week to Date Sales');
  t('Year to Date Sales');

  $export['prod'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'product_sales_by_system_wide';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Product Sales by System Wide';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'product_sales_by_system_wide_reports',
      'display' => 'system_wide_day_sales',
      'args' => '%4',
      'title' => 'Day Sales',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'path' => 'empty_page',
      'title' => 'Week to Date Sales',
      'weight' => '-99',
      'type' => 'callback',
    ),
    2 => array(
      'path' => 'empty_page',
      'title' => 'Month to Date Sales',
      'weight' => '-98',
      'type' => 'callback',
    ),
    3 => array(
      'path' => 'empty_page',
      'title' => 'Year to Date Sales',
      'weight' => '-97',
      'type' => 'callback',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Day Sales');
  t('Month to Date Sales');
  t('Product Sales by System Wide');
  t('Week to Date Sales');
  t('Year to Date Sales');

  $export['product_sales_by_system_wide'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'sales_by_area_report';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Sales by Area Report';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'sales_by_area',
      'display' => 'day_sales',
      'args' => '%1',
      'title' => 'Day Sales',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'path' => 'empty_page',
      'title' => 'Week to Date Sales',
      'weight' => '-99',
      'type' => 'callback',
    ),
    2 => array(
      'path' => 'empty_page',
      'title' => 'Month to Date Sales',
      'weight' => '-98',
      'type' => 'callback',
    ),
    3 => array(
      'path' => 'empty_page',
      'title' => 'Year to Date Sales',
      'weight' => '-97',
      'type' => 'callback',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Day Sales');
  t('Month to Date Sales');
  t('Sales by Area Report');
  t('Week to Date Sales');
  t('Year to Date Sales');

  $export['sales_by_area_report'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'sales_by_store';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Sales By Store';
  $quicktabs->tabs = array(
    0 => array(
      'path' => 'sales-by-store/%/day-sales',
      'title' => 'Day Sales',
      'weight' => '-100',
      'type' => 'callback',
    ),
    1 => array(
      'path' => 'empty_page',
      'title' => 'Week to Date Sales',
      'weight' => '-99',
      'type' => 'callback',
    ),
    2 => array(
      'path' => 'empty_page',
      'title' => 'Month to Date Sales',
      'weight' => '-98',
      'type' => 'callback',
    ),
    3 => array(
      'path' => 'empty_page',
      'title' => 'Year to Date Sales',
      'weight' => '-97',
      'type' => 'callback',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Day Sales');
  t('Month to Date Sales');
  t('Sales By Store');
  t('Week to Date Sales');
  t('Year to Date Sales');

  $export['sales_by_store'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'store_transfers';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'Store Transfers';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'store_transfers',
      'display' => 'store_transfer_in',
      'args' => '%2',
      'title' => 'Store Transfer In',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'store_transfers',
      'display' => 'store_transfer_out',
      'args' => '%2',
      'title' => 'Store Transfer Out',
      'weight' => '-99',
      'type' => 'view',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'default';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Store Transfer In');
  t('Store Transfer Out');
  t('Store Transfers');

  $export['store_transfers'] = $quicktabs;

  $quicktabs = new stdClass();
  $quicktabs->disabled = FALSE; /* Edit this to true to make a default quicktabs disabled initially */
  $quicktabs->api_version = 1;
  $quicktabs->machine_name = 'system_wide_sales_report';
  $quicktabs->ajax = 0;
  $quicktabs->hide_empty_tabs = FALSE;
  $quicktabs->default_tab = 0;
  $quicktabs->title = 'System Wide Sales Report';
  $quicktabs->tabs = array(
    0 => array(
      'vid' => 'sales_by_system_wide',
      'display' => 'day_sales',
      'args' => '',
      'title' => 'Day Sales',
      'weight' => '-100',
      'type' => 'view',
    ),
    1 => array(
      'vid' => 'sales_by_system_wide',
      'display' => 'page_1',
      'args' => '',
      'title' => 'Week to Date Sales',
      'weight' => '-99',
      'type' => 'view',
    ),
    2 => array(
      'path' => 'empty_page',
      'title' => 'Month to Date Sales',
      'weight' => '-98',
      'type' => 'callback',
    ),
    3 => array(
      'path' => 'empty_page',
      'title' => 'Year to Date Sales',
      'weight' => '-97',
      'type' => 'callback',
    ),
  );
  $quicktabs->renderer = 'quicktabs';
  $quicktabs->style = 'nostyle';
  $quicktabs->options = array();

  // Translatables
  // Included for use with string extractors like potx.
  t('Day Sales');
  t('Month to Date Sales');
  t('System Wide Sales Report');
  t('Week to Date Sales');
  t('Year to Date Sales');

  $export['system_wide_sales_report'] = $quicktabs;

  return $export;
}
