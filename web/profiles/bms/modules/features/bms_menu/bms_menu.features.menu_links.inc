<?php
/**
 * @file
 * bms_menu.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function bms_menu_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: menu-bms-menu_-cash-sales:cash-management.
  $menu_links['menu-bms-menu_-cash-sales:cash-management'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'cash-management',
    'router_path' => 'cash-management',
    'link_title' => ' Cash Sales',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_-cash-sales:cash-management',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -43,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_admin-user:manage-employee.
  $menu_links['menu-bms-menu_admin-user:manage-employee'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-employee',
    'router_path' => 'manage-employee',
    'link_title' => 'Admin User',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_admin-user:manage-employee',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          260930237 => 260930237,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_settings:<front>',
  );
  // Exported menu link: menu-bms-menu_attendance:attendance.
  $menu_links['menu-bms-menu_attendance:attendance'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'attendance',
    'router_path' => 'attendance',
    'link_title' => 'Attendance',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_attendance:attendance',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          64780366 => 64780366,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_by-area:sales-by-area.
  $menu_links['menu-bms-menu_by-area:sales-by-area'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'sales-by-area',
    'router_path' => 'sales-by-area',
    'link_title' => 'by Area',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-area:sales-by-area',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_sales:<front>',
  );
  // Exported menu link: menu-bms-menu_by-store:product-sales.
  $menu_links['menu-bms-menu_by-store:product-sales'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'product-sales',
    'router_path' => 'product-sales',
    'link_title' => 'by Store',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-store:product-sales',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_product-sales:<front>',
  );
  // Exported menu link: menu-bms-menu_by-store:sales-by-store.
  $menu_links['menu-bms-menu_by-store:sales-by-store'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'sales-by-store',
    'router_path' => 'sales-by-store',
    'link_title' => 'by Store',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-store:sales-by-store',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_sales:<front>',
  );
  // Exported menu link: menu-bms-menu_by-system-wide:product-sales/by-system-wide.
  $menu_links['menu-bms-menu_by-system-wide:product-sales/by-system-wide'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'product-sales/by-system-wide',
    'router_path' => 'product-sales/by-system-wide',
    'link_title' => 'by System Wide',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-system-wide:product-sales/by-system-wide',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_product-sales:<front>',
  );
  // Exported menu link: menu-bms-menu_by-system-wide:sales-by-system-wide.
  $menu_links['menu-bms-menu_by-system-wide:sales-by-system-wide'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'sales-by-system-wide',
    'router_path' => 'sales-by-system-wide',
    'link_title' => 'by System Wide',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-system-wide:sales-by-system-wide',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_sales:<front>',
  );
  // Exported menu link: menu-bms-menu_by-transaction:sales-transaction.
  $menu_links['menu-bms-menu_by-transaction:sales-transaction'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'sales-transaction',
    'router_path' => 'sales-transaction',
    'link_title' => 'by Transaction',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_by-transaction:sales-transaction',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_sales:<front>',
  );
  // Exported menu link: menu-bms-menu_commissary:<front>.
  $menu_links['menu-bms-menu_commissary:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Commissary',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_commissary:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          65280424 => 65280424,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-file-text',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -47,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_crew:administer-employee.
  $menu_links['menu-bms-menu_crew:administer-employee'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'administer-employee',
    'router_path' => 'administer-employee',
    'link_title' => 'Crew',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_crew:administer-employee',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
          260930237 => 260930237,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_settings:<front>',
  );
  // Exported menu link: menu-bms-menu_dashboard:dashboard.
  $menu_links['menu-bms-menu_dashboard:dashboard'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'dashboard',
    'router_path' => 'dashboard',
    'link_title' => 'Dashboard',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_dashboard:dashboard',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          64780366 => 64780366,
          246010071 => 246010071,
          65280424 => 65280424,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-dashboard',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_inventory:inventory.
  $menu_links['menu-bms-menu_inventory:inventory'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'inventory',
    'router_path' => 'inventory',
    'link_title' => 'Inventory',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_inventory:inventory',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          65280424 => 65280424,
          140330203 => 140330203,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_settings:<front>',
  );
  // Exported menu link: menu-bms-menu_inventory:inventory-summary.
  $menu_links['menu-bms-menu_inventory:inventory-summary'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'inventory-summary',
    'router_path' => 'inventory-summary',
    'link_title' => 'Inventory',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_inventory:inventory-summary',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-attendance:manage-attendance.
  $menu_links['menu-bms-menu_manage-attendance:manage-attendance'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-attendance',
    'router_path' => 'manage-attendance',
    'link_title' => 'Manage Attendance',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-attendance:manage-attendance',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-cash-sales:manage-cash-management.
  $menu_links['menu-bms-menu_manage-cash-sales:manage-cash-management'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-cash-management',
    'router_path' => 'manage-cash-management',
    'link_title' => 'Manage Cash Sales',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-cash-sales:manage-cash-management',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-employee-schedule:manage-schedule.
  $menu_links['menu-bms-menu_manage-employee-schedule:manage-schedule'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-schedule',
    'router_path' => 'manage-schedule',
    'link_title' => 'Manage Employee Schedule',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-employee-schedule:manage-schedule',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-inventory:manage-inventory.
  $menu_links['menu-bms-menu_manage-inventory:manage-inventory'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-inventory',
    'router_path' => 'manage-inventory',
    'link_title' => 'Manage Inventory',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-inventory:manage-inventory',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-production:manage-production.
  $menu_links['menu-bms-menu_manage-production:manage-production'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-production',
    'router_path' => 'manage-production',
    'link_title' => 'Manage Production',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-production:manage-production',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_manage-sales-transaction:manage-sales-transaction.
  $menu_links['menu-bms-menu_manage-sales-transaction:manage-sales-transaction'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-sales-transaction',
    'router_path' => 'manage-sales-transaction',
    'link_title' => 'Manage Sales Transaction',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_manage-sales-transaction:manage-sales-transaction',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
  );
  // Exported menu link: menu-bms-menu_order--delivery:commissary.
  $menu_links['menu-bms-menu_order--delivery:commissary'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'commissary',
    'router_path' => 'commissary',
    'link_title' => 'Order & Delivery',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_order--delivery:commissary',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -48,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_order-request:commissary/order.
  $menu_links['menu-bms-menu_order-request:commissary/order'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'commissary/order',
    'router_path' => 'commissary/order',
    'link_title' => 'Order Request',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_order-request:commissary/order',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          65280424 => 65280424,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_commissary:<front>',
  );
  // Exported menu link: menu-bms-menu_product-sales:<front>.
  $menu_links['menu-bms-menu_product-sales:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Product Sales',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_product-sales:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -44,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_product:manage-product.
  $menu_links['menu-bms-menu_product:manage-product'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'manage-product',
    'router_path' => 'manage-product',
    'link_title' => 'Product',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_product:manage-product',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_settings:<front>',
  );
  // Exported menu link: menu-bms-menu_production:production-summary.
  $menu_links['menu-bms-menu_production:production-summary'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'production-summary',
    'router_path' => 'production-summary',
    'link_title' => 'Production',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_production:production-summary',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -46,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_pull-out-request:commissary/pullout.
  $menu_links['menu-bms-menu_pull-out-request:commissary/pullout'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'commissary/pullout',
    'router_path' => 'commissary/pullout',
    'link_title' => 'Pull Out Request',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_pull-out-request:commissary/pullout',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          65280424 => 65280424,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_commissary:<front>',
  );
  // Exported menu link: menu-bms-menu_reports:<front>.
  $menu_links['menu-bms-menu_reports:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Reports',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_reports:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          64780366 => 64780366,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-table',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -44,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_sales:<front>.
  $menu_links['menu-bms-menu_sales:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Sales',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_sales:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          246010071 => 246010071,
          140330203 => 140330203,
          154199432 => 154199432,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -45,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_settings:<front>.
  $menu_links['menu-bms-menu_settings:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Settings',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_settings:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          65280424 => 65280424,
          140330203 => 140330203,
          49915785 => 49915785,
          260930237 => 260930237,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-gears',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -49,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_store-operations-mgmt:<front>.
  $menu_links['menu-bms-menu_store-operations-mgmt:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Store Operations Mgmt',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_store-operations-mgmt:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-gears',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -48,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_store-transfer:<front>.
  $menu_links['menu-bms-menu_store-transfer:<front>'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => '<front>',
    'router_path' => '',
    'link_title' => 'Store Transfer',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_store-transfer:<front>',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-file-text-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 1,
    'has_children' => 1,
    'expanded' => 1,
    'weight' => -46,
    'customized' => 1,
  );
  // Exported menu link: menu-bms-menu_store-transfers:store-transfers.
  $menu_links['menu-bms-menu_store-transfers:store-transfers'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'store-transfers',
    'router_path' => 'store-transfers',
    'link_title' => 'Store Transfers',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_store-transfers:store-transfers',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          49915785 => 49915785,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_reports:<front>',
  );
  // Exported menu link: menu-bms-menu_store:store-settings.
  $menu_links['menu-bms-menu_store:store-settings'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'store-settings',
    'router_path' => 'store-settings',
    'link_title' => 'Store',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_store:store-settings',
      'roles_for_menu' => array(
        'show' => array(
          30037204 => 30037204,
          140330203 => 140330203,
          260930237 => 260930237,
        ),
        'hide' => array(),
      ),
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_settings:<front>',
  );
  // Exported menu link: menu-bms-menu_trans-in-request:store-transfer/transfer-in-request.
  $menu_links['menu-bms-menu_trans-in-request:store-transfer/transfer-in-request'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'store-transfer/transfer-in-request',
    'router_path' => 'store-transfer/transfer-in-request',
    'link_title' => 'Trans in Request',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_trans-in-request:store-transfer/transfer-in-request',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -50,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-transfer:<front>',
  );
  // Exported menu link: menu-bms-menu_trans-out-request:store-transfer/transfer-out-request.
  $menu_links['menu-bms-menu_trans-out-request:store-transfer/transfer-out-request'] = array(
    'menu_name' => 'menu-bms-menu',
    'link_path' => 'store-transfer/transfer-out-request',
    'router_path' => 'store-transfer/transfer-out-request',
    'link_title' => 'Trans out Request',
    'options' => array(
      'alter' => TRUE,
      'external' => 0,
      'identifier' => 'menu-bms-menu_trans-out-request:store-transfer/transfer-out-request',
      'attributes' => array(
        'class' => array(
          0 => 'fa',
          1 => 'fa-circle-o',
        ),
      ),
      'item_attributes' => array(
        'id' => '',
        'class' => '',
        'style' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -49,
    'customized' => 1,
    'parent_identifier' => 'menu-bms-menu_store-transfer:<front>',
  );

  // Translatables
  // Included for use with string extractors like potx.
  t(' Cash Sales');
  t('Admin User');
  t('Attendance');
  t('Commissary');
  t('Crew');
  t('Dashboard');
  t('Inventory');
  t('Manage Attendance');
  t('Manage Cash Sales');
  t('Manage Employee Schedule');
  t('Manage Inventory');
  t('Manage Production');
  t('Manage Sales Transaction');
  t('Order & Delivery');
  t('Order Request');
  t('Product');
  t('Product Sales');
  t('Production');
  t('Pull Out Request');
  t('Reports');
  t('Sales');
  t('Settings');
  t('Store');
  t('Store Operations Mgmt');
  t('Store Transfer');
  t('Store Transfers');
  t('Trans in Request');
  t('Trans out Request');
  t('by Area');
  t('by Store');
  t('by System Wide');
  t('by Transaction');

  return $menu_links;
}
