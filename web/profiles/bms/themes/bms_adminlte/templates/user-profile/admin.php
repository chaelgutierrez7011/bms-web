<section class="left fields-visible">
  <div class="photo-container">
    <img src="<?php print $user_image; ?>">
  </div>
</section>

<section class="center fields-visible ">

  <?php if (isset($elements['#account']->roles)) : ?>
    <div class="field field-row">
      <div class="field-label">Admin User Type:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php foreach ($elements['#account']->roles as $role_key => $role): ?>
            <?php if ($role != 'authenticated user'): ?>
              <?php print $role; ?>
            <?php endif; ?>
          <?php endforeach; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_firstname']) || isset($profile['field_lastname'])) : ?>
    <div class="field field-row">
      <div class="field-label">Full Name:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php print $profile['field_firstname'][0]['#markup']; ?>
          <?php print $profile['field_lastname'][0]['#markup']; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($elements['#account']->name)) : ?>
    <div class="field field-row">
      <div class="field-label">Username:</div>
      <div class="field-items">
        <div class="field-item even">
          <?php print $elements['#account']->name; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($user_profile['field_account_employee_id'])) : ?>
    <div class="field-row">
      <?php print render($user_profile['field_account_employee_id']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_address'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_address']); ?>
    </div>
  <?php endif; ?>

  <?php if (isset($elements['#account']->mail)) : ?>
    <div class="field field-row">
      <div class="field-label">Email:</div>
      <div class="field-items">
        <div class="field-item even email-word">
          <?php print $elements['#account']->mail; ?>
        </div>
      </div>
    </div>
  <?php endif; ?>

  <?php if (isset($profile['field_mobile_number'])) : ?>
    <div class="field-row">
      <?php print render($profile['field_mobile_number']); ?>
    </div>
  <?php endif; ?>

  <?php if ($employee_role == 'franchisee') : ?>
    <?php if (isset($profile['field_store_franchised'])) : ?>
      <?php print render($profile['field_store_franchised']); ?>
    <?php endif; ?>
  <?php endif; ?>

</section>

<div class="hidden-container">
  <?php print render($profile['field_gender']); ?>
  <?php print render($profile['field_birthdate']); ?>
  <?php print render($profile['field_marital_status']); ?>
  <?php print render($profile['field_date_of_hire']); ?>
  <?php print render($profile['field_end_of_contract']); ?>
  <?php print render($user_profile['field_store']); ?>
</div>


<section class="access-information">
  <h1>Access Information</h1>
  <div class="profile-links-wrapper">

    <?php if (isset($column_1)) : ?>
      <ul class="profile-links">
        <?php foreach ($column_1 as $key => $value) : ?>
          <?php print $value; ?>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if (isset($column_2)) : ?>
      <ul class="profile-links">
        <?php foreach ($column_2 as $key => $value) : ?>
          <?php print $value; ?>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if (isset($column_3)) : ?>
      <ul class="profile-links">
        <?php foreach ($column_3 as $key => $value) : ?>
          <?php print $value; ?>
        <?php endforeach; ?>
      </ul>
    <?php endif; ?>

    <?php if ($employee_role == 'franchisee') : ?>
      <p class="access-p">*Access limited to store(s) mentioned above.</p>
    <?php endif; ?>
  </div>
</section>
