<?php if ($employee_type == 'admin') : ?>
  <div class="profile-type-admin">
    <?php include_once 'user-profile-form-admin.php'; ?>
  </div>
<?php elseif ($employee_type == 'crew') : ?>
  <div class="profile-type-crew">
    <?php include_once 'user-profile-form-crew.php'; ?>
  </div>
<?php endif; ?>

<?php print drupal_render_children($form); ?>
  