<div class="box box-solid">
    <div class="box-header with-border">
        <h4 class="box-title">Add Overtime</h3>
    </div>
    <div class="box-body">
        <div class="form-group">
        <?php print render($form['overtime']); ?>
        <!-- /.input group -->
        </div>
        <div class="form-group">
        <?php print render($form['store_assigment']); ?>
        <?php print render($form['uid']); ?>
        </div>
        <?php print render($form['submit']); ?>
        <?php print drupal_render_children($form); ?>
    </div>
</div>