(function ($) {
  Drupal.behaviors.bmsDashboard = {
    attach: function (context, settings) {
      var peso_sales_by_monthly = Drupal.settings.bms_visualization.monthly_performance;

    /*
     * BAR CHART MONTHLY
     * ---------
     */
      var byMonthlyBarData = {
        labels: peso_sales_by_monthly.label,
        datasets: [
          {
            label: "Digital Goods",
            fillColor: "rgba(60,141,188,0.9)",
            strokeColor: "rgb(239, 148, 11)",
            pointColor: "#3b8bba",
            pointStrokeColor: "rgba(60,141,188,1)",
            pointHighlightFill: "#fff",
            pointHighlightStroke: "rgba(60,141,188,1)",
            data: peso_sales_by_monthly.data
          }
        ]
      };

      var byMonthlybarChartCanvas = $("#bar-chart-monthly").get(0).getContext("2d");
      var byMonthlybarChart = new Chart(byMonthlybarChartCanvas);
      var byMonthlybarChartData = byMonthlyBarData;

      byMonthlybarChartData.datasets[0].fillColor = "#F09B15";
      var byMonthlybarChartOptions = {
        //Boolean - Whether the scale should start at zero, or an order of magnitude down from the lowest value
        scaleBeginAtZero: true,
        //Boolean - Whether grid lines are shown across the chart
        scaleShowGridLines: true,
        //String - Colour of the grid lines
        scaleGridLineColor: "rgba(0,0,0,.05)",
        //Number - Width of the grid lines
        scaleGridLineWidth: 1,
        //Boolean - Whether to show horizontal lines (except X axis)
        scaleShowHorizontalLines: true,
        //Boolean - Whether to show vertical lines (except Y axis)
        scaleShowVerticalLines: true,
        //Boolean - If there is a stroke on each bar
        barShowStroke: true,
        //Number - Pixel width of the bar stroke
        barStrokeWidth: 2,
        //Number - Spacing between each of the X value sets
        barValueSpacing: 5,
        //Number - Spacing between data sets within X values
        barDatasetSpacing: 1,
        //String - A legend template
        legendTemplate: "<ul class=\"<%=name.toLowerCase()%>-legend\"><% for (var i=0; i<datasets.length; i++){%><li><span style=\"background-color:<%=datasets[i].fillColor%>\"></span><%if(datasets[i].label){%><%=datasets[i].label%><%}%></li><%}%></ul>",
        //Boolean - whether to make the chart responsive
        responsive: true,
        maintainAspectRatio: true
      };

      byMonthlybarChartOptions.datasetFill = false;
      byMonthlybarChart.Bar(byMonthlybarChartData, byMonthlybarChartOptions);
    /* END BAR CHART MONTHLY*/
    }
  }
})(jQuery);