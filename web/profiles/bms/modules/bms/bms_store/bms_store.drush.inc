<?php

/**
 * Implements hook_drush_command().
 */
function bms_store_drush_command() {
  $items = array();

  $items['generate-inventory'] = array(
    'callback' => 'generate_inventory',
    'description' => "Generate Inventory",
    'aliases' => array('gen-inventory'),
  );

  $items['generate-production'] = array(
    'callback' => 'generate_production',
    'description' => "Generate production inventory",
    'aliases' => array('gen-production'),
  );

  return $items;
}

/**
 * Implements hook_drush_help().
 */
function bms_store_drush_help($section) {
  switch ($section) {
    case 'drush:generate-inventory':
      $message = dt("Generate Inventory.");
      break;
    case 'drush:generate-production':
      $message = dt("Generate product inventory.");
      break;
  }
  return $message;
}

/**
 * Import inventory
 */
function generate_inventory() {
  $formatted_date = date('Y-m-d h:i:s', REQUEST_TIME);
  $date = _datetime_format_UTC($formatted_date);
  $branch = get_all_branch();
  $type = 'inventory';

  foreach ($branch as $store) {
    foreach(get_all_raw_materials($store->id, $type) as $product) {
      $entity = entity_create('store', array('type' => 'inventory'));
      $wrapper = entity_metadata_wrapper('store', $entity);
      $wrapper->title->set($store->title);
      $wrapper->field_store_id->set($store->id);
      $wrapper->field_reference_store_id->set($store->id);
      $wrapper->field_store_name->set($store->title);
      $wrapper->field_store_code->set($store->field_store_code_value);
      $wrapper->save();

      $entity_info = entity_load_single('store', $entity->id);

      drush_print('Processing store inventory of ' . $store->title . ' raw materials ' .  $product['item_name'] . ' ...');
      // Beginning inventory
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_beginning_inventory'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_qty[LANGUAGE_NONE][0]['value'] = $product['item_quantity'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Commissary Delivery
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_commissary_delivery'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Commissary Pullout
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_commissary_pullout'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Store trans in
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_store_transin'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Store trans out
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_store_transout'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Production/Processed
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_production_processed_open'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Wastages
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_wastages'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Ending Inventory Theoretical
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_ending_inventory_theo'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Ending Inventory Actual
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_ending_inventory_actual'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
      // Variance
      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_inventory_variance'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
    }
  }

  watchdog('bms_store', 'Generated inventory');
}

/**
 * Import production inventory.
 */
function generate_production() {
  $formatted_date = date('Y-m-d h:i:s', REQUEST_TIME);
  $date = _datetime_format_UTC($formatted_date);
  $branch = get_all_branch();
  $type = 'production';

  foreach ($branch as $branch_key => $store) {
    foreach(get_all_raw_materials($store->id, $type) as $raw_key => $product) {
      $entity = entity_create('store', array('type' => 'production'));
      $wrapper = entity_metadata_wrapper('store', $entity);
      $wrapper->title->set($store->title);
      $wrapper->field_store_id->set($store->id);
      $wrapper->field_reference_store_id->set($store->id);
      $wrapper->field_created->set(REQUEST_TIME);
      $wrapper->field_store_name->set($store->title);
      $wrapper->field_store_code->set($store->field_store_code_value);
      $wrapper->save();

      $entity_info = entity_load_single('store', $entity->id);

      drush_print('Processing store production inventory of ' . $store->title . ' raw materials ' .  $product['item_name'] . '...');

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_beginning_inventory'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_qty[LANGUAGE_NONE][0]['value'] = $product['item_quantity'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_processed_opened'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_sales'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_wastages'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_ending_inventory_theo'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_ending_inventory_actual'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();

      $fieldcollection = entity_create('field_collection_item', array('field_name' => 'field_production_variance'));
      $fieldcollection->setHostEntity('entity', $entity_info);
      $fieldcollection->field_created[LANGUAGE_NONE][0]['value'] = $date;
      $fieldcollection->field_item_id[LANGUAGE_NONE][0]['value'] = $product['item_id'];
      $fieldcollection->field_category[LANGUAGE_NONE][0]['tid'] = $product['category'];
      $fieldcollection->field_item[LANGUAGE_NONE][0]['value'] = $product['item_name'];
      $fieldcollection->field_uom[LANGUAGE_NONE][0]['value'] = $product['uom'];
      $fieldcollection->field_unit_price[LANGUAGE_NONE][0]['value'] = $product['unit_price'];
      $fieldcollection->field_item_code[LANGUAGE_NONE][0]['value'] = $product['item_code'];
      $fieldcollection->save();
    }
  }
  watchdog('bms_store', 'Generated production');
}

/**
 * Helper function getting raw materials.
 */
 function get_all_raw_materials($sid, $type) {
   $items = array();
   $raw_mats = init_raw_materials($sid, $type);

   if(empty($raw_mats)) {
     $query = db_select('eck_store', 'eck');
     $query->join('field_data_field_category', 'c', 'c.entity_id = eck.id');
     $query->join('field_data_field_item_code', 'sc', 'sc.entity_id = eck.id');
     $query->join('field_data_field_inventory_uom', 'uom', 'uom.entity_id = eck.id');
     $query->join('field_data_field_production_measurement', 'iup', 'iup.entity_id = eck.id');
     $query->fields('eck', array('title', 'id'));
     $query->fields('c', array('field_category_tid'));
     $query->fields('sc', array('field_item_code_value'));
     $query->fields('uom', array('field_inventory_uom_value'));
     $query->fields('iup', array('field_production_measurement_value'));
     $query->condition('eck.type', 'product');
     $query->condition('c.bundle', 'product');
     $query->condition('sc.bundle', 'product');
     $query->condition('uom.bundle', 'product');
     $query->condition('iup.bundle', 'product');

     $results = $query->execute()->fetchAll();
     foreach($results as $result) {
       $items[] = array(
         'item_id' => $result->id,
         'category' => $result->field_category_tid,
         'uom' => $result->field_inventory_uom_value,
         'unit_price' => $result->field_production_measurement_value,
         'item_name' => $result->title,
         'item_quantity' => 0,
         'item_code' => $result->field_item_code_value,
       );
     }
   }
   else {
     $items = $raw_mats;
   }

   return $items;
 }

 /**
  * Init raw materials
  */
 function init_raw_materials($sid, $type) {
   $items = array();

   $date = date('Y-m-d',strtotime("-1 days"));
   $query = db_select('eck_store', 'eck');
   $query->join('field_data_field_beginning_inventory', 'bi', 'bi.entity_id = eck.id');
   $query->join('field_data_field_ending_inventory_actual', 'ia', 'ia.entity_id = eck.id');
   $query->join('field_data_field_reference_store_id', 'si', 'si.entity_id = eck.id');
   $query->join('field_data_field_category', 'c', 'c.entity_id = eck.id');
   $query->join('field_data_field_item_code', 'sc', 'sc.entity_id = eck.id');
   $query->fields('c', array('field_category_tid'));
   $query->join('field_data_field_created', 'fc', 'fc.entity_id = bi.field_beginning_inventory_value');
   $query->join('field_data_field_item', 'fi', 'fi.entity_id = bi.field_beginning_inventory_value');
   $query->join('field_data_field_item_id', 'ii', 'ii.entity_id = bi.field_beginning_inventory_value');
   $query->join('field_data_field_qty', 'fq', 'fq.entity_id = ia.field_ending_inventory_actual_value');
   $query->fields('ii', array('field_item_id_value'));
   $query->fields('fi', array('field_item_value'));
   $query->fields('fq', array('field_qty_value'));
   $query->condition('si.field_reference_store_id_target_id', $sid);
   $query->condition('c.bundle', 'product');
   $query->condition('fc.field_created_value', db_like($date) . '%', 'LIKE');
   $query->condition('type', $type);
   $query->condition('sc.bundle', 'product');

   $results = $query->execute()->fetchAll();
   foreach($results as $result) {
     $items[] = array(
       'item_id' => $result->field_item_id_value,
       'category' => $result->field_category_tid,
       'item_name' => $result->field_item_value,
       'item_quantity' => $result->field_qty_value,
       'item_code' => $result->field_item_code_value,
     );
   }
   return $items;
 }

/**
 * Helper function getting branch.
 */
function get_all_branch() {
  $query = db_select('eck_store', 'eck');
  $query->join('field_data_field_store_code', 'sc', 'sc.entity_id = eck.id');
  $query->fields('eck', array('title', 'id'));
  $query->fields('sc', array('field_store_code_value'));
  $query->condition('eck.type', 'store');
  $results = $query->execute()->fetchAll();
  return $results;
}
