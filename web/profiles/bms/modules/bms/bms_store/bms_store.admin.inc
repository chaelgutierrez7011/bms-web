<?php
/**
 * Implement idgenerator_config_form
 */
function discount_config_form($form, &$form_state) {

  $form['comp_abbr'] = array(
    '#title' => t('Company Abbreviation'),
    '#type' => 'textfield',
    '#default_value' => variable_get('comp_abbr', 'BMS')
  );

  $form['emp_number'] = array(
    '#title' => t('Employee number format'),
    '#type' => 'textfield',
    '#default_value' => variable_get('emp_number', '000'),
  );

  $form['order_number'] = array(
    '#title' => t('Order number format'),
    '#type' => 'textfield',
    '#default_value' => variable_get('order_number', '000'),
  );

  return system_settings_form($form);
}

/**
 * Store inventory configuration
 */
function inventory_form($form, &$form_state) {

  $form['item'] = array(
    '#type' => 'textfield',
    '#title' => t('Item')
  );

  $form['inventory_uom'] = array(
    '#type' => 'select',
    '#title' => t('Inventory unit of measurement'),
    '#options' => array(
      '' => t('Select unit of measurement'),
      'box' => t('Box'),
      'bottle' => t('Bottle'),
      'bag' => t('Bag'),
      'pack' => t('Pack'),
      'pcs' => t('Pcs'),
      'grams' => t('Grams')
    ),
  );

  $form['production_uom'] = array(
    '#type' => 'select',
    '#title' => t('Production unit of measurement'),
    '#options' => array(
      '' => t('Select production unit of measurement'),
      'pcs' => t('Pcs'),
      'bottle' => t('Bottle'),
      'oz' => t('Oz'),
      'grams' => t('Grams')
    ),
  );

  $form['production_quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Production quantity')
  );

  $form['category'] = array(
    '#type' => 'textfield',
    '#title' => t('Category')
  );

  $form['sub_category'] = array(
    '#type' => 'textfield',
    '#title' => t('Sub-category')
  );

  $form['quantity'] = array(
    '#type' => 'textfield',
    '#title' => t('Quantity')
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );

  return $form;
}

/**
 * Implement cashier versions configuration.
 */
function store_cashier_versions_form($form, &$form_state) {

  $form['cashier_versions_group'] = array(
    '#type' => 'fieldset',
    '#title' => t('Cashier versions settings'),
    '#weight' => 5,
    '#collapsible' => TRUE,
    '#collapsed' => FALSE,
  );

  $active = array(0 => t('Generic'), 1 => t('Advance'));

  $form['cashier_versions_group']['cashier_version'] = array(
    '#type' => 'radios',
    '#title' => t('Type of cashier display'),
    '#default_value' => variable_get('cashier_version', 0),
    '#options' => $active,
    '#description' => t('When generic is selected the display of items in cashier are category > products and advance displays item product > flavors.'),
    '#access' => $admin,
  );

  return system_settings_form($form);
}

/**
 * Implement bms device skin configuration.
 */
function bms_device_skin_form($form, &$form_state) {

  $form['bms_logo'] = array(
    '#title' => t('BMS Logo'),
    '#type' => 'managed_file',
    '#description' => t('Upload BMS Logo here.'),
    '#default_value' => variable_get('bms_logo', ''),
    '#upload_location' => 'public://device/',
  );

  $form['bms_background'] = array(
    '#title' => t('BMS Background'),
    '#type' => 'managed_file',
    '#description' => t('Upload BMS background here.'),
    '#default_value' => variable_get('bms_background', ''),
    '#upload_location' => 'public://device/',
  );

  $form['#submit'][] = 'saved_skin_image';

  return system_settings_form($form);
}

/**
 * Submit handler for device skin
 */
function saved_skin_image($form, &$form_state) {
  // Logo
  // Load the file via file.fid.
  if(!empty($form_state['values']['bms_logo'])) {
    $file1 = file_load($form_state['values']['bms_logo']);
    // Change status to permanent.
    $file1->status = FILE_STATUS_PERMANENT;
    // Save.
    file_save($file1);
    // Record that the module (in this example, user module) is using the file.
    file_usage_add($file1, 'user', 'user', $file1->uid);
  }
  // Background
  if(!empty($form_state['values']['bms_background'])) {
    $file2 = file_load($form_state['values']['bms_background']);
    // Change status to permanent.
    $file2->status = FILE_STATUS_PERMANENT;
    // Save.
    file_save($file2);
    // Record that the module (in this example, user module) is using the file.
    file_usage_add($file2, 'user', 'user', $file2->uid);
  }
}

/**
 * Attendance Handler
 */
function attendance_configuration_form($form, &$form_state) {

  $form['attendance_break'] = array(
    '#type' => 'checkbox',
    '#title' => t('Disable attendance freezer'),
    '#description' => t('Disable freezer in reports'),
    '#default_value' => variable_get('attendance_break', 1)
  );

  return system_settings_form($form);
}
