; Packaging information for bms
name = "BMS"
description = ""
exclusive = "1"
core = "7.x"
; ----------
; Dependencies
; ----------
; Access control
dependencies[] = roles_for_menu
; Administration
dependencies[] = admin_menu
dependencies[] = adminimal_admin_menu
dependencies[] = module_filter
dependencies[] = module_missing_message_fixer
; BMS Commissary Services
dependencies[] = order_commissary_services
dependencies[] = pullout_request_services
; BMS Core
dependencies[] = angular_bms
dependencies[] = bms_calendar
dependencies[] = bms_core
dependencies[] = bms_dashboard
dependencies[] = bms_idgenerator
dependencies[] = bms_others
dependencies[] = bms_store
dependencies[] = bms_visualization
dependencies[] = convertion
dependencies[] = product_importer
dependencies[] = reasons
dependencies[] = request_manager
dependencies[] = user_manager
dependencies[] = views_advance_formatter
; BMS Core Services API
dependencies[] = attendance
dependencies[] = bms_services
dependencies[] = cash_management
dependencies[] = cashier_services
dependencies[] = commissary_services
dependencies[] = inventory_services
dependencies[] = product_services
dependencies[] = production_services
dependencies[] = store_services
dependencies[] = transfer_services
; BMS Transfer Services
dependencies[] = transfer_in_request_services
dependencies[] = transfer_out_request_services
; Chaos tool suite
dependencies[] = ctools
; Context
dependencies[] = context
dependencies[] = context_reaction_theme
dependencies[] = context_ui
; Core
dependencies[] = block
dependencies[] = color
dependencies[] = comment
dependencies[] = contextual
dependencies[] = dblog
dependencies[] = field
dependencies[] = field_sql_storage
dependencies[] = field_ui
dependencies[] = file
dependencies[] = filter
dependencies[] = help
dependencies[] = image
dependencies[] = list
dependencies[] = menu
dependencies[] = node
dependencies[] = number
dependencies[] = options
dependencies[] = path
dependencies[] = rdf
dependencies[] = search
dependencies[] = system
dependencies[] = taxonomy
dependencies[] = text
dependencies[] = trigger
dependencies[] = update
dependencies[] = user
; Date/Time
dependencies[] = date
dependencies[] = date_api
dependencies[] = date_popup
dependencies[] = date_views
dependencies[] = views_between_dates_filter
; Development
dependencies[] = profiler_builder
dependencies[] = profiler_builder_extras
; ECK
dependencies[] = eck
dependencies[] = eck_services
; Fields
dependencies[] = double_field
dependencies[] = entityreference
dependencies[] = entityreference_autofill
dependencies[] = field_collection
dependencies[] = field_group
dependencies[] = field_permissions
dependencies[] = office_hours
dependencies[] = phone
dependencies[] = references
dependencies[] = user_reference
; Link Badges
dependencies[] = link_badges
; Menu
dependencies[] = void_menu
; Other
dependencies[] = angularjs_bms_report
dependencies[] = anonymous_login
dependencies[] = asaf
dependencies[] = autodialog
dependencies[] = bms_deploy
dependencies[] = cors
dependencies[] = custom_field
dependencies[] = disable_draggable
dependencies[] = entity
dependencies[] = entity_bulk_delete
dependencies[] = entity_token
dependencies[] = example_report
dependencies[] = field_collection_table
dependencies[] = field_collection_views
dependencies[] = fontawesome
dependencies[] = identicon
dependencies[] = image_url_formatter
dependencies[] = libraries
dependencies[] = menu_attributes
dependencies[] = menu_badges
dependencies[] = menu_block
dependencies[] = pathauto
dependencies[] = pathauto_entity
dependencies[] = phpexcel
dependencies[] = profile2
dependencies[] = profile2_page
dependencies[] = quicktabs
dependencies[] = quicktabs_tabstyles
dependencies[] = role_export
dependencies[] = strongarm
dependencies[] = token
dependencies[] = token_filter
dependencies[] = views_extra_field
dependencies[] = visualization
; Profile2 (contrib)
dependencies[] = account_profile
; Rules
dependencies[] = rules
dependencies[] = rules_admin
; Services
dependencies[] = services
; Services - servers
dependencies[] = rest_server
; Spam control
dependencies[] = captcha
; Trash
dependencies[] = table_trash
; UUID
dependencies[] = uuid
; User interface
dependencies[] = accordion_menu
dependencies[] = jquery_update
; Views
dependencies[] = better_exposed_filters
dependencies[] = editableviews
dependencies[] = views
dependencies[] = views_aggregator
dependencies[] = views_calc
dependencies[] = views_data_export
dependencies[] = views_load_more
dependencies[] = views_php
dependencies[] = views_ui
; Features
dependencies[] = features
dependencies[] = account_settings
dependencies[] = bms_context
dependencies[] = bms_image_styles
dependencies[] = bms_menu
dependencies[] = bms_misc
dependencies[] = bms_reports
dependencies[] = bms_roles
dependencies[] = bms_url_alias
dependencies[] = entity_type
dependencies[] = features_override
dependencies[] = features_roles_permissions
dependencies[] = inventory
dependencies[] = profile_type
dependencies[] = services_configuration
dependencies[] = store_employee
dependencies[] = store_franchisee
dependencies[] = transfer_request
dependencies[] = trash_table_configuration
dependencies[] = uuid_features

; ----------
; Variables
; ----------
variables[account_profile_wrap_account_title] = 'User account'
variables[admin_theme] = 'seven'
variables[anonymous] = 'Anonymous'
variables[asaf_forms] = 'eck__entity__form_edit_store_product
eck__entity__form_add_store_product
eck__entity__form_add_store_store
eck__entity__form_add_store_cashier_items
asaf_admin_settings_form
add_user_form
user_register_form
field_collection_item_form'
variables[autodialog_default_options] = '{"width":"700", "resizable":false, "draggable":false}'
variables[autodialog_source] = 'block'
variables[cache_class_cache_ctools_css] = 'CToolsCssCache'
variables[captcha_default_challenge] = 'captcha/Math'
variables[captcha_log_wrong_responses] = 1
variables[captcha_placement_map_cache][user_login_block][key] = 'actions'
variables[captcha_placement_map_cache][user_login_block][weight] = 99
variables[context_block_rebuild_needed] = true
variables[date_api_version] = '7.2'
variables[date_default_timezone] = 'Asia/Manila'
variables[date_format_bms_default_date] = 'm/d/Y'
variables[date_format_bms_short_date] = 'F j, Y - g:ia'
variables[date_format_bms_time] = 'H:i a'
variables[date_format_date] = 'Y M d'
variables[date_format_day] = 'l'
variables[date_format_month] = 'F'
variables[date_format_time] = 'h:i:s a'
variables[date_views_day_format_without_year] = 'l, F j'
variables[date_views_day_format_with_year] = 'l, F j, Y'
variables[date_views_month_format_without_year] = 'F'
variables[date_views_month_format_with_year] = 'F Y'
variables[date_views_week_format_without_year] = 'F j'
variables[date_views_week_format_with_year] = 'F j, Y'
variables[entityreference:base-tables][comment][0] = 'comment'
variables[entityreference:base-tables][comment][1] = 'cid'
variables[entityreference:base-tables][store][0] = 'eck_store'
variables[entityreference:base-tables][store][1] = 'id'
variables[entityreference:base-tables][field_collection_item][0] = 'field_collection_item'
variables[entityreference:base-tables][field_collection_item][1] = 'item_id'
variables[entityreference:base-tables][node][0] = 'node'
variables[entityreference:base-tables][node][1] = 'nid'
variables[entityreference:base-tables][profile2][0] = 'profile'
variables[entityreference:base-tables][profile2][1] = 'pid'
variables[entityreference:base-tables][profile2_type][0] = 'profile_type'
variables[entityreference:base-tables][profile2_type][1] = 'id'
variables[entityreference:base-tables][file][0] = 'file_managed'
variables[entityreference:base-tables][file][1] = 'fid'
variables[entityreference:base-tables][taxonomy_term][0] = 'taxonomy_term_data'
variables[entityreference:base-tables][taxonomy_term][1] = 'tid'
variables[entityreference:base-tables][taxonomy_vocabulary][0] = 'taxonomy_vocabulary'
variables[entityreference:base-tables][taxonomy_vocabulary][1] = 'vid'
variables[entityreference:base-tables][user][0] = 'users'
variables[entityreference:base-tables][user][1] = 'uid'
variables[entityreference:base-tables][rules_config][0] = 'rules_config'
variables[entityreference:base-tables][rules_config][1] = 'id'
variables[filter_fallback_format] = 'plain_text'
variables[identicon_blocks] = '2'
variables[identicon_enabled] = 1
variables[identicon_resolution] = '2'
variables[identicon_source] = 'uid'
variables[menu_expanded][0] = 'menu-bms-menu'
variables[node_admin_theme] = '1'
variables[node_options_page][0] = 'status'
variables[pathauto_blog_pattern] = 'blogs/[user:name]'
variables[pathauto_forum_pattern] = '[term:vocabulary]/[term:name]'
variables[pathauto_node_pattern] = 'content/[node:title]'
variables[pathauto_profile2_main_pattern] = 'profile/[profile2:field-firstname]-[profile2:field-lastname]'
variables[pathauto_punctuation_hyphen] = 1
variables[pathauto_store_commissary_pattern] = 'commissary/order-request/[store:id]'
variables[pathauto_store_commissary_pullout_pattern] = 'commissary/pullout-request/[store:id]'
variables[pathauto_store_store_pattern] = 'store/[store:field_store_name]/view'
variables[pathauto_store_store_transfer_pattern] = 'transfer-item/[store:id]'
variables[pathauto_store_store_transfer_pullout_pattern] = 'transfer-item/[store:id]'
variables[pathauto_taxonomy_term_pattern] = '[term:vocabulary]/[term:name]'
variables[pathauto_user_pattern] = 'users/[user:name]'
variables[path_alias_whitelist][admin] = true
variables[path_alias_whitelist][taxonomy] = true
variables[path_alias_whitelist][user] = true
variables[services_security_update_1] = true
variables[site_frontpage] = 'dashboard'
variables[table_trash_decorations][1][decoration-params][search-box] = 0
variables[table_trash_decorations][1][decoration-params][column-reorder] = 1
variables[table_trash_decorations][1][decoration-params][export-buttons] = 1
variables[table_trash_decorations][1][decoration-params][retrieve-data] = 0
variables[table_trash_decorations][1][decoration-params][pager-style] = ''
variables[table_trash_decorations][1][decoration-params][page-height] = ''
variables[table_trash_decorations][1][decoration-params][dont-sort-columns] = ''
variables[table_trash_decorations][1][decoration-params][x-scroll] = ''
variables[table_trash_decorations][1][decoration-params][fixed-left-columns] = ''
variables[table_trash_decorations][1][decoration-params][fixed-header] = 0
variables[table_trash_decorations][1][decoration-params][responsive][responsive-expand-col] = ''
variables[table_trash_decorations][1][decoration-params][responsive][responsive-collapse-cols-phone] = ''
variables[table_trash_decorations][1][decoration-params][responsive][responsive-collapse-cols-tablet] = ''
variables[table_trash_decorations][1][pages-and-selector][include-pages] = 'sales
cash-management'
variables[table_trash_decorations][1][pages-and-selector][exclude-pages] = 'admin/reports/status
admin/modules*'
variables[table_trash_decorations][1][pages-and-selector][table-selector] = ''
variables[table_trash_global_settings][load_from] = 'module'
variables[table_trash_global_settings][use_datatables_css] = 1
variables[table_trash_global_settings][use_table_trash_css] = 1
variables[theme_default] = 'bms_adminlte'
variables[user_cancel_method] = 'user_cancel_block'
variables[user_email_verification] = 1
variables[user_pictures] = 1
variables[user_picture_default] = 'profiles/bms/themes/bms_adminlte/assets/images/no-image.jpg'
variables[user_picture_file_size] = '1500'
variables[user_picture_path] = 'pictures'
variables[user_picture_style] = 'square_400x400'
variables[user_register] = '0'

