<?php

/**
 * @file
 * bms_others.block.inc
 */

/**
 * Render Store Assignments Block.
 */
function bms_others_render_store_assignments_block_block() {
  global $base_url;
  $user_id = arg(1);
  $theme = $base_url . '/' . drupal_get_path('theme', 'bms_adminlte');

  $employee_user_main = user_load($user_id, $reset = FALSE);

  $variables = array();

  if (isset($employee_user_main->field_store['und'])) {
    $variables['stores']  = array();
    foreach ($employee_user_main->field_store['und'] as $key => $value) {
      $variables['stores'][] = $value['entity']->title;
    }
  }

  $output = theme('bms_others_store_assignments_block', $variables);

  return array(
    'subject' => '',
    'content' => $output,
  );
}

/**
 * Render Tab Menu Block.
 */
function bms_others_render_tab_menu_block_block() {

  $variables = array();

  $attendance_report = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'attendance');
  $manage_attendance_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-attendance');
  $inventory_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'inventory-summary');
  $production_summary_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'production-summary');
  $manage_production_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-production');
  $cash_management_detail = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'cash-management');
  $manage_cash_sales_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-cash-management');
  $manage_inventory_inner = (arg(0) == 'store' && is_numeric(arg(1)) && arg(2) == 'manage-inventory');

  if ($attendance_report || $manage_attendance_inner) {
    $variables['tab_menu_type'] = 'attendance';
  }
  elseif ($inventory_summary_detail || $manage_inventory_inner) {
    $variables['tab_menu_type'] = 'inventory_summary';
  }
  elseif ($production_summary_detail || $manage_production_inner) {
    $variables['tab_menu_type'] = 'production_summary';
  }
  elseif ($cash_management_detail || $manage_cash_sales_inner) {
    $variables['tab_menu_type'] = 'cash_management';
  }

  $output = theme('bms_others_tab_menu_block', $variables);

  return array(
    'subject' => '',
    'content' => $output,
  );
}