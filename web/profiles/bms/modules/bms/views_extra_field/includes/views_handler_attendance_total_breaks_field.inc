<?php
/**
 * Custom handler class.
 *
 * @ingroup views_field_handlers
 */
  class views_handler_attendance_total_breaks_field extends views_handler_field {
  /**
   * {@inheritdoc}
   *
   * Perform any database or cache data retrieval here. In this example there is
   * none.
   */
  function query() {

  }
  /**
   * {@inheritdoc}
   *
   * Modify any end user views settings here. Debug $options to view the field
   * settings you can change.
   */
  function option_definition() {
    $options = parent::option_definition();
    return $options;
  }

  /**
   * {@inheritdoc}
   *
   * Make changes to the field settings form seen by the end user when adding
   * your field.
   */
   function options_form(&$form, &$form_state) {
     parent::options_form($form, $form_state);
   }

 /**
  * Render callback handler.
  *
  * Return the markup that will appear in the rendered field.
  */
  function render($values) {
    $date = explode(' ', $values->field_field_date_filter_1[0]['raw']['value']);
    $sid = $values->_field_data['field_reference_store_id_eck_store_id']['entity']->field_store_id['und'][0]['value'];
    $username = $values->field_field_username[0]['raw']['value'];

    return $this->get_total_breaks($date[0], $sid, $username);
  }

  /**
   * Callback function getting total break.
   */
   public static function get_total_breaks($date, $sid, $username) {
    $user = user_load_by_name($username);
    $uid = $user->uid;

    $query = db_query("SELECT
        TIME_FORMAT(SEC_TO_TIME(SUM(TIME_TO_SEC(field_total_no_of_breaks_minutes_value))), '%H:%i')
        FROM
        eck_store eck
        INNER JOIN field_data_field_date_filter df ON df.entity_id = eck.id AND df.bundle = 'attendance'
        INNER JOIN field_data_field_reference_store_id si ON si.entity_id = eck.id AND si.bundle = 'attendance'
        INNER JOIN field_data_field_break_freezer fzr ON fzr.entity_id = eck.id
        INNER JOIN field_data_field_total_no_of_breaks_minutes fb ON fb.entity_id = fzr.field_break_freezer_value
        WHERE (field_reference_store_id_target_id = :sid) AND (field_date_filter_value LIKE :date_filter) AND (eck.uid = :uid)
        GROUP BY fzr.entity_id", 
        array(':sid' => $sid, ':date_filter' => '%' . db_like($date) . '%', ':uid' => $uid))->fetchField();
    return $query;
   }
}