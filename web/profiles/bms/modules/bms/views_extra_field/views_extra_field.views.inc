<?php
/**
 * Implements hook_views_data().
 */
function views_extra_field_views_data() {
  $data['views_extra_field']['table']['group'] = t('views extra field');
  $data['views_extra_field']['table']['join'] = array(
    // Exist in all views.
    '#global' => array(),
  );

  $data['views_extra_field']['average_check_field'] = array(
    'title' => t('Average Check'),
    'help' => t('Displays average check.'),
    'field' => array(
      'handler' => 'views_handler_average_check_field',
    ),
  );

  $data['views_extra_field']['transaction_count_field'] = array(
    'title' => t('Transaction Count'),
    'help' => t('Displays transaction count.'),
    'field' => array(
      'handler' => 'views_handler_transaction_count_field',
    ),
  );

  $data['views_extra_field']['percentage_to_sales_field'] = array(
    'title' => t('Percentage to sales'),
    'help' => t('Displays percentage to sales.'),
    'field' => array(
      'handler' => 'views_handler_percentage_to_sales_field',
    ),
  );

  $data['views_extra_field']['hourly_range_field'] = array(
    'title' => t('Hour'),
    'help' => t('Display hourly range.'),
    'field' => array(
      'handler' => 'views_handler_hourly_range_field',
    ),
  );

  $data['views_extra_field']['amount_field'] = array(
    'title' => t('Amount'),
    'help' => t('Display Amount.'),
    'field' => array(
      'handler' => 'views_handler_amount_field',
    ),
  );

  $data['views_extra_field']['weekly_range_field'] = array(
    'title' => t('Week'),
    'help' => t('Display week range'),
    'field' => array(
      'handler' => 'views_handler_weekly_range_field',
    ),
  );

  $data['views_extra_field']['ending_inventory_theoretical_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Display ending inventory theoretical'),
    'field' => array(
      'handler' => 'views_handler_ending_inventory_theo_field',
    ),
  );
  
  $data['views_extra_field']['ending_inventory_variance_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Display ending inventory variance'),
    'field' => array(
      'handler' => 'views_handler_ending_inventory_variance_field',
    ),
  );

  $data['views_extra_field']['ending_inventory_production_theoretical_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Display ending inventory production theoretical'),
    'field' => array(
      'handler' => 'views_handler_ending_production_inventory_theo_field',
    ),
  );

  $data['views_extra_field']['ending_inventory_production_variance_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Display ending inventory production variance'),
    'field' => array(
      'handler' => 'views_handler_ending_production_inventory_variance_field',
    ),
  );

  $data['views_extra_field']['product_sales_qty'] = array(
    'title' => t('Quantity'),
    'help' => t('Display product sales quantity'),
    'field' => array(
      'handler' => 'views_handler_product_sales_quantity_field',
    ),
  );

  $data['views_extra_field']['product_sales_total'] = array(
    'title' => t('Peso Total'),
    'help' => t('Display product sales total'),
    'field' => array(
      'handler' => 'views_handler_product_sales_total_field',
    ),
  );

  $data['views_extra_field']['product_sales_percentage'] = array(
    'title' => t('Peso%'),
    'help' => t('Display product sales percentage in peso'),
    'field' => array(
      'handler' => 'views_handler_product_sales_percentage_field',
    ),
  );

  $data['views_extra_field']['attendance_total_breaks_field'] = array(
    'title' => t('Total Breaks'),
    'help' => t('Display breaks in total'),
    'field' => array(
      'handler' => 'views_handler_attendance_total_breaks_field',
    ),
  );

  $data['views_extra_field']['product_sales_volume'] = array(
    'title' => t('Volume %'),
    'help' => t('Display product sales volume in peso'),
    'field' => array(
      'handler' => 'views_handler_product_sales_volume_field',
    ),
  );

  $data['views_extra_field']['employee_full_name'] = array(
    'title' => t('Crew'),
    'help' => t('Display employee name'),
    'field' => array(
      'handler' => 'views_handler_employee_full_name_field',
    ),
  );

  $data['views_extra_field']['button_link_commissary'] = array(
    'title' => t('Add commissary button'),
    'help' => t('Display commissary button'),
    'area' => array(
      'handler' => 'views_handler_button_link_commissary_field'
    ),
  );

  $data['views_extra_field']['button_link_transfer_in'] = array(
    'title' => t('Add transfer'),
    'help' => t('Display add transfer button lik'),
    'area' => array(
      'handler' => 'views_handler_button_link_transfer_field'
    ),
  );

  return $data;
}
