<div ng-app="starter" ng-controller="StoreProductionCtrl">

    <div class="tab-navigations">
        <ul class="tabs-menu">
            <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Beginning Inventory</a></li>
            <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Processed/Opened</a></li>
            <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Sales</a></li>
            <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Wastages</a></li>
            <li ng-class="{'active': activeTab == 4}"><a href="" ng-click="setActiveTab(4)">Ending Inventory (Theoretical)</a></li>
            <li ng-class="{'active': activeTab == 5}"><a href="" ng-click="setActiveTab(5)">Ending Inventory (Actual)</a></li>
            <li ng-class="{'active': activeTab == 6}"><a href="" ng-click="setActiveTab(6)">Variance</a></li>
        </ul>
    </div>
    <div class="tab-content">
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
            <div ng-hide="!tabs[0].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[0].content">
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
            <div ng-hide="!tabs[1].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Time</th>
                    <th>Crew Name</th>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    <th>Edit</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[1].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }" ng-cloak>{{ item.created }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }" ng-cloak>{{ item.crew_name }}</td>
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                        <td>
                          <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalProductionOpened(item._id, 1, $index)">
                            <span class="glyphicon glyphicon-edit"></span>
                          </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
            <div ng-hide="!tabs[2].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[2].content">
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
            <div ng-hide="!tabs[3].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Time</th>
                    <th>Crew Name</th>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    <th>Reason</th>
                    <th>Edit</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[3].content">
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }" ng-cloak>{{ item.created }}</td>
                        <td ng-class="{ 'hide-data': (item.class.length > 0) }" ng-cloak>{{ item.crew_name }}</td>
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.reason }}</td>
                        <td>
                          <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalProductionWastage(item._id, 3, $index)">
                            <span class="glyphicon glyphicon-edit"></span>
                          </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 4, 'tab-pane' : activeTab !== 4}">
            <div ng-hide="!tabs[4].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(4)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[4].content">
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[4].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 5, 'tab-pane' : activeTab !== 5}">
            <div ng-hide="!tabs[5].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(5)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Crew Name</th>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    <th>Edit</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[5].content">
                        <td ng-cloak>{{ item.crew_name }}</td>
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                        <td>
                          <button class="btn btn-block btn-default" id="transaction-view" ng-click="showModalProductionActual(item._id, 5, $index)">
                            <span class="glyphicon glyphicon-edit"></span>
                          </button>
                        </td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[5].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 6, 'tab-pane' : activeTab !== 6}">
            <div ng-hide="!tabs[6].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(6)" placeholder="yyyy-MM-dd" required />
                <table datatable="ng" class="row-border hover" dt-options="dtOptions">
                    <thead>
                    <th>Inventory Category</th>
                    <th>Inventory Item</th>
                    <th>Inventory Item Code</th>
                    <th>Converted QTY</th>
                    <th>UOM</th>
                    <th>Unit Price</th>
                    <th>Total</th>
                    </thead>
                    <tbody>
                    <tr ng-repeat="item in tabs[6].content">
                        <td ng-cloak>{{ item.category }}</td>
                        <td ng-cloak>{{ item.item }}</td>
                        <td ng-cloak>{{ item.item_code }}</td>
                        <td ng-cloak>{{ item.converted_qty }}</td>
                        <td ng-cloak>{{ item.uom }}</td>
                        <td ng-cloak>{{ item.unit_price | currency:"₱":2}}</td>
                        <td ng-cloak>{{ item.total | currency:"₱":2}}</td>
                    </tr>
                    </tbody>
                </table>
            </div>
            <div ng-hide="tabs[6].isLoaded"><loading></loading></div>
          </div>
       </div>
    </div>
</div>
