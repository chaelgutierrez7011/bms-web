(function ($, Drupal, window, document, undefined) {
// To understand behaviors, see https://drupal.org/node/756722#behaviors
Drupal.behaviors.bms_theme = {
  attach: function(context, settings) {

    // Timepicker.
    $(".timepicker").timepicker({
      showInputs: false
    });

    // Change title of Store Operations Mgmt: Manage Attendance.
    var full_name = '';
    $('a.autodialog').click(function () {
      $('a.autodialog').removeClass('clicked');
      $(this).addClass('clicked');
    });

    if ($('.page-store-manage-inventory').length) {
    }
    else {
      if ($('#field-collection-item-form').length) {
        var full_name = $('a.autodialog.clicked').attr('full-name');
        $('.ui-dialog-title').html('Edit: ' + full_name);
      }
    }

    // Initialize checkbox ui.
    if ($('.store-inventory, .store-products').length) {
      $('input[type="checkbox"]').iCheck({
        checkboxClass: 'icheckbox_flat-green',
      });
    }

    // Initialize Select2 on select boxes.
    var select_boxes = $('.views-exposed-form select, #eck-entity-form-edit-store-store select, #user-profile-form select');
    select_boxes.select2();

    // Renaming first option value to 'All'.
    select_boxes.on('select2:open', function (evt) {
      setTimeout(function() {
        if ($('.view.colored-boxes').length || $('.view.view-boxes').length) {
          $('.select2-container ul.select2-results__options li:first-child').html('All');
        }
      }, 10);
    });
    $('.select2-selection--single').on('click', function() {
      if ($('.view.colored-boxes').length || $('.view.view-boxes').length) {
        $('.select2-container ul.select2-results__options li:first-child').html('All');
      }
    });
    $(window).load(function() {
      if ($('form[name="sales-by-store-form"]').length) {
        $('select').each(function () {
          var option_text = $(this).find('option').eq(0).html();
          $(this).prepend('<option value="">All</option>');
          $(this).children('option').each(function() {
            if ($(this).html() == option_text) {
              $(this).css('display', 'none');
            }
          }); 
        });
      }
    });
      
    
    // Change title of Product settings modal forms.
    if ($('.page-manage-product').length) {

      $('input[id*="edit-title"]').change(function() {
        var value = $(this).val();
        $('input[id*="edit-field-product-name-und-0-value"]').val(value);
      });

      if ($('#eck-entity-form-add-store-cashier-items').length) {
        $('.ui-dialog-title').html('Add Product');
      }
      if ($('#eck-entity-form-edit-store-product').length) {
        var product_title = $('#eck-entity-form-edit-store-product .form-item-title input').val();
        $('.ui-dialog-title').html('Edit: ' + product_title);
      }
    }

    // Change title of Inventory settings modal forms.
    if ($('.page-inventory').length) {
      if ($('#eck-entity-form-add-store-product').length) {
        $('.ui-dialog-title').html('Add Item');
      }
      if ($('form.edit-inventory-form').length) {
        var inventory_title = $('.form-item-field-item-inventory input').val();
        $('.ui-dialog-title').html('Edit: ' + inventory_title);
      }
    }

    // Change title of Crew settings modal forms.
    if ($('.page-administer-employee').length) {
      if ($('#user-register-form').length) {
        $('.ui-dialog-title').html('Add Crew');
        $('.ui-dialog-titlebar-close').html('<span class="ui-button-icon-primary ui-icon ui-icon-closethick"></span>');
        $('.ui-dialog-titlebar-close').addClass('ui-button ui-widget ui-state-default ui-corner-all ui-button-icon-only');

        $('label.option:contains("crew")').prev().prop('checked', true);
        $('.form-item-status input[value="1"]').prop('checked', true);

        $('select[id*="edit-user-position"]').change(function() {
          var selected = $(this).val();
          $('[id*="edit-roles"] input').prop('checked', false);
          $('label.option:contains("authenticated user")').prev().prop('checked', true);
          $('label.option:contains("' + selected + '")').prev().prop('checked', true);
        });

        $('select[id*="edit-user-status"]').change(function () {
          var selected = $(this).val();
          if (selected == 1) {
            $('.form-item-status input[value="1"]').prop('checked', true);
          }
          else if (selected == 0) {
            $('.form-item-status input[value="0"]').prop('checked', true);
          }
        });

      }
    }

    // For Tabs.
    if ($('.reports-store-transfers-inner, .page-commissary-store').length) {
      if ($('.quicktabs-tabpage:nth-child(1) .tab-navigations').length) {
        $(".quicktabs-tabpage:nth-child(1) .tab-navigations .tabs-menu a").click(function(event) {
          event.preventDefault();
          $(this).parent().addClass("current");
          $(this).parent().siblings().removeClass("current");
          var tab = $(this).attr("href");
          $(".quicktabs-tabpage:nth-child(1) .tab-content").not(tab).css("display", "none");
          $('.quicktabs-tabpage:nth-child(1)').find(tab).show();
        });
      }
      if ($('.quicktabs-tabpage:nth-child(2) .tab-navigations').length) {
        $(".quicktabs-tabpage:nth-child(2) .tab-navigations .tabs-menu a").click(function(event) {
          event.preventDefault();
          $(this).parent().addClass("current");
          $(this).parent().siblings().removeClass("current");
          var tab = $(this).attr("href");
          $(".quicktabs-tabpage:nth-child(2) .tab-content").not(tab).css("display", "none");
          $('.quicktabs-tabpage:nth-child(2)').find(tab).show();
        });
      }
    }
    else {
      if (!$('.page-store-manage-inventory, .page-store-manage-production, .page-store-inventory-summary, .page-store-production-summary, .page-store-manage-cash-management, .page-store-cash-management, .page-sales-by-store, .page-sales-by-area, .page-sales-by-system-wide, .page-product-sales-store-category, .page-product-sales-by-system-wide, .page-product-sales-by-system-wide-all, .page-product-sales-store-all, .page-product-sales-by-system-wide-product-all, .page-store-attendance, .page-store-manage-attendance, .page-store-inventory-report, .page-store-production-report').length) {
        if ($('.tab-navigations').length) {
          $(".tab-navigations .tabs-menu a").click(function(event) {
            event.preventDefault();
            $(this).parent().addClass("current");
            $(this).parent().siblings().removeClass("current");
            var tab = $(this).attr("href");
            $(".tab-content").not(tab).css("display", "none");
            $(tab).show();
          });
        }
      }
    }

    // Adding class to modals.
    if ($('#manage-transaction-void-form').length) {
      $('#manage-transaction-void-form').closest('.ui-dialog').addClass('manage-transaction-void-form');
    }
    if ($('#manage-transaction-form').length) {
      $('#manage-transaction-form').closest('.ui-dialog').addClass('manage-transaction-form');
    }
    if ($('.edit-cash-pullout-form').length) {
      $('.edit-cash-pullout-form').closest('.ui-dialog').addClass('edit-cash-pullout-form');
    }
    if ($('.edit-cash-deposit-form').length) {
      $('.edit-cash-deposit-form').closest('.ui-dialog').addClass('edit-cash-deposit-form');
    }

    // Adding Edit button to commissary modal forms.
    if ($('#order-pullout-view-form, #order-request-view-form, #store-transfer-in-view-form, #store-transfer-out-view-form, .page-admin-commissary #commissary-order-request-view-form, .page-admin-commissary #commissary-pullout-request-view-form').length) {
      var edit = '<div id="edit"><a href="#">Edit</a></div>';
      $('.ui-dialog-titlebar').once().append(edit);
    }

    // For Ajax Throbber image.
    $(document).ajaxStart(function() {
      $('.throbber-wrapper').show();
    });
    $(document).ajaxSuccess(function() {
      $('.throbber-wrapper').hide();
    });

    // Disable input fields in commissary and store transfers.
    if ($('#store-transfer-in-view-form, #store-transfer-out-view-form, #order-request-view-form, #order-pullout-view-form, #commissary-pullout-request-view-form, #commissary-order-request-view-form, #commissary-pullout-request-view-form').length) {
      $('input[type="text"]').attr('disabled', 'disabled');
      $('.page-store-transfer .form-select.form-control').attr('disabled', 'disabled');
    }

    // Hide cancel and save button
    $("#store-transfer-in-view-form input[value='Cancel'], #store-transfer-out-view-form input[value='Cancel']").hide();
    $("#store-transfer-in-view-form input[value='Save Changes'], #store-transfer-out-view-form input[value='Save Changes']").hide();

    $("#order-request-view-form input[value='Cancel'], #order-pullout-view-form input[value='Cancel'], #commissary-order-request-view-form input[value='Cancel'], #commissary-pullout-request-view-form input[value='Cancel']").hide();
    $("#order-request-view-form input[value='Save Changes'], #order-pullout-view-form input[value='Save Changes'], #commissary-order-request-view-form input[value='Save Changes'], #commissary-pullout-request-view-form input[value='Save Changes']").hide();

    //$('div#target-date-hide').show();
    //$('div#target-date-show').hide();

    if($("#hide-edit").length > 0) {
      $("#edit").remove();
    }

    $("#edit").on('click', function(){
    $("#store-transfer-in-view-form input[value='Cancel'], #store-transfer-out-view-form input[value='Cancel']").show();
    $("#store-transfer-in-view-form input[value='Save Changes'], #store-transfer-out-view-form input[value='Save Changes']").show();
    $('div#target-date-show').show();
    $('div#target-date-hide').hide();

    $("#order-request-view-form input[value='Cancel'], #order-pullout-view-form input[value='Cancel'], #commissary-order-request-view-form input[value='Cancel'], #commissary-pullout-request-view-form input[value='Cancel']").show();
    $("#order-request-view-form input[value='Save Changes'], #order-pullout-view-form input[value='Save Changes'], #commissary-order-request-view-form input[value='Save Changes'], #commissary-pullout-request-view-form input[value='Save Changes']").show();

      $("#edit").hide();
      // Hide other button
      $("#store-transfer-in-view-form input[value='Disapprove'], #store-transfer-out-view-form input[value='Disapprove']").hide();
      $("#store-transfer-in-view-form input[value='Approve'], #store-transfer-out-view-form input[value='Approve']").hide();

      $("#order-request-view-form input[value='Disapprove'], #order-pullout-view-form input[value='Disapprove'], #commissary-order-request-view-form input[value='Disapprove'], #commissary-pullout-request-view-form input[value='Disapprove']").hide();
      $("#order-request-view-form input[value='Approve'], #order-pullout-view-form input[value='Approve'], #commissary-order-request-view-form input[value='Approve'], #commissary-pullout-request-view-form input[value='Approve']").hide();

      if ($('#store-transfer-in-view-form, #store-transfer-out-view-form, #order-request-view-form, #order-pullout-view-form, #commissary-order-request-view-form, #commissary-pullout-request-view-form').length) {
        $('.form-item-delivery-date-co-date input, .form-item-commissary-delivery-no input, .form-item-pullout-date-co-date input').removeAttr('disabled');
        $('.form-item-target-transfer-date-date input').removeAttr('disabled');
        $('table input, table select').removeAttr('disabled');
        $('.page-store-transfer select').removeAttr('disabled');
      }

    });

    $.fn.savechanges = function(data) {
      $("#edit").show();
      $("div#edit-delivery-date-co div input[type='text']").attr('disabled', 'disabled');
      
      // Orders
      $("#order-request-view-form input[value='Disapprove'], #order-pullout-view-form input[value='Disapprove'], #commissary-order-request-view-form input[value='Disapprove'], #commissary-pullout-request-view-form input[value='Disapprove']").show().removeAttr("disabled");
      $("#order-request-view-form input[value='Approve'], #order-pullout-view-form input[value='Approve'], #commissary-order-request-view-form input[value='Approve'], #commissary-pullout-request-view-form input[value='Approve']").show().removeAttr("disabled");

      // Transfers
      $("#store-transfer-in-view-form input[value='Disapprove'], #store-transfer-out-view-form input[value='Disapprove']").show().removeAttr("disabled");
      $("#store-transfer-in-view-form input[value='Approve'], #store-transfer-out-view-form input[value='Approve']").show().removeAttr("disabled");
    }

    // Product auto compute
    /* Price with vat */
    var total_w_o_vat = $("#edit-field-selling-price-w-o-vat-und-0-value");

    $("#edit-field-selling-price-w-o-vat-und-0-value").change(function() {
      var vat = total_w_o_vat.val() * .12;
      var total = (parseFloat(total_w_o_vat.val()) + parseFloat(vat));

      $("#edit-field-price-und-0-value").val(total.toFixed(2));
      $("#edit-field-vat-und-0-value").val(vat.toFixed(2));
    });

    /* Price without vat */
    var total_w_vat = $("#edit-field-price-und-0-value");

    $("#edit-field-price-und-0-value").change(function() {
      var no_vat = total_w_vat.val() / 1.12;
      $("#edit-field-selling-price-w-o-vat-und-0-value").val(no_vat.toFixed(2));
      var vat = $("#edit-field-selling-price-w-o-vat-und-0-value").val() * .12;

      $("#edit-field-vat-und-0-value").val(vat.toFixed(2));
    });

    // Inventory auto compute
    var production_quantity = $("#edit-field-production-quantity-und-0-value");
    var product_price = $("#edit-field-price-und-0-value");
    var price_per_unit = $("#edit-field-price-per-unit-und-0-value");

    product_price.change(function() {
      var price_per_unit = (parseFloat(product_price.val()) / parseFloat(production_quantity.val()));
      $("#edit-field-price-per-unit-und-0-value").val(price_per_unit.toFixed(2));
    });

    price_per_unit.change(function() {
      var price_per_pack = (parseFloat(production_quantity.val()) * parseFloat(price_per_unit.val()));
      $("#edit-field-price-und-0-value").val(price_per_pack.toFixed(2));
    });

    /* Make sure the value will not return Nan */
    function getNum(val) {
      if(isNaN(val)) {
        return 0;
      }
      return val;
    }
    // Clear textbox
    if(document.getElementById('date-overtime') != null && document.getElementById('date-undertime') != null) {
      document.getElementById('date-overtime').value = "";
      document.getElementById('date-undertime').value = "";
    }
    // Hide Request
    if(document.getElementById('hide-overtime') != null && document.getElementById('hide-undertime') != null) {
      document.getElementById('hide-overtime').style.display = 'none';
      document.getElementById('hide-undertime').style.display = 'none';
    }

    $('select#edit-request').change(function() {
      var selected = $(this).val();
      if(selected == 'overtime') {
        document.getElementById('hide-overtime').style.display = 'block';
        document.getElementById('hide-undertime').style.display = 'none';
      }
      if(selected == 'undertime') {
        document.getElementById('hide-undertime').style.display = 'block';
        document.getElementById('hide-overtime').style.display = 'none';
      }
    });

    // Login Form
    if ($('body').is('.front, .not-logged-in')) {
      var captcha = $('.captcha .field-prefix').text();
      $('.captcha input[name="captcha_response"]').attr('placeholder', captcha);
      $('.user-login-block .item-list').appendTo('.user-login-block .form-actions');
    }

    // Slicknav.
    $(window).load(function() {
      $(document).on("click", ".slicknav_menu a, span.slicknav_arrow", function(e) {
        if ($('body.admin-menu').length) {
          e.preventDefault(); // prevent the default action
          e.stopPropagation(); // stop the click from bubbling
          setTimeout(function() {
            var header_height = $('header.main-header').outerHeight();
            var slick_height = $('.slicknav_menu').outerHeight() + header_height;
            $('aside.main-sidebar').css('padding-top', slick_height + 'px');
          }, 500);
        }
      });
    });

	if ($('.image-preview', context).length) {
		$('#edit-field-image-preview').hide();
	}
	else {
		$('#edit-field-image-preview').show();
	}

  }
};


}(jQuery, Drupal, this, this.document));
