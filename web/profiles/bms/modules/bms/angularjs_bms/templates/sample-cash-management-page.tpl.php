<div ng-app="starter" ng-controller="CashManagementCtrl">
    <ul class="nav nav-tabs">
      <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Added to Vault</a></li>
      <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Sales</a></li>
      <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Pull Out</a></li>
      <li ng-class="{'active': activeTab == 4}"><a href="" ng-click="setActiveTab(4)">Deposit</a></li>
      <li ng-class="{'active': activeTab == 5}"><a href="" ng-click="setActiveTab(5)">Remaining Cash on Vault</a></li>
    </u>
    <div class="tab-content">
      <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
        <table border=1>
            <thead>
              <th>Cashier</th>
              <th>Time</th>
              <th>amount</th>
              <th>Total</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ add_to_vault.cashier }}</td>
                <td>{{ add_to_vault.time }}</td>
                <td>{{ add_to_vault.amount }}</td>
                <td>{{ add_to_vault.total }}</td>
              </tr>
            </tbody>
        </table>
      </div>
      <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
        <table border=1>
            <thead>
              <th>Cash Sales</th>
              <th>GC Sales</th>
              <th>Total Sales</th>
              <th>Cash Count</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ sales.cash_sales }}</td>
                <td>{{ sales.gc_sales }}</td>
                <td>{{ sales.total_sales }}</td>
                <td>{{ add_to_vault.total }}</td>
              </tr>
            </tbody>
        </table>
      </div>
      <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
        <table border=1>
            <thead>
              <th>Time</th>
              <th>Pulled Out By</th>
              <th>Requestor</th>
              <th>Amount</th>
              <th>Total Pulled Out Cash</th>
              <th>Reason</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ pullout.pullout_by }}</td>
                <td>{{ pullout.pullout_by }}</td>
                <td>{{ pullout.pullout_by }}</td>
                <td>{{ pullout.reason }}</td>
                <td>{{ add_to_vault.amount }}</td>
                <td>{{ add_to_vault.total }}</td>
              </tr>
            </tbody>
        </table>
      </div>
      <div ng-class="{'tab-pane active' : activeTab === 4, 'tab-pane' : activeTab !== 4}">
        <table border=1>
            <thead>
              <th>Time</th>
              <th>Deposited By</th>
              <th>Bank</th>
              <th>Transaction No.</th>
              <th>Amount</th>
              <th>Total Deposited Cash</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ add_to_vault.cashier }}</td>
                <td>{{ add_to_vault.time }}</td>
                <td>{{ add_to_vault.amount }}</td>
                <td>{{ add_to_vault.total }}</td>
              </tr>
            </tbody>
        </table>
      </div>
      <div ng-class="{'tab-pane active' : activeTab === 5, 'tab-pane' : activeTab !== 5}">
        <table border=1>
            <thead>
              <th>Remaining Cash on Vault</th>
            </thead>
            <tbody>
              <tr>
                <td>{{ add_to_vault.cashier }}</td>
              </tr>
            </tbody>
        </table>
      </div>
    </div>
</div>