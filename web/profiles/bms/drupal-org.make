; bms make file for d.o. usage
core = "7.x"
api = "2"

; +++++ Modules +++++

projects[roles_for_menu][version] = "1.1"
projects[roles_for_menu][subdir] = "contrib"

projects[admin_menu][version] = "3.0-rc5"
projects[admin_menu][subdir] = "contrib"

projects[adminimal_admin_menu][version] = "1.7"
projects[adminimal_admin_menu][subdir] = "contrib"

projects[module_filter][version] = "2.0"
projects[module_filter][subdir] = "contrib"

projects[module_missing_message_fixer][version] = "1.1"
projects[module_missing_message_fixer][subdir] = "contrib"

projects[order_commissary_services][version] = "1.0"
projects[order_commissary_services][subdir] = "contrib"

projects[pullout_request_services][version] = "1.0"
projects[pullout_request_services][subdir] = "contrib"

projects[angular_bms][version] = "1.0"
projects[angular_bms][subdir] = "contrib"

projects[bms_calendar][version] = "1.0"
projects[bms_calendar][subdir] = "contrib"

projects[bms_core][version] = "1.0"
projects[bms_core][subdir] = "contrib"

projects[bms_dashboard][version] = "1.0"
projects[bms_dashboard][subdir] = "contrib"

projects[bms_idgenerator][version] = "0.1"
projects[bms_idgenerator][subdir] = "contrib"

projects[bms_others][version] = "1.0"
projects[bms_others][subdir] = "contrib"

projects[bms_store][version] = "1.0"
projects[bms_store][subdir] = "contrib"

projects[bms_visualization][version] = "0.1"
projects[bms_visualization][subdir] = "contrib"

projects[convertion][version] = "1.0"
projects[convertion][subdir] = "contrib"

projects[product_importer][version] = "1.0"
projects[product_importer][subdir] = "contrib"

projects[reasons][version] = "1.0"
projects[reasons][subdir] = "contrib"

projects[request_manager][version] = "1.0"
projects[request_manager][subdir] = "contrib"

projects[user_manager][version] = "1.0"
projects[user_manager][subdir] = "contrib"

projects[views_advance_formatter][version] = "1.0"
projects[views_advance_formatter][subdir] = "contrib"

projects[attendance][version] = "1.0"
projects[attendance][subdir] = "contrib"

projects[bms_services][version] = "1.0"
projects[bms_services][subdir] = "contrib"

projects[cash_management][version] = "1.0"
projects[cash_management][subdir] = "contrib"

projects[cashier_services][version] = "1.0"
projects[cashier_services][subdir] = "contrib"

projects[commissary_services][version] = "1.0"
projects[commissary_services][subdir] = "contrib"

projects[inventory_services][version] = "1.0"
projects[inventory_services][subdir] = "contrib"

projects[product_services][version] = "1.0"
projects[product_services][subdir] = "contrib"

projects[production_services][version] = "1.0"
projects[production_services][subdir] = "contrib"

projects[store_services][version] = "1.0"
projects[store_services][subdir] = "contrib"

projects[transfer_services][version] = "1.0"
projects[transfer_services][subdir] = "contrib"

projects[account_settings][version] = "1.0"
projects[account_settings][subdir] = "contrib"

projects[bms_context][version] = "1.0"
projects[bms_context][subdir] = "contrib"

projects[bms_image_styles][version] = "1.0"
projects[bms_image_styles][subdir] = "contrib"

projects[bms_menu][version] = "1.0"
projects[bms_menu][subdir] = "contrib"

projects[bms_misc][version] = "1.0"
projects[bms_misc][subdir] = "contrib"

projects[bms_reports][version] = "1.0"
projects[bms_reports][subdir] = "contrib"

projects[bms_roles][version] = "1.0"
projects[bms_roles][subdir] = "contrib"

projects[bms_url_alias][version] = "1.0"
projects[bms_url_alias][subdir] = "contrib"

projects[entity_type][version] = "1.0"
projects[entity_type][subdir] = "contrib"

projects[inventory][version] = "1.0"
projects[inventory][subdir] = "contrib"

projects[profile_type][version] = "1.0"
projects[profile_type][subdir] = "contrib"

projects[services_configuration][version] = "1.0"
projects[services_configuration][subdir] = "contrib"

projects[store_employee][version] = "1.0"
projects[store_employee][subdir] = "contrib"

projects[store_franchisee][version] = "1.0"
projects[store_franchisee][subdir] = "contrib"

projects[transfer_request][version] = "1.0"
projects[transfer_request][subdir] = "contrib"

projects[trash_table_configuration][version] = "1.0"
projects[trash_table_configuration][subdir] = "contrib"

projects[transfer_in_request_services][version] = "1.0"
projects[transfer_in_request_services][subdir] = "contrib"

projects[transfer_out_request_services][version] = "1.0"
projects[transfer_out_request_services][subdir] = "contrib"

projects[ctools][version] = "1.9"
projects[ctools][subdir] = "contrib"

projects[context][version] = "3.6"
projects[context][subdir] = "contrib"

projects[context_reaction_theme][version] = "1.x-dev"
projects[context_reaction_theme][subdir] = "contrib"

projects[date][version] = "2.9"
projects[date][subdir] = "contrib"

projects[views_between_dates_filter][version] = "1.0"
projects[views_between_dates_filter][subdir] = "contrib"

projects[profiler_builder][version] = "1.2"
projects[profiler_builder][subdir] = "contrib"

projects[eck][version] = "2.0-rc8"
projects[eck][subdir] = "contrib"

projects[features][version] = "2.10"
projects[features][subdir] = "contrib"

projects[features_override][version] = "2.0-rc3"
projects[features_override][subdir] = "contrib"

projects[features_roles_permissions][version] = "1.2"
projects[features_roles_permissions][subdir] = "contrib"

projects[uuid_features][version] = "1.0-alpha4"
projects[uuid_features][subdir] = "contrib"

projects[double_field][version] = "2.4"
projects[double_field][subdir] = "contrib"

projects[entityreference][version] = "1.1"
projects[entityreference][subdir] = "contrib"

projects[entityreference_autofill][version] = "1.2"
projects[entityreference_autofill][subdir] = "contrib"

projects[field_collection][version] = "1.0-beta11"
projects[field_collection][subdir] = "contrib"

projects[field_group][version] = "1.5"
projects[field_group][subdir] = "contrib"

projects[field_permissions][version] = "1.0-beta2"
projects[field_permissions][subdir] = "contrib"

projects[office_hours][version] = "1.4"
projects[office_hours][subdir] = "contrib"

projects[phone][version] = "1.0-beta1"
projects[phone][subdir] = "contrib"

projects[references][version] = "2.1"
projects[references][subdir] = "contrib"

projects[link_badges][version] = "1.1"
projects[link_badges][subdir] = "contrib"

projects[void_menu][version] = "1.9"
projects[void_menu][subdir] = "contrib"

projects[angularjs_bms_report][version] = "1.0"
projects[angularjs_bms_report][subdir] = "contrib"

projects[anonymous_login][version] = "1.3"
projects[anonymous_login][subdir] = "contrib"

projects[asaf][version] = "1.1"
projects[asaf][subdir] = "contrib"

projects[autodialog][version] = "1.x-dev"
projects[autodialog][subdir] = "contrib"

projects[bms_deploy][version] = "1.0"
projects[bms_deploy][subdir] = "contrib"

projects[cors][version] = "1.3"
projects[cors][subdir] = "contrib"

projects[custom_field][version] = "1.0"
projects[custom_field][subdir] = "contrib"

projects[disable_draggable][version] = "1.0"
projects[disable_draggable][subdir] = "contrib"

projects[entity][version] = "1.6"
projects[entity][subdir] = "contrib"

projects[entity_bulk_delete][version] = "1.1"
projects[entity_bulk_delete][subdir] = "contrib"

projects[example_report][version] = "1.0"
projects[example_report][subdir] = "contrib"

projects[field_collection_table][version] = "1.0-beta3"
projects[field_collection_table][subdir] = "contrib"

projects[field_collection_views][version] = "1.0-beta3"
projects[field_collection_views][subdir] = "contrib"

projects[fontawesome][version] = "2.5"
projects[fontawesome][subdir] = "contrib"

projects[identicon][version] = "1.0-rc3"
projects[identicon][subdir] = "contrib"

projects[image_url_formatter][version] = "1.4"
projects[image_url_formatter][subdir] = "contrib"

projects[libraries][version] = "2.2"
projects[libraries][subdir] = "contrib"

projects[menu_attributes][version] = "1.0"
projects[menu_attributes][subdir] = "contrib"

projects[menu_badges][version] = "1.2"
projects[menu_badges][subdir] = "contrib"

projects[menu_block][version] = "2.7"
projects[menu_block][subdir] = "contrib"

projects[pathauto][version] = "1.3"
projects[pathauto][subdir] = "contrib"

projects[pathauto_entity][version] = "1.0"
projects[pathauto_entity][subdir] = "contrib"

projects[phpexcel][version] = "3.11"
projects[phpexcel][subdir] = "contrib"

projects[profile2][version] = "1.3"
projects[profile2][subdir] = "contrib"

projects[quicktabs][version] = "3.6"
projects[quicktabs][subdir] = "contrib"

projects[role_export][version] = "1.0"
projects[role_export][subdir] = "contrib"

projects[strongarm][version] = "2.0"
projects[strongarm][subdir] = "contrib"

projects[token][version] = "1.6"
projects[token][subdir] = "contrib"

projects[token_filter][version] = "1.1"
projects[token_filter][subdir] = "contrib"

projects[views_extra_field][version] = "1,0"
projects[views_extra_field][subdir] = "contrib"

projects[visualization][version] = "1.0-beta2"
projects[visualization][subdir] = "contrib"

projects[account_profile][version] = "2.0"
projects[account_profile][subdir] = "contrib"

projects[rules][version] = "2.9"
projects[rules][subdir] = "contrib"

projects[services][version] = "3.13"
projects[services][subdir] = "contrib"

projects[services][version] = "3.13"
projects[services][subdir] = "contrib"

projects[captcha][version] = "1.3"
projects[captcha][subdir] = "contrib"

projects[table_trash][version] = "1.x-dev"
projects[table_trash][subdir] = "contrib"

projects[uuid][version] = "1.0-beta1"
projects[uuid][subdir] = "contrib"

projects[accordion_menu][version] = "1.2"
projects[accordion_menu][subdir] = "contrib"

projects[jquery_update][version] = "3.0-alpha3"
projects[jquery_update][subdir] = "contrib"

projects[better_exposed_filters][version] = "3.2"
projects[better_exposed_filters][subdir] = "contrib"

projects[editableviews][version] = "1.0-beta10"
projects[editableviews][subdir] = "contrib"

projects[views][version] = "3.14"
projects[views][subdir] = "contrib"

projects[views_aggregator][version] = "1.4"
projects[views_aggregator][subdir] = "contrib"

projects[views_calc][version] = "1.1"
projects[views_calc][subdir] = "contrib"

projects[views_data_export][version] = "3.1"
projects[views_data_export][subdir] = "contrib"

projects[views_load_more][version] = "1.5"
projects[views_load_more][subdir] = "contrib"

projects[views_php][version] = "1.0-alpha3"
projects[views_php][subdir] = "contrib"

; +++++ Libraries +++++

; ColorBox
libraries[colorbox][directory_name] = "colorbox"
libraries[colorbox][type] = "library"
libraries[colorbox][destination] = "libraries"
libraries[colorbox][download][type] = "get"
libraries[colorbox][download][url] = "https://github.com/jackmoore/colorbox/archive/master.zip"

; +++++ Patches +++++

projects[accordion_menu][patch][] = "http://drupal.org/files/menu_block-7.x-2.3.patch"

