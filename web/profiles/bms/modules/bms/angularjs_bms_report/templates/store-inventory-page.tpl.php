<div ng-app="starter">
  <form name="inventory-form" ng-controller="StoreInventoryCtrl">
    <div class="tab-navigations">
      <ul class="tabs-menu">
        <li ng-class="{'active': activeTab == 0}"><a href="" ng-click="setActiveTab(0)">Beginning Inventory</a></li>
        <li ng-class="{'active': activeTab == 1}"><a href="" ng-click="setActiveTab(1)">Commissary Delivery</a></li>
        <li ng-class="{'active': activeTab == 2}"><a href="" ng-click="setActiveTab(2)">Commissary Pull-Out</a></li>
        <li ng-class="{'active': activeTab == 3}"><a href="" ng-click="setActiveTab(3)">Store Trans-In</a></li>
        <li ng-class="{'active': activeTab == 4}"><a href="" ng-click="setActiveTab(4)">Store Trans-Out</a></li>
        <li ng-class="{'active': activeTab == 5}"><a href="" ng-click="setActiveTab(5)">Production</a></li>
        <li ng-class="{'active': activeTab == 6}"><a href="" ng-click="setActiveTab(6)">Wastage</a></li>
        <li ng-class="{'active': activeTab == 7}"><a href="" ng-click="setActiveTab(7)">Ending Inventory (Theo)</a></li>
        <li ng-class="{'active': activeTab == 8}"><a href="" ng-click="setActiveTab(8)">Ending Inventory (Actual)</a></li>
        <li ng-class="{'active': activeTab == 9}"><a href="" ng-click="setActiveTab(9)">Variance</a></li>
      </ul>
    </div>
    <div class="tab-content">
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 0, 'tab-pane' : activeTab !== 0}" data-ng-init="setActiveTab(0)">
          <div ng-hide="!tabs[0].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(0)" placeholder="yyyy-MM-dd" required />        
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[0].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[0].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 1, 'tab-pane' : activeTab !== 1}">
          <div ng-hide="!tabs[1].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(1)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[1].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[1].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 2, 'tab-pane' : activeTab !== 2}">
          <div ng-hide="!tabs[2].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(2)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[2].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[2].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 3, 'tab-pane' : activeTab !== 3}">
          <div ng-hide="!tabs[3].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(3)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[3].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[3].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 4, 'tab-pane' : activeTab !== 4}">
          <div ng-hide="!tabs[4].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(4)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[4].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[4].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 5, 'tab-pane' : activeTab !== 5}">
          <div ng-hide="!tabs[5].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(5)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[5].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[5].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 6, 'tab-pane' : activeTab !== 6}">
          <div ng-hide="!tabs[6].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(6)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[6].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[6].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 7, 'tab-pane' : activeTab !== 7}">
          <div ng-hide="!tabs[7].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(7)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[7].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[7].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 8, 'tab-pane' : activeTab !== 8}">
          <div ng-hide="!tabs[8].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(8)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[8].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[8].isLoaded"><loading></loading></div>
          </div>
       </div>
       <div class="tab-content">
          <div ng-class="{'tab-pane active' : activeTab === 9, 'tab-pane' : activeTab !== 9}">
          <div ng-hide="!tabs[9].isLoaded">
            <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter(9)" placeholder="yyyy-MM-dd" required />
            <table datatable="ng" dt-options="dtOptions" class="row-border hover">
                <thead>
                  <th>Inventory Category</th>
                  <th>Inventory Item</th>
                  <th>Inventory Item Code</th>
                  <th>QTY</th>
                  <th>UOM</th>
                  <th>Unit Price</th>
                  <th>Total</th>
                </thead>
                <tbody>
                  <tr ng-repeat="item in tabs[9].content">
                    <td>{{ item.category }}</td>
                    <td>{{ item.item }}</td>
                    <td>{{ item.item_code }}</td>
                    <td>{{ item.qty }}</td>
                    <td>{{ item.uom }}</td>
                    <td>{{ item.unit_price | currency:"₱":2}}</td>
                    <td>{{ item.total | currency:"₱":2}}</td>
                  </tr>
                </tbody>
            </table>
          </div>
          <div ng-hide="tabs[9].isLoaded"><loading></loading></div>
          </div>
       </div>
    </div>
  </form>
</div>