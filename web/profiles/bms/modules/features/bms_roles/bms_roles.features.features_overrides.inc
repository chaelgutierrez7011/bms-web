<?php
/**
 * @file
 * bms_roles.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function bms_roles_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: user_role
  $overrides["user_role.accounting.machine_name"] = '';
  $overrides["user_role.central office.machine_name"] = '';
  $overrides["user_role.commissary.machine_name"] = '';
  $overrides["user_role.crew.machine_name"] = '';
  $overrides["user_role.hr.machine_name"] = '';

 return $overrides;
}
