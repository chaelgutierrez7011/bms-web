<?php
/**
 * @file
 * trash_table_configuration.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function trash_table_configuration_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'table_trash_decorations';
  $strongarm->value = array(
    1 => array(
      'decoration-params' => array(
        'search-box' => 0,
        'column-reorder' => 1,
        'export-buttons' => 1,
        'retrieve-data' => 0,
        'pager-style' => '',
        'page-height' => '',
        'dont-sort-columns' => '',
        'x-scroll' => '',
        'fixed-left-columns' => '',
        'fixed-header' => 0,
        'responsive' => array(
          'responsive-expand-col' => '',
          'responsive-collapse-cols-phone' => '',
          'responsive-collapse-cols-tablet' => '',
        ),
      ),
      'pages-and-selector' => array(
        'include-pages' => 'sales
cash-management',
        'exclude-pages' => 'admin/reports/status
admin/modules*',
        'table-selector' => '',
      ),
    ),
  );
  $export['table_trash_decorations'] = $strongarm;

  return $export;
}
