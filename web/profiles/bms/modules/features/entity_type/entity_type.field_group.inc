<?php
/**
 * @file
 * entity_type.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function entity_type_field_group_info() {
  $field_groups = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_cash_management|store|cash_management|full';
  $field_group->group_name = 'group_cash_management';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Cash Management',
    'weight' => '6',
    'children' => array(
      0 => 'group_pullout',
      1 => 'group_vault',
      2 => 'group_deposit',
      3 => 'group_sales',
      4 => 'group_store',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_deposit|store|cash_management|full';
  $field_group->group_name = 'group_deposit';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_cash_management';
  $field_group->data = array(
    'label' => 'Deposit',
    'weight' => '2',
    'children' => array(
      0 => 'field_cash_management_deposit',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_pullout|store|cash_management|full';
  $field_group->group_name = 'group_pullout';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_cash_management';
  $field_group->data = array(
    'label' => 'Pullout',
    'weight' => '1',
    'children' => array(
      0 => 'field_cash_management_pullout',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_sales|store|cash_management|full';
  $field_group->group_name = 'group_sales';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_cash_management';
  $field_group->data = array(
    'label' => 'Sales',
    'weight' => '4',
    'children' => array(
      0 => 'field_cash_management_sales',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-sales field-group-htab',
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_store|store|cash_management|full';
  $field_group->group_name = 'group_store';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_cash_management';
  $field_group->data = array(
    'label' => 'Store',
    'weight' => '0',
    'children' => array(
      0 => 'field_store_name',
      1 => 'field_store_code',
      2 => 'field_store_id',
      3 => 'field_created',
      4 => 'field_cashier',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'label' => 'Store',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-store field-group-htab',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $field_groups[''] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_vault|store|cash_management|full';
  $field_group->group_name = 'group_vault';
  $field_group->entity_type = 'store';
  $field_group->bundle = 'cash_management';
  $field_group->mode = 'full';
  $field_group->parent_name = 'group_cash_management';
  $field_group->data = array(
    'label' => 'Vault',
    'weight' => '3',
    'children' => array(
      0 => 'field_cash_management_vault',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => '',
        'id' => '',
      ),
    ),
  );
  $field_groups[''] = $field_group;

  // Translatables
  // Included for use with string extractors like potx.
  t('Cash Management');
  t('Deposit');
  t('Pullout');
  t('Sales');
  t('Store');
  t('Vault');

  return $field_groups;
}
