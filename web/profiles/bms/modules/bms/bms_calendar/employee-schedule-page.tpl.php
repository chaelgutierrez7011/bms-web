<!-- Content Header (Page header) -->
<section class="content-header">
  <h1>
    <?php print render($form['name']); ?>
    <small><?php print t('Manage Schedule'); ?></small>
  </h1>
  <div class="breadcrumb">
    <a href="/"><span><?php print t('Home'); ?></span></a> >
    <a href="/"><span><?php print t('Store Operations Mgmt'); ?></span></a> >
    <a href="/manage-schedule"><span><?php print t('Manage Employee Schedule'); ?></span></a> >
    <a href="/employee/<?php print arg(1); ?>/schedule"><span><?php print render($form['name']); ?></span></a>
  </div>
  <div class="stores-wrapper">
    <?php print render($form['store_name']); ?>
  </div>
</section>

<!-- Main content -->
<section class="content">
  <div class="row">
    <div class="col-sm-4 col-md-3 actions-box">
      <div class="box box-solid">
        <div class="box-header with-border">
          <h4 class="box-title"><?php print t('Legend'); ?></h4>
        </div>
        <div class="box-body">
          <!-- the events -->
          <div id="external-events">
            <span class="assigned-sched">
              <small class="label pull-left" style="background-color:#3F8CBC">&nbsp;</small>
              <label><?php print t('Assigned Schedule'); ?></lable>
            </span>
            <span class="undertime">
              <small class="label pull-left" style="background-color:#1AA95A">&nbsp;</small>
              <label><?php print t('Undertime'); ?></lable>
            </span>
            <span class="overtime">
              <small class="label pull-left" style="background-color:#E57606">&nbsp;</small>
              <label><?php print t('Overtime'); ?></lable>
            </span>
          </div>
        </div><!-- /.box-body -->
      </div><!-- /. box -->
      <div class="box box-solid" id="calendar-schedule-box">
        <div class="box box-solid">
            <div class="box-header with-border">
                <h4 class="box-title"><?php print t('Add Schedule'); ?></h3>
            </div>
            <div class="box-body">
                <div class="form-group">
                <?php print render($form['schedule']); ?>
                <!-- /.input group -->
                </div>
                <div class="form-group">
                <?php print render($form['store_assigment']); ?>
                <?php print render($form['request']); ?>
                <div id="hide-overtime">
                  <?php print render($form['date_overtime']); ?>
                </div>
                <div id="hide-undertime">
                  <?php print render($form['date_undertime']); ?>
                </div>
                <?php print render($form['uid']); ?>
                </div>
                <?php print render($form['submit']); ?>
                <?php print drupal_render_children($form);?>
            </div>
        </div>
      </div>
    </div><!-- /.col -->
    <div class="col-sm-8 col-md-9 calendar-box">
      <div class="box box-primary">
        <div class="box-body no-padding">
          <!-- THE CALENDAR -->
          <div id="calendar"></div>
        </div><!-- /.box-body -->
      </div><!-- /. box -->
    </div><!-- /.col -->
  </div><!-- /.row -->
</section><!-- /.content -->
