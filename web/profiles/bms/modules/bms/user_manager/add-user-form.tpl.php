<div class="box box-default">
  <div class="box-header with-border">
    <h3 class="box-title">Add user form</h3>
  </div><!-- /.box-header -->
  <div class="box-body">
    <?php print render($form['username']); ?>
    <?php print render($form['mail']); ?>
    <?php print render($form['password']); ?>
    <?php print render($form['confirm_password']); ?>
    <?php print render($form['status']); ?>
    <?php print render($form['store']); ?>
    <?php print render($form['submit']); ?>
  </div><!-- /.box-body -->
</div>
<?php print drupal_render_children($form); ?>
