<?php print render($form['profile_main']['field_firstname']); ?>
<?php print render($form['profile_main']['field_lastname']); ?>
<?php print render($form['profile_main']['field_address']); ?>

<?php print render($form['profile_main']['field_mobile_number']); ?>
<?php print render($form['account']['mail']); ?>
<?php print render($form['profile_main']['field_gender']); ?>
<?php print render($form['profile_main']['field_birthdate']); ?>

<?php print render($form['profile_main']['field_marital_status']); ?>
<?php print render($form['account']['name']); ?>
<?php print render($form['account']['pass']); ?>
<?php print render($form['user_position']); ?>

<?php print render($form['field_account_employee_id']); ?>
<?php print render($form['profile_main']['field_date_of_hire']); ?>
<?php print render($form['profile_main']['field_end_of_contract']); ?>
<?php print render($form['user_status']); ?>

<?php print render($form['field_store']); ?>
<?php print render($form['account']['roles']); ?>
<?php print render($form['account']['status']); ?>

<div class="hidden-container">
  <?php print render($form['account']['notify']); ?>
  <?php print render($form['profile_main']['field_store_franchised']); ?>
  <?php print render($form['profile_main']['field_my_employee']); ?>
  <?php print render($form['profile_main']['field_manage_schedule']); ?>
</div>

<?php print drupal_render_children($form); ?>