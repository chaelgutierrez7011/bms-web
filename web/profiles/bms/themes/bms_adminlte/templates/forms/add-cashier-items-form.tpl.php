<?php print render($form['title']); ?>
<?php print render($form['field_product_code']); ?>
<?php print render($form['field_category']); ?>
<?php print render($form['field_selling_price_w_o_vat']); ?>
<?php print render($form['field_vat']); ?>
<?php print render($form['field_price']); ?>
<?php print render($form['field_accepted_reason']); ?>

<div class="hidden-container">
  <?php print render($form['field_component_count']); ?>
  <?php print render($form['field_selling_price_with_vat']); ?>
  <?php print render($form['field_product_name']); ?>
</div>

<?php print drupal_render_children($form); ?>