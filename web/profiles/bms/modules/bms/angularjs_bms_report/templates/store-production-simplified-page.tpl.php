<div ng-app="starter" ng-controller="StoreProductionSimplifiedCtrl">
    <div ng-hide="!isLoaded.spinner">
    <input type="date" name="dateFilter" ng-model="dateFilter.value" ng-change="updateFilter()" placeholder="yyyy-MM-dd" required />
    <table datatable="ng" dt-options="dtOptions" class="row-border hover">
        <thead>
            <th>Inventory Category</th>
            <th>Inventory Item</th>
            <th>Inventory Item Code</th>
            <th>Beginning Inventory</th>
            <th>Opened</th>
            <th>Sold</th>
            <th>Wastage</th>
            <th>Ending Inventory</th>
            <th>Actual</th>
            <th>Variance</th>
            <th>UOM</th>
        </thead>
        <tbody>
            <tr ng-repeat="item in items">
            <td ng-cloak>{{ item.category }}</td>
            <td ng-cloak>{{ item.item }}</td>
            <td ng-cloak>{{ item.item_code }}</td>
            <td ng-cloak>{{ item.beginning_inventory }}</td>
            <td ng-cloak>{{ item.process }}</td>
            <td ng-cloak>{{ item.sold }}</td>
            <td ng-cloak>{{ item.wastage }}</td>
            <td ng-cloak>{{ item.theoretical }}</td>
            <td ng-cloak>{{ item.actual }}</td>
            <td ng-cloak>{{ item.variance }}</td>
            <td ng-cloak>{{ item.uom }}</td>
            </tr>
        </tbody>
    </table>
    </div>
    <div ng-hide="isLoaded.spinner"><loading></loading></div>
</div>